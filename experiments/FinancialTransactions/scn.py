scenario.warnOnMissingReaction(False)

res = []

def handleNonAcceptingVerdictsStep(verdictsIter, brokenProperties):
    try:
        mon = verdictsIter.next().getMonitor()
    except StopIteration:
        scenario.debuggerCmd("where",
            lambda result: res.append((brokenProperties, result))
        )
        return

    name = scenario.getName(mon)
    brokenProperties.add(name)
    scenario.debuggerPrintln("SCENARIO: Violation of property " + name)
    scenario.suspendMonitor(mon, lambda: handleNonAcceptingVerdictsStep(verdictsIter, brokenProperties))


def handleNonAcceptingVerdicts(matchingVerdicts):
    handleNonAcceptingVerdictsStep(iter(matchingVerdicts), set())

def handleProgramLeave():
    if not len(res):
        print("No non-accepting verdict was seen.");
        return

    print("Program broke:")
    for (brokenProperties, result) in res:
        print((" - property" if len(brokenProperties) < 2 else "properties") + ", ".join(brokenProperties))
        print(result)

def newInterface():
        scenario.resetAllMonitors(lambda:None)

def handleProgramJoin(prgm):
    scenario.addPoint("MethodEntry transactionsystem.Interface.<init>()", newInterface)

scenario.on("any non-accepting verdict", handleNonAcceptingVerdicts)
scenario.on("program join", handleProgramJoin)
scenario.on("program leave", handleProgramLeave)
