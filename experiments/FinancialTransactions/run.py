#!/usr/bin/env python3
import os
import sys

ALL_PROPS = [2, 4, 5, 6, 7, 9, 10]

def run(autoquit, props):
    WD = os.path.dirname(os.path.realpath(__file__))
    VERDE_DIR = (
        os.environ["VERDE_DIR"]
            if "VERDE_DIR" in os.environ
            else os.path.dirname(os.path.dirname(WD))
    )

    os.environ["VERDE_JDB_PRINT_TIME"] = "1"
    os.environ["VERDE_BUS_PRINT_TIME"] = "1"

    b = VERDE_DIR + "/bin/dist-verde"

    propargs = []

    for prop in props:
        propargs.extend(["--prop", "p" + str(prop) + ".prop"])

    os.execv(b, [b,
        "--prgm", "transactionsystem.Main",
            "-dbg", "jdb",
            "-wd", WD] +
        propargs +
        (["--scn",  "scn.py"] if autoquit else [])
    )

if len(sys.argv) < 2 or sys.argv[1] == "help":
    print("Usage: " + sys.argv[0] + """ CMD [...]
where CMD is:
    output:
        Show all the defects and quit

    try:
        Same, but don't quit.

    bench:
        Perform performance measures.
        additional parameters are for listing the properties to check.
""")
else:
    if sys.argv[1] == "output":
        run(autoquit=True, props=ALL_PROPS)
    elif sys.argv[1] == "try":
        run(autoquit=False, props=ALL_PROPS)
    elif sys.argv[1] == "bench":
        run(autoquit=True, props=sys.argv[2:])
