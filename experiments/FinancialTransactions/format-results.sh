#!/usr/bin/env sh

gettotal() {
    grep -F  "$1": | grep -E -o "TOTAL [0-9]+" | cut -d' ' -f 2
}

nbevents() {
    gettotal "Event Notifications"
}

nbverdicts() {
    gettotal "Verdicts from"
}

round() {
    # https://askubuntu.com/questions/179898/how-to-round-decimals-using-bc-in-bash
    echo $(printf %.$1f $(echo "scale=$1;(((10^$1)*$(cat))+0.5)/(10^$1)" | bc))
}

format_number() {
    if [ "${FORMAT_NUMBER_AS_TEX}" = "true" ]; then
        format='$%s$'
    else
        format='%s'
    fi
    printf "$format" "$(cat | bc | sed 's/\.0\+$//g' | numfmt --grouping)"
}

gettraceinfo() {
    $VERDE_DIR/bin/tracecount.py < "trace-$1"
}

make_csv() {
    echo "Properties;Events;Verdicts;Execution time"
    cat "res.log" | while read line; do
        l="$(printf "%s" "$line" | sed -E 's/^props: ([0-9 ]+);.*after cont: ([0-9\.]+) sec.*/\1;\2/g')"
        props=$(printf "%s" "$l" | cut -d ';' -f1)
        traceinfo="$(gettraceinfo "$props")"
        printf "%s;%s;%s;%s\n"       \
            "$(col 1 "$l")"          \
            "$(printf "%s" "$traceinfo" | nbevents   "$props")" \
            "$(printf "%s" "$traceinfo" | nbverdicts "$props")" \
            "$(col 2 "$l")"
    done
}

col() {
    printf "%s" "$2" | cut -d';' -f$1
}

to_human_numbers() {
    read line;
    printf "%s\n" "$line";

    while read line; do
        printf "%s;%s;%s;%s;%s;%s\n"               \
            "$(col 1 "$line" | sed 's/ /, /g')" \
            "$(col 2 "$line" | format_number)"  \
            "$(col 3 "$line" | format_number)" \
            "$(col 4 "$line" | round 2 | format_number) s" \
            "$(echo "$(col 5 "$line")*100" | bc -l | round 0 | format_number)%" \
            "$(echo "$(col 6 "$line")*100" | bc -l | round 0 | format_number)%"
    done
}

overhead() {
    echo "($2 - $1) / $1" | bc -l
}

add_overhead() {
    read line;
    printf "%s;Overhead;Overhead JDB\n" "$line";

    baseline=$(cat "execution-time-without-instrumentation" | head -1 | cut -d' ' -f2)
    jdb=$(cat "execution-time-without-instrumentation" | tail -1 | cut -d' ' -f2)
    while read line; do
        printf "%s;%s;%s\n" \
            "$line" \
            "$(overhead "$baseline" "$(col 4 "$line")")" \
            "$(overhead "$jdb" "$(col 4 "$line")")"
    done
}

csvtable2tex() {
    sed -e '1s/^/\\\heading{/g'           | # add \heading
    sed -e '1s/$/}/g'                     | # add \heading
    sed -e '1s/;/};\\\heading{/g'         | # add \heading
    sed -e '1s/_/\\\_/g'                  | # _ → \_
    sed -e 's/%/\\\,\\\%/g'               | # % → \,\%
    sed -e 's/$/ \\\\/g'                  | # add \\
    sed 's/;/ \& /g'                      | # ; → &
    sed '1 a\\\hline'                     | # \hline after the first line
    sed '1 i\\\hline'                     | # \hline before the first line
    sed '$ a \\\hline'                    | # \hline at the end
    cat
}

maybecsvtable2tex() {
    if [ "$DISABLE_LATEX" = "" ]; then
        csvtable2tex
    else
        cat
    fi
}

export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export EXCLUDE_INVARIANTS=true

if [ "$DISABLE_LATEX" = "" ]; then
    export FORMAT_NUMBER_AS_TEX="true"
fi

cd "$1"

cat "execution-time-without-instrumentation"
make_csv | add_overhead | to_human_numbers | maybecsvtable2tex
