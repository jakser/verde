var checkpoint = null;

scenario.on("any non-accepting verdict", () => {
    scenario.restoreCheckpoint(checkpoint, () => {
        scenario.debuggerCmd("eval sid = tr.USER_login(uid)")
    });
});

scenario.on("any verdict with state begin", () => {
    scenario.createCheckpoint(cid => {
        checkpoint = cid;
    })
});
