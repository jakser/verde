# Needed so gdb doesn't crash, and is less annoying to use
set height 0
set width 0

source ../verde/gdbcommands.py

verde load-property queue-size.prop queue-size.py
verde show-graph
verde run-with-program
echo At this point, you can use the debugger interactively to explore the bug. For example, try to type “list” and then “backtrace”. You can use “quit” to quit GDB.\n
# quit
