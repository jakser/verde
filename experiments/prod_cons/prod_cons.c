#define _DEFAULT_SOURCE // for usleep
#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include "queue.h"
#include "common.h"

/* Variables partagées */

bool cont = true;
queue_t queue;

void* producer(void* data) {
	size_t producer_id = (size_t) data;

	while (cont) {
		usleep((rand() % (MAX_PROD_WAIT_MICROTIME - MIN_PROD_WAIT_MICROTIME)) + MIN_PROD_WAIT_MICROTIME);
		queue_push_synchronized(&queue, new_value(), producer_id);
	}

	return NULL;
}

void* consumer(void* data) {
	size_t consumer_id = (size_t) data;

	int value;

	while (cont) {
		usleep((rand() % (MAX_CONS_WAIT_MICROTIME - MIN_CONS_WAIT_MICROTIME)) + MIN_CONS_WAIT_MICROTIME);
		value = queue_pop_synchronized(&queue, consumer_id);
	}

	UNUSED(value);
	return NULL;
}

int main() {
	srand(time(NULL));

	queue_new(&queue, QUEUE_SIZE_MAX);

	pthread_t threads_prod[NBPROD];
	pthread_t threads_cons[NBCONS];

	for (size_t i = 0; i < NBPROD; i++) {
		pthread_create(&threads_prod[i], NULL, producer, (void*) i);
	}

	for (size_t i = 0; i < NBCONS; i++) {
		pthread_create(&threads_cons[i], NULL, consumer, (void*) i);
	}

#if MAX_EXECUTION_MICROTIME > 0
	usleep(MAX_EXECUTION_MICROTIME);
	cont = false;
#endif

	for (int i = 0; i < NBPROD; i++) {
		pthread_join(threads_prod[i], NULL);
	}

	for (int i = 0; i < NBCONS; i++) {
		pthread_join(threads_cons[i], NULL);
	}

	queue_delete(&queue);

	return EXIT_SUCCESS;
}

