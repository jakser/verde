#include "heap_queue.h"
#include <pthread.h>
#include "common.h"

#ifndef HEAP_SIZE_MAX
	#define HEAP_SIZE_MAX 50
#endif

int heap[HEAP_SIZE_MAX];
const int size_max = HEAP_SIZE_MAX;

int n = 0;

pthread_cond_t cond_heap_access = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_heap_access = PTHREAD_MUTEX_INITIALIZER;

/* producteur, consommateur */

void push_synchronized(int value, size_t producer_id) {
	UNUSED(producer_id);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&mutex_heap_access);
	}

	#if DIRECT_DEBUG
		debug("Producer %zu is pushing %d... (heap size: %d)", producer_id, value, n);
	#endif

	while (!break_now && n >= size_max) {
		pthread_cond_wait(&cond_heap_access, &mutex_heap_access);
	}

	heap[n] = value;
	n++;

	size_t new_heap_size = n;
	UNUSED(new_heap_size);

	#if DIRECT_DEBUG
		debug("Producer %zu pushed %d... (heap size: %d)", producer_id, value, n);
	#endif

	pthread_cond_broadcast(&cond_heap_access);
	if (!break_now) {
		pthread_mutex_unlock(&mutex_heap_access);
	}
}

int pop_synchronized(size_t consumer_id) {
	UNUSED(consumer_id);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&mutex_heap_access);
	}

	#if DIRECT_DEBUG
		debug("Consumer %zu is poping... (heap size: %d)", consumer_id, n);
	#endif

	while (!break_now && n <= 0) {
		pthread_cond_wait(&cond_heap_access, &mutex_heap_access);
	}

	int value = heap[--n];

	size_t new_heap_size = n;
	UNUSED(new_heap_size);

	#if DIRECT_DEBUG
		debug("Consumer %zu poped %d... (heap size: %d)", consumer_id, value, n);
	#endif

	pthread_cond_broadcast(&cond_heap_access);

	if (!break_now) {
		pthread_mutex_unlock(&mutex_heap_access);
	}

	return value;
}

void init_queue_heap() {
	// nothing to do
}
