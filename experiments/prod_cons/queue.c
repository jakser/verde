#include "queue.h"
#include "common.h"

#include <stdatomic.h>

void queue_new(queue_t* queue, size_t size) {
    queue->container          = malloc(sizeof(int) * size);
    queue->size_max           = size;
    queue->begin_queue_index  = 0;
    queue->end_queue_index    = 0;
    pthread_mutex_init(&(queue->mutex_queue_access), NULL);

    sem_init(&(queue->sem_pop), 0, 0);
    sem_init(&(queue->sem_push), 0, queue->size_max);
}

void queue_delete(queue_t* queue) {
    sem_destroy(&(queue->sem_pop));
    sem_destroy(&(queue->sem_push));
    pthread_mutex_destroy(&(queue->mutex_queue_access));
    free(queue->container);
}


void queue_push(queue_t* queue, int value, size_t producer_id) {

	static _Atomic size_t global_push_count = ATOMIC_VAR_INIT(0);
	static __thread size_t thread_push_count = 0;

	size_t gpc = 1 + atomic_fetch_add(&global_push_count, 1);
	++thread_push_count;

	printf("queue.c: push! (Producer %zu, pushes: %zu, Total: %zu)\n", producer_id, thread_push_count, gpc);
	queue->container[queue->end_queue_index] = value;
	queue->end_queue_index = (queue->end_queue_index + 1) % queue->size_max;
}

void queue_push_synchronized(queue_t* queue, int value, size_t producer_id) {
	sem_wait(&queue->sem_push);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&queue->mutex_queue_access);
	}

	#if DIRECT_DEBUG
		debug("Producer %zu is pushing %d... (queue size: %d)", producer_id, value, begin_queue_index - end_queue_index);
	#endif

	queue_push(queue, value, producer_id);

	#if DIRECT_DEBUG
		debug("Producer %zu pushed %d... (queue size: %d)", producer_id, value, begin_queue_index - end_queue_index);
	#endif

	if (!break_now) {
		pthread_mutex_unlock(&queue->mutex_queue_access);
	}

	sem_post(&queue->sem_pop);
}

int queue_pop(queue_t* queue, size_t consumer_id) {
	UNUSED(consumer_id);

	int current_begin_queue_index = queue->begin_queue_index;
	queue->begin_queue_index = (queue->begin_queue_index + 1) % queue->size_max;
	return queue->container[current_begin_queue_index];
}

int queue_pop_synchronized(queue_t *queue, size_t consumer_id) {
	sem_wait(&queue->sem_pop);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&queue->mutex_queue_access);
	}

	#if DIRECT_DEBUG
		debug("Consumer %zu is poping... (queue size: %d)", consumer_id, begin_queue_index - end_queue_index);
	#endif

	int value = queue_pop(queue, consumer_id);

	#if DIRECT_DEBUG
		debug("Consumer %zu poped %d... (queue size: %d)", consumer_id, value, begin_queue_index - end_queue_index);
	#endif


	if (!break_now) {
		pthread_mutex_unlock(&(queue->mutex_queue_access));
	}

	sem_post(&(queue->sem_push));
	return value;
}

