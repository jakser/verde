slice on clientProgramId, serverThreadId

initialization {
    received_server_bye = False
    received_client_bye = False
    server_thread_initialized = False
    client_thread_initialized = False
}

state init {
    transition {
        event from chat_server #join
        success waiting_clients
    }
}

state abstract_server_waiting_clients {
    transition {
        event from chat_server ChatServerThread.<init>()
        success {
            if client_thread_initialized:
                return "chat_established"
            server_thread_initialized = True
        } chat_server_thread_created
    }
}

state waiting_clients import transitions from state abstract_server_waiting_clients {
    transition {
        event from chat_client #join()
        success client_joining
    }

    transition {
        event from chat_server #leave
        success {
            if received_server_bye:
                return expecting_bye_from_client
        } server_left_early
    }

    transition {
        event from chat_server ChatServer.handle(input as msg) {
            print(msg == ".bye", msg)
            return msg == ".bye"
        }

        success {
            if received_client_bye:
                return "closed"
            received_server_bye = True
        } expecting_bye_from_client

        failure {
            if received_server_bye:
                return msg_while_expecting_bye
        }
    }
}

state client_joining import transitions from state waiting_clients {
    transition {
        event from chat_client ChatClientThread.<init>()
        success {
            if server_thread_initialized:
                return "chat_established"
            client_thread_initialized = True
        } chat_client_thread_created
    }

    transition {
        event from chat_client ChatClient.handle(msg) {
            print("CLIENT MSG: " + msg)
            return msg == ".bye"
        }

        success {
            if received_server_bye:
                return "closed"

            received_client_bye = True
        } expecting_bye_from_server

        failure {
            if received_client_bye:
                return msg_while_expecting_bye
        }
    }

    transition {
        event from chat_client #leave
        success {
            if received_client_bye:
                return expecting_bye_from_server
        } client_left_early
    }
}

# This state is not reachable.
state abstract_client_ready import transitions from state client_joining

# A client opened a connection to the server, we are waiting for events to the
# server.
# But we could have events from the client that happen when the connection is established.
state chat_client_thread_created import transitions from state client_joining
state chat_server_thread_created import transitions from state client_joining

state chat_established import transitions from state chat_client_thread_created

state expecting_bye_from_server import transitions from state chat_established
state expecting_bye_from_client import transitions from state chat_established

state closed accepting

state msg_while_expecting_bye non-accepting
state server_left_early non-accepting
state client_left_early non-accepting
