slice on msg

initialization {
    import re

    # The set of ECs, at any time.
    ecs = set()

    # The set of ECs at the time of the first occurence of a message
    # (for when the slice parameter msg is instanciated)
    ecs_for_this_message = {}

    # The set of ECs that received this message so far
    # (for when the slice parameter msg is instanciated)
    ec_received_this_message = {}
    expected_messages = set()

    def handle_message(ecid, msg, ID):
        global ecs_for_this_message
        global ec_received_this_message
        global message_sender_id

        if ID:
            expected_message = str(ID) + ": " + msg
            expected_messages.add(expected_message)
            print("Expecting message " + expected_message)
            return "ok"

        if msg not in expected_messages:
            return "should_not_have_received_this_message"

        if msg not in ecs_for_this_message:
            ecs_for_this_message[msg] = set(ecs)
            ec_received_this_message[msg] = set()

        if ecid not in ecs_for_this_message[msg]:
            return "should_not_have_received_this_message"

        if ecid in ec_received_this_message[msg]:
            return "received_twice"

        ec_received_this_message[msg].add(ecid)

        if ec_received_this_message[msg] == ecs_for_this_message[msg]:
            del ec_received_this_message[msg]
            del ecs_for_this_message[msg]
            expected_messages.remove(msg)

            return "ok"

        return "waiting_all_msgs"
}

state abstract_handle_msgs {
    # Abstract: this state is only there for other states to import its
    # transitions. This state is not reachable.
    # This is a hack. Things currently go wrong is we just copy paste the
    # transitions in differents states (because events get released and then
    # requested again)

    transition {
        event from chat_server ChatServer.handle(#ecid as ecid, input as msg, ID) {
            return msg != ".bye"
        }

        success {
            return handle_message(ecid, msg, ID)
        } waiting_all_msgs
    }

    transition {
        event from chat_client ChatClient.handle(#ecid as ecid, msg) {
            return msg != ".bye"
        }

        success {
            return handle_message(ecid, msg, None)
        } waiting_all_msgs
    }
}

state init import transitions from state abstract_handle_msgs {
    transition {
        event from chat_client #join(#ecid as ecid)
        success {
            print("(client) New ECID:" + str(ecid))
            ecs.add(ecid)
        } init
    }

    transition {
        event from chat_client #leave(#ecid as ecid)

        success {
            if ecid in ecs:
                print("(client) Remove ECID:" + str(ecid))
                ecs.remove(ecid)
        } init
    }

    transition {
        event from chat_server #join(#ecid as ecid)
        success {
            print("(server) New ECID:" + str(ecid))
        } init
    }

    transition {
        event from chat_server #leave(#ecid as ecid)

        success {
            print("(server) Remove ECID:" + str(ecid))
        } init
    }
}

# In this state, parameter slice msg is instanciated
state waiting_all_msgs import transitions from state abstract_handle_msgs {
    transition {
        event from chat_server #leave(#ecid as ecid)
        success left_early
    }

    transition {
        event from chat_client #leave(#ecid as ecid) {
            return ecid in ecs_for_this_message[msg] and ecid not in ec_received_this_message[msg]
        }

        success left_early
    }
}

state should_not_have_received_this_message non-accepting forget-slice
state received_twice non-accepting forget-slice
state left_early non-accepting forget-slice
state ok accepting forget-slice
