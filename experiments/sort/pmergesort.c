/*
 *  Parallel Merge Sort
 *  Created by Malith Jayaweera on 1/11/19.
 *  Published at malithjayaweera.com for public use.
 *  Edited to build a Verde experiment by Raphaël Jakse.
 *  Copyright © 2019 Malith Jayaweera. All rights reserved.
 *  Copyright © 2019 Raphaël Jakse. All rights reserved.
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <omp.h>

static void fill(size_t *tab, size_t N) {
    for (size_t i = 0; i < N; i++) {
        tab[i] = N - i;
    }
}

// static void print(char* label, size_t *tab, size_t N) {
//     printf("%s: [", label);
//     for (size_t i = 0; i < N; i++) {
//         if (i) {
//             printf(", ");
//         }
//
//         printf("%ld", tab[i]);
//     }
//     printf("]\n");
// }

/* test to ensure that the array is in sorted order */
void test_array_is_in_order(size_t arr[], size_t LENGTH) {
    for (size_t i = 1; i < LENGTH; i ++) {
        if (arr[i] < arr[i - 1]) {
            printf("Error. Out of order sequence: %ld found\n", arr[i]);
            return;
        }
    }
    printf("Array is in sorted order\n");
}

/* merge function */
void merge(size_t arr[], size_t left, size_t middle, size_t right) {
    size_t i = 0;
    size_t j = 0;
    size_t k = 0;
    size_t left_length = middle - left + 1;
    size_t right_length = right - middle;
    size_t* left_array = malloc(sizeof(size_t) * left_length);
    size_t* right_array = malloc(sizeof(size_t) * right_length);

    /* copy values to left array */
    for (size_t i = 0; i < left_length; i ++) {
        left_array[i] = arr[left + i];
    }

    /* copy values to right array */
    for (size_t j = 0; j < right_length; j ++) {
        right_array[j] = arr[middle + 1 + j];
    }

    i = 0;
    j = 0;
    /** chose from right and left arrays and copy */
    while (i < left_length && j < right_length) {
        if (left_array[i] <= right_array[j]) {
            arr[left + k] = left_array[i];
            i ++;
        } else {
            arr[left + k] = right_array[j];
            j ++;
        }
        k ++;
    }

    /* copy the remaining values to the array */
    while (i < left_length) {
        arr[left + k] = left_array[i];
        k ++;
        i ++;
    }
    while (j < right_length) {
        arr[left + k] = right_array[j];
        k ++;
        j ++;
    }

    free(left_array);
    free(right_array);
}

/* perform merge sort */
void merge_sort(size_t arr[], size_t left, size_t right) {
//     printf("merge_sort(%ld, %ld)\n", left, right);
//     print("merge_sort", arr, 1 << 4);
    if (left < right) {
        size_t middle = left + (right - left) / 2;
        merge_sort(arr, left, middle);
        merge_sort(arr, middle + 1, right);
        merge(arr, left, middle, right);
    }
}

static size_t min(size_t a, size_t b) {
    return a < b ? b : a;
}

static void sort(size_t* arr, size_t LENGTH) {
    size_t NUM_THREADS = min(LENGTH, omp_get_max_threads());
    size_t chunk = LENGTH / NUM_THREADS;
    size_t OFFSET = LENGTH % NUM_THREADS;

    #pragma omp parallel for num_threads(NUM_THREADS)
    for (size_t left = 0; left < LENGTH; left += chunk) {
        size_t right = left + chunk - 1;
        if (left + chunk > LENGTH) {
            right += OFFSET;
        }

        merge_sort(arr, left, right);
    }

    do {
        for (size_t left = 0; left < LENGTH; left += 2 * chunk) {
            size_t right = left + 2 * chunk - 1;
            size_t middle = left + chunk - 1;
            if (right >= LENGTH) {
                right = LENGTH - 1;
            }
            merge(arr, left, middle, right);
        }
        chunk *= 2;
    } while (chunk < LENGTH);
}

int main(int argc, const char * argv[]) {
    if (argc != 2) {
        printf("Usage: %s N, where N determines the size of the array to sort, such as 2^N is the size.\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t LENGTH = ((size_t) 1) << ((size_t) atoi(argv[1]));

    printf("Size: %ld\n", LENGTH);

    size_t* arr = malloc(LENGTH * sizeof(size_t));

    fill(arr, LENGTH);
    sort(arr, LENGTH);
//     print("after", arr, LENGTH);

    /* test to ensure that the array is in sorted order */
//     test_array_is_in_order(arr, LENGTH);

    free(arr);
    return 0;
}
