#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

// static void swap(int *x,int *y)
// {
//     int temp;
//     temp = *x;
//     *x = *y;
//     *y = temp;
// }
//
// static int choose_pivot(int i,int j )
// {
//     return((i+j) /2);
// }
//
// static void quicksort_rec(int list[],int m,int n)
// {
//     int key,i,j,k;
//     if( m < n)
//     {
//         k = choose_pivot(m,n);
//         swap(&list[m],&list[k]);
//         key = list[m];
//         i = m+1;
//         j = n;
//         while(i <= j)
//         {
//             while((i <= n) && (list[i] <= key))
//                 i++;
//             while((j >= m) && (list[j] > key))
//                 j--;
//             if( i < j)
//                 swap(&list[i],&list[j]);
//         }
//         /* swap two elements */
//         swap(&list[m],&list[j]);
//
//         /* recursively sort the lesser list */
//         quicksort_rec(list,m,j-1);
//         quicksort_rec(list,j+1,n);
//     }
// }
//
// static void sort(int *tab, size_t N, int elem_size) {
//     quicksort_rec(tab, 0, N);
// }


static void fill(int *tab, size_t N) {
    for (size_t i = 0; i < N; i++) {
        tab[i] = N - i;
    }
}

static int compare(const void *x, const void *y) {
  int xp = *((int *) x);
  int yp = *((int *) y);

  return (xp == yp) ? 0 : (xp < yp) ? -1 : 1;
}

static void merge(int *tab, size_t chunk_size, size_t elem_size) {
//     for (size_t i = 0, j = chunk_size; i < chunk_size; i++) {
//         if (tab[i] > tab[j]) {
//             int tmp = tab[i];
//             tab[i] = tab[j];
//
//             size_t k;
//
//             for (k = j + 1; (k < (2 * chunk_size)) && (tab[k] < tmp); k++) {
//                 tab[k - 1] = tab[k];
//             }
//
//             tab[k - 1] = tmp;
//         }
//     }
    qsort(tab, 2 * chunk_size, sizeof(int), compare);
}

static size_t max(int a, int b) {
    return (a > b) ? a : b;
}

static void parallel_qsort_sort(int *tab, size_t N) {
    int nb_procs = omp_get_max_threads();
    size_t size_chunk = max(1, N / nb_procs);

    #pragma omp parallel for num_threads(nb_procs)
    for (size_t i = 0; i < N; i += size_chunk) {
        qsort(tab + i, size_chunk, sizeof(int), compare);
//         sort(tab + i, size_chunk, sizeof(int));
    }

    do {
        nb_procs /= 2;
        #pragma omp parallel for num_threads(nb_procs)
        for (size_t i = 0; i < N; i += 2 * size_chunk) {
            merge(tab + i, size_chunk, sizeof(int));
        }
        size_chunk *= 2;
    } while (nb_procs > 1);
}

// static void print(char* label, int *tab, size_t N) {
//     printf("%s: [", label);
//     for (size_t i = 0; i < N; i++) {
//         if (i) {
//             printf(", ");
//         }
//
//         printf("%d", tab[i]);
//     }
//     printf("]\n");
// }

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: %s N, where N determines the size of the array to sort, such as 2^N is the size.\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t N = 1 << atoi(argv[1]);
    printf("Size: %ld\n", N);

    int *tab = malloc(N * sizeof(int));

    if (tab == NULL) {
        fprintf(stderr, "malloc failed.\n");
        return EXIT_FAILURE;
    }

    fill(tab, N);
//     print("before", tab, N);
    parallel_qsort_sort(tab, N);
//     print("after", tab, N);

    free(tab);
    return EXIT_SUCCESS;
}
