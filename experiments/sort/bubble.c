#include <stdio.h>
#include<omp.h>

#include <x86intrin.h>

#define NBEXPERIMENTS    7
static long long unsigned int experiments [NBEXPERIMENTS] ;

/*
  bubble sort -- sequential, parallel --
*/

#define    NBCHUNKS    64

static   unsigned int N ;

typedef  int  *array_int ;

/* the X array will be sorted  */

static   array_int X  ;

long long unsigned int average (long long unsigned int *exps)
{
  unsigned int i ;
  long long unsigned int s = 0 ;

  for (i = 2; i < (NBEXPERIMENTS-2); i++)
    {
      s = s + exps [i] ;
    }

  return s / (NBEXPERIMENTS-2) ;
}

void init_array (array_int T)
{
  register int i ;

  for (i = 0 ; i < N ; i++)
    {
      T [i] = N - i ;
    }
}

void print_array (array_int T)
{
  register int i ;

  for (i = 0 ; i < N ; i++)
    {
      printf ("%d ", T[i]) ;
    }
  printf ("\n") ;
}

int is_sorted (array_int T)
{
  register int i ;

  for (i = 1 ; i < N ; i++)
    {
        /* test designed specifically for our usecase */
      if (T[i-1] +1  != T [i] )
        return 0 ;
    }
  return 1 ;
}

void sequential_bubble_sort (int *T, const int size)
{
  register unsigned int i ;
  register unsigned int switched  ;

  do
    {
      switched = 0 ;

      for (i = 0 ; i < (size - 1); i++)
        {
          if (T [i] > T [i+1])
            {
              int swap = T [i] ;

              T [i] = T [i+1] ;
              T [i+1] = swap ;
              switched = 1 ;
            }
        }
    }
  while (switched != 0) ;

  return ;
}

void parallel_bubble_sort (int *T, const int size)
{
  unsigned int i ;
  unsigned int chunk_size ;
  unsigned int nb_chunks ;

  unsigned  switched  ;
  unsigned int nb_threads ;
  unsigned int p ;

  nb_threads = omp_get_max_threads () ;

  nb_chunks = NBCHUNKS ;
  chunk_size = size / nb_chunks ;

  do
    {

      switched = 0 ;
#pragma omp parallel for schedule (static) reduction (|:switched)
      for (i = 0; i < N; i = i + chunk_size )
        {
          register unsigned int j ;

          for (j = 0; j < (chunk_size-1); j++)
            {
              if (T [i+j] > T [i+j+1])
                {
                  int swap = T [i+j] ;

                  T [i+j] = T [i+j+1] ;
                  T [i+j+1] = swap ;
                  switched = 1 ;
                }
            }
        }

      p = 0 ;

      /*
        update the chunk borders
      */

      for (i = 0 ; i < (nb_chunks-1); i++)
        {
          p += chunk_size ;
          if (T [p-1] > T [p])
            {
              int swap = T [p-1] ;

              T[p-1] = T [p] ;
              T[p]   = swap ;
              switched = 1 ;
            }
        }
    }

  while (switched != 0) ;

  return ;
}

void parallel_bubble_sort1 (int *T, const int size)
{
  unsigned int i ;
  unsigned int chunk_size ;
  unsigned int nb_chunks ;

  unsigned  switched  ;
  unsigned int nb_threads ;
  unsigned int p ;

  nb_threads = omp_get_max_threads () ;

  nb_chunks = NBCHUNKS ;
  chunk_size = size / nb_chunks ;

    do
    {
      switched = 0 ;

#pragma omp parallel for schedule (dynamic) reduction (|:switched)
      for (i = 0; i < N; i = i + chunk_size )
        {
          register unsigned int j ;

          for (j = 0; j < (chunk_size-1); j++)
            {
              if (T [i+j] > T [i+j+1])
                {
                  int swap = T [i+j] ;

                  T [i+j] = T [i+j+1] ;
                  T [i+j+1] = swap ;
                  switched = 1 ;
                }
            }
        }

      p = 0 ;

      /*
        update the chunk borders
      */

      for (i = 0 ; i < (nb_chunks-1); i++)
        {
          p += chunk_size ;
          if (T [p-1] > T [p])
            {
              int swap = T [p-1] ;

              T[p-1] = T [p] ;
              T[p]   = swap ;
              switched = 1 ;
            }
        }

    }

  while (switched != 0) ;

  return ;
}

void parallel_bubble_sort2 (int *T, const int size)
{
  unsigned int i ;
  unsigned int chunk_size ;
  unsigned int nb_chunks ;

  unsigned  switched  ;
  unsigned int nb_threads ;
  unsigned int p ;

  nb_threads = omp_get_max_threads () ;

  nb_chunks = NBCHUNKS ;
  chunk_size = size / nb_chunks ;
  //  chunk = chunk / 4 ;

  do
    {
      switched = 0 ;

#pragma omp parallel for schedule (guided) reduction (|:switched)
      for (i = 0; i < N; i = i + chunk_size )
        {
          register unsigned int j ;

          for (j = 0; j < (chunk_size-1); j++)
            {
              if (T [i+j] > T [i+j+1])
                {
                  int swap = T [i+j] ;

                  T [i+j] = T [i+j+1] ;
                  T [i+j+1] = swap ;
                  switched = 1 ;
                }
            }
        }

      p = 0 ;

      /*
        update the chunk borders
      */

      for (i = 0 ; i < (nb_chunks-1); i++)
        {
          p += chunk_size ;
          if (T [p-1] > T [p])
            {
              int swap = T [p-1] ;

              T[p-1] = T [p] ;
              T[p]   = swap ;
              switched = 1 ;
            }
        }
    }

  while (switched != 0) ;

  return ;
}

int main (int argc, char **argv)
{
  unsigned long long int start, end, residu ;
  unsigned long long int av ;
  unsigned int exp ;

  if (argc != 2)
    {
      fprintf (stderr, "bubble N \n") ;
      exit (-1) ;
    }

  N = 1 << (atoi(argv[1])) ;
  X = (int *) malloc (N * sizeof(int)) ;

  printf("--> Sorting an array of size %u\n",N);

  start = _rdtsc () ;
  end   = _rdtsc () ;
  residu = end - start ;


  for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
    {
      init_array (X) ;

      start = _rdtsc () ;

         sequential_bubble_sort (X, N) ;

      end = _rdtsc () ;
      experiments [exp] = end - start ;

      /* verifying that X is properly sorted */
      if (! is_sorted (X))
        {
          fprintf(stderr, "error in the sort\n") ;
          exit (-1) ;
        }
    }

  av = average (experiments) ;

  printf ("\n bubble serial \t\t\t %Ld cycles\n\n", av-residu) ;


  for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
    {
      init_array (X) ;
      start = _rdtsc () ;

          parallel_bubble_sort (X, N) ;

      end = _rdtsc () ;
      experiments [exp] = end - start ;

      /* verifying that X is properly sorted */
      if (! is_sorted (X))
        {
          fprintf(stderr, "error in the sort\n") ;
          exit (-1) ;
        }
    }

  av = average (experiments) ;
  printf ("\n bubble parallel static \t %Ld cycles\n\n", av-residu) ;

    for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
    {
      init_array (X) ;
      start = _rdtsc () ;

          parallel_bubble_sort1 (X, N) ;

      end = _rdtsc () ;
      experiments [exp] = end - start ;

      /* verifying that X is properly sorted */
      if (! is_sorted (X))
        {
          fprintf(stderr, "error in the sort\n") ;
          exit (-1) ;
        }
    }

  av = average (experiments) ;
  printf ("\n bubble parallel dynamic \t %Ld cycles\n\n", av-residu) ;

  for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
    {
      init_array (X) ;
      start = _rdtsc () ;

          parallel_bubble_sort2 (X, N) ;

      end = _rdtsc () ;
      experiments [exp] = end - start ;

      /* verifying that X is properly sorted */
      if (! is_sorted (X))
        {
          fprintf(stderr, "error in the sort\n") ;
          exit (-1) ;
        }
    }

  av = average (experiments) ;
  printf ("\n bubble parallel guided \t %Ld cycles\n\n", av-residu) ;

  // print_array (X) ;

}
