#include <stdio.h>
#include <stdlib.h>

static void fill(int *tab, size_t N) {
    for (size_t i = 0; i < N; i++) {
        tab[i] = N - i;
    }
}

void CopyArray(int* A, size_t iBegin, size_t iEnd, int* B)
{
    for(size_t k = iBegin; k < iEnd; k++)
        B[k] = A[k];
}

//  Left source half is A[ iBegin:iMiddle-1].
// Right source half is A[iMiddle:iEnd-1   ].
// Result is            B[ iBegin:iEnd-1   ].
void TopDownMerge(int* A, size_t iBegin, size_t iMiddle, size_t iEnd, int* B)
{
    size_t i = iBegin, j = iMiddle;

    // While there are elements in the left or right runs...
    for (size_t k = iBegin; k < iEnd; k++) {
        // If left run head exists and is <= existing right run head.
        if (i < iMiddle && (j >= iEnd || A[i] <= A[j])) {
            B[k] = A[i];
            i = i + 1;
        } else {
            B[k] = A[j];
            j = j + 1;
        }
    }
}

// Sort the given run of array A[] using array B[] as a source.
// iBegin is inclusive; iEnd is exclusive (A[iEnd] is not in the set).
void TopDownSplitMerge(int* B, size_t iBegin, size_t iEnd, int* A)
{
    if(iEnd - iBegin < 2)                       // if run size == 1
        return;                                 //   consider it sorted
    // split the run longer than 1 item into halves
    size_t iMiddle = (iEnd + iBegin) / 2;              // iMiddle = mid point
    // recursively sort both runs from array A[] into B[]
    TopDownSplitMerge(A, iBegin,  iMiddle, B);  // sort the left  run
    TopDownSplitMerge(A, iMiddle,    iEnd, B);  // sort the right run
    // merge the resulting runs from array B[] into A[]
    TopDownMerge(B, iBegin, iMiddle, iEnd, A);
}

void parallel_qsort_sort(int* A, int* B, int n)
{
    CopyArray(A, 0, n, B);           // duplicate array A[] into B[]
    TopDownSplitMerge(B, 0, n, A);   // sort data from B[] into A[]
}

// static void print(char* label, int *tab, size_t N) {
//     printf("%s: [", label);
//     for (size_t i = 0; i < N; i++) {
//         if (i) {
//             printf(", ");
//         }
//
//         printf("%d", tab[i]);
//     }
//     printf("]\n");
// }

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: %s N, where N determines the size of the array to sort, such as 2^N is the size.\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t N = 1 << atoi(argv[1]);
    printf("Size: %ld\n", N);

    int *A = malloc(N * sizeof(int));
    int *B = malloc(N * sizeof(int));

    if (A == NULL || B == NULL) {
        fprintf(stderr, "malloc failed.\n");
        return EXIT_FAILURE;
    }

    fill(A, N);
//     print("before", A, N);
    parallel_qsort_sort(A, B, N);
//     print("after", B, N);

    free(A);
    free(B);
    return EXIT_SUCCESS;
}
