import os
import signal
import json
import sys
import subprocess

mon = 0
ec  = 0
scn = 0

is_java_version = "SORT_VERSION" in os.environ and os.environ["SORT_VERSION"] == "java"

VERDE_DIR = os.path.dirname(os.path.realpath(__file__))  + "/../../"

def trigger(bus, event, msg=None):
    global mon, ec, scn, is_java_version, VERDE_DIR

    if event == "listening":
        port = msg
        print("Launching the monitor")
        os.environ["VERDE_TRACE_MSGS"] = "/tmp/messages.mon"
        if is_java_version:
            mon = os.spawnlp(os.P_NOWAIT, "verde-monitor", "verde-monitor", VERDE_DIR + "/examples/sort/qsort-java.prop")
        else:
            mon = os.spawnlp(os.P_NOWAIT, "verde-monitor", "verde-monitor", VERDE_DIR + "/examples/sort/qsort-c.prop")

        print("Launching the scenario")
        os.environ["VERDE_TRACE_MSGS"] = "/tmp/messages.scn"

        #os.environ["JAVA_TOOL_OPTIONS"] = "-Xrunhprof:cpu=samples,file=/tmp/scenario.hprof"
        scn = os.spawnlp(os.P_NOWAIT, "verde-scenario", "verde-scenario", VERDE_DIR + "/src/java/scenario/examples/stopexec.py")
        #del os.environ["JAVA_TOOL_OPTIONS"]
        #scn = os.spawnlp(os.P_NOWAIT, "python3", "python3", VERDE_DIR + "/verde-python/dumbscenario.py")
        #scn = os.spawnlp(os.P_NOWAIT, "python3", "python3", VERDE_DIR + "/verde-python/lessdumbscenario.py")

        os.environ["VERDE_TRACE_MSGS"] = "/tmp/messages.prgm"

        print("Launching the prgm")
        if is_java_version:
            if "PROFILE_SORT" in os.environ:
                os.environ["JAVA_TOOL_OPTIONS"] = "-agentlib:hprof=cpu=samples"

            subprocess.Popen([
                "verde-jdb",
                "QSort", "100"
            ], stdin=sys.stdin, stdout=sys.stdout, cwd=VERDE_DIR + "/examples/sort/")

            #os.spawnlp(os.P_NOWAIT, "konsole", "konsole", "--separate", "--hold", "-e", "verde-jdb", VERDE_DIR + "/examples/sort/", "QSort", "100")
            
            if "PROFILE_SORT" in os.environ:
                del os.environ["JAVA_TOOL_OPTIONS"]
        else:
            if "SORT_WITH_CALLGRIND" in os.environ:
                os.spawnlp("valgrind", "--tool=callgrind", "--vgdb=yes", "--vgdb-error=0", VERDE_DIR + "/examples/sort/qsort", "100")

                #os.spawnlp(os.P_NOWAIT, "konsole", "konsole", "--separate", "--hold", "-e", "verde-gdb", "-ex", 'pi input("Press enter when ready:")', "-ex", "target remote | vgdb", VERDE_DIR + "/examples/sort/qsort")
                subprocess.Popen([
                    "verde-gdb",
                    "-ex", 'pi input("Press enter when ready:")',
                    "-ex", "target remote | vgdb",
                    VERDE_DIR + "/examples/sort/qsort"
                ], stdin=sys.stdin, stdout=sys.stdout)

            elif "SORT_CPROFILE" in os.environ:
                subprocess.Popen([
                    "verde-gdb",
                    "-ex", 'pi import cProfile',
                    "-ex", 'pi pr = cProfile.Profile()',
                    "-ex", 'pi pr.enable()',
                    "-ex", 'run 100',
                    "-ex", 'pi pr.disable()',
                    "-ex", 'pi pr.dump_stats(' + json.dumps(os.environ["SORT_CPROFILE"]) + ')',
                    VERDE_DIR + "/examples/sort/qsort"
                ], stdin=sys.stdin, stdout=sys.stdout)

                #os.spawnlp(os.P_NOWAIT, "konsole", "konsole", "--separate", "--hold", "-e", "verde-gdb",  "-ex", 'pi import cProfile',  "-ex", 'pi pr = cProfile.Profile()',  "-ex", 'pi pr.enable()',  "-ex", 'run 100',  "-ex", 'pi pr.disable()',  "-ex", 'pi pr.dump_stats(' + json.dumps(os.environ["SORT_CPROFILE"]) + ')', VERDE_DIR + "/examples/sort/qsort")

            else:
                subprocess.Popen([
                    "verde-gdb",
                    VERDE_DIR + "/examples/sort/qsort", "100"
                ], stdin=sys.stdin, stdout=sys.stdout)
                sys.stdin.close()
                #os.spawnlp(os.P_NOWAIT, "konsole", "konsole", "--separate", "--hold", "-e", "verde-gdb", VERDE_DIR + "/examples/sort/qsort", "100")

    if event == "hello" and msg.hello.serviceName == "executionController":
        ec = msg.sender

    if event == "bye" and msg == ec:
        #if mon:
            #os.environ["VERDE_TRACE_MSGS"] = "/tmp/messages.mon"
            #os.kill(mon, signal.SIGTERM)
            #mon = os.spawnl(os.P_NOWAIT, "verde-monitor", "verde-monitor", VERDE_DIR + "/example/sort/sort-c.prop")

        bus.shutdown()
