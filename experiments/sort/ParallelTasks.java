import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Callable;

public class ParallelTasks {
    static final int MAX_THREADS = Runtime.getRuntime().availableProcessors();

    int nb_threads = MAX_THREADS;
    ThreadPoolExecutor executor = null;

    private ThreadPoolExecutor newThreadPoolExecutor() {
        return new ThreadPoolExecutor(
            nb_threads,
            nb_threads,
            0L,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>()
        );
    }

    ParallelTasks() {
        executor = newThreadPoolExecutor();
    }

    public void submit(Callable<Void> f) {
        executor.submit(f);
    }

    public void waitTasks() throws InterruptedException {
        executor.shutdown();

        // Wait for everything to finish.
        while (!executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)) {
            System.err.println("Awaiting completion of threads.");
        }

        executor = newThreadPoolExecutor();
    }

    public void setNbThreads(int n) {
//         nb_threads = n > 1 ? n : 1;
        executor.setCorePoolSize(nb_threads);
        executor.setMaximumPoolSize(nb_threads);
    }
}
