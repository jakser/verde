public class Test {
    static final ParallelTasks tasks = new ParallelTasks();

    public static void f(int c) {
        System.out.println("Number: " + c);
    }

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 1000; i++) {
            final int c = i;
            tasks.submit(
                () -> {
                    f(c);
                    return null;
                }
            );
        }

        tasks.waitTasks();

        return;
    }
}
