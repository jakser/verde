#!/usr/bin/env sh

# cd $RESULTS;
# input: res.csv

csvtable2tex() {
    sed -e '1s/^/\\\heading{/g'           | # add \heading
    sed -e '1s/$/}/g'                     | # add \heading
    sed -e '1s/;/};\\\heading{/g'         | # add \heading
    sed -e '1s/_/\\\_/g'                  | # _ → \_
    sed -e 's/$/ \\\\/g'                  | # add \\
    sed 's/;/ \& /g'                      | # ; → &
    sed '1 a\\\hline'                     | # \hline after the first line
    sed '1 i\\\hline'                     | # \hline before the first line
    sed '$ a \\\hline'                    | # \hline at the end
    cat
}

transpose_csv_semicolon() {
    ruby -rcsv -e 'puts CSV.parse(STDIN, { headers: false, col_sep: ";"}).transpose.map {|i| i.to_csv({col_sep: ";"})}'
}

select_results() {
    # grep -v '0;[1-5];' | sed 's/^1;//g'
    sed 's/^0;//g'
}

make_csv() {
    input="$(cat)"

    # header
    printf "N;"
    printf "%s" "$input"            |
        grep -F ";raw"              |
        transpose_csv_semicolon     |
        head -1

    for field in raw verde dist-verde; do
        printf "$field;"
        printf "%s" "$input"        |
            grep -F ";$field"       |
            transpose_csv_semicolon |
            tail -1
    done
}

overhead() {
    echo "($2 - $1) / $1" | bc -l
}

column() {
    printf "%s" "$2" | cut -f"$1" -d";"
}

nbevents() {
    $VERDE_DIR/bin/tracecount.py < "dist-verde-trace-0-$1"  |
        grep -F "Event Notifications:" |
        grep -E -o "TOTAL [0-9]+"      | cut -d' ' -f 2
}

evt_per_sec() {
    echo "($1 / $2)" | bc -l
}

interpret_results() {
    read line
    printf "%s\n" "$line;nbevents;evt_per_sec_raw;evt_per_sec_verde;evt_per_sec_dist_verde;overhead_verde;overhead_dist_verde"
    while read line; do
        N=$(column 1 "$line")
        raw=$(column 2 "$line")
        verde=$(column 3 "$line")
        dist_verde=$(column 4 "$line")

        events=$(nbevents "$N")
        printf "%s\n" "$line;$events;$(evt_per_sec $events $raw);$(evt_per_sec $events $verde);$(evt_per_sec $events $dist_verde);$(overhead "$raw" "$verde");$(overhead "$raw" "$dist_verde")"
    done
}
cd "$1"

if [ ! -e "formated.csv" ]; then
    cat "res.csv" | select_results | make_csv | transpose_csv_semicolon | interpret_results > "formated.csv"
fi

cat "formated.csv" |
    sed -E 's/\.([0-9][0-9])[0-9]+/.\1/g' | # no more than 2 digits after the decimal separator
    sed -E 's/([^0-9])\./\10./g'          | # zero before the dot
    csvtable2tex |
    cat
