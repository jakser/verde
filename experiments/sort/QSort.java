import java.util.Arrays;

public class QSort {
 static final int NBEXPERIMENTS = 100;
 static final ParallelTasks tasks = new ParallelTasks();
 static long[] experiments = new long[NBEXPERIMENTS];

 static void qsort(int[] T, int from, int to) { Arrays.sort(T, from, to); }
/*
  bubble sort (sequential, parallel)
*/


static int     N ;

static int[] X ;

static void init_array (int[] T)
{
  int i ;

  for (i = 0 ; i < N ; i++)
    {
      T [i] = N - i ;
    }
}

static void print_array (int[] T)
{
  int i ;

  for (i = 0 ; i < N ; i++)
    {
      System.out.printf ("%d ", T[i]);
    }
  System.out.println() ;
}

static boolean is_sorted (int[] T)
{
  int i ;

  for (i = 1 ; i < N ; i++)
    {
        /* test designed specifically for our usecase */
        if (T[i-1] +1  != T [i] ) {
            System.out.printf("Error at index %d\n", i);
            return false ;
        }
    }
  return true ;
}

static long average (long[] exps)
{
  int i ;
  long s = 0 ;

  for (i = 2; i < (NBEXPERIMENTS-2); i++)
    {
      s = s + exps [i] ;
    }

  return s / (NBEXPERIMENTS-2) ;
}


// static void qsort_sort (int[] T, int size)
// {
//
//   qsort (T, 0, size) ;
//
//   return ;
// }
//

static void merge (int[] T, int start, int size)
{
  /*
    merge without memory duplication
  */

  int i ;
  int j ;
  int k  ;

  for (i = 0, j = size; i < size; i = i + 1)
    {
      if (T[j + start] < T [i + start] )
        {
          int swap = T [i + start] ;

          T [i + start] = T [j + start] ;

          for (k = j + 1; k < (2 * size) && (T [k + start] < swap); k++)
            {
              T [k-1 + start] = T [k + start] ;
            }

          T [k-1 + start] = swap ;
        }
    }
}


// static void merge (int[] T, int start, int size)
// {
//   /*
//      Merge two chunks in a temporary chunk!
//      Memory duplication;-(
//    */
//
//   int[] X = new int[2 * size];
//
//   int i = 0 ;
//   int j = size ;
//   int k = 0 ;
//
//   while ((i < size) && (j < 2*size))
//     {
//       if (T[i + start] < T [j + start])
//         {
//           X [k] = T [i + start] ;
//           i = i + 1 ;
//         }
//       else
//         {
//           X [k] = T [j + start] ;
//           j = j + 1 ;
//         }
//       k = k + 1 ;
//     }
//
//   if (i < size)
//     {
//       for (; i < size; i++, k++)
//         {
//           X [k] = T [i + start] ;
//         }
//     }
//   else
//     {
//       for (; j < 2*size; j++, k++)
//         {
//           X [k] = T [j + start] ;
//         }
//     }
//
//   System.arraycopy(X, 0, T, start, 2 * size);
// }
//
static void submit_task_qsort(int[] T, int i, int end) {
//     System.out.println("qsort   " + i + " " + end);
    tasks.submit(
        () -> {
//             System.out.println("qsorting " + i + " " + end);
            qsort (T, i, end) ;
            return null;
        }
    );
}

static void submit_task_merge(int[] T, int i, int chunk) {
//     System.out.println("merge   " + i + " " + end);
    tasks.submit(
        () -> {
//         System.out.println("merging   " + i + " " + end);
        merge (T, i, chunk);
        return null;
        }
    );
}

static void parallel_qsort_sort (int[] T, int size) throws InterruptedException
{
  int i ;
  int chunk ;
  int nb_threads ;

  nb_threads = ParallelTasks.MAX_THREADS;
  chunk = size / nb_threads ;

  tasks.setNbThreads(nb_threads);

  for (i = 0; i < size; i = i + chunk )
    {
      submit_task_qsort(T, i, i + chunk);
    }

  tasks.waitTasks();

  do
    {
    nb_threads = nb_threads / 2 ;
    tasks.setNbThreads(nb_threads);
    for (i = 0; i < size; i = i + 2 * chunk)
      {
        submit_task_merge(T, i, chunk);
      }
    chunk = chunk * 2 ;
    tasks.waitTasks();
    } while (chunk != size) ;


  return ;
}

// static void parallel_qsort_sort2 (int[] T, int size) throws InterruptedException
// {
//   int i ;
//   int chunk ;
//   int nb_threads ;
//
//   nb_threads = ParallelTasks.MAX_THREADS ;
//   chunk = size / nb_threads ;
//
//   tasks.setNbThreads(nb_threads);
//   for (i = 0; i < size; i = i + chunk )
//     {
//       submit_task_qsort(T, i, i + chunk);
//     }
//
//     tasks.waitTasks();
//
//   do
//     {
//       nb_threads = nb_threads / 2 ;
//     tasks.setNbThreads(nb_threads);
//     for (i = 0; i < size; i = i + 2 * chunk)
//       {
//         submit_task_merge(T, i, chunk);
//       }
//
//     tasks.waitTasks();
//
//     chunk = chunk * 2 ;
//     } while (chunk != size) ;
//
//   return ;
// }
//
// static void parallel_qsort_sort3 (int[] T, int size) throws InterruptedException
// {
//   int i ;
//   int chunk ;
//   int nb_threads ;
//
//   nb_threads = ParallelTasks.MAX_THREADS ;
//   chunk = size / nb_threads ;
//
//   tasks.setNbThreads(nb_threads);
//   for (i = 0; i < size; i = i + chunk )
//     {
//       submit_task_qsort(T, i, i + chunk);
//     }
//
//   tasks.waitTasks();
//
//   do
//     {
//       nb_threads = nb_threads / 2 ;
//     tasks.setNbThreads(nb_threads);
//     for (i = 0; i < size; i = i + 2 * chunk)
//       {
//         submit_task_merge(T, i, chunk);
//       }
//     chunk = chunk * 2 ;
//     tasks.waitTasks();
//     } while (chunk != size) ;
//
//   return ;
// }
//
//
public static void main (String[] args) throws InterruptedException
{
  long start, end, residu ;
  long av ;
  int exp ;

  if (args.length != 1)
    {
      System.err.println("qsort N \n") ;
      System.exit (-1) ;
    }

  N = 1 << (Integer.parseInt(args[0])) ;
  X = new int[N] ;

  start = System.nanoTime () ;
  end   = System.nanoTime () ;
  residu = end - start ;

//   for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
//     {
//       init_array (X) ;
//
//       start = System.nanoTime () ;
//
//                qsort_sort (X, N) ;
//
//       end = System.nanoTime () ;
//       experiments [exp] = end - start ;
//
//       if (! is_sorted (X))
//         {
//           System.err.print("error in the sort\n") ;
//           System.exit (-1) ;
//         }
//     }
//
//   av = average (experiments) ;
//   System.out.printf ("\n qsort serial\t\t %d ns\n\n", av-residu) ;


  for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
    {
      init_array (X) ;

      start = System.nanoTime () ;

           parallel_qsort_sort (X, N) ;

      end = System.nanoTime () ;
      experiments [exp] = end - start ;

      if (! is_sorted (X))
        {
          System.err.print("error in the sort\n") ;
          System.exit (-1) ;
        }
    }

  av = average (experiments) ;
  System.out.printf ("\n qsort parallel static\t %d ns\n\n", av-residu) ;

  //   print_array (X) ;

//   for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
//     {
//       init_array (X) ;
//
//       start = System.nanoTime () ;
//
//            parallel_qsort_sort2 (X, N) ;
//
//       end = System.nanoTime () ;
//       experiments [exp] = end - start ;
//
//       if (! is_sorted (X))
//         {
//           System.err.print("error in the sort\n") ;
//           System.exit (-1) ;
//         }
//     }
//
//   av = average (experiments) ;
//   System.out.printf ("\n qsort parallel dynamic\t %d ns\n\n", av-residu) ;
//
//   // print_array (X) ;
//
//     for (exp = 0 ; exp < NBEXPERIMENTS; exp++)
//     {
//       init_array (X) ;
//
//       start = System.nanoTime () ;
//
//            parallel_qsort_sort3 (X, N) ;
//
//       end = System.nanoTime () ;
//       experiments [exp] = end - start ;
//
//       if (! is_sorted (X))
//         {
//           System.err.print("error in the sort\n") ;
//           System.exit (-1) ;
//         }
//     }
//
//   av = average (experiments) ;
//   System.out.printf ("\n qsort parallel guided\t %d ns\n\n", av-residu) ;

  //  print_array (X) ;
}
}
