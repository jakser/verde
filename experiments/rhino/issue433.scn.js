var debuggerPrintStack = () => scenario.debuggerCmd("where", (out) => java.lang.System.out.println(out));

scenario.on("state calling_execIdCall", () => {
    java.lang.System.out.println("In calling_execIdCall.");
    debuggerPrintStack();

    var point = scenario.addPoint("Line org.mozilla.javascript.NativeArray:282", () => {
        java.lang.System.out.println("In line breakpoint.");
        scenario.removePoint(point);
        scenario.debuggerCmd("set inNewExpr = false", (out) => {
            java.lang.System.out.println("After setting: " + out);
        });
    });
});

scenario.on("any non-accepting state", () => {
    scenario.suspendExecution();
});
