#!/usr/bin/env python3

import os
import sys
import subprocess
import pexpect
import shutil

properties = [
    "leader_in_vote.prop",
    "consensus_reached.prop",
    "allsameleader.prop",
    "unique_leader_better.prop"
]

VERDE_DIR = os.environ["VERDE_DIR"] if "VERDE_DIR" in os.environ else "../../"
bin_folder = VERDE_DIR + "/bin"
scn = bin_folder + "/../scenarios/stopexec.js"

def h():
    print("Usage: " + sys.argv[0] + """ CMD
prepare:
    Prepare the experiment. Please set the following environment variables:
     - LOGCABIN_NB_NODES: the number of nodes to run (default: 3)
     - LOGCABIN_PORT_START: ports to use (default: 5254)

run-bare: run the experiment, without verde
run-verde: run the experiment, with verde
clean: clean everything
     """)
    sys.exit(0)

def experiment(withVerde):
    if not os.path.isdir("local-expe"):
        print("Please run " + sys.argv[0] + " prepare before. " + sys.argv[0] + " help to see how to configure things.")
        sys.exit(-1)

    with open("local-expe/NB_NODES") as f:
        NB_NODES = int(f.read())

    with open("local-expe/PORT_START") as f:
        PORT_START = int(f.read())

    children = []
    for node in range(0, NB_NODES):
        node = str(node)
        children.append(
            subprocess.Popen(
                ["./LogCabin-stop"] + (
                    ["verde:" + node, bin_folder + "/verde-gdb"] if withVerde else ["bare:" + node]
                ) + [
                    "logcabin/build/LogCabin",
                    "--config",
                    "local-expe/logcabin-" + node + ".conf"
                ],
                stdin=None
            )
        )

    print("waiting?")
    if not withVerde:
        for child in children:
            child.wait()


def main():
    try:
        cmd = sys.argv[1]
    except IndexError:
        h()

    if cmd == "help":
        h()

    if cmd == "prepare":
        os.makedirs("local-expe", exist_ok=True)
        shutil.rmtree("storage", ignore_errors=True)

        PORT_START= int(os.environ["LOGCABIN_PORT_START"]) if "LOGCABIN_PORT_START" in os.environ else 5254
        NB_NODES  = int(os.environ["LOGCABIN_NB_NODES"]) if "LOGCABIN_NB_NODES" in os.environ else 3

        for node in range(0, NB_NODES):
            port = node + PORT_START
            with open("local-expe/logcabin-" + str(node) + ".conf", "w") as config_file:
                config_file.write(
                    "serverId = " + str(node) + "\n" +
                    "listenAddresses = 127.0.0.1:" + str(port) + "\n"
                )

        with open("local-expe/PORT_START", "w") as f:
            f.write(str(PORT_START))

        with open("local-expe/NB_NODES", "w") as f:
            f.write(str(NB_NODES))

        children = []
        allServers=""
        hosts = []
        for node in range(NB_NODES):
            cmd = ["logcabin/build/LogCabin", "--config", "local-expe/logcabin-" + str(node) + ".conf"]

            if node == 0:
                subprocess.run(cmd + ["--bootstrap"])
            else:
                allServers += ","

            host = "127.0.0.1:" + str(PORT_START + node)
            allServers += host
            hosts += [host]

            child = pexpect.spawnu(" ".join(cmd), encoding='utf-8')
            child.expect_exact("init()")
            children += [child]

        subprocess.run([
            "logcabin/build/Examples/Reconfigure",
            "--cluster=" + allServers,
            "set"
        ] + hosts)

        for child in children:
            child.terminate(force=True)

    elif cmd == "run-bare":
        experiment(False)

    elif cmd == "run-verde":
        os.execl(bin_folder + "/verde-bus", "verde-bus", "--quiet", "--script", sys.argv[0])
        #| grep -F "LogCabin Time"

    elif cmd == "clean":
        shutil.rmtree("local-expe", ignore_errors=True)
        shutil.rmtree("storage", ignore_errors=True)

    else:
        print("Unrecognized command.")
        h()

def trigger(bus, event, p=None):
    global properties, bin_folder, scn

    if event == "init":
        bus.setNbMonitors(len(properties))
        pass

    elif event == "listening":
        (_, port) = p.tcp

        os.environ["VERDE_BUS_PORT"] = str(port)
        os.environ["VERDE_BUS_UNIX_SOCKET"] = p.unix

        mon = bin_folder + "/verde-monitor"

        for prop in properties:
            os.spawnlp(os.P_NOWAIT, mon, mon, prop, "quiet")

        verdescn = bin_folder + "/verde-scenario"
        os.spawnlp(os.P_NOWAIT, verdescn, verdescn, scn)
        scnRunning = True
        experiment(True)

    elif event == "bye":
        bus.shutdown()
        pass


if __name__ == "__main__":
    main()
