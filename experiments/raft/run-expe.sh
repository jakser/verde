#!/usr/bin/env sh

export EXPE_LOGCABIN_HOST_1=194.199.25.236
export EXPE_LOGCABIN_HOST_2=194.199.25.209
export EXPE_LOGCABIN_HOST_3=194.199.25.192

if [ "$1" = "bootstrap" ]; then
	./run-distant.py bootstrap && touch bootstrapped
	exit
fi

if [ ! -e bootstrapped ]; then
	echo "ERROR: run this with parameter bootstrap the first time."
	exit 1
fi

if [ "$1" = "with-verde" ]; then
    EXPE_LOGCABIN_VERDE_DIR=/home/raph/prog/verde \
    VERDE_BUS_HOST=0.0.0.0 \
    VERDE_BUS_UNIX_SOCKET=/tmp/irv-bus \
    verde-bus --quiet --script run-distant.py | grep -F "LogCabin Time"
elif [ "$1" = "bare" ]; then
    ./run-distant.py bare | grep -F "LogCabin Time"
else
    cat <<EOF
Usage: run-expe.sh bootstrap
   or: run-expe.sh with-verde
   or: run-expe.sh bare
EOF
fi
