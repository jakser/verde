#!/usr/bin/env sh
# Usage: pipe output of LogCabin in this script

log="$(grep -E 'init()|printElectionState()')"
start="$(printf "%s" "$log" | grep -F 'init()'               | head -n 1 | cut -d' ' -f1,2)"
end="$(printf   "%s" "$log" | grep -F 'printElectionState()' | tail -n 1 | cut -d' ' -f1,2)"

python - <<EOF
import datetime, time
def parse_date(dt):
    d = datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S.%f")
    return time.mktime(d.timetuple()) + (d.microsecond / 1000000.0)

print(parse_date("${end}") - parse_date("${start}")) # seconds
EOF
