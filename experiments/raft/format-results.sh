#!/usr/bin/env sh

prepend() {
    while read line; do
        printf "%s\n" "$1$line";
    done;
}

get_running_field() {
    grep "LogCabin Time"       |
    grep -F "$1:"              |
    grep -o "running [0-9.]\+" |
    cut -d' ' -f2
}

average() {
    lines=$(get_running_field $1)
    nblines=$(echo "$lines" | wc -l)

    echo "($(echo "$lines" | paste -s -d"+"))/${nblines}" | bc -l
}

min() {
    get_running_field $1 | sort | head -1
}

max() {
    get_running_field $1 | sort | tail -1
}

make_csv() {
    echo "type;average;min;max"
    for t in bare verde; do
        printf "$t"
        for fun in average min max; do
            printf ";$(cat "$RESULT_FILE" | $fun $t)"
        done
        echo
    done
}

add_overead_line() {
    table=$(cat)
    printf "%s\n" $table;
    verde=$(printf "%s\n" "$table" | grep -F "verde;")
    bare=$(printf "%s\n" "$table" | grep -F "bare;")

    printf "overhead"
    for i in 2 3 4; do
        nverde="$(printf "%s" $verde | cut -d';' -f"$i")"
        nbare="$(printf "%s"  $bare  | cut -d';' -f"$i")"
        printf ";%s" "$(echo "($nverde-$nbare)/$nbare" | bc -l)"
    done
    echo
}

check_consistency() {
    nb=""
    for t in verde bare; do
        for i in $(seq 0 19); do
            tmp="$(cat "$RESULT_FILE" | grep -F "($t:$i)" | wc -l)"
            if [ "$nb" = "" ]; then
                nb=$tmp
            elif [ "$nb" != "$tmp" ]; then
                echo "ERROR:$t:$i = $tmp";
                exit
            fi
        done
    done
}

col() {
    printf "%s" "$2" | cut -d';' -f$1
}

round() {
    # https://askubuntu.com/questions/179898/how-to-round-decimals-using-bc-in-bash
    echo $(printf %.$1f $(echo "scale=$1;(((10^$1)*$(cat))+0.5)/(10^$1)" | bc))
}

to_human_numbers() {
    read line;
    printf "%s\n" $line;

    for i in 1 2; do
        read line
        printf "%s" "$(col 1 "$line")"

        for j in $(seq 2 4); do
            printf ";$(col "$j" "$line" | round 2)"
        done
        echo
    done
#
#     # overhead
    read line
    printf "%s" "$(col 1 "$line")"
    for i in $(seq 2 4); do
        printf "%s" ";$(echo "$(col "$i" "$line")*100" | bc -l | round 2)%"
    done
    echo
}

export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export EXCLUDE_INVARIANTS=true

RESULT_FILE="$1/res.log"

check_consistency

make_csv | add_overead_line | to_human_numbers
