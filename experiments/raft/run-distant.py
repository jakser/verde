#!/usr/bin/env python3
import subprocess
import pexpect
import sys
import os
import signal
import time
# dependencies for LogCabin:
# apt install build-essential wget libcrypto++-dev
# wget https://github.com/protocolbuffers/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.bz2
# tar xf protobuf-2.6.1.tar.bz2
# cd protobuf-2.6.1
# ./configure
# make -j4
# sudo make install
# sudo ldconfig

bin_folder = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + "/bin"
scn = bin_folder + "/../scenarios/stopexec.js"

default_ssh_port = "22"

logcabin_folder = os.path.dirname(os.path.realpath(__file__)) + "/logcabin/"
logcabin_conf = "logcabin-run-distant.conf"

VERDE_BUS_PORT = None

bootstrap = False

monitor_count = 0

def experiment(withVerde):
    global VERDE_BUS_PORT

    logcabin_hosts = []
    children = {}
    i = 1
    while True:
        if ("EXPE_LOGCABIN_HOST_" + str(i)) in os.environ:
            logcabin_host = os.environ["EXPE_LOGCABIN_HOST_" + str(i)] + ":5254"
            logcabin_hosts.append(logcabin_host)
        else:
            break
        i += 1

    i = -1

    for (idx, logcabin_host) in enumerate(logcabin_hosts):
        serverId = str(idx + 1)

        host = os.environ["EXPE_LOGCABIN_HOST_" + serverId]
        print("HOST: " + host)

        dest = (
            os.environ["EXPE_LOGCABIN_DEST_FOLDER_" + serverId]
                if ("EXPE_LOGCABIN_DEST_FOLDER_" + serverId) in os.environ
                else "/tmp/logcabin-run-distant"
        )

        ssh_port = (
            os.environ["EXPE_LOGCABIN_HOST_SSH_PORT_" + serverId]
                if ("EXPE_LOGCABIN_HOST_SSH_PORT_" + serverId) in os.environ
                else str(default_ssh_port)
        )

        if bootstrap:
            subprocess.run(["rsync", "-avzp", "--delete", "-e", "ssh -p " + ssh_port, logcabin_folder, host + ":" + dest + "/"])

            setup = subprocess.Popen([
                "ssh",
                    "-p", ssh_port,
                    host,
                    'killall LogCabin -s SIGKILL; cd "' + dest + '"; printf "%b\\n" "serverId = ' + serverId + '\\nlistenAddresses = ' + host + ':5254" > \'' + logcabin_conf + "';"
                    "cat > LogCabin-stop; chmod +x LogCabin-stop"
            ], stdin=subprocess.PIPE)

            setup.stdin.write(bytes("""\
#!/usr/bin/env python3
# encoding: utf-8

import pexpect
import time
import sys
import subprocess
from pexpect import fdpexpect

start = time.time()
with open("/tmp/logcabin-stdout", "wb") as out:
    command = subprocess.Popen(sys.argv[1:], stdout=out, stderr=subprocess.PIPE)
    # child = pexpect.spawnu(sys.argv[1], sys.argv[2:], encoding='utf-8', logfile=out)
    child = fdpexpect.fdspawn(command.stderr, logfile=out)
    child.expect_exact("init(")
    start2 = time.time()
    while True:
        child.expect_exact("printElectionState(")
        child.expect_exact("leader=")
        child.expect(".")
        if child.after != bytes("0", encoding="utf-8"):
            end=time.time()
            print("LogCabin Time: total", end - start, "sec, running", end - start2, "sec")
            time.sleep(1)
            command.kill()
            child.expect(pexpect.EOF)
            command.wait()
            break
""", encoding="utf-8"))
            setup.stdin.close()
            setup.wait()

            print("ok")

            if idx == 0:
                subprocess.run([
                    "ssh",
                        "-p", ssh_port,
                        host,
                        dest + "/build/LogCabin --config " +  dest + "/" + logcabin_conf + " --bootstrap"
                ])

            child = pexpect.spawnu("ssh", [
                "-p", ssh_port,
                host,
                dest + "/build/LogCabin --config " + dest + "/" + logcabin_conf
            ], encoding='utf-8')

            child.expect_exact("init()")
            print("OK INIT!")

        else:
            cmd = ""
            if withVerde:
                cmd = (
                    "export VERDE_BUS_PORT='" + str(VERDE_BUS_PORT) + "'; "
                    "export VERDE_BUS_HOST=$(echo $SSH_CLIENT | cut -d' ' -f1); "
                )

            cmd += "killall LogCabin 2> /dev/null > /dev/null;" + dest + "/LogCabin-stop "

            if withVerde:
                cmd += os.environ["EXPE_LOGCABIN_VERDE_DIR"] + "/bin/verde-gdb "

            cmd += dest + "/build/LogCabin --config " + dest + "/" + logcabin_conf

            #if withVerde:
            child = subprocess.Popen(["ssh", "-p", ssh_port, host, cmd])
            #else:
                #child = pexpect.spawnu("ssh", [ "-p", ssh_port, host, cmd ], encoding='utf-8')

        children[idx] = child

    if bootstrap:
        allServers = ",".join(logcabin_hosts)
        subprocess.run([
            logcabin_folder + "/build/Examples/Reconfigure",
            "--cluster=" + allServers,
            "set"
        ] + logcabin_hosts)

        for (idx, child) in children.items():
            child.terminate(force=True)
    #else:
        #for (idx, child) in children.items():
            #child.wait()

        #def exit_gracefully(arg1, arg2):
            #for (idx, child) in children.items():
                #print("Terminating", idx)
                #host = os.environ["EXPE_LOGCABIN_HOST_" + str(idx + 1)]
                #fixme horrible

                #child.sendcontrol('c')
                #child.close()
                #subprocess.run([
                    #"ssh",
                    #"-p", ssh_port,
                    #host,
                    #"killall LogCabin"
                #])

            #if not withVerde:
                #sys.exit()

        #if not withVerde:
            #signal.signal(signal.SIGINT, exit_gracefully)
            #signal.signal(signal.SIGTERM, exit_gracefully)

            #time.sleep(5)
            #exit_gracefully(0, 0)


def trigger(bus, event, p=None):
    global VERDE_BUS_PORT
    global monitor_count

    if event == "init":
        pass

    elif event == "listening":
        (_, port) = p.tcp
        VERDE_BUS_PORT = port

        os.environ["VERDE_BUS_PORT"] = str(port)
        os.environ["VERDE_BUS_UNIX_SOCKET"] = p.unix

        mon = bin_folder + "/verde-monitor"
        os.spawnlp(os.P_NOWAIT, mon, mon, "leader_in_vote.prop", "quiet")
        os.spawnlp(os.P_NOWAIT, mon, mon, "consensus_reached.prop", "quiet")
        os.spawnlp(os.P_NOWAIT, mon, mon, "allsameleader.prop", "quiet")
        os.spawnlp(os.P_NOWAIT, mon, mon, "unique_leader_better.prop", "quiet")

    elif event == "hello":
        if p.hello.serviceName == "monitor":
            monitor_count += 1

            if monitor_count == 3:
                verdescn = bin_folder + "/verde-scenario"
                os.spawnlp(os.P_NOWAIT, verdescn, verdescn, scn)
                scnRunning = True

        elif p.hello.serviceName == "scenario":
            experiment(True)
    elif event == "bye":
        bus.shutdown()
        pass

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "bootstrap":
        print("bootstrapping...")
        bootstrap = True
        experiment(False)
    elif len(sys.argv) > 1 and sys.argv[1] == "bare":
        experiment(False)
    else:
        print("""\
Usage: run-distant.py bootstrap
       run-distant.py bare
       verde-bus --script run-distant.py
Read the source for environment variables to declare""")

