#!/usr/bin/env python3

import os
import subprocess
import pexpect

cmd = os.argv[1]

if cmd == "prepare":
    PORT_START=os.environ["LOGCABIN_LOCAL_PORT_START"] if "LOGCABIN_LOCAL_PORT_START" in os.environ else 5254
    NB_NODES=os.environ["LOGCABIN_NB_NODES"] if "LOGCABIN_NB_NODES" in os.environ else 3


    for port in range(0, NB_NODES):
        port = node + PORT_START
        with open("local-expe/logcabin-" + str(node) + ".conf", "w") as config_file:
            config_file.write(
                "serverId = " + str(node) + "\n" +
                "listenAddresses = 127.0.0.1:" + port + "\n"
            )

    with open("local-expe/PORT_START") as f:
        f.write(PORT_START)

    with open("local-expe/NB_NODES") as f:
        f.write(NB_NODES)

    children = []
    allServers=""
    hosts = []
    for node in range(NB_NODES):
        cmd = ["build/LogCabin", "--config", "local-expe/logcabin-" + str(node) + ".conf"]

        if node == 0:
            subprocess.run(cmd + ["--bootstrap"])
        else:
            allServers += ","

        host = "127.0.0.1:" + str(PORT_START + node)
        allServers += host
        hosts += [host]

        child = pexpect.spawnu(cmd, encoding='utf-8')
        child.expect_exact("init()")
        print("OK INIT!")
        children += [child]

    subprocess.run([
        "LogCabin/build/Examples/Reconfigure",
        "--cluster=" + allServers,
        "set"
    ] + hosts)

    for child in children:
        child.terminate(force=True)

elif cmd == "run":
    with open("local-expe/NB_NODES") as f:
        NB_NODES = int(f.read())

    with open("local-expe/PORT_START") as f:
        PORT_START = int(f.read())
