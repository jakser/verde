initialization {
    knownPrgms = 0
    knownTerm  = 0

    followers  = {}
    leaders    = {}
}

state init {
    transition {
        event #join(#ecid as prgm)
        success {
            knownPrgms += 1
        }
    }

    transition {
        event #leave(#ecid as prgm)
        success {
            knownPrgms -= 1
        }
    }

    transition {
        event RaftConsensus.startNewElection(#ecid as prgm, currentTerm : int) {
            return currentTerm >= knownTerm
        }

        success {
            followers[currentTerm] = set()
            knownTerm = currentTerm
        } waiting_for_leader
    }
}


# A new election started
state waiting_for_leader import transitions from state init {
    transition {
        event RaftConsensus.becomeLeader(#ecid as prgm, currentTerm : int)
        success {
            l = leaders.get(currentTerm, None)
            if l and l != prgm:
                return "several_leaders"

            leaders[currentTerm] = prgm

            try:
                if prgm in followers[currentTerm]:
                    followers[currentTerm].remove(prgm)

                if len(followers[currentTerm]) == knownPrgms - 1:
                    return "leader_ok"
            except KeyError:
                return "leader_elected"

        } leader_elected
    }

    transition {
        event RaftConsensus.stepDown(#ecid as prgm, newTerm : int) {
            return newTerm >= knownTerm
        }

        success {
            knownTerm = newTerm
            try:
                followers[newTerm].add(prgm)
            except KeyError:
                followers[newTerm] = set([prgm])

            try:
                if leaders[newTerm] == prgm:
                    leaders[newTerm] = None

                if leaders[newTerm] and len(followers[newTerm]) == knownPrgms - 1:
                    return "leader_ok"
            except KeyError:
                pass
        }
    }

    transition {
        event RaftConsensus.updateLogMetadata(#ecid as prgm, currentTerm : int, state : int) {
            return currentTerm >= knownTerm
        }

        success {
            knownTerm = currentTerm

            if state == 0: # FOLLOWER
                try:
                    followers[currentTerm].add(prgm)
                except KeyError:
                    followers[currentTerm] = set([prgm])

                try:
                    if leaders[currentTerm] == prgm:
                        leaders[currentTerm] = None
                except KeyError:
                    pass

            elif state == 2: # LEADER
                l = leaders.get(currentTerm, None)
                if l and l != prgm:
                    return "several_leaders"

                leaders[currentTerm] = prgm

                try:
                    if prgm in followers[currentTerm]:
                        followers[currentTerm].remove(prgm)

                    if len(followers[currentTerm]) == knownPrgms - 1:
                        return "leader_ok"
                except KeyError:
                    return "leader_elected"
        }

    }

    transition {
        event RaftConsensus.startNewElection(#ecid as prgm)
        success waiting_for_leader
    }
}

state leader_elected import transitions from state waiting_for_leader
state leader_ok import transitions from state leader_elected
state several_leaders non-accepting
