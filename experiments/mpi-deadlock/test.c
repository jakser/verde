#define _DEFAULT_SOURCE
#include <mpi.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
static const int NONE = -1;

typedef enum {
    INVALID_REQUEST = 0,
    OK,
    REQUEST_FORK,
    RELEASE_FORK
} msg_t;



int main(int argc, char* argv[]){
    MPI_Init(&argc, &argv);

    // Get the number of processes
    int world_size = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Barrier(MPI_COMM_WORLD);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len = 0;
    MPI_Get_processor_name(processor_name, &name_len);

    if (world_rank > 0) {
        msg_t action = REQUEST_FORK;
        printf("SENDER: Sending from %d to %d\n", world_rank, 0);
        MPI_Send(&action, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    } else {
        MPI_Request requests[world_size-1];
        msg_t msg[world_size-1];

        for (int i = 1; i < world_size; i++) {
            requests[i-1] = MPI_REQUEST_NULL;
            MPI_Irecv(&(msg[i-1]), 1, MPI_INT, i, 0, MPI_COMM_WORLD, &(requests[i-1]));
            printf("RECEIVER: Listening to %d\n", i);
        }

        int index = NONE;
        MPI_Status status;

        for (int i = 1; i < world_size; i++) {
            printf("world_size: %d\n", world_size);
            MPI_Waitany(world_size - 1, requests, &index, &status);
            printf("RECEIVER: receiving message %d from %d\n", msg[index], status.MPI_SOURCE);
            requests[index] = MPI_REQUEST_NULL;
        }
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}
