initialization {
    nbNodes = 0
    rankByEcid = {}
    listenedNodeByNode = {}
    receivingFrom = {}
    events = []
    sizeof_MPI_Request = 0
    senderByRequests = {}
}

functions {
    import sys

    def getCycle(rankByEcid, listenedNodeByNode, r, color=None, cycle=None):
        if not color:
            cycle = []
            color = {}

        color[r] = 1

        if r not in listenedNodeByNode or not listenedNodeByNode[r]:
            return []

        newCycle = cycle + [r]

        for s in listenedNodeByNode[r]:
            if s == r:
                continue

            if s not in color:
                newerCycle = getCycle(rankByEcid, listenedNodeByNode, s, color, newCycle)
                if newerCycle:
                    return newerCycle

            elif color[s] == 1:
                return newCycle

        color[r] = 2
        return []

    def print_wait(r, s):
        print("Node", r, "waits for a message from node", s)

    def transition_success_block_receive(receivingFrom, listenedNodeByNode, rankByEcid, rank, events, sender, is_irecv, requestAddr=None):
        if (rank in receivingFrom) and (sender in receivingFrom[rank]):
            print_wait(rank, sender)
            print("which already sent a message.")
            receivingFrom[rank].remove(sender)
            return "init"
        else:
            print_wait(rank, sender)
            print("which has not sent a message yet.")

        print("receivingFrom:", receivingFrom)

        if is_irecv and (rank in listenedNodeByNode):
            listenedNodeByNode[rank].add(sender)
        else:
            listenedNodeByNode[rank] = set([sender])

        events += [(rank, sender)]
        cycle = getCycle(rankByEcid, listenedNodeByNode, rank)
        if cycle:
            print("Cycle detected: ", cycle)
            print("Whole graph: ", listenedNodeByNode)
            print("Course of actions:")
            course = []

            browsed = []
            for (r, s) in reversed(events):
                if r in cycle and r not in browsed:
                    course += [(r, s)]
                    browsed += [r]

                    if len(browsed) == len(cycle):
                        break

            for (r, s) in reversed(course):
                sys.stdout.write(" - ")
                print_wait(r, s)

            return "deadlock"

        return "init"
}

state init {
    transition {
        event #join
        success {
            nbNodes += 1
        } init
    }

    transition {
        event #leave
        success {
            nbNodes -= 1
        } init
    }

    transition {
        after event MPI_Comm_rank(#ecid as procSender, * arg 2 as rank, expr("sizeof(MPI_Request)") as requestSize)
        success {
            rankByEcid[procSender] = rank
            sizeof_MPI_Request = requestSize
        } init
    }

    transition {
        event MPI_Recv(#ecid as procReceiver, arg 4 as sender : int)
        success {
            return transition_success_block_receive(receivingFrom, listenedNodeByNode, rankByEcid, rankByEcid[procReceiver], events, sender, False)
        }
    }

    transition {
        event MPI_Irecv(#ecid as procReceiver, arg 4 as sender : int, arg 7 as requestAddr)
        success {
            rank = rankByEcid[procReceiver]
            senderByRequests[(rank, requestAddr)] = sender

            return transition_success_block_receive(receivingFrom, listenedNodeByNode, rankByEcid, rank, events, sender, True)
        }
    }

    transition {
        after event MPI_Waitany(#ecid as procReceiver, arg 2 as requests, * arg 3 as index)
        success {
            requestAddr = requests + sizeof_MPI_Request * index
            rank = rankByEcid[procReceiver]
            sender = senderByRequests[(rank, requestAddr)]
            del senderByRequests[(rank, requestAddr)]
            print("Node", rank, "does not wait for a message from", sender, "anymore")
            try:
                listenedNodeByNode[rank].remove(sender)
            except KeyError:
                pass
        }
    }

    transition {
        event MPI_Send(#ecid as procSender, arg 4 as receiver : int)
        success {
            rank = rankByEcid[procSender]
            print("Node", rank, " sends a message to node", receiver)

            try:
                listenedNodeByNode[receiver].remove(rank)
                print("This node was waiting for a message from that node")
            except (KeyError, ValueError):
                print("This node was not waiting for a message from that node")
                if receiver in receivingFrom:
                    receivingFrom[receiver].add(rank)
                else:
                    receivingFrom[receiver] = set([rank])
        } init
    }
}

state deadlock non-accepting {
    transition {
        event MPI_Send(#ecid as procSender, arg 4 as receiver : int) {
            print("Let's ignore this event")
            return None
        }

        success deadlock
    }


    transition {
        after event MPI_Comm_rank(#ecid as procSender, * arg 2 as rank) {
            print("Let's ignore this event")
            return None
        }

        success deadlock
    }
}
