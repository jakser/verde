#define _DEFAULT_SOURCE
#include <mpi.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

// Source: https://en.wikipedia.org/w/index.php?title=Dining_philosophers_problem&oldid=890834797

static const int NONE = -1;

typedef enum {
    THINKING_WAITING_LEFT_FORK = 1,
    THINKING_WAITING_RIGHT_FORK,
    EATING,
    PUTTING_RIGHT_FORK,
    PUTTING_LEFT_FORK
} state_t;

typedef enum {
    INVALID_REQUEST = 0,
    OK,
    REQUEST_FORK,
    RELEASE_FORK
} msg_t;

void action_fork(int action, int fork_id) {
    printf("Sending action %d to %d\n", action, fork_id);
    MPI_Send(&action, 1, MPI_INT, fork_id, 0, MPI_COMM_WORLD);
    printf("Sent action %d\n", action);
    printf("Waiting for reply\n");
    MPI_Recv(&action, 1, MPI_INT, fork_id, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("ok.\n");

    if (action != OK) {
        MPI_Finalize();
        exit(EXIT_FAILURE);
    }
}

void node_philosopher(int world_size, int world_rank) {
    state_t state = THINKING_WAITING_LEFT_FORK;
    int left_fork = world_rank - 1;
    int right_fork = (world_rank + 1) % world_size;

    while (true) {
        printf("State: %d\n", state);
        switch (state) {
            case THINKING_WAITING_LEFT_FORK:
                action_fork(REQUEST_FORK, left_fork);
                state = THINKING_WAITING_RIGHT_FORK;
                break;

            case THINKING_WAITING_RIGHT_FORK:
                action_fork(REQUEST_FORK, right_fork);
                state = EATING;
                break;

            case EATING:
                usleep(100000);
                state = PUTTING_RIGHT_FORK;
                break;

            case PUTTING_RIGHT_FORK:
                action_fork(RELEASE_FORK, right_fork);
                state = PUTTING_LEFT_FORK;
                break;

            case PUTTING_LEFT_FORK:
                action_fork(RELEASE_FORK, left_fork);
                state = THINKING_WAITING_LEFT_FORK;
                break;
        }
    }
}

void ok(int dest) {
    msg_t msg = OK;
    MPI_Send(&msg, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
}

void node_fork(int world_size, int world_rank) {
    MPI_Status status;
    msg_t msg[2];

    int philosopher_using_the_fork = NONE;
    int requesting_philosopher     = NONE;

    int left_philosopher  = world_rank > 0 ? world_rank - 1 : world_size - 1;

    int right_philosopher = world_rank + 1 < world_size ? world_rank + 1 : 0;

    MPI_Request requests[2] = {MPI_REQUEST_NULL, MPI_REQUEST_NULL};

    while (true) {
        if (requesting_philosopher != NONE && philosopher_using_the_fork == NONE) {
            philosopher_using_the_fork = requesting_philosopher;
            printf("sending ok to %d\n", philosopher_using_the_fork);
            ok(philosopher_using_the_fork);
            requesting_philosopher = NONE;
        }

        printf("Requesting: %d, philosopher_using_the_fork: %d\n", requesting_philosopher, philosopher_using_the_fork);

        if (left_philosopher != requesting_philosopher && requests[0] == MPI_REQUEST_NULL) {
            printf("Listening to %d\n", left_philosopher);
            MPI_Irecv(&(msg[0]), 1, MPI_INT, left_philosopher, 0, MPI_COMM_WORLD, &(requests[0]));
        }

        if (right_philosopher != requesting_philosopher && requests[1] == MPI_REQUEST_NULL) {
            printf("Listening to %d\n", right_philosopher);
            MPI_Irecv(&(msg[1]), 1, MPI_INT, right_philosopher, 0, MPI_COMM_WORLD, &(requests[1]));
        }


        int index = NONE;
        MPI_Waitany(2, requests, &index, &status);
        printf("receiving message %d from %d\n", msg[index], status.MPI_SOURCE);
        requests[index] = MPI_REQUEST_NULL;

        if (msg[index] == REQUEST_FORK) {
            requesting_philosopher = status.MPI_SOURCE;
        } else if (msg[index] == RELEASE_FORK && philosopher_using_the_fork == status.MPI_SOURCE) {
            printf("sending ok to %d\n", philosopher_using_the_fork);
            ok(philosopher_using_the_fork);
            philosopher_using_the_fork = NONE;
        }
    }
}

int main() {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len = 0;
    MPI_Get_processor_name(processor_name, &name_len);

    if (world_rank & 1) {
        printf("Node %d is a philosopher\n", world_rank);
        node_philosopher(world_size, world_rank);
    } else {
        printf("Node %d is a fork\n", world_rank);
        node_fork(world_size, world_rank);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}
