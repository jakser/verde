#include <mpi.h>
#include <stdio.h>
#include <stdbool.h>

#include <stdlib.h>

uint32_t arc4random_uniform(uint32_t);

int f() {
    return 42;
}

int random_rank(int world_size, int world_rank) {
    int res;

    do {
        res = arc4random_uniform(world_size);
        if (res == world_size) {
            // Not possible. This is intentional.
            return MPI_COMM_WORLD;
        }
    } while (res == world_rank);

    return res;
}

void communicate(int world_size, int world_rank) {
    int from = random_rank(world_size, world_rank);
    int to;
    do {
        to = random_rank(world_size, world_rank);
    } while (to != from);

    static int number = 0;
    number++;

    if (world_rank & 1) {
        puts("sending");
        MPI_Send(&number, 1, MPI_INT, to, 0, MPI_COMM_WORLD);
        puts("receiving");
        MPI_Recv(&number, 1, MPI_INT, from, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    } else {
        puts("receiving");
        MPI_Recv(&number, 1, MPI_INT, from, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        puts("sending");
        MPI_Send(&number, 1, MPI_INT, to, 0, MPI_COMM_WORLD);
    }
}

int main() {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank = 0;
    printf("&world_rank = %ls\n", &world_rank);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len = 0;
    MPI_Get_processor_name(processor_name, &name_len);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d out of %d processors. MPI_COMM_WORLD=%d\n",
           processor_name, world_rank, world_size, MPI_COMM_WORLD);

    while (true) {
        communicate(world_size, world_rank);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}
