function printVerdict(v) {
    java.lang.System.out.println(
        (v.initial ? " Initial" : "Non-initial")
            + " Verdict, states " + v.stateList
            + ", accepting: " + v.accepting
    );
}

function printVerdicts(verdicts) {
    if (verdicts) {
        java.lang.System.out.println("-- verdicts (" + verdicts.length + ") --");
        for (var i = 0; i < verdicts.length; i++) {
            java.lang.System.out.println("---- verdict " + i + " --");
            printVerdict(verdicts[i]);
        }
    } else {
        java.lang.System.out.println("No verdicts passed");
    }
}

// var checkpoints = [];

var preparingComputationCheckpoint;
var fixWasApplied;

scenario.on("initial verdict", () => {
    java.lang.System.out.println("INIT!");
    preparingComputationCheckpoint = null;
    fixWasApplied = false;
//     checkpoints = [];
})

scenario.on("state preparing_computation", () => {
    scenario.createCheckpoint(
        (checkpoint) => {
            preparingComputationCheckpoint = checkpoint;
            scenario.debuggerPrintln("SCENARIO: Checkpointing done.");

//             scenario.debuggerPrintln("SCENARIO: Checkpoint: " + checkpoints.length);
//             checkpoints.push(checkpoint);
        }
    )
});

scenario.on("any accepting verdict with non-initial state", (verdicts) => {
    java.lang.System.out.println("A verdict!");
    printVerdicts(verdicts);
});
/*
scenario.on("any non-accepting verdict with non-initial state", (verdicts) => {
}*/

scenario.on("any non-accepting verdict", (verdicts) => {
    java.lang.System.out.println("A non accepting verdict!");
    printVerdicts(verdicts);

    if (fixWasApplied) {
        scenario.debuggerPrintln("SCENARIO: The fix did not work.");
        scenario.suspendExecution();
        return;
    }

    scenario.debuggerPrintln("Non accepting verdict from the monitor ! Restoring checkpoint and fixing answer.");

    if (!preparingComputationCheckpoint) {
        scenario.debuggerPrintln("SCENARIO: The checkpoint does not exist.");
        scenario.suspendExecution();
        return;
    }

    scenario.restoreCheckpoint(preparingComputationCheckpoint, () => {
        java.lang.System.out.println("SCENARIO: Checkpoint restored!");
        scenario.debuggerPrintln("SCENARIO: Checkpoint restored!");

        scenario.addPointOnce("MethodEntry Main.computeAnswer()", () => {
            java.lang.System.out.println("SCENARIO: MethodEntry Main.computeAnswer() hit!");

//             scenario.debuggerCmd("where", (ans) => {
//                 scenario.debuggerPrintln(ans);
//                 scenario.debuggerCmd("list", (ans) => {
//                     scenario.debuggerPrintln(ans);
                    scenario.debuggerCmd("set a = 42", (ans) => {
                        fixWasApplied = true;
                        scenario.debuggerPrintln("SCENARIO: Debugger answered this to command set a = 42: \n---\n" + ans + "\n---\na should be set to 42");
                        java.lang.System.out.println("a should be set to 42");
//                         scenario.debuggerCmd("print a", () => {});
                    });
//                 });
//             });
        });
//                     scenario.resume();
    });
});
