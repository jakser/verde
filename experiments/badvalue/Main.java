public class Main {
    static void goodAnswer() {
        System.out.println("good answer!");
    }

    static void badAnswer() {
        System.out.println("bad answer!");
    }

    static boolean checkAnswer(int ans) {
        System.out.println("At the first line of checkAnswer, the answer is " + ans);

        if (ans == 42) {
            System.out.println("ok, the answer is correct");
            goodAnswer();
            return true;
        }

        System.out.println("NO, the answer is not correct, expected 42");
        badAnswer();
        return false;
    }

    static int computeAnswer(int a) {
        // A big computation takes place here
        return a;
    }

    static void prepareComputation() {
        // Nothing here.
    }

    public static void main(String[] args) {
        // suggested value for the argument: 41
        prepareComputation();
        int answer = computeAnswer(Integer.parseInt(args[0]));
        checkAnswer(answer);
    }
}
