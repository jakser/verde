scenario.addReaction(
    "any non-accepting verdict",
    () => {
        scenario.debuggerPrompt(
            "Hello! the program is wrong. Please input 'ok'\n",
            a => {
                scenario.debuggerPrintln("User answered: " + a);
                if (a === "ok") {
                    scenario.debuggerPrintln("Let's set a breakpoint");

                    var fpush = function (element) {
                        scenario.debuggerPrintln("push reached! This is being added: " + element);
                        scenario.removePoint(pointPop);

                        scenario.getValues(["question", "answer"], (question, answer) => {
                            scenario.debuggerPrintln("The question: " + question);
                            scenario.debuggerPrintln("The answer: " + answer);
                            scenario.debuggerCmd("set answer = " + (answer + 2), () => {
                                scenario.debuggerCmd("print answer", a => {
                                    scenario.debuggerPrintln("The debugger printed: " + a);
                                });
                            });
                        });
                    };

                    var fpop = function () {
                        scenario.debuggerPrintln("pop reached!");
                    };

                    scenario.addPoint(
                        "method MyList.push(#1)",
                        fpush
                    );

                    var pointPop = scenario.addPoint(
                        "method MyList.pop()",
                        fpop
                    );

                    scenario.suspendExecution(false);
                }
            }
        );
    }
);
