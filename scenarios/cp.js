checkpoints = [];

java.lang.System.out.println("Hello !");

scenario.addReaction("all state INIT", () => {
    java.lang.System.out.println("Program reinitialized");
    checkpoints = [];
});

scenario.addReaction("any accepting verdict", () => {
    scenario.createCheckpoint(checkpoint => {
        checkpoints.push(checkpoint);
    });
});

scenario.addReaction("any non-accepting verdict", () => {
    scenario.debuggerPrompt(
        "Hello! the program is wrong. Here are the checkpoints: " + JSON.stringify(checkpoints) + ". Which one do you want to restore?\n",
        cpidAsStr => {
            scenario.restoreCheckpoint(
                parseInt(cpidAsStr),
                id => scenario.debuggerPrintln("Checkpoint Restored")
            );
        }
    );
});
