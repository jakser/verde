VERDE_DIR?=$(realpath $(dir ${MAKEFILE_LIST}))
TARGET?=${VERDE_DIR}/target
ASPECTJ_HOME?=${TARGET}/dependencies/aspectj
AJC?=${ASPECTJ_HOME}/bin/ajc
TMP_DIR?=/tmp
JDB_TOOLS_JAR?="${JAVA_HOME}/lib/tools.jar"


.PHONY: all ok deps check_versions check_system_bin strip-jython

all: ok

# dependencies

deps: check_env \
	${AJC} \
	${TARGET}/dependencies/jython-standalone.jar \
	${TARGET}/dependencies/rhino.jar \
	${TARGET}/dependencies/protobuf-java.jar \
	check_system_bin

clean:
	rm -rf verde.source.sh verde.source.fish target
	cd artifact; make clean
	cd experiments; make clean
	cd extra/libdummyinotify; make clean
	cd extra/irv-spin; make clean

${AJC}:
	@echo "Downloading and installing AspectJ..."
	mkdir -p "${ASPECTJ_HOME}"

	@download_aj() { wget -c https://www.eclipse.org/downloads/download.php\?file\=/tools/aspectj/aspectj-1.8.13.jar\&r\=1 -O ${TMP_DIR}/aspectj.jar || exit 1; }; \
	if [ -e "${TMP_DIR}/aspectj.jar" ]; then \
		if [ "$$(sha256sum ${TMP_DIR}/aspectj.jar | cut -d' ' -f1)" = "be150d5a67fefc63b055e799e4bd394a2e9864a88a4f12a2bdbc1c7ec3782e83" ]; then \
			DONT_REMOVE_ASPECTJ=1; \
		else \
			download_aj; \
		fi; \
	else \
		download_aj; \
	fi ; \
	\
	if [ ! "$$(sha256sum ${TMP_DIR}/aspectj.jar | cut -d' ' -f1)" = "be150d5a67fefc63b055e799e4bd394a2e9864a88a4f12a2bdbc1c7ec3782e83" ]; then \
		echo "The downloaded AspectJ jar seems corrupted or in the wrong version."; \
		echo "Run make again, or update Dist-Verde's Makefile to reflect the right checksum."; \
		exit 1; \
	fi; \
	\
	java -jar ${TMP_DIR}/aspectj.jar -to ${ASPECTJ_HOME}; \
	rm -rf ${ASPECTJ_HOME}/doc ; \
	if [ -z "${DONT_REMOVE_ASPECTJ}" ]; then \
		rm -f ${TMP_DIR}/aspectj.jar ; \
	fi

# removing the AspectJ doc folder is important, otherwise sample aspects
# from the this folder may be used unexpectedly

${TARGET}/dependencies/jython-standalone.jar:
ifeq (,$(wildcard ${VERDE_DIR}/.jython-stripped))
	@echo "Downloading Jython..."
	mkdir -p "${TARGET}/dependencies"
	wget -c -O "$@" http://central.maven.org/maven2/org/python/jython-standalone/2.7.0/jython-standalone-2.7.0.jar
else
	@echo "Jython has been stripped from this copy of Dist-Verde"
endif

${TARGET}/dependencies/rhino.jar:
	@echo "Downloading Rhino..."
	mkdir -p "${TARGET}/dependencies"
	wget -c -O "$@" http://central.maven.org/maven2/org/mozilla/rhino/1.7.10/rhino-1.7.10.jar

${TARGET}/dependencies/protobuf-java.jar:
	@echo "Downloading Protocol Buffers for Java..."
	mkdir -p "${TARGET}/dependencies"
	wget -c -O "$@" http://central.maven.org/maven2/com/google/protobuf/protobuf-java/3.5.1/protobuf-java-3.5.1.jar

check_versions:
	@echo "Verifying checksums of downloaded dependencies..."

	@if [ -z "$$('${AJC}' -version | grep -F 'AspectJ Compiler 1.8.')" ]; then \
		echo "${AJC} -version: Expected AspectJ Compiler version 1.8. Got something else."; \
		echo "Please run: "; \
		echo "rm -rf '${ASPECTJ_HOME}'"; \
		echo "and run make again, or update Dist-Verde's Makefile."; \
		echo "Note that AspectJ breaks when moved, so if Dist-Verde's folder was moved to another place, this is expected."; \
		exit 1; \
	fi

	@if [ ! -f "${VERDE_DIR}/.jython-stripped" ] && [ ! "$$(sha256sum ${TARGET}/dependencies/jython-standalone.jar | cut -d' ' -f1)" = "dd0666914501cfa39739e84cf64574fd1c65037970a2f5ab3daa9e9da0af646a" ]; then \
		echo "${TARGET}/dependencies/jython-standalone.jar seems corrupted or in the wrong version."; \
		echo "Please remove it and run make again, or update Dist-Verde's Makefile to reflect the right checksum."; \
		exit 1; \
	fi

	@if [ ! "$$(sha256sum ${TARGET}/dependencies/rhino.jar | cut -d' ' -f1)" = "38eb3000cf56b8c7559ee558866a768eebcbf254174522d6404b7f078f75c2d4" ]; then \
		echo "${TARGET}/dependencies/rhino.jar seems corrupted or in the wrong version."; \
		echo "Please remove it and run make again, or update Dist-Verde's Makefile to reflect the right checksum."; \
		exit 1; \
	fi

	@if [ ! "$$(sha256sum ${TARGET}/dependencies/protobuf-java.jar | cut -d' ' -f1)" = "b5e2d91812d183c9f053ffeebcbcda034d4de6679521940a19064714966c2cd4" ]; then \
		echo "${TARGET}/dependencies/protobuf-java.jar seems corrupted or in the wrong version."; \
		echo "Please remove it and run make again, or update Dist-Verde's Makefile to reflect the right checksum."; \
		exit 1; \
	fi

verde.source.sh:
	@echo "export JAVA_HOME='${JAVA_HOME}'" > $@
	@echo "# https://unix.stackexchange.com/questions/4650/determining-path-to-sourced-shell-script" >> $@
	@echo 'if test -n "$$BASH" ; then script="$$BASH_SOURCE"' >> $@
	@echo 'elif test -n "$$TMOUT"; then script="$${.sh.file}"' >> $@
	@echo 'elif test -n "$$ZSH_NAME" ; then script="$${(%):-%x}"' >> $@
	@echo 'elif test $${0##*/} = dash; then script="$$(lsof -p $$$$ -Fn0 | tail -1 | sed "s/^[^n]*n//g")"' >> $@
	@echo 'else script="$$0"' >> $@
	@echo 'fi' >> $@
	@echo 'export VERDE_DIR="$$(realpath "$$(dirname "$$script")")"' >> $@
ifeq ($(realpath ${ASPECTJ_HOME}), $(realpath ${VERDE_DIR}/target/dependencies/aspectj))
	@echo 'export ASPECTJ_HOME="$${VERDE_DIR}/target/dependencies/aspectj"' >> $@
else
	@echo "export ASPECTJ_HOME='${ASPECTJ_HOME}'" >> $@
endif
	@echo 'export PATH=$${JAVA_HOME}/bin:$${VERDE_DIR}/bin:$${PATH}' >> $@

verde.source.fish:
	@echo "export JAVA_HOME='${JAVA_HOME}'" > $@
	@echo 'export VERDE_DIR="$$(realpath "$$(dirname "$$0")")"' >> $@
ifeq ($(realpath ${ASPECTJ_HOME}), $(realpath ${VERDE_DIR}/target/dependencies/aspectj))
	@echo 'export ASPECTJ_HOME="$${VERDE_DIR}/target/dependencies/aspectj"' >> $@
else
	@echo "export ASPECTJ_HOME='${ASPECTJ_HOME}'" >> $@
endif
	@echo 'set PATH $$JAVA_HOME/bin ${VERDE_DIR}/bin $$PATH' >> $@

ok: check_env check_system_bin deps check_versions verde.source.sh verde.source.fish
	@echo "Ensuring that everything that should be built is built..."
	cd src/python; make
	cd src/java/scenario; make
	cd src/java/jdb-client; make

	@echo

	@if [ ! -z "$$(which verde-bus)" ] && [ "$$(realpath "$$(which verde-bus)")" = "$$(realpath "${VERDE_DIR}/bin/verde-bus")" ] \
	 && [ "$$(${JAVA_HOME}/bin/java -version 2>&1 | head -1)" = "$$(java -version 2>&1 | head -1)" ] \
	 && [ "$$(${JAVA_HOME}/bin/javac -version 2>&1 | head -1)" = "$$(javac -version 2>&1 | head -1)" ] ; then \
		echo "    +-----------------------------------+"; \
		echo "    | Ok, Dist-Verde should be all set! |"; \
		echo "    +-----------------------------------+"; \
		echo ; \
		echo "In the future, if you want to use verde, run:"; \
	else \
		echo "Dist-Verde is almost ready. Each time you want to use verde, including now, run:"; \
	fi

	@echo "On most POSIX shells (bash, zsh, ...):"
	@echo
	@echo ". '${VERDE_DIR}/verde.source.sh' "
	@echo
	@echo "On fish:"
	@echo
	@echo "source '${VERDE_DIR}/verde.source.fish'"
	@echo
	@echo "If you don't want to have to do this each time, we suggest you put this command"
	@echo "in your .bashrc or the relevant resource file of your shell."
	@echo "Be aware that this puts Java 8 in your PATH."
	@echo
	@echo " ------------------- Advanced and optional notice ------------------"
	@echo "  When sourcing this file, \$$JAVA_HOME/bin is also added to your"
	@echo "  PATH. If you don't like this, feel free to edit verde.source.sh."
	@echo "  Be aware that this file may be overwritten by further calls to"
	@echo "  command make."
	@echo "  As an alternative, set JAVA_HOME and also put Dist-Verde's bin folder"
	@echo "  in PATH if you which to be able to call verde programs without"
	@echo "  putting the whole common path each time."
	@echo " -------------------------------------------------------------------"

check_env:
	@if [ ! "${VERDE_DIR}" = "$(realpath $(dir ${MAKEFILE_LIST}))" ]; then \
		echo "VERDE_DIR points to a different path than this directory."; \
		echo "Maybe the Dist-Verde directory has been moved?"; \
		echo "Please run:"; \
		echo "rm verde.source.sh verde.source.fish; unset VERDE_DIR"; \
		echo "and re-run make."; \
		echo "You may also want to fix paths in files in target/dependencies/aspectj/bin."; \
		exit 1; \
	fi

	@if [ -z "$$JAVA_HOME" ]; then \
		if [ -z "$$(which java)" ]; then \
			echo "Java 8 seems missing. To install it on Debian or Ubuntu, use:"; \
			echo ; \
			echo "sudo apt install openjdk-8-jdk --no-install-recommends"; \
			echo ; \
			echo "Note: on Ubuntu, this package is in universe. This must be enabled in /etc/apt/sources.list"; \
			echo "If it is already installed somewhere, set JAVA_HOME like that:"; \
			echo ; \
			echo "export JAVA_HOME=..."; \
		else \
			printf "*** Please set JAVA_HOME to point to a Java 8 installation, like this: ***\n"; \
			if [ -d "/usr/lib/jvm/java-8-openjdk-amd64" ]; then \
				echo "export JAVA_HOME='/usr/lib/jvm/java-8-openjdk-amd64'"; \
			elif [ -d "/usr/lib/jvm/java-8-openjdk-arm64" ]; then \
				echo "export JAVA_HOME='/usr/lib/jvm/java-8-openjdk-arm64'"; \
			elif [ -d "/usr/lib/jvm/java-1.8.0" ]; then \
				echo "export JAVA_HOME='/usr/lib/jvm/java-1.8.0'"; \
			elif [ -d "/usr/lib64/jvm/java-1.8.0-openjdk" ]; then \
				echo "export JAVA_HOME='/usr/lib64/jvm/java-1.8.0-openjdk'"; \
			else \
				echo "export JAVA_HOME=..."; \
			fi; \
		fi; \
		echo ; \
		exit 1; \
	fi

check_system_bin: check_env
	@if [ -z "$$(which gdb)" ]; then \
		echo "WARNING: gdb is missing. On Debian or Ubuntu, use:"; \
		echo "sudo apt install --no-install-recommends gdb"; \
	elif [ -z "$$(gdb -q -ex "pi import sys; print (sys.version)" -ex "quit" | head -1 | grep '^3\.')" ]; then \
		echo "WARNING: gdb is lacking support for Python 3. On Debian, gdb-minimal may be installed, but verde-gdb needs a full featured gdb. Use:"; \
		echo "sudo apt install --no-install-recommends gdb"; \
		echo ; \
		echo "You can check Python 3 support in gdb with:"; \
		echo "gdb -q -ex 'pi import sys; print (sys.version)' -ex 'quit'"; \
	fi

	@if [ -z "$$(which protoc)" ]; then \
		echo "ERROR: the Protocol Buffer Compiler was not found. On Debian or Ubuntu, use:"; \
		echo "sudo apt install --no-install-recommends protobuf-compiler"; \
		exit 1; \
	fi

	@if [ -z "$$(which python3)" ]; then \
		echo "ERROR: python3 was not found. On Debian or Ubuntu, use:"; \
		echo "sudo apt install --no-install-recommends python3"; \
		exit 1; \
	fi

	@if [ ! -z "$$(python3 -c "import google.protobuf" 2>&1)" ]; then \
		echo "ERROR: python3-protobuf was not found. On Debian or Ubuntu, use:"; \
		echo "sudo apt install python3-protobuf --no-install-recommends"; \
		exit 1; \
	fi

	@if [ -z "$$(which sha256sum)" ]; then \
		echo "ERROR: sha256sum was not found. On Debian or Ubuntu, use:" \
		echo "sudo apt install --no-install-recommends coreutils"; \
		exit 1; \
	fi

	@if [ -z "$$(which wget)" ]; then \
		echo "ERROR: wget is required to download dependencies and was not found. On Debian or Ubuntu, use:"; \
		echo "sudo apt install --no-install-recommends wget"; \
		exit 1; \
	fi

	@if [ -z "$$(which realpath)" ]; then \
		echo "ERROR: realpath was not found. On Debian or Ubuntu, use:"; \
		echo "sudo apt install --no-install-recommends coreutils"; \
		exit 1; \
	fi

	@if [ -z "$$(which cc)" ] &&  [ -z "$$(which gcc)" ] && [ -z "$$(which clang)" ]; then \
		echo "WARNING: no C compiler found. You might want to install one to compile C examples. On Ubuntu or Debian:"; \
		echo "sudo apt install gcc"; \
		echo "Or:"; \
		echo "sudo apt install clang"; \
	fi

	@if [ ! -x "${JAVA_HOME}/bin/java" ]; then \
		echo "ERROR: java was not found at ${JAVA_HOME}/bin. Please install Java 8 and set JAVA_HOME accordingly." \
		echo "On Debian and Ubuntu, use:"; \
		echo "sudo apt install openjdk-8-jdk --no-install-recommends"; \
		echo "Note: on Ubuntu, this package is in universe. This must be enabled in /etc/apt/sources.list"; \
		exit 1; \
	fi

	@if [ ! -x "${JAVA_HOME}/bin/javac" ]; then \
		echo "ERROR: javac was not found at ${JAVA_HOME}/bin. Please install Java 8 and set JAVA_HOME accordingly." \
		echo "On Debian and Ubuntu, use:"; \
		echo "sudo apt install openjdk-8-jdk --no-install-recommends"; \
		echo "Note: on Ubuntu, this package is in universe. This must be enabled in /etc/apt/sources.list"; \
		exit 1; \
	fi

	@if [ ! -e "${JDB_TOOLS_JAR}" ]; then \
		echo "ERROR: ${JDB_TOOLS_JAR} was not found. Please download tools.jar and set JDB_TOOLS_JAR accordingly."; \
		exit 1; \
	fi

# 	@if [ ! "$$(${JAVA_HOME}/bin/java -version 2>&1 | head -1)" = "$$(java -version 2>&1 | head -1)" ] ; then \
# 		echo "ERROR: java and ${JAVA_HOME}/bin/java are not equal."; \
# 		exit 1; \
# 	fi
#
# 	@if [ ! "$$(${JAVA_HOME}/bin/javac -version 2>&1 | head -1)" = "$$(javac -version 2>&1 | head -1)" ] ; then \
# 		echo "ERROR: javac and ${JAVA_HOME}/bin/javac are not equal."; \
# 		exit 1; \
# 	fi

	@if [ -z "$$(${JAVA_HOME}/bin/java -version 2>&1 | head -1 | grep -F '1.8.')" ] ; then \
		echo 'WARNING: Unless you know what you are doing, ${JAVA_HOME}/bin/java -version should read:'; \
		echo 'openjdk version "1.8.something"'; \
		echo 'If not, this may mean that JAVA_HOME does not point to the right version of Java.'; \
		echo 'Dist-Verde requires Java 8.'; \
		exit 1; \
	fi

	@if [ -z "$$(${JAVA_HOME}/bin/javac -version 2>&1 | head -1 | grep -F '1.8.')" ] ; then \
		echo 'WARNING: Unless you know what you are doing, ${JAVA_HOME}/bin/javac -version should read:'; \
		echo 'openjdk version "1.8.something"'; \
		echo 'If not, this may mean that JAVA_HOME does not point to the right version of Java.'; \
		echo 'Dist-Verde requires Java 8.'; \
		exit 1; \
	fi

strip-jython:
	@echo 'package verde;' > src/java/common/scripting/PythonInterpreter.java
	@grep -F import src/java/common/scripting/ScriptInterpreter.java >> src/java/common/scripting/PythonInterpreter.java
	@echo 'public class PythonInterpreter extends ScriptInterpreter { PythonInterpreter() { throw new Error("Python support has been stripped."); }'  >> src/java/common/scripting/PythonInterpreter.java
	@grep -F abstract src/java/common/scripting/ScriptInterpreter.java | grep -F '(' | sed 's/abstract //' | grep -F "public Object"  | sed 's/;/ {return null;}/'  >> src/java/common/scripting/PythonInterpreter.java
	@grep -F abstract src/java/common/scripting/ScriptInterpreter.java | grep -F '(' | sed 's/abstract //' | grep -F "public boolean" | sed 's/;/ {return false;}/' >> src/java/common/scripting/PythonInterpreter.java
	@grep -F abstract src/java/common/scripting/ScriptInterpreter.java | grep -F '(' | sed 's/abstract //' | grep -F "public int"     | sed 's/;/ {return 0;}/'     >> src/java/common/scripting/PythonInterpreter.java
	@grep -F abstract src/java/common/scripting/ScriptInterpreter.java | grep -F '(' | sed 's/abstract //' | grep -F "public void"    | sed 's/;/ {}/'              >> src/java/common/scripting/PythonInterpreter.java
	@echo '}' >> src/java/common/scripting/PythonInterpreter.java
	touch "${TARGET}/dependencies/protobuf-java.jar"
	touch .jython-stripped

../verde.tar.gz:
	rm -rf /tmp/verde
	cp -r "${VERDE_DIR}" /tmp/verde
	cd /tmp/verde && make clean && rm -rf .git
	cd /tmp/ && tar -czf "${VERDE_DIR}/../verde.tar.gz" verde
	rm -rf /tmp/verde
