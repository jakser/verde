# Simple-Verde

Simple-Verde is a python extension for GDB that defines some commands.
Program `bin/simple-verde` loads and use these commands to provide an
easy-to-use user interface for basic usage:

    simple-verde --show-graph property prgm [args...]

where:

- `--show-graph` is an optional argument to display a graphical visualisation of
the property
- `property` is a property file writen for Verde's Monitor
- `prgm` is the path to a program preferably compiled with debug symbols
- `args` is the program arguments.

Simple-Verde provides more flexibility than that. To manually use Simple-Verde,
run:

    gdb prgm [args...]

Load Simple-Verde:

    source verde/src/python/scripts/simple-verde-gdb.py

Ensure GDB will display many lines correctly:

    set height 0

Load a property:

    verde load-property sample.prop \

Maybe load a scenario:

    verde load-scenario sample.scn'" \

Run the program:

    verde run-with-program

Ask for help:

    help verde
