# Dist-Verde

Dist-Verde is composed of a buch of components.

## Environement Setup

First, do not forget to source Verde's environement setup script each time dist-verde is used.

- On most POSIX-compatible shells:

    . verde/verde.source.sh

- On Fish:

    source verde/verde.source.fish

You can put this line in your `.bashrc`, `.zshrc` or other shell's resource file
is you want Verde to be ready in any command line session.
Be aware that this puts Java 8 your PATH though.

## Basic Usage

Program `bin/dist-verde` provides an easy-to-use command line interface to
loads these components. It is suitable for basic usage.

    dist-verde [args...]

These parameters are available:

- `--prop [file]`: This file will be used as a property in a Verde Monitor.

- `--scn [file.py|file.js]`: This JavaScript or Python file will be used as a
  scenario. Rhino is used for Javascript scenarios and Jython is used for Python
  scenarios.

- `--prgm prgm-name args...`: This program will be run in a i-RV compatible
  debugger. Arguments must be provided to tell dist-verde how to load the program:
  - `-dbg [jdb|gdb|checkpointed-jdb]` (needed): This debugger (jdb or gdb) will
    be used. checkpointed-jdb runs jdb inside a wrapper that takes care of
    adding support for checkpoints using CRIU for applications that do not
    access the network nor files.
  - `-wd working-directory` (optional): This will be the program's working directory.
  - `-arg ARG` (optional): An argument for the program. This parameter can be
    repeated to give more than one argument.

All properties are loaded. When all corresponding monitors are ready, all
programs are loaded. When all programs are ready, the scenario is loaded.
If several properties are given, corresponding monitors are launched in
separate terminal windows.
If several programs are given, they are launched in separate terminal windows.
Running multiple monitors or programs using dist-verde in a real Linux
non-graphical terminal is not supported for the moment.

`dist-verde` will try to use the default terminal application on your system but
will probably fail. You can set `VERDE_TERMINAL` to the application of your
choice.

This application must accept a parameter `-e` with a command to run.
On Ubuntu and Debian, you can also run:

    sudo update-alternatives --config x-terminal-emulator

### Example

#### Sort

With a Java program; type `cont` in the jdb (`main[1]`) prompt (Note that this
example exhibits a bug in the instrumentation used in the jdb client):

    cd "${VERDE_DIR}/examples/sort"; javac -g QSort.java
    dist-verde --prop "${VERDE_DIR}/examples/sort/qsort-java.prop" --scn "${VERDE_DIR}/scenarios/stopexec.py" --prgm QSort -dbg jdb -wd "${VERDE_DIR}/examples/sort/" -arg 100

With a C program:

    cd "${VERDE_DIR}/examples/sort"; cc -g -lgomp qsort.c
    dist-verde --prop "${VERDE_DIR}/examples/sort/qsort-c.prop" --scn "${VERDE_DIR}/scenarios/stopexec.py" --prgm "${VERDE_DIR}/examples/sort/qsort" -dbg gdb -arg 100

#### Producer, Consumer

    cd examples/prod_cons
    make

With Dist-Verde:

    dist-verde --prop queue-size.prop --prgm ./prod_cons -dbg gdb

With Simple-Verde:

    simple-verde queue-size.prop queue-size.scn ./prod_cons


## Advanced usage

Program `bin/dist-verde` is actually a very thin wrapper which runs the bus with
a bus script. Bus scripts are presented in the next section.
This bus script can be found at `src/python/dist-verde-bus-script.py`.

### Verde-Bus and Bus Scripts

The bus is ran using `bin/verde-bus`, which is essentially an alias for
`python3 ${VERDE_DIR}/src/python/irvbus.py`.

Usage: `verde-bus [--quiet] [--script file.py [script args...]]`

The bus listens to sockets, handles joining components and forwards messages.
Verde-Bus is scriptable using bus scripts.
Bus scripts are python scripts that define a function `trigger(bus, eventname, p)`
where:
- `bus` provides functions to control the bus. Currently, one method is define:
  `bus.shutdown`, which orders the bus to say bye to every components, close
  communication channels and stop.
- `eventname` is the name of an event that happened in the bus.
- `p` is the event parameter.

When a script is given to the bus, the bus loads it calls its function `trigger`
at various time:
- At the begining, with `eventname = 'init'` and `p`, the list of arguments
  given to the bus for the bus script. This event is used to set things up in
  the script.
- When the bus is ready, with `eventname = 'listening'` and `p` is an object
  with two attributes:
    - `tcp`, which value is a tuple (BUS_HOST, BUS_PORT) used to listen
      connections using TCP
    - `unix`, which value is the path of the UNIX socket used
- When a component joins the bus, with `eventname = 'hello'` and p the
  component's hello message as defined in `src/proto/`.
- When a component leaves the bus, with `eventname = 'bye'` and p the
  component's sender id.

Bus scripts are intented to set up and manage i-RV sessions.
If the default `dist-verde` tool is to limited for your use case, you can use
them for more flexibility. This includes running programs and properties at a
different time or on different computers, with different runtimes or monitors.
See how bus scripts are used by reading `bin/dist-verde` and how they are
written by looking at `src/python/dist-verde-bus-script.py`.

See [PROTOCOL.md](PROTOCOL.md) for a description of the protocol used in Verde.
This is useful for implementing or debugging components.
