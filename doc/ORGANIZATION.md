# Components and Folder Organization

Dist-Verde is spread accross multiple folders and multiple components.

## Components

Dist-Verde is composed of:

- A bus
- A monitor
- A scenario
- An execution controller based on GDB for C/C++ programs
- An execution controller based on JDB for Java 8 programs

## Folders

Here is a non-exhaustive summary of the folder tree.

- `src`: Contains Verde's sources.
    - `src/proto` contains the definition of the messages of the protocol used in
Dist-Verde between components
    - `src/python` contains the sources for:
        - The bus
        - The monitor
        - Simple-Verde
- `bin`: Contains helper programs to run Dist-Verde and Simple-Verde.
These programs provide a simple user interface for Verde.
They offer usage simplicity over flexibility. For more advanced usage,
please see how they are written to achieve what you whish to achieve. Also do
not hesitate to contact us for more questions.

