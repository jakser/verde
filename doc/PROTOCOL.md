# The Protocol Used in Dist-Verde

Understanding the protocol is useful for debugging Verde and for building a
new component to support another programming langage or run another monitor.

## Basic Principles

### Socket and Byte Level

The protocol is based on protocol buffers. The bus opens a socket and waits for
component to join.
On the byte level, "packets" are composed of a length and the serialization of
a irv.Root protobuf message of this length. The length is represented by a 4
byte big endian unsigned integer.

A message is read like that:

    msglenbytes = sock.recv(4, socket.MSG_WAITALL)
    msglen = int.from_bytes(msglenbytes, byteorder='big')
    msgbytes = sock.recv(msglen, socket.MSG_WAITALL)
    msg = irv.Root()
    msg.ParseFromString(msgbytes)

A message is sent like that:

    msgstr = msg.SerializeToString() # msg is of type irv.Root
    sock.sendall(
        len(msgstr).to_bytes(4, byteorder='big') +
        msgstr
    )

It is strongly advised to use TCP_NODELAY when using TCP sockets. Otherwise,
the component will spend its time waiting for nothing.

### Message Level

A `irv.Root` message `Root {sender = s, recipients = r, content = c}` is used
to send message to the bus and to components.

- `sender` is the sender id of the component sending the message.
- `recipients` is the list of senders id of the components receiving the message.
- `c` is the message content.

Sender ids are assigned when joining the bus.
The bus has the special sender id 0.

#### Hello

When a component C connects to the bus, the bus waits for a hello message from C.
Component C sends `Root {sender = 0, recipients = 0, content = hello}` where hello
is an hello message with relevant the fields filled. See the protobuf
specification and implementations in Python or Java that are shipped with Verde.
`hello.serviceName` must be `"monitor"` for monitors, `"scenario"` for
scenarios, `"executionController"` for execution controller and `"bus"` for
buses.

The bus answers `Root {sender = 0, recipients = ID, content = helloBus}`
where `ID` is a unique identifier assigned to the component by the bus and
`helloBus` a message that identifies the bus. Component C now has its identifier
in the system.

The bus then sends `Root {sender = ID, recipients = r, content = hello}` to any
component `r` that joined the system and maps this message to component C.
The bus also sends every hello messages mapped to every connected component to
component C. Then, every component know about C and C knows about every connected
component.



#### Bye

Kind components send `b = Root {sender = ID, recipients = 0, content = Bye {}}`
to the bus when they leave. The bus answers by
`Root {sender = 0, recipients = ID, content = Bye {}}`
and closes the connection. It then forwards `b` to every other components.

If a component leaves and the bus notices, `Root {sender = ID, recipients = 0, content = Bye {}}`
is sent to every other components.

Kind buses send `Root {sender = 0, recipients = 0, content = Bye {}}` to every
connected component before leaving.

Leaving without telling works but this is not nice, therefore not recommended.
Not leaving in good terms may look like the component leaving crashed.

#### Inter-Component Communication

Component A sends a message to component B by sending `Root {sender = idA, recipients = idB, content = c}`, where:

- `idA` is the sender id of component A, as given in field `recipients` of the
first hello message from the bus
- `idB` is the sender id of component B, as given in its hello message sent by
the bus to other components
- `c` is the message to be sent.

TODO: complete this document.
