#!/usr/bin/env sh

csvtable2tex() {
    sed -e '1s/^/\\\heading{/g'           | # add \heading
    sed -e '1s/$/}/g'                     | # add \heading
    sed -e '1s/;/};\\\heading{/g'         | # add \heading
    sed -e '1s/_/\\\_/g'                  | # _ → \_
    sed -e 's/$/ \\\\/g'                  | # add \\
    sed 's/;/ \& /g'                      | # ; → &
    sed '1 a\\\hline'                     | # \hline after the first line
    sed '1 i\\\hline'                     | # \hline before the first line
    sed '$ a \\\hline'                    | # \hline at the end
    cat
}

e2ten() {
    sed 's/e+/*10^/g'
}

round() {
    # https://askubuntu.com/questions/179898/how-to-round-decimals-using-bc-in-bash
    echo $(printf %.$1f $(echo "scale=$1;(((10^$1)*$(cat))+0.5)/(10^$1)" |  e2ten | bc))
}

format_number() {
    if [ "${FORMAT_NUMBER_AS_TEX}" = "true" ]; then
        format='$%s$'
    else
        format='%s'
    fi
    printf "$format" "$(cat | e2ten | bc | sed 's/\.0\+$//g' | numfmt --grouping)"
}

format_time() {
    if [ "${FORMAT_NUMBER_AS_TEX}" = "true" ]; then
        format='$%s$'
    else
        format='%s'
    fi

    seconds=$(e2ten | bc -l)

    if [ "$(echo $seconds | round 0)" -gt 3600 ]; then
        echo "$(printf "$format" "$(echo "$seconds/3600" | bc -l | round 2)")h"
    elif [ "$(echo $seconds | round 0)" -gt 59 ]; then
        echo "$(printf "$format" "$(echo "$seconds/60" | bc -l | round 2)")min"
    else
        echo "$(printf "$format" "$(echo $seconds | round 2)")s"
    fi
}


format() {
    if [ "$EXCLUDE_INVARIANTS" = "" ]; then
        propprint=";\1"
        propcol=";props"
        echo 'necs;nmonitors;nrequests;props;time;nbstates;nbstatesvisited;transitions;depth;memory;errors'
    else
        propprint=""
        propcol=""
    fi

    echo "necs;nmonitors;nrequests${propcol};time;nbstates;nbstatesvisited;transitions;depth;memory;errors"

    for file in spin_-f-a-b-N*NECS*_NMONITORS_*_NBREQUESTS_*.log; do
        if [ "$EXCLUDE_INVARIANTS" = "" ] || echo $file | grep -v -q invariant; then
            data=$(printf "$file" | sed -E 's/^spin_-f-a-b-N[_-]+([^_-]*)[_-]+NECS_(.*)_NMONITORS_(.*)_NBREQUESTS_(.*)\.log$/\2;\3;\4'$propprint'/g')
            states="$(cat $file | grep 'states, stored' | sed -E 's/^[^0-9.e+]+([0-9.e+]+)[^0-9]+([0-9.e+]+)[^0-9]+$/\1/g' | format_number)"
            statesvisited="$(cat $file | grep 'states, stored' | sed -E 's/^[^0-9.e+]+([0-9.e+]+)[^0-9]+([0-9.e+]+)[^0-9]+$/\2/g' | format_number)"
            transitions="$(cat $file | grep 'transition' | xargs | cut -d' ' -f1 | format_number)"
            time="$(cat "$file" | grep "elapsed time" | grep -E -o '[0-9][^ ]*' | format_time)"
            errors="$(cat "$file" | grep -o "errors: [0-9]\+" | cut -d' ' -f 2 | format_number)"
            depth="$(cat "$file" | grep -o "depth reached [0-9]\+" | cut -d' ' -f 3 | format_number)"
            memory="$(cat "$file" | grep "total actual memory usage" | xargs | cut -d' ' -f 1 | round 0 | format_number)"
            echo "$data;$time;$states;$statesvisited;$transitions;$depth;$memory;$errors"
        fi
    done | sort
}

maybecsvtable2tex() {
    if [ "$DISABLE_LATEX" = "" ]; then
        csvtable2tex
    else
        cat
    fi
}

export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export EXCLUDE_INVARIANTS=true

if [ "$DISABLE_LATEX" = "" ]; then
    export FORMAT_NUMBER_AS_TEX="true"
fi

cd "$1"

format | maybecsvtable2tex
