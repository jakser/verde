divert(`1')

ifelse(`
 This m4 library provides the following macros:
 - forall and exists, quantifiers that are not provided by spin.
 - leadsTo, which is simply defined as leadsTo(A, B) = A -> <> B.
 - implies, different from operator implies "->" provided by spin
   because the implication is computed by the preprocessor and not
   at runtime. This allows smaller formulas.
 - defineLTL to define an LTL property with a name that can be reused in
   another property
 - defineAlwaysLTL, same as defineLTL, but the property is prepended with an
   always "[]" operator.

Thanks https://www.gnu.org/software/m4/manual/m4.html#Forloop for the for loop')

define(`forall', `pushdef(`$1', `$2')_forall($@)popdef(`$1')')
define(`_forall', ($4)`ifelse($1, `$3', `', `define(`$1', incr($1)) && $0($@)')')

define(`exists', `pushdef(`$1', `$2')_exists($@)popdef(`$1')')
define(`_exists', ($4)`ifelse($1, `$3', `', `define(`$1', incr($1)) || $0($@)')')

define(`leadsTo', (($1) -> <> ($2)))

define(`apply_noteq_implies', `define(`implies', `ifelse'(`eval'(`$'1), 1, `$'2,`'))dnl
patsubst(patsubst(patsubst(patsubst($1, `(([
 ]+)) &&'), `&& (([
 ]+))'), `([
 ]+) &&'), `&& ([
 ]+)')dnl
undefine(`implies')')

define(`defineLTL', `define(`$1', `apply_noteq_implies($2)') ltl prop_$1 {
    $1
}')

define(`defineAlwaysLTL',
`define(`$1', `$2')
ltl prop_$1 { [] ($1) }')

divert(`0')dnl
