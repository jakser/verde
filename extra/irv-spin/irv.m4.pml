// We model the i-RV protocol.
//
// In this protocol, a scenario, a set of monitors and a set of execution
// controllers communicate through a bus.
// In this model, each component is a process.

// The model can be configured in file constants.m4, included here.
include(`constants.m4')

// We don't use the regular C preprocessor that SPIN uses by default.
// We use m4, which lets us define recursive macros.
// M4's default starting comment string is "#". We change that to //.
changecom(`//')

// Component exchange messages through the bus. These messages have the
// following types.
mtype:type = {

    // A component advertises itself on the bus. This message is broadcast to
    // any component on / joining the bus.
    Hello,

    // A component leaves the system.
    Bye,

    // This message is sent by the bus to the scenario so the scenario knows the
    // number of monitors in the system.
    NbMonitors,

    // This message is sent by the execution controller to the monitors and the
    // scenario when an event in the program happens.
    EventNotification,

    // This message is sent by the execution controller to the scenario.
    // It carries the number of monitors receiving an EventNotification for
    // the last event.
    ExpectedVerdictNumber,

    // This message is sent instead of ExpectedVerdictNumber for the final event
    // generated in the program (which is the program finishing).
    FinalVerdictNumber,

    // This message is sent by the monitor to the scenario when receiving an
    // EventNotification from the execution controller.
    // This is the number of verdicts that the scenario should receive for an
    // event.
    Verdict,

    // This message is sent by the monitors and the scenario to an execution
    // controller to receive an event.
    Request,

    // This message is a request from the scenario to an execution controller to
    // leave.
    QuitRequest,

    // This message is a reply to a request.
    Ack,

    // This message is a request from the scenario to an execution controller to
    // resume its execution (suspended by the generation of an event).
    ResumeRequest
}

// The type of identifiers of components.
define(`ProcIdentifier', `byte')

// This is an enumeration. Though it is not written as one because it does not
// work…
define(`ComponentKind', byte)
define(`CNone',                0)
define(`CMonitor',             1)
define(`CScenario',            2)
define(`CExecutionController', 3)

// Let's have proper keyword for booleans
define(`true',  1)
define(`false', 0)

// In the model, we will need a "random generator".
// When model checking, each possibility is actually tested.
// The macro takes the variable to assign $1, and the max $2, and
// sets the given variable $1 to a value in the range [0, N].
// The way to generate a random number is documented at
// https://web.archive.org/web/20180425155337/http://spinroot.com/spin/Man/rand.html
define(`random', `
    $1 = 0;
    do
    :: $1 < $2 -> $1++
    :: break
    od
')

// This macro checks whether the identifier $1 given in argument is the
// identifier of an execution controller.
define(`isExecutionController',
    ifelse(NECs, `1', `($1 == FIRST_EC)', `($1 >= FIRST_EC && $1 <= LAST_EC)')
)

// This macro checks whether the identifier $1 given in argument is the
// identifier of a monitor.
define(`isMonitor',
    ifelse(NMONITORS, `1', `($1 == FIRST_MONITOR)', `($1 >= FIRST_MONITOR && $1 <= LAST_MONITOR)')
)

// This macro checks whether the identifier $1 given in argument is the
// identifier of the scenario.
define(`isScenario',            `($1 == SCENARIO)')

// This macro checks whether the identifier $1 given in argument is the
// identifier of the scenario or one of the monitors.
define(`isEventReceiver',       `(isScenario($1) || isMonitor($1))')


// In an array containing a value for each execution controller, this macro
// gives the index of the given execution controller's identifier in this array
define(`indexOfEC', `($1 - FIRST_EC)')

// Structure of messages
typedef Message {
    ProcIdentifier sender
    ProcIdentifier recipient
    mtype:type type
    byte custom // for Hello, Verdict and NbMonitors messages
}

// When sending an Hello message, a component sets the custom field to the kind of
// component (see ComponentKind)
define(`senderKind', `$1.custom')

// When sending a Verdict message, a monitor sets the custom field to identifier
// of the execution controller that sent the corresponding EventNotification
define(`msgEC',      `$1.custom')

// When sending the NbMonitors message to the scenario,
// the bus sets the custom field to the number of expected monitors.
define(`msgNumber',  `$1.custom')


// Messages are sent to components using queues, called "channels" in Promela
// We use an array of channels where the receiving queue of a component is
// indexed by its identifier.
chan message_queues[NB_COMPONENTS + 1] = [MSG_QUEUE_SIZE] of { Message }

// curMsg[id] is the last message retreived by component id.
// This is a gloal variable so it can be referred to by properties when checking
// the model.
Message curMsg[MAX_COMPONENT_ID+1];

// Getting a message is setting curMsg[id] to the head of the relevel queue.
inline GetMessage() {
    message_queues[self] ? curMsg[self]
}

// Copies a message from b to a.
inline setMsg(a, b) {
    atomic {
        a.sender    = b.sender
        a.recipient = b.recipient
        a.type      = b.type
        a.custom    = b.custom
    }
}

// Sends a message to the given component.
// Only used directly by the bus.
inline sendMessage(to, msgtype, msgsender, msgrcpt, msgcustom) {
    message_queues[to] ! msgsender, msgrcpt, msgtype, msgcustom
}

// Sends a message of the given type to the given recipient with the given
// custom field (0 if irrelevant).
// In Promela, inline function act like macro; self is the identifier of the
// calling component.
inline send(msgtype, msgrcpt, msgcustom) {
    sendMessage(BUS, msgtype, self, msgrcpt, msgcustom)
}

// Send an Hello message of the given kind.
inline SayHello(kind) {
    sendMessage(BUS, Hello, self, BUS, kind)
}

// Send a Bye message
inline SayBye() {
    sendMessage(BUS, Bye, self, BUS, 0)
}

// If verification of properties using temporal logic on message
// is enabled, we will need a boolean saying whether a new message
// has just been received.
ifelse(HANDLE_VERIF_MESSAGE, true, bool newmsg = false)


////////////////////////////////////////////////////////////////////////////////
//                          The bus process                                   //
////////////////////////////////////////////////////////////////////////////////

// This function retrieves the new message in the bus.
// If verification of temporal properties is enabled, this
// macro handles the boolean newmsg.
define(awaitNewBusMessage,
    ifelse(HANDLE_VERIF_MESSAGE, true,
        `atomic { newmsg = true; GetMessage(); }
        newmsg = false;',
        `GetMessage()'
    )
)

// Bus process: main procedure
active proctype Bus() {
    ProcIdentifier self = BUS

    // A counter for loops
    byte i = 0

    // This array is used for storing hello messages from every components.
    // These stored hello messages are broadcast to any joining component.
    Message knownComponents[NB_COMPONENTS]
    byte nKnownComponents = 0

    // Main loop
    do
    :: awaitNewBusMessage()
        if

        // Case: the current message is an Hello message
        ::  curMsg[BUS].type == Hello ->

            if
            // If the joining component is the scenario, we send
            // the number of expected monitors
            :: senderKind(curMsg[BUS]) == CScenario ->
                sendMessage(curMsg[BUS].sender, NbMonitors, BUS, curMsg[BUS].sender, NMONITORS)

            // Otherwise, do nothing.
            // In Promela, any if statement without an "executable" case will
            // block the execution, hence this case that does nothing.
            :: senderKind(curMsg[BUS]) != CScenario ->
                skip
            fi

            atomic {
                // Let's forward this hello message to every known components
                // and send Hello messages from these component to the joining
                // component
                for (i : 0 .. nKnownComponents - 1) {
                    message_queues[knownComponents[i].sender] ! curMsg[BUS]
                    message_queues[curMsg[BUS].sender] ! (knownComponents[i])
                }

            // Save the Hello message
                setMsg(knownComponents[nKnownComponents], curMsg[BUS])
                nKnownComponents++
            }

        // If this is a Bye message
        ::  curMsg[BUS].type == Bye ->
            atomic {
                // Forward the bye message and remove the component
                for (i : 0 .. nKnownComponents - 1) {
                    if
                    ::  knownComponents[i].sender != curMsg[BUS].sender ->
                        message_queues[knownComponents[i].sender] ! curMsg[BUS]

                    ::  knownComponents[i].sender == curMsg[BUS].sender ->
                        setMsg(knownComponents[i], knownComponents[nKnownComponents - 1])
                    fi
                }

                i = 0
                nKnownComponents = nKnownComponents - 1
            }

            // If all components left, stop the process.
            if
            :: nKnownComponents == 0 -> break
            :: nKnownComponents != 0 -> skip
            fi

        // If the message is not Hello and not Bye, forward it to the recipient.
        ::  curMsg[BUS].type != Hello && curMsg[BUS].type != Bye ->
            message_queues[curMsg[BUS].recipient] ! curMsg[BUS]
        fi
    od
}

////////////////////////////////////////////////////////////////////////////////
//                          The monitor process                               //
////////////////////////////////////////////////////////////////////////////////

// This function checks whether a monitor is waiting for acknowledgments
// for previously sent requests, and, if it does not, sends verdicts
inline MaybeSendVerdicts() {
    if
    ::  pendingRequests != 0 -> skip
    ::  pendingRequests == 0 ->
        if
        :: scenarioKnown  ->
            atomic {
                for (i : 0 .. nPendingVerdicts - 1) {
                    send(Verdict, SCENARIO, pendingVerdicts[i])
                }
                i = 0
                nPendingVerdicts = 0
            }
        :: !scenarioKnown ->
            atomic {
                for (i : 0 .. nPendingVerdicts - 1) {
                    pendingScenarioMessages[nPendingScenarioMessages].type      = Verdict
                    pendingScenarioMessages[nPendingScenarioMessages].sender    = self
                    pendingScenarioMessages[nPendingScenarioMessages].recipient = SCENARIO
                    pendingScenarioMessages[nPendingScenarioMessages].custom    = pendingVerdicts[i]
                }
                i = 0
                nPendingVerdicts = 0
            }
        fi
    fi
}


byte curMonId = FIRST_MONITOR

// Monitor process: main procedure
active [NMONITORS] proctype Monitor() {

    // When a monitor is spawned, it computes its identifier.
    // This is different from the real protocol, in which the bus assign
    // identifiers to the components, and identifiers for all components of a
    // kind are not contiguous.
    ProcIdentifier self;
    atomic {
        self = curMonId
        curMonId++
    }

    // This array stores the verdicts to send to the verdicts when the monitor
    // receives its hello message
    Message pendingScenarioMessages[NECs]
    byte nPendingScenarioMessages = 0

    // This array stores the identifiers of the execution controllers for which
    // verdicts are to be sent.
    ProcIdentifier pendingVerdicts[NECs]
    byte nPendingVerdicts = 0

    // This boolean becomes true when the monitor receives the Hello message
    // of the scenario
    bool scenarioKnown = false

    // Number of expected requests from the execution controllers
    byte pendingRequests = 0

    byte pendingRequestsForThisEC = 0

    // Counter for loops.
    byte i = 0

    // Send an Hello message to the bus, with kind Monitor
    SayHello(CMonitor)

    do
    // Get the next message
    :: GetMessage()
        atomic {
        if

        // If the new message is Hello from a monitor, let's ignore it.
        ::  curMsg[self].type == Hello && senderKind(curMsg[self]) != CScenario && senderKind(curMsg[self]) != CExecutionController ->
            skip


        // If the new message is Hello from a scenario, we can send pending verdicts.
        ::  curMsg[self].type == Hello && senderKind(curMsg[self]) == CScenario ->

            // In the real protocol, the identifier of the scenario is not
            // supposed to be known in advance (no SCENARIO constant is available).
            // We check that the protocol allows the monitor to know the
            // identifier of the scenario before sending it any message
            // With the following boolean and the following assert.
            atomic {
                scenarioKnown = true
                assert(curMsg[self].sender == SCENARIO)

                // Let's send pending verdicts.
                for (i : 1 .. nPendingScenarioMessages) {
                    message_queues[BUS] ! pendingScenarioMessages[nPendingScenarioMessages - i]
                }

                i = 0
            }

        // If the message is an event notification, or an hello from an execution controller.
        ::  curMsg[self].type == EventNotification || (curMsg[self].type == Hello && (senderKind(curMsg[self]) == CExecutionController)) ->

            atomic {
                // First, we remember that a verdict for this event must be generated.
                pendingVerdicts[nPendingVerdicts] = curMsg[self].sender
                nPendingVerdicts++

                // Then, the monitor is supposed to update its state and request or
                // release events to the execution controller.
                // Let's pick a random number of event requests to send.
                random(pendingRequestsForThisEC, NB_MONITOR_REQUESTS)
            }

            if
                // If no requests are to be sent to this execution controller,
                // try to send verdicts to the scenario.
                :: pendingRequestsForThisEC == 0 ->

                    MaybeSendVerdicts()

                // Otherwise, send send all the requests to the execution controller.
                :: pendingRequestsForThisEC != 0 ->

                    atomic {
                        for (i : 0 .. pendingRequestsForThisEC - 1) {
                            send(Request, curMsg[self].sender, 0)
                        }

                        i = 0
                        pendingRequests = pendingRequests + pendingRequestsForThisEC
                        pendingRequestsForThisEC = 0
                    }
            fi


        // If the message is the acknowledgment of a request
        ::  curMsg[self].type == Ack ->
            atomic {
                // There is one less acknowledgment to wait.
                pendingRequests--

                // If there is no acknowledgment to wait anymore, try to send verdicts
                MaybeSendVerdicts()
            }

        // If the message is the notification for a leaving component
        ::  curMsg[self].type == Bye ->

            // If the leaving component is a scenario, let's leave too.
            if
            ::   scenarioKnown && (curMsg[self].sender == SCENARIO)  ->
                SayBye();
                break

            :: !(scenarioKnown && (curMsg[self].sender == SCENARIO)) -> skip
            fi
        fi
        }
    od
}

////////////////////////////////////////////////////////////////////////////////
//                          The scenario process                              //
////////////////////////////////////////////////////////////////////////////////


// This function sends an action (request) to the given execution controller, if
// there are still actions to send.
// Otherwise, it sends a resume request.
inline sendActionOrResume(ec) {
    atomic {
        if
        :: pendingActionsByEC[indexOfEC(ec)] == 0 ->
            send(ResumeRequest, ec, 0)

        :: pendingActionsByEC[indexOfEC(ec)] != 0 ->
            pendingActionsByEC[indexOfEC(ec)] = pendingActionsByEC[indexOfEC(ec)] - 1
            send(Request, ec, 0)
        fi
    }
}

// This function picks a random number of actions to send, and then sends one
// action, or a resume request if no actions are to be sent (using
// sendActionOrResume)
inline sendActionsAndResume(ec) {
    atomic {
        receivedVerdictsByEC[indexOfEC(ec)] =  0
        expectedVerdictsByEC[indexOfEC(ec)] = -1

        // nbActions is defined in the main procedure bellow.
        random(nbActions, NB_SCENARIO_ACTIONS)

        pendingActionsByEC[indexOfEC(ec)] = pendingActionsByEC[indexOfEC(ec)] + nbActions

        sendActionOrResume(ec)
    }
}

// Upon reception of a ExpectedVerdictNumber message from an execution
// controller, the scenario checks whether it received all the corresponding
// verdicts. If so, it sends a action / resume using sendActionsAndResume,
// or a quit request in case the ExpectedVerdictNumber actually was a
// FinalVerdictNumber.
// Otherwise, the number of expected verdicts for this execution controller is
// stored and will be used as verdicts are received.
inline handleExpectedVerdictNumber(msg) {
    if

    :: msgNumber(msg) == receivedVerdictsByEC[indexOfEC(msg.sender)] ->
        if
        :: finalNumberReceived[indexOfEC(msg.sender)]  -> send(QuitRequest, msg.sender, 0)
        :: !finalNumberReceived[indexOfEC(msg.sender)] -> sendActionsAndResume(msg.sender)
        fi

    :: msgNumber(msg) != receivedVerdictsByEC[indexOfEC(msg.sender)] ->
        expectedVerdictsByEC[indexOfEC(msg.sender)] = msgNumber(msg)
    fi
}

// Scenario process: main procedure
active proctype Scenario() {
    ProcIdentifier self = SCENARIO

    // The number of actions to be sent by execution controller.
    // Initially, no actions are to be sent.
    // Action will be sent to an execution controller as reactions to a verdict
    // steming from this execution controller
    byte pendingActionsByEC[NECs]   = {0}

    // For each execution controller, this array stores the number of expected
    // verdicts steming this execution controller.
    // This number is given by the execution controller itself in
    // ExpectedVerdictNumber messages.
    int  expectedVerdictsByEC[NECs] = {0}

    // For each execution controller, this array stores the number of received
    // verdicts steming this execution controller.
    // When this number is different from zero and equal to the number expected
    // verdicts, reactions should be triggered (actions, and then a resume or a
    // quit request are sent)
    int  receivedVerdictsByEC[NECs] = {0}

    // For each execution controller, a boolean that is true if we received the
    // final verdict number, sent by the execution controller when no more
    // events are expected (because the program has terminated)
    bool finalNumberReceived[NECs]  = {false}


    // Kinds for known component, by identifier
    ComponentKind knownComponents[NB_COMPONENTS] = {CNone}

    // The number of known monitors, used to determine the number of verdicts
    // that the scenario should receive when a new execution controller is seen
    byte nbKnownMonitors = 0

    // The number of known execution controller.
    // The scenario leaves when every execution controller have left ( = when
    // this variable is reset to zero).
    byte nbKnownECs = 0

    // Counter for loops
    byte i = 0

    // Variable used in sendActionsAndResume to pick a random number of actions
    // to send
    byte nbActions = 0

    // Send an hello message to the bus
    SayHello(CScenario)

    do
    // Get the next message
    :: GetMessage()
        atomic {
        if

        // If this is an Hello message
        ::  curMsg[self].type == Hello ->

            // The scenario stores the kind of the joining component.
            // This is used to determine the kind of component that is leaving
            // when receiving a Bye message.
            knownComponents[curMsg[self].sender - 1] = senderKind(curMsg[self])

            if
                // Execution controllers is counted.
                :: senderKind(curMsg[self]) == CExecutionController ->
                    nbKnownECs++

                // Monitors are not: the number of monitors is given by the
                // special NbMonitors message sent by the bus to the scenario.
                :: senderKind(curMsg[self]) != CExecutionController ->
                    skip
            fi

        // If this is a Bye message
        ::  curMsg[self].type == Bye ->
            if

            // If the component is an execution controller, keep track of the
            // number of execution controllers. If all of them left, leave too.
            :: knownComponents[curMsg[self].sender - 1] == CExecutionController ->
                nbKnownECs--
                if
                :: nbKnownECs == 0 -> SayBye() ; break
                :: nbKnownECs != 0 -> skip
                fi

            // Keep track of the number of monitors too. This is only correct
            // if no monitors leave before the NbMonitors message, which is the
            // case because NbMonitors is the first message sent by the bus
            // to the scenario.
            :: knownComponents[curMsg[self].sender - 1] == CMonitor ->
                nbKnownMonitors--
            fi

        // If this is a verdict
        ::  curMsg[self].type == Verdict ->
            if
            // If the number of received verdicts (including this one) for this
            // execution controller matches the number of expected verdicts,
            // react
            :: expectedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] == receivedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] + 1 ->
                if

                // send a quit request if the program stopped
                :: finalNumberReceived[indexOfEC(msgEC(curMsg[self]))] ->
                    send(QuitRequest, msgEC(curMsg[self]), 0)

                // otherwise, send a number of actions, and then a resume
                :: !finalNumberReceived[indexOfEC(msgEC(curMsg[self]))] ->
                    sendActionsAndResume(msgEC(curMsg[self]))
                fi

            // Otherwise, update the number of received verdicts
            :: expectedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] != receivedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] + 1 ->
                receivedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] = receivedVerdictsByEC[indexOfEC(msgEC(curMsg[self]))] + 1
            fi

        // If this is an ExpectedVerdictNumber message, react to this message.
        // See handleExpectedVerdictNumber.
        :: curMsg[self].type == ExpectedVerdictNumber ->
            handleExpectedVerdictNumber(curMsg[self])

        // If this is an FinalVerdictNumber message, same thing but the scenario
        // remembers that the execution controller should be sent a quit request.
        :: curMsg[self].type == FinalVerdictNumber ->
            finalNumberReceived[indexOfEC(curMsg[self].sender)] = true
            handleExpectedVerdictNumber(curMsg[self])

        // If this is an event notification (presumably because of a prior
        // request):
        :: curMsg[self].type == EventNotification ->
            if
                // If no verdicts are expected for this event, let's send
                // actions and resume the execution.
                :: expectedVerdictsByEC[indexOfEC(curMsg[self].sender)] == 0 ->
                    sendActionsAndResume(curMsg[self].sender)

                // Otherwise, actions and resume will be / have been handled
                // when verdicts are / have been received
                :: expectedVerdictsByEC[indexOfEC(curMsg[self].sender)] != 0 ->
                    skip
            fi

        // If this is an NbMonitors messages, let's store the given number and
        // set the number of expected verdicts for every execution controller
        // to this number.
        // This is cheating, since execution controllers are not known yet at
        // this point in the real protocol. In the real protocol, this is doneµ
        // upon the reception of hello messages of the execution controllers.
        // Verdicts for an execution controller can be received before their
        // hello messages.
        // This makes the model simpler.
        :: curMsg[self].type == NbMonitors ->
            nbKnownMonitors = msgNumber(curMsg[self])
            for (i : 0 .. NECs - 1) {
                expectedVerdictsByEC[i] = nbKnownMonitors
            }

            i = 0

        // If this is an acknowledgment for a request, let's continue handling
        // actions and resumes.
        :: curMsg[self].type == Ack ->
            sendActionOrResume(curMsg[self].sender)
        fi
        }
    od
}

////////////////////////////////////////////////////////////////////////////////
//                    The execution controller process                        //
////////////////////////////////////////////////////////////////////////////////

byte curECId  = FIRST_EC

// Bus process: main procedure
active [NECs] proctype ExecutionController() {
    ProcIdentifier self

    atomic {
        self = curECId
        curECId++
    }

    // The event receivers are the monitors and the scenario.
    // These are components that can request and receive events.
    // This array is a list of these component, by order of arrival (reception
    // of Hello messages by this execution controller)
    byte eventReceivers[NMONITORS + 1]
    byte nEventReceivers = 0

    // The number of events that have been generated until now
    byte eventsSent = 0

    // A counter for loops
    byte i = 0

    byte expectedVerdicts = 0

    // Send an Hello message to the bus
    SayHello(CExecutionController)

    do
    // Get the next message
    :: GetMessage()
        atomic {
        if

        // If this is an Hello message
        :: curMsg[self].type == Hello ->
            if

            // If the joining component is a monitor or a scenario, its
            // identifier is stored in the list of event receivers
            :: senderKind(curMsg[self]) == CMonitor || senderKind(curMsg[self]) == CScenario ->
                atomic {
                    eventReceivers[nEventReceivers] = curMsg[self].sender
                    nEventReceivers++
                }

            // Otherwise, the message is ignored
            :: !(senderKind(curMsg[self]) == CMonitor || senderKind(curMsg[self]) == CScenario) ->
                skip
            fi

        // If this is a leaving component
        :: curMsg[self].type == Bye ->
            if

            // If this is an event receiver, it is removed from the list of
            // known event receivers
            :: senderKind(curMsg[self]) == CMonitor || senderKind(curMsg[self]) == CScenario ->
                atomic {
                    i = 0
                    do
                    :: eventReceivers[i] == curMsg[self].sender ->
                        eventReceivers[i] = eventReceivers[nEventReceivers - 1]
                        nEventReceivers--
                        break
                    :: eventReceivers[i] != curMsg[self].sender -> i++
                    od
                    i = 0
                }

            // Otherwise, the message is ignored
            :: !(senderKind(curMsg[self]) == CMonitor || senderKind(curMsg[self]) == CScenario) ->
                skip
            fi

        // If this is a resume request
        :: curMsg[self].type == ResumeRequest ->
            mtype:type expectedVerdictNumberKind

            // an event is generated, and either the message is final, or not.
            // if so, final verdict number will be sent. Otherwise, an expected
            // verdict number will be sent.
            // The number of generated events can be
            //  - limited (if LIMIT_EVENTS is true), in which case eventsSent
            //    counts the number of sent events (using the fact that
            //    LIMIT_EVENTS = true = 1), and EVENT_MAX is the maximum
            //    number of non-final events to generate,
            //  - or not (if LIMIT_EVENTS is false), in which case eventsSent
            //    remains equal to zero (using the fact that
            //    LIMIT_EVENTS = false = 0), to avoid changing the state of the
            //    system needlessly.
            //
            // I'm not sure limiting the number of events is a great idea in this
            // model anyway, so we are probably better off keeping LIMIT_EVENTS
            // to false.
            if
                :: ifelse(LIMIT_EVENTS, true, `(eventsSent < EVENT_MAX) -> eventsSent = eventsSent + 1;', `') expectedVerdictNumberKind = ExpectedVerdictNumber
                :: expectedVerdictNumberKind = FinalVerdictNumber
            fi

            // For each event receiver, either we sent an event notification
            // or not. The number of monitors receiving the event notification
            // is computed and will be sent to the scenario.

            atomic {
                i = 0
                expectedVerdicts = 0

                for (i : 0 .. nEventReceivers - 1) {
                    if
                        :: send(EventNotification, eventReceivers[i], 0)
                        expectedVerdicts = expectedVerdicts + (eventReceivers[i] != SCENARIO)
                        :: skip
                    fi
                }

                i = 0

                // The number of expected verdicts is sent.
                send(expectedVerdictNumberKind, SCENARIO, expectedVerdicts)
            }


        // If this is a quit request, the execution controller leaves
        :: curMsg[self].type == QuitRequest ->
            SayBye()
            break

        // If this is a regular request, the execution controller sends an
        // acknowledgment
        :: curMsg[self].type == Request ->
            send(Ack, curMsg[self].sender, 0)
        fi
        }
    od
}

////////////////////////////////////////////////////////////////////////////////
//                               Properties                                   //
////////////////////////////////////////////////////////////////////////////////

// We include a library that provides the following macros:
// - forall and exists, quantifiers that are not provided by spin.
// - leadsTo, which is simply defined as leadsTo(A, B) = A -> <> B.
// - implies, different from operator implies "->" provided by spin
//   because the implication is computed by the preprocessor and not
//   at runtime. This allows smaller formulas.
// - defineLTL to define an LTL property with a name that can be reused in
//   another property
// - defineAlwaysLTL, same as defineLTL, but the property is prepended with an
//   always "[]" operator.
include(`betterltl.m4')

/**************************
 *      INVARIANTS        *
 **************************/

// Components receiving event notifications are monitors and scenarios.
defineAlwaysLTL(eventRecipientsAreMonitorsAndTheScenario, (
    (curMsg[BUS].type == EventNotification) -> isEventReceiver(curMsg[BUS].recipient) && isExecutionController(curMsg[BUS].sender)
))

// Requests are sent by monitors and scenarios and received by execution controllers.
defineAlwaysLTL(requestsFromEventReceiversAndToEC, (
    (curMsg[BUS].type == Request) -> (
            isEventReceiver(curMsg[BUS].sender)
        &&  isExecutionController(curMsg[BUS].recipient)
    )
))

// Verdicts are sent by monitors to scenarios.
defineAlwaysLTL(verdictsFromMonitorsAndToScenarios, (
    (curMsg[BUS].type == Verdict) -> (
            isMonitor(curMsg[BUS].sender)
        &&  isScenario(curMsg[BUS].recipient)
    )
))

defineLTL(conj_invariants, (
        eventRecipientsAreMonitorsAndTheScenario
    &&  verdictsFromMonitorsAndToScenarios
    &&  requestsFromEventReceiversAndToEC
))

defineLTL(invariants, ([] (conj_invariants)))

/**************************
 *        Liveness        *
 **************************/

ifelse(HANDLE_VERIF_MESSAGE, true,

    // These properties are only defined if HANDLE_VERIF_MESSAGE is true.
    //
    //
    // These properties rely on checking values of the current messages at
    // any time during the execution.
    //
    // Only curMsg[BUS] should be used, except for messages that are
    // broadcast / generated by the bus (Hello messages, NbMonitors).
    //
    // It is hard to think about messages from other queues correctly without
    // doing mistakes. For instance, one could be tempted to write: "Event
    // messages from any execution controller leads to a resume or quit
    // request":
    //
    //   forall mon, forall ec,
    //       curMsg[mon].type == EventNotification and
    //       curMsg[mon].sender == ec
    //                leads to
    //       curMsg[ec].type == ResumeRequest
    //
    // But this properties does not check what we want: the last message
    // received by the execution controller (curMsg[ec]) could already be a
    // resume request, making the "lead to" clause pass immediately.

    // A new message happens when newmessage is true, and in the next state,
    // newmessage becomes false
    define(`NewMessage', `(newmsg && (X !newmsg))')

    // When an event is sent by execution controller ec to monitor mon, a
    // verdict from monitor mon steming from ec will be seen after.
    defineLTL(eventLeadsToVerdicts, ([] (
        forall(ec, FIRST_EC, LAST_EC,
            forall(mon, FIRST_MONITOR, LAST_MONITOR,
                leadsTo((
                       curMsg[BUS].type      == EventNotification
                    && curMsg[BUS].sender    == ec
                    && curMsg[BUS].recipient == mon
                ), (
                       curMsg[BUS].type      == Verdict
                    && msgEC(curMsg[BUS])    == ec
                    && curMsg[BUS].sender    == mon
                ))
            )
        )
    )))

    // Expected verdict numbers are followed by a resume request
    defineLTL(expectedVerdictLeadsToResume, ([] (
        forall(ec, FIRST_EC, LAST_EC,
            leadsTo((
                    curMsg[BUS].type      == ExpectedVerdictNumber
                &&  curMsg[BUS].sender    == ec
                &&  curMsg[BUS].recipient == SCENARIO
            ), (
                    curMsg[BUS].type      == ResumeRequest
                &&  curMsg[BUS].recipient == ec
            ))
        )
    )))

    // Events from ec are followed by a resume or a quit request to ec.
    defineLTL(eventLeadsToResume, ([] (
        forall(ec, FIRST_EC, LAST_EC,
            leadsTo((
                    curMsg[BUS].type      == EventNotification
                &&  curMsg[BUS].sender    == ec
            ), (
                ((curMsg[BUS].type == ResumeRequest) || (curMsg[BUS].type == QuitRequest))
                &&  curMsg[BUS].recipient == ec
            ))
        )
    )))

    // Hello from any component is seen by any component.
    defineLTL(helloForEveryone,
        forall(rcpt, MIN_COMPONENT_ID, MAX_COMPONENT_ID,
            forall(sdr, MIN_COMPONENT_ID, MAX_COMPONENT_ID,
                implies(sdr != rcpt, (<> (curMsg[rcpt].type == Hello && curMsg[rcpt].sender == sdr)))
            )
        )
    )

    // An event notification from ec to mon cannot be seen after an event
    // notification from ec to mon until a verdict has been issued by mon
    // steming from ec.
    defineLTL(notTwoEvents, (
        forall(ec, FIRST_EC, LAST_EC,
            forall(mon, FIRST_MONITOR, LAST_MONITOR,
                [] (
                    (
                        NewMessage && (
                                curMsg[BUS].type      == EventNotification
                            &&  curMsg[BUS].sender    == ec
                            &&  curMsg[BUS].recipient == mon
                        )
                    ) -> X ((
                        NewMessage -> !(
                                curMsg[BUS].type      == EventNotification
                            &&  curMsg[BUS].sender    == ec
                            &&  curMsg[BUS].recipient == mon
                        )
                    ) U (curMsg[BUS].type == Verdict && msgEC(curMsg[BUS]) == ec))
                )
            )
        )
    ))

    defineLTL(liveness, (
            eventLeadsToVerdicts
        &&  expectedVerdictLeadsToResume
        &&  notTwoEvents
        &&  eventLeadsToResume
        &&  helloForEveryone
    ))

    defineLTL(all, (
            invariants
        &&  liveness
    ))
)
