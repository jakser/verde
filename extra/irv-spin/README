This is a model for the i-RV protocol.

This model is written in the Promela language and uses the GNU m4 preprocessor.
SPIN is used for model checking.

The model is defined and explained in irv.m4.pml and can be configured
in file constants.m4.

At the moment, checking the model with more than 1 monitor and 1 execution
controller seems impossible on a machine with 32 GiB of RAM.

TIP: Setting the maximum number of monitor requests and of scenario actions
zero greatly reduces checking time, but disables essential features from the
protocol (depending on what should be checked).

Requirements
============

 - GNU m4. On GNU/Linux, this is probably already installed.
 - Spin. This model requires a recent (unreleased as of April 2019) version of
   Spin that include fixes to handle large LTL formulas.
   This version can be found at https://github.com/nimble-code/Spin.git

   Without this version, property helloForEveryone should be removed from the
   LTL liveness_properties around line 969 in irv.m4.ml.
   Setting a number of monitors or execution controller higher than one requires
   fiddling with formulas because they are too big for the current release of
   Spin. First things involve removing property ltl_all, and probably also
   properties liveness and helloForEveryone.

   Beware: Spin needs to be compiled with the -DNXT flag to enable
   the "X" (next) LTL operator, which should be the default when compiling from
   sources. However, the version from the Debian repository is not compiled with
   this flag. So Spin from Debian will not work for now (or disable properties
   that use this operator).

Installing Spin
===============

1) install bison
2) Run:
    git clone https://github.com/nimble-code/Spin.git
3) make
4) sudo make install

Model checking
==============
Compile
-------

    make

Run
---

    ./pan -a -N prop_all

ltl_all can be replaced with any property to check. Property all checks all
invariants and liveness properties. Alternatively, one of these properties can
be used:

 - eventRecipientsAreMonitorsAndTheScenario
 - requestsFromEventReceiversAndToEC
 - verdictsFromMonitorsAndToScenarios
 - invariants
 - eventLeadsToVerdicts
 - expectedVerdictLeadsToResume
 - eventLeadsToResume
 - helloForEveryone
 - notTwoEvents
 - liveness
 - all

The following command was used to build this list:

    <irv.m4.pml grep "define" | grep "LTL" | cut -d',' -f1 | cut -d'(' -f2 | grep -v '//'

Simulation
==========

    Simulation does not require compiling the model with spin. However, it is
    required to build irv.pml from irv.m4.pml using m4.

    make irv.pml
    spin irv.pml
