divert(`1')

// Set this to false to disable checking liveness properties
define(HANDLE_VERIF_MESSAGE, true)

// The number of monitors in the system
define(NMONITORS,           2)

// The number of execution controllers in the system
define(NECs,                1)

// The maximum number of monitor requests upon the reception of an event
define(NB_MONITOR_REQUESTS, 0)

// The maximum number of scenario actions when reacting
define(NB_SCENARIO_ACTIONS, 0)

// The size of the message queues
define(MSG_QUEUE_SIZE,      8)

// Should the number of generated events before quitting be strictly limited?
define(LIMIT_EVENTS, false)

// If so, the maximum number of generated events:
define(EVENT_MAX, 1)

// Identifiers of the different components
define(BUS,           0)
define(SCENARIO,      1)
define(FIRST_MONITOR, 2)
define(LAST_MONITOR,  eval(FIRST_MONITOR + NMONITORS - 1))
define(FIRST_EC,      eval(LAST_MONITOR + 1))
define(LAST_EC,       eval(FIRST_EC + NECs - 1))

// Number of components
define(NB_COMPONENTS, eval(1 + NMONITORS + NECs))

define(MIN_COMPONENT_ID, SCENARIO)
define(MAX_COMPONENT_ID, LAST_EC)

divert(`0')dnl
