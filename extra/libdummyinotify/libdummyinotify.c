#include <stdint.h>
#include <unistd.h>

static int pipefd[2];

int inotify_init(void) {
    static int initialized = 0;

    if (!initialized) {
        initialized = 1;
        pipe(pipefd);
    }

    return pipefd[0];
}

/* Create and initialize inotify instance.  */
int inotify_init1(__attribute__((unused)) int __flags) {
    return inotify_init();
}

int inotify_add_watch(__attribute__((unused)) int __fd, __attribute__((unused)) const char *__name, __attribute__((unused)) uint32_t __mask) {
    return 1;
}

int inotify_rm_watch (__attribute__((unused)) int __fd, __attribute__((unused)) int __wd) {
    return 1;
}
