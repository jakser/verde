#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2019 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/target")

import irv_pb2 as irv
import math
from collections import Counter

def getMessage():
    msglenbytes = sys.stdin.buffer.read(4)

    if not msglenbytes:
        return None

    msglen = int.from_bytes(msglenbytes, byteorder='big')
    msgbytes = sys.stdin.buffer.read(msglen)
    msg = irv.Root()
    msg.ParseFromString(msgbytes)
    return msg


VERDE_TRACE_TIME = bool(sys.stdin.buffer.read(1)[0])

requestsById = Counter()
releasesById = Counter()
eventsById   = Counter()
verdictsById   = Counter()
eventsByPoint = Counter()
eventsByRecipent = Counter()
uniqueEventsByPoint = Counter()
componentsById = {}
resquestToEvent = {}
reactedFor = {}

def printCounters(title, counter):
    print(title + ": (TOTAL " + str(sum(counter.values())) + ")")
    for (elem, count) in counter.most_common():
        print(" ", count, "\t#" + str(elem), "(" + (componentsById[elem].hello.serviceName + " " + os.path.basename(componentsById[elem].hello.programName)).strip() + ")")

    print()

scnSenderId = -1

try:
    while True:
        if VERDE_TRACE_TIME:
            t = struct.unpack("d", sys.stdin.buffer.read(8))[0]

        msg = getMessage()

        if not msg:
            break

        if msg.HasField("hello"):
            if msg.sender == 0 and msg.recipients[0] == 0:
                lastHello = msg

            if msg.sender == 0 and msg.recipients[0] != 0:
                componentId = msg.recipients[0]
                componentsById[componentId] = lastHello

                lastHello.recipients.remove(0)
                lastHello.sender = componentId
                if lastHello.hello.serviceName == "scenario":
                    scnSenderId = componentId
                else:
                    reactedFor[componentId] = True
            continue

        elif msg.HasField("eventRequest"):
            requestsById[msg.sender] += 1
            resquestToEvent[(msg.sender, msg.eventRequest.requestId)] = ((msg.eventRequest.className + ".") if msg.eventRequest.className else "") + msg.eventRequest.methodName + (("#" + msg.eventRequest.fieldName) if msg.eventRequest.fieldName else "") + ((":" + str(msg.eventRequest.lineNumber)) if msg.eventRequest.lineNumber else "")

        elif msg.HasField("eventRelease"):
            releasesById[msg.sender] += 1

        elif msg.HasField("verdict"):
            verdictsById[msg.sender] += 1

        elif msg.sender == scnSenderId:
            reactedFor[msg.recipients[0]] = True

        elif msg.HasField("event"):
            if reactedFor[msg.sender]:
                uniqueEventsByPoint[resquestToEvent[(msg.recipients[0], msg.event.requestId)]] += 1
                reactedFor[msg.sender] = False

            eventsById[msg.sender] += 1
            eventsByPoint[resquestToEvent[(msg.recipients[0], msg.event.requestId)]] += 1
            eventsByRecipent[msg.recipients[0]] += 1

finally:
    printCounters("Requests from", requestsById)
    printCounters("Releases from", releasesById)
    printCounters("Verdicts from", verdictsById)
    printCounters("Event Notifications Producers", eventsById)


    printCounters("Event Notifications to", eventsByRecipent)

    print("Event Notifications: (TOTAL " + str(sum(eventsByPoint.values())) + ")")

    for (elem, count) in eventsByPoint.most_common():
        print(" ", count, "\t" + elem)
    print()

    print("Unique Events: (TOTAL " + str(sum(uniqueEventsByPoint.values())) + ")")

    for (elem, count) in uniqueEventsByPoint.most_common():
        print(" ", count, "\t" + elem)

