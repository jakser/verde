#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2019 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from VerdeBusClient import VerdeBusClient

import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/src/common")

import irv_pb2 as irv

class ListBusClients(VerdeBusClient):
    def printClients(self):
        print()

        for (k, v) in self.serviceById.items():
            print(" #" + str(k) + "\t\t- " + v)

        print()

    def __init__(self):
        self.serviceById = {}
        self.connectToBus("dbg-hello")

        while True:
            msg = self.getMessage()

            if msg.HasField("hello"):
                print("-- Hello from " + msg.hello.serviceName + " #" + str(msg.sender))
                self.serviceById[msg.sender] = msg.hello.serviceName
                self.printClients()

            elif msg.HasField("bye"):
                print("-- Bye from #" + str(msg.sender))
                del self.serviceById[msg.sender]
                self.printClients()

l = ListBusClients()
