#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2019 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/target")

import irv_pb2 as irv
import math

MAX_COLS = 4
JOINING_COMPONENT_COLOR = 'deepgreen'
LEAVING_COMPONENT_COLOR = 'deepred'
NORMAL_COMPONENT_COLOR  = 'black'
MESSAGE_ARROW_COLOR     = 'deepblue'

# https://goodcode.io/articles/python-dict-object/
class ObjectView(object):
    def __init__(self, d):
        self.__dict__ = d

def getMessage():
    msglenbytes = sys.stdin.buffer.read(4)

    if not msglenbytes:
        return None

    msglen = int.from_bytes(msglenbytes, byteorder='big')
    msgbytes = sys.stdin.buffer.read(msglen)
    msg = irv.Root()
    msg.ParseFromString(msgbytes)
    return msg
 #(msg.sender, msg.hello.serviceName, msg.hello.programName)
#busMsg = ObjectView({"sender":0, "hello": ObjectView({"serviceName":"bus","programName":""})})
components = []
componentsById = {}

nCols = 0
nLines = 0

def computeLines():
    global nCols, nLines

    nCols = min(MAX_COLS, math.ceil(math.sqrt(len(components))))
    nLines = math.ceil(len(components) / nCols)

def componentColor(component, joiningComponent, leavingComponent):
    if component.sender == joiningComponent:
        return JOINING_COMPONENT_COLOR

    if component.sender == leavingComponent:
        return LEAVING_COMPONENT_COLOR

    return NORMAL_COMPONENT_COLOR

def drawComponent(component, color, cx, cy):
    pname = os.path.basename(component.hello.programName).replace("_", "\\_")

    print("""            \draw ({x}, {y}) node[component,color={componentColor}] (C{id}) {{
                {serviceName}\\\\
                \\#{id}{programName}
            }};
    """.format(
            componentColor=color,
            id=component.sender,
            x=cx * 4,
            y=cy * 2.5,
            serviceName=component.hello.serviceName.replace("executionController", "EC"),
            programName=((" - " + pname) if pname else "")
        )
    )

def drawComponents(joiningComponent, leavingComponent):
    cx = 0
    cy = nLines

    for component in components:
        color = componentColor(component, joiningComponent, leavingComponent)
        drawComponent(component, color, cx, cy)

        cx += 1
        if cx == nCols:
            cx = 0
            cy -= 1

def drawMessageArrow(msg):
    global MESSAGE_ARROW_COLOR
    for recipient in msg.recipients:
        if recipient != 0 and msg.sender != 0:
            print("            \\path[draw, -latex,color={arrowColor}] (C{sender}) edge node[label,above left,color={arrowColor}] {{{msgcontent}}}    (C{recipient});".format(
                sender=msg.sender,
                recipient=recipient,
                arrowColor=MESSAGE_ARROW_COLOR,
                msgcontent = msg.WhichOneof("content")
            ));

def drawMessageContent(msg):
    print(("\\begin{{lstlisting}}\n"
          "{msg}"
          "\\end{{lstlisting}}").format(msg=str(msg)))

def drawFrame(joiningComponent=None, leavingComponent=None, msg=None, t=0):
    print("""    \\begin{frame}[fragile]
        \\begin{tikzpicture}""")

    drawComponents(joiningComponent, leavingComponent)

    if msg: drawMessageArrow(msg)

    print("""        \\end{tikzpicture}""")

    if msg:
        drawMessageContent(msg)
        if t:
            print("""        time = {}""" % t)

    print("    \\end{frame}")


print("""\\documentclass{beamer}
\\usepackage{color}
\\usepackage{tikz}
\\usepackage{listings}
\\definecolor{deepblue}{rgb}{0,0,0.5}
\\definecolor{deepred}{rgb}{0.6,0,0}
\\definecolor{deepgreen}{rgb}{0,0.5,0}

\\tikzset{
    component/.style={
        rectangle,
        draw=black,
        text width=6em,
        align=center,
        minimum height=4em,
        font=\\tiny
    },
    label/.style={
        font=\\tiny
    }
}

\\lstset{
    basicstyle=\\tiny
}

\\begin{document}
""")

lastHello = None

VERDE_TRACE_TIME = bool(sys.stdin.buffer.read(1)[0])

t = 0.

try:
    while True:
        if VERDE_TRACE_TIME:
            t = struct.unpack("d", sys.stdin.buffer.read(8))[0]

        msg = getMessage()

        if not msg:
            break

        if msg.HasField("hello"):
            if msg.sender == 0 and msg.recipients[0] == 0:
                lastHello = msg

            if msg.sender == 0 and msg.recipients[0] != 0:
                componentId = msg.recipients[0]

                components.append(lastHello)
                componentsById[componentId] = lastHello
                computeLines()

                lastHello.recipients.remove(0)
                lastHello.sender = componentId
                drawFrame(joiningComponent=componentId, msg=lastHello, t=t)
            continue

            #if msg.sender == 0:
                #components[len(components) - 1].sender = msg.recipients[0]

        elif msg.HasField("bye"):
            if msg.sender in componentsById:
                drawFrame(leavingComponent=msg.sender, t=t)
                components.remove(componentsById[msg.sender])
                del componentsById[msg.sender]
                computeLines()

            continue

        drawFrame(msg=msg, t=t)
finally:
    print("\\end{document}")
