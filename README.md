# Verde

Verde is the reference implementation for Interactive Runtime Verification,
released under license GPLv2+.
Interactive Debugging with a traditional debugger can be tedious.
One has to manually run a program step by step and set breakpoints to track a
bug.
i-RV is an approach to bug fixing that aims to help developpers during their
Interactive Debugging sessions using Runtime Verification.

Verde is composed of:

- Simple-Verde: the original implementation of monolithic interactive runtime
  verification, suitable for debugging single C/C++ programs with GDB using one
  property. Simple-Verde is a Python plugin.
- Dist-Verde: a distributed implementation allowing interactive runtime
  verification of multiple process programs; using several properties.
  It also has a more powerful implementation of scenarios.
  Currently, it supports:
   - Java 8 programs through a AspectJ-based extension of JDB
   - C/C++ programs through a Python GDB extension.

For a more torough presentation of Dist-Verde, see [ORGANIZATION.md](ORGANIZATION.md)

# Getting Verde

## Notice on Supported Systems

Verde is only supported on GNU/Linux and tested on the latest and up to date
Ubuntu LTS and Debian testing. As such, it is advised to use a Virtual Machine
or a Debian / Ubuntu chroot on other systems.

To run the sort example, be sure use a multicore machine. In a Virtual Machine,
be sure to allocate two CPU cores. This is not otherwise needed in Verde.

That said, few things are specific to a particular system and Verde will
certainly work fine on most GNU/Linux distributions provided the right
dependencies are there, should work on BSD, might work on macOS.

On Windows, your best bet is probably WSL, though Cygwin might work too.
You might need to disable UNIX sockets in `src/python/conf.py`.
Newer versions of Windows support AF_UNIX sockets but this might not be
available in Windows build of Python 3 just yet.

Checkpointing is based on CRIU and is therefore only available on Linux.

Patches are welcome for unsupported systems.

## Download

First, install git and run:

    git clone https://gitlab.inria.fr/monitoring/verde.git

NOTE: to run experimentations from our pre-2019 papers, please use the previous
version and [follow our detailled instructions from there](https://gitlab.inria.fr/monitoring/verde/blob/a97829d63ac27bf333f57f99e4f872a018c654e2/artifact/README.md):

    git checkout pre-2019-experiments

# Install system dependencies

## Install everything:

This commands install everything needed to run both Simple-Verde and Dist-Verde.
It does not include dependencies for the graph displaying feature of Simple-Verde.
See later to install these dependencies. On Debian and Ubuntu:

    sudo apt update
    sudo apt install --no-install-recommends make openjdk-8-jdk protobuf-compiler wget python3 gdb libc6-dev python3-protobuf python3-pexpect

Ubuntu users: Please note that `openjdk-8-jdk` is in `universe` on Ubuntu 18.04.
This channel must be enabled in `/etc/apt/sources.list`.

For convenience (but needs double check), these commands should install what is needed in Fedora:

    dnf install java-1.8.0-openjdk-devel protobuf-devel wget python3 gdb glibc-devel python3-protobuf python3-pexpect
    dnf group install "C Development Tools and Libraries"

Explanations of the dependencies:

- `make`: run make, to prepare, install and run Dist-Verde
- `openjdk-8-jdk`: compile and run the scenario, the jdb client and java 8
   program being interactive runtime verified.
- `protobuf-compiler`: Protocol Buffers are used for communication between the
   different Dist-Verde components
- `wget` is used to download dependencies when installing Dist-Verde.
- `python3` is used to run the bus.
- `gdb` is used to interatively verify C/C++ programs
- `libc6-dbg` is used to debug programs using the libc (C/C++ programs).
- `python3-protobuf` is used in the bus and the gdb Dist-Verde extension for
   communications.
- `python3-pexpect` is used in verde-jdb to start the prorgam execution
- automatically by sending `cont` to the standard input of jdb.

NOTE: if you whish to be able to use features related to checkpointing, please
also install CRIU (Checkpoint / Restore In Userspace).

## To use Dist-Verde with Java Programs Only:

    sudo apt install --no-install-recommends openjdk-8-jdk protobuf-compiler wget python3 python3-protobuf python3-pexpect

## To use Simple-Verde Only:

    sudo apt install gdb libc6-dbg wget

Simple-Verde provides a graphical view of properties.
To be able to show graph of properties in Simple-Verde:

    sudo apt install --no-install-recommends graphviz python3-pip python3-pyqt5 python3-pyqt5.qtwebkit
    sudo pip3 install graphviz

# Prepare Dist-Verde

Please change the current directory to the verde folder and then run make and
read given instructions carefully.
First runs will invite you to set `JAVA_HOME`, install missing dependencies and
source Verde's environment set up script.

    cd verde
    make

WARNING: Verde will download about 54,6 M of dependencies on first make.
If you want to save some megabytes by removing support for Python scripting
in scenarios, execute the following line before running `make`:

    make strip-jython

Jython, which is about 36 M, will not be downloaded.

# Try Dist-Verde

## On a C example:

    cd $VERDE_DIR/examples/sort
    gcc -c -fopenmp qsort.c -o qsort

    dist-verde --prop $VERDE_DIR/examples/sort/qsort-c.prop --prgm $VERDE_DIR/examples/sort/qsort -dbg gdb -arg 100

## On a Java Example:

    source verde/verde.source.sh

    cd $VERDE_DIR/examples/sort

    javac -g QSort.java

    dist-verde --prop $VERDE_DIR/examples/sort/qsort-java.prop --prgm QSort -dbg jdb -arg 100

When main[1] is shown, issue `cont`.
The default scenario, `stopexec`, which suspends the execution when a
non-accepting state is reached, is used.
A scenario can be explicitely specified using the `--scn` switch (e.g. `--scn $VERDE_DIR/scenarios/stopexec.js`)

Usage of Dist-Verde is described in [doc/USING_DIST_VERDE.md](doc/USING_DIST_VERDE.md).

# Try Simple-Verde

For Simple-Verde, sourcing `verde.source.sh` is not actually needed, but still
useful to define VERDE_DIR.

    source verde/verde.source.sh

    simple-verde $VERDE_DIR/verde/examples/sort/qsort-c.prop $VERDE_DIR/examples/sort/qsort 100

Notice: sourcing `verde.source.sh` requires installing dependencies for Dist-Verde.
If you do not want that, just declare `$VERDE_DIR` like that:

    export VERDE_DIR="$(realpath verde/)"

Usage Simple-Verde is described in [doc/USING_SIMPLE_VERDE.md](doc/USING_SIMPLE_VERDE.md).

# Authors
Verde is written by Raphaël Jakse, Jean-François Méhaut and Yliès Falcone at
Université Grenoble Alpes (France).

# Publications
 - *Interactive Runtime Verification*, Technical Report on ArXiv: [hxttps://arxiv.org/abs/1705.05315](https://arxiv.org/abs/1705.05315).
 - *Interactive Runtime Verification*, conference paper at ISSRE'17: pending publication.
