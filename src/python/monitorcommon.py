#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module which contains object shared between the monitor and its interface.
"""

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from property import State

class StopExecution(Exception):
    """ This class is used in the monitor whenever the execution of the
        monitored program must be stopped. """
    def __init__(self, msg: str) -> None:
        super(StopExecution, self).__init__(msg)


class MonitorException(Exception):
    def __init__(self, msg: str) -> None:
        self.msg = msg
        super(MonitorException, self).__init__(msg)


class SetCurrentState(Exception):
    """ This class is used in the monitor whenever the current states must be
        changed while the monitor is taking transitions. """

    def __init__(self, state: "State", stop: bool = False):
        """ This constructor takes a set of the new current states and a
            parameter deciding whether the execution of the program must be
            stopped. """

        super(SetCurrentState, self).__init__("")
        self.state = state
        self.stop   = stop

events = {
    "state_changed":    1,
    "transition_taken": 2,
    "event_applied":    4,
    "event_applying":   8,
    "checkpoint_restart": 16
}
