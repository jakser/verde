#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module which drives gdb on a program from a property.
"""

import sys
from typing import Any, Optional, List, Dict, Iterable, Callable, Generator, Tuple, Set, Counter, Union
from types import CodeType
from dataclasses import dataclass

from eventmanager import EventManager
from property import Property, State, Event, RuntimeEvent, RuntimeEventProgramTarget, EventProgramTarget, Transition, TransitionEndBlock, Env, SliceInstance, eventName
from automatonslice import AutomatonSlice, EventWithTarget, dummyState

STEP_COLOR    = "\033[1;94m"
ERROR_COLOR   = "\033[1;91m"
STATES_COLOR  = "\033[0;1m"
ENV_KEY_COLOR = "\033[0;92m"
SLICES_COLOR  = "\033[0;1m"

from inspect import getmembers

from time import gmtime, strftime

from execution import (
    CONTINUE,
    STOP,
    DEBUGGER_SHELL
)

from monitorinterface import MonitorInterface

from monitorcommon import (
    MonitorException,
    StopExecution,
    SetCurrentState,
    events
)

from scenario import Scenario, ScenarioEvent, ScenarioEventType

import copy

import traceback

def colored(color, msg):
    """ Returns a colored version of the msg string with the given color. """
    return color + msg + "\033[0;0m"

def error(msg, monitor=None, exc=None):
    """ Prints an error and throws the exception given, if given. If a monitor
        is given, its state is also printed. """
    global ERROR_COLOR

    print(colored(ERROR_COLOR, "Error: ") + msg)
    if exc:
        raise exc

def print_step(step: str) -> None:
    """ Prints a step, with, prepended with the current time. """
    global STEP_COLOR
    print(colored(STEP_COLOR, strftime("[%H:%M:%S] ", gmtime()) + step))


def list_set_elements(s: Set[State]) -> str:
    """ Makes a string from an iterable, separating its elements by a comma. """
    return ", ".join([state.name for state in s])


class SlicesByInstance:
    depth: int
    container: Union[Set[AutomatonSlice], Env]

    def __init__(self, root_slice: AutomatonSlice):
        self.depth = len(root_slice.slice_instance)

        # Turning this into a ternary annoys mypy: https://github.com/python/mypy/issues/7780
        if self.depth == 0:
            self.container = set()
        else:
            self.container = {}

        self.root_slice = root_slice
        self.add_recursive(root_slice) # FIXME probably costly as hell

    def add(self, s: AutomatonSlice, instance_tuple: SliceInstance = None, container: Any = None, depth: int = 0) -> None:
        if depth == 0:
            container = self.container

        if depth == self.depth:
            assert(isinstance(container, set))
            container.add(s)
            return

        if instance_tuple is None:
            instance_tuple = s.get_slice_instance()

        assert(isinstance(container, dict))
        if instance_tuple[0] not in container:
            container[instance_tuple[0]] = {} if depth + 1 < self.depth else set()

        self.add(s, instance_tuple[1:], container[instance_tuple[0]], depth + 1)

    def remove(self, s : AutomatonSlice, instance_tuple: SliceInstance = None, container: Any = None, depth: int = 0) -> bool:
        if depth == 0:
            container = self.container

        if depth == self.depth:
            assert(isinstance(container, set))
            container.remove(s)
            return (not container)

        if instance_tuple is None:
            instance_tuple = s.get_slice_instance()

        try:
            assert(isinstance(container, dict))
            child_container = container[instance_tuple[0]]
        except KeyError:
            #FIXME
            return False

        if self.remove(s, instance_tuple[1:], child_container, depth + 1):
            container.pop(instance_tuple[0], None)
            return (not child_container)

        return False

    def get(self, instance_tuple: SliceInstance, container : Any = None, depth: int = 0) -> Set[AutomatonSlice]:
        if container is None:
            assert depth == 0, "container is None, depth should be 0"
            container = self.container

        if depth == self.depth:
            assert(isinstance(container, set))
            return set(container)

        param = instance_tuple[0]

        res = set()

        assert(isinstance(container, dict))

        if param is None:
            for p in container:
                res.update(self.get(instance_tuple[1:], container[p], depth + 1))
        else:
            if param in container:
                res.update(self.get(instance_tuple[1:], container[param], depth + 1))

            if None in container:
                res.update(self.get(instance_tuple[1:], container[None], depth + 1))

        return res

    def add_recursive(self, s: AutomatonSlice) -> None:
        self.add(s)
        for c in s.get_children_recursive():
            self.add(c)

@dataclass
class MonitorCheckpoint:
    definitive_states: Set[State]
    watched_events: Counter[EventWithTarget]

def globals_line(p: Env) -> str:
    l = ",".join(i for i in p if i != "__builtins__")
    return "global " + l  + "\n" if l else ""

def instance_is_less_specific(i1: SliceInstance, i2: SliceInstance) -> bool:
    for i in range(len(i1)):
        if i1[i] is not None and i2[i] != i1[i]:
            return False
    return True

def instance_compatible(new_instance: SliceInstance, slices: Iterable[AutomatonSlice]) -> bool:
    for other_s in slices:
        if instance_is_less_specific(new_instance, other_s.get_slice_instance()):
            return False

    return True

class Monitor:
    """ The Monitor class is used to monitor a program against a given
        property. """
    cur_id                   : int                       = 0
    quiet                    : bool                      = False
    tmpname                  : str                       = "_r"
    step_by_step             : bool                      = False
    prop                     : Property                  = Property()
    initial_state            : State                     = dummyState
    root_slice               : AutomatonSlice            = AutomatonSlice(0)
    trigger_event_hit        : int                       = 0
    lastCheckpointId         : int                       = 0
    name                     : str                       = ""

    scenarioEventsByStateByMoment : Dict[bool, Dict[Union[State, int], Set[ScenarioEvent]]] = {False: {}, True: {}}

    # if a StopExecution is caught, drop to the debugger's shell.
    stop_is_debugger_shell   : bool                      = True

    event_manager            : EventManager
    interface                : MonitorInterface
    slices_by_instance       : SlicesByInstance
    registered_events        : Dict[int, Set[Callable]]
    checkpoints              : Dict[int, MonitorCheckpoint]
    states                   : List[State]
    states_by_name           : Dict[str, State]
    definitive_states        : Set[State]
    externToRealCheckpointId : Dict[int, int]
    watched_events           : Counter[EventWithTarget]
    updated_slices           : Dict[SliceInstance, State]
    scenario_env             : Env
    begin_state_by_trans     : Dict[Transition, State] = {}
    scenario_triggered_by_moment : Dict[bool, Set[ScenarioEvent]]
    states_involved_during_event : Dict[bool, Set[State]]
    compiled_code            : Dict[Union[Transition, TransitionEndBlock, ScenarioEvent], CodeType]
    prop_initialization      : Env

    def __init__(self, eventManager: EventManager, prop: Optional[Property] = None):
        """ This constructor expects a property to check against the monitored
            program. """

        Monitor.cur_id += 1

        self.checkpoints                = {}
        self.interface                  = MonitorInterface(self)
        self.states                     = []
        self.states_by_name             = {}
        self.registered_events          = {}
        self.definitive_states          = set()
        self.externToRealCheckpointId   = {}
        self.event_manager              = eventManager
        self.watched_events             = Counter()
        self.updated_slices             = {}
        self.monitor_id                 = Monitor.cur_id
        self.compiled_code              = {}
        self.prop_initialization        = {}

        self.scenario_triggered_by_moment = {False: set(), True: set()}
        self.states_involved_during_event = {False: set(), True: set()}

        eventManager.set_callback(self.trigger_event)

        if prop:
            self.set_property(prop)

    def checkpoint(self, cid: int = 0) -> int:
        # WARNING: cid should be a nonzero and strictly increasing number

        realCid = self.lastCheckpointId + 1

        if cid:
            self.externToRealCheckpointId[cid] = realCid


        self.checkpoints[realCid] = MonitorCheckpoint(
            definitive_states=set(self.definitive_states),
            watched_events=Counter(self.watched_events)
        )

        self.lastCheckpointId = realCid

        self.root_slice.checkpoint(realCid)
        self.event_manager.checkpoint(realCid)

        return self.lastCheckpointId

    def restore_checkpoint(self, cid: int, isRealCid: bool = False) -> None:
        # WARNING don't forget to register slices again when restoring a checkpoint

        realCid = (cid
                            if isRealCid
                            else self.externToRealCheckpointId[cid])

        c = self.checkpoints[realCid]
        self.definitive_states = set(c.definitive_states)
        self.watched_events = Counter(c.watched_events)

        self.root_slice.restore_checkpoint(realCid)
        self.slices_by_instance = SlicesByInstance(self.root_slice) # FIXME: costly as hell
        self.set_root_slice(self.root_slice)
        self.event_manager.restore_checkpoint(realCid)

        if not self.quiet:
            print("-- monitor restored --")
            self.print_state()

        self.emit(events["checkpoint_restart"], ())

    def drop_checkpoint(self, cid: int, isRealCid: bool = False) -> None:
        realCid = (cid if isRealCid else self.externToRealCheckpointId[cid])

        del self.checkpoints[realCid]
        self.root_slice.drop_checkpoint(realCid)
        #self.slices_by_instance.drop_checkpoint(realCid)
        self.event_manager.drop_checkpoint(realCid)

    def remove_checkpoint(self, cid: int) -> None:
        del self.checkpoints[cid] # FIXME: untested

    def set_quiet(self, b: bool = True) -> None:
        """ Sets the monitor quiet or not """
        self.quiet = b

    def set_property(self, prop: Property) -> None:
        """ Sets the property to check the program against. """
        self.prop = prop
        self.prepare_states()

    def scenario_bind_evt_to_state(self, is_entering: bool, scenario_evt: ScenarioEvent, state: Union[State, int]) -> None:
        if state not in self.scenarioEventsByStateByMoment[is_entering]:
            self.scenarioEventsByStateByMoment[is_entering][state] = set()

        self.scenarioEventsByStateByMoment[is_entering][state].add(scenario_evt)

    def set_scenario(self, scenario: Scenario) -> None:
        """ Sets the property to check the program against. """

        for scenario_evt in scenario.events:
            if scenario_evt.slice_policy == ScenarioEventType.ALL_SLICES:
                self.scenario_bind_evt_to_state(scenario_evt.entering, scenario_evt, ScenarioEventType.ALL_SLICES)
            elif scenario_evt.accepting is None:
                if scenario_evt.state_name:
                    self.scenario_bind_evt_to_state(
                        scenario_evt.entering,
                        scenario_evt,
                        self.states_by_name[scenario_evt.state_name]
                    )
            else:
                for state in self.states:
                    if state.accepting == scenario_evt.accepting:
                        self.scenario_bind_evt_to_state(scenario_evt.entering, scenario_evt, state)

        self.scenario_env = {}
        self.scenario_env["monitor"] = self.interface

        for (k, v) in scenario.initialization.items():
            self.scenario_env[k] = v


    def init_env(self, s: AutomatonSlice) -> None:
        """ Called internally. Prepares the environment by calling the code of
            the property's initialization block. """

        env = dict(self.prop.functions)
        env.update(self.prop_initialization)
        s.set_env(env)

        while self.tmpname in env:
            self.tmpname += "_"

    def prepare_states(self) -> None:
        """ Called internally. Maps state name -> state data for all the states
            of the property. """

        state_star_non_accepting = None
        state_star_accepting     = None

        for state in self.prop.states:
            if state.name == "*":
                if state.accepting is True or state.accepting is None:
                    state_star_accepting = state.transitions

                if state.accepting is False or state.accepting is None:
                    state_star_non_accepting = state.transitions
            else:
                self.states_by_name[state.name] = state
                self.states.append(state)
                if state.initial:
                    self.initial_state = state

        if not self.initial_state:
            if "init" not in self.states_by_name:
                error(
                    "Missing initial state. " +
                    "Your property must have an initial state named 'init'."
                )

                sys.exit(2)

            self.initial_state = self.states_by_name["init"]

        for state in self.prop.states:
            new_transitions = state.transitions

            if state.import_transitions:
                try:
                    new_transitions += self.states_by_name[state.import_transitions].transitions
                except KeyError:
                    error(
                        "Missing state " + state.import_transitions +
                        " when importing transition in state " + state.name
                    )

            if state_star_accepting and state.accepting and state_star_accepting != state.transitions:
                new_transitions += state_star_accepting

            elif not state.accepting and state_star_non_accepting:
                new_transitions += state_star_non_accepting

            for trans in state.transitions:
                self.begin_state_by_trans[trans] = state

            new_state = state._replace(transitions=new_transitions)
            self.states_by_name[state.name] = new_state
            self.states[self.states.index(state)] = new_state

    def run(self) -> None:
        """ Launches the monitor. """

        if not self.prop:
            raise MonitorException(
                "A property must be loaded into the monitor before running it."
            )

        self.prop_initialization = dict(self.prop.initialization)
        self.prop_initialization["debugger_shell"] = lambda sel: sel.debugger_shell()
        self.prop_initialization["stop_execution"] = lambda sel: sel.stop_execution()

        s = AutomatonSlice(len(self.prop.slice_bindings))
        self.init_env(s)
        self.set_root_slice(s)

        assert s.env is not None, "s.env is None in run"

        if not self.quiet:
            self.show_env("Initialization:")

        self.trigger_event(None, None)
        self.initialCheckpoint = self.checkpoint()

    def reset(self) -> None:
        self.restore_checkpoint(self.initialCheckpoint, isRealCid=True)
        if not self.quiet:
            self.print_current_state()

    def suspend(self) -> None:
        self.event_manager.suspend()

        if not self.quiet:
            print("-- monitor suspended --")

    def resume(self) -> None:
        self.event_manager.resume()

        if not self.quiet:
            print("-- monitor resumed --")

    def active_states(self) -> List[State]:
        return self.root_slice.active_states() if self.root_slice else []

    def get_event_program_target(self, event: Event, s: AutomatonSlice) -> RuntimeEventProgramTarget:
        # Events can be targeted to any program, or to specific programs.
        # The program target is:
        #  - None for any program (no particular target)
        #  - a string for any program which name is this string
        #  - a number for any specific program with this id

        if event.prgm_target:
            (prgm_name, prgm_target_param) = event.prgm_target

            if prgm_target_param:
                # FIXME: probably inefficient
                env = dict(zip(self.prop.slice_bindings, s.get_slice_instance()))
                senv = s.get_env()
                if senv is not None:
                    env.update(senv)

                if prgm_target_param.py_value is None:
                    prgm_target = None
                else:
                    prgm_target = eval(prgm_target_param.py_value, env)
            else:
                prgm_target = prgm_name
        else:
            prgm_target = None

        return prgm_target

    def unwatch_events(self, s: AutomatonSlice, oldevents: Iterable[EventWithTarget]) -> None:
        for eventWithTarget in oldevents:
            self.watched_events[eventWithTarget] -= 1
            if not self.watched_events[eventWithTarget]:
                self.event_manager.unwatch(eventWithTarget[0], eventWithTarget[1])

    def safe_execute_action(self, state: State, s: AutomatonSlice) -> bool:
        try:
            self.execute_action(state, s)
        except StopExecution:
            return True

        return False

 
    def watch_state(self, state: State, s: AutomatonSlice) -> bool:
        for trans in state.transitions:
            program_target = self.get_event_program_target(trans.event, s)
            eventWithTarget = (trans.event, program_target)

            if not self.watched_events[eventWithTarget]:
                self.event_manager.watch(trans.event, program_target)

            self.watched_events[eventWithTarget] += 1
            s.watched_events.add(eventWithTarget)

        return self.safe_execute_action(state, s)

    def unsafe_set_current_state(self, s: AutomatonSlice, new_state: State) -> bool:
        """ Called internally. Replaces current states by given new states.
            Can throw a StopExecution exception.
        """

        #print("unsafe_set_current_state", s.get_current_state().name if s.get_current_state() else None, new_state.name)

        old_state = s.get_current_state()

        if not self.quiet:
            self.updated_slices[s.get_slice_instance()] = old_state

        oldevents = s.watched_events
        s.watched_events = set()

        s.set_current_state(new_state)

        if old_state == new_state:
            stop = self.safe_execute_action(new_state, s)
        else:
            stop = self.watch_state(new_state, s)


        if old_state and old_state != dummyState:
            self.states_involved_during_event[False].add(old_state)

            if old_state.accepting:
                self.accepting_states_have_been_involved_during_event[False] = True
            else:
                self.non_accepting_states_have_been_involved_during_event[False] = True

            stop = self.trigger_scenario_events(False, old_state, old_state, new_state, s) or stop

        if new_state:
            if new_state.accepting:
                self.accepting_states_have_been_involved_during_event[True] = True
            else:
                self.non_accepting_states_have_been_involved_during_event[True] = True

            self.states_involved_during_event[True].add(new_state)
            stop = self.trigger_scenario_events(True, new_state, old_state, new_state, s) or stop

        self.emit(events['state_changed'], (s, new_state))

        if old_state and old_state != dummyState and old_state != new_state and not self.root_slice.is_state_active(old_state):
            self.unwatch_events(s, oldevents)

        return stop

    def apply_scenario_evt(self, scenario_evt: ScenarioEvent, s: Optional[AutomatonSlice], old_state: Optional[State], new_state: Optional[State]) -> bool:
        if scenario_evt not in self.compiled_code:
            self.compiled_code[scenario_evt] = self.compile_code(scenario_evt.code)

        self.scenario_env["old_state"] = old_state.name if old_state else ""
        self.scenario_env["new_state"] = new_state.name if new_state else ""
        self.scenario_env["current_slice"] = s

        try:
            exec(self.compiled_code[scenario_evt], self.scenario_env)

        except StopExecution:
            return True

        except Exception as exc:
            error(
                "Failed to execute the scenario event " +
                str(scenario_evt) + ".",
                exc
            )
            raise exc

        return False

    def trigger_scenario_events(self, moment: bool, state: State, old_state: State, new_state: State, s: AutomatonSlice) -> bool:
        stop = False

        if state not in self.scenarioEventsByStateByMoment[moment]:
            return stop

        for scenario_evt in self.scenarioEventsByStateByMoment[moment][state]:
            if scenario_evt.slice_policy == ScenarioEventType.ONE_SLICE:
                if scenario_evt in self.scenario_triggered_by_moment[moment]:
                    continue
                self.scenario_triggered_by_moment[moment].add(scenario_evt)

            elif scenario_evt.slice_policy == ScenarioEventType.ALL_SLICES:
                print("FIXME: This should not happen")
                continue

            stop = self.apply_scenario_evt(scenario_evt, s, old_state, new_state) or stop

        return stop

    def has_several_slices(self) -> bool:
        return self.root_slice.has_children()

    def get_slices(self) -> Generator[AutomatonSlice, None, None]:
        yield self.root_slice
        for s in self.root_slice.get_children_recursive():
            yield s

    def updated_slice_string(self, s: AutomatonSlice) -> str:
        try:
            begin_state = self.updated_slices[s.get_slice_instance()]
        except KeyError:
            return ""

        if begin_state:
            return " (from " + begin_state.name + ")"

        return ""

    def print_current_state(self) -> None:
        global STATES_COLOR, SLICES_COLOR
        """ Prints current states. """
        i = 1

        if self.has_several_slices():
            print_step("Current state (monitor #" + str(self.monitor_id) + "):")

            for s in self.get_slices():
                print(colored(SLICES_COLOR, "  Slice " + str(i)), "<", end="")
                print(*s.get_slice_instance(), sep=", ", end="")
                print(
                    ">: ",
                    colored(STATES_COLOR, s.get_current_state().name),
                    self.updated_slice_string(s),
                    sep=""
                )

                self.show_env_slice(s)

                i += 1

            if self.definitive_states:
                print(
                    "    Final slices: " + colored(
                        STATES_COLOR,
                        list_set_elements(self.definitive_states)
                    )
                )
        else:
            print_step(
                "Current state (monitor #" + str(self.monitor_id) + "): " + colored(
                    STATES_COLOR,
                    list_set_elements(self.definitive_states.union({self.root_slice.get_current_state()}))
                ) + self.updated_slice_string(self.root_slice)
            )

        if not self.quiet:
            self.updated_slices = {}

    def show_env_slice(self, s: AutomatonSlice, indent: str = "") -> None:
        env = s.get_env()
        assert(env is not None)
        for key in env:
            if key != "__builtins__" and not callable(env[key]):
                print(
                    "    " + indent +
                    colored(ENV_KEY_COLOR, key) + ": " +
                    str(env[key])
                )


    def show_env(self, title: str = "") -> None:
        """ Prints the current environment. """
        global ENV_KEY_COLOR

        if title:
            print_step(title)

        i = 1

        for s in self.get_slices():
            if self.has_several_slices():
                print("  Slice " + str(i))

            self.show_env_slice(s, indent="    ")
            i += 1

        print("")

    def print_state(self) -> None:
        """ Prints monitor's current state by printing current states and the
            environment. """
        self.print_current_state()
        self.show_env()

    # Returns the name of a state
    def execute_action(self, end_block: Union[TransitionEndBlock, State], s: AutomatonSlice) -> Optional[str]:
        """ Called internally. If a transition or a state contains an action,
            execute this action when it is taken / reached (respectively). """
        if not end_block.action_name:
            return None

        env = s.get_env()

        if env is not None and end_block.action_name in env:
            return env[end_block.action_name](self.interface, s)

        error(
            "Action " + end_block.action_name + " is not defined.",
            self
        )

        return None

    def to_state(self, state_name: Optional[str], point: str) -> Optional[State]:
        if not state_name:
            return None

        try:
            return self.states_by_name[state_name]
        except KeyError:
            error(
                "Error while executing the " + point +
                " point of the transition: the returned value, " +
                state_name + ", is not a state of the property."
            )
            return None

    def take_transition_point(self, s: AutomatonSlice, trans: Transition, point: str, params: Env) -> Optional[State]:
        """ Called internally. Handles the end of a transition, with eventual
            code to execute. point is either "success" or "failure", when the
            guard succeeded or failed, respectively. """

        #print("take_transition_point")

        state = None

        trans_point = getattr(trans, point)
        if trans_point:
            if trans_point.inline_action:

                # We copy the environment so that the event parameters are not kept.
                env = {}

                for (p, v) in zip(self.prop.slice_bindings, s.get_slice_instance()):
                    env[p] = v

                senv = s.get_env()

                if senv is not None:
                    env.update(senv)

                env.update(params)

                if trans_point not in self.compiled_code:
                    code = globals_line(env) + trans_point.inline_action
                    try:
                        self.compiled_code[trans_point] = self.compile_code(code)
                    except Exception as exc:
                        error(
                            "Failing to parse the " + point +
                            " point of the transition",
                            self,
                            exc
                        )

                try:
                    exec(self.compiled_code[trans_point], env)
                except Exception as exc:
                    traceback.print_exc()
                    error(
                        "An error occured when executing the action of the " + point +
                        " point of the transition",
                        self,
                        exc
                    )

                #FIXME: check usage of tmpname in the compiled code, could
                # conflict.

                state_name = env[self.tmpname]
                del env[self.tmpname]

                for (key, val) in env.items():
                    if key not in params:
                        s.set_env_key(key, val)

                state = self.to_state(state_name, point)

            state2 = self.to_state(self.execute_action(trans_point, s), point)

            state = state2 if state2 else (state if state else self.to_state(trans_point.state, point))

            if state:
                self.emit(events["transition_taken"], (trans, point, state))

            return state

        return None

    def compile_code(self, python_code: str) -> CodeType:
        """ Called internally. returns the compiled version of the code, storing
            its return value inside env[self.tmpname]. """

        code = "def " + self.tmpname + "():\n" + (
                    "\n" +
                    python_code
                ).replace("\n", "\n\t") + "\n" + self.tmpname + " = " + self.tmpname + "()\n"

        return compile(code, "<string>", "exec")

    def eval(self, val: Any) -> Any:
        return eval(val, self.root_slice.get_env())

    def compile_guard(self, trans : Transition, event: Event) -> None:
        """ Called internally. Ensures the guard of the transition is compiled.
            event is here for easing the debug of a property, and isn't
            necessary from an algorithmic point of view. """

        # if there is a guard, we compile it
        if trans in self.compiled_code or not trans.guard:
            return

        try:
            self.compiled_code[trans] = self.compile_code(trans.guard)
        except Exception as exc:
            error(
                "Failed to parse guard while handling event " +
                "\033[0;1m" + eventName(event) + "\033[0;0m in state " +
                "\033[0;1m" + self.begin_state_by_trans[trans].name + "\033[0;0m.\n" +
                "def " + self.tmpname + "():\n" + (
                    "\n" +
                    trans.guard
                ).replace("\n", "\n\t") + "\n" +
                self.tmpname + " = " + self.tmpname + "()\n",
                self,
                exc
            )

        # We compile a code in this form:
        #
        #     def _r():
        #         <user's code with returns>
        #     _r = _r()
        #
        # so we have what the user's code returned in env["_r"]
        #
        # We do this that way to work around the lack of anonymous
        # functions in python

    def apply_transition(self, s: AutomatonSlice, runtime_event: RuntimeEvent, trans: Transition, env: Env) -> Tuple[bool, Optional[State]]:
        stop = False
        next_state = None

        assert env is not None

        self.compile_guard(trans, runtime_event.formalEvent)

        params = runtime_event.parameters
        assert(isinstance(params, Dict))

        if trans in self.compiled_code:
            for (p, v) in zip(self.prop.slice_bindings, s.get_slice_instance()):
                env[p] = v

            for p in params:
                env[p] = params[p]

            try:
                exec(self.compiled_code[trans], env)
            except Exception as exc:
                error(
                    "Failed to execute guard while handling event " +
                    "\033[0;1m" + eventName(runtime_event.formalEvent) + "\033[0;0m in state "  +
                    "\033[0;1m" + str(self.begin_state_by_trans[trans]) + "\033[0;0m.",
                    self,
                    exc
                )

            guard_res = env[self.tmpname]

            if guard_res is not None:
                try:
                    next_state = self.take_transition_point(
                        s,
                        trans,
                        "success" if guard_res else "failure",
                        params
                    )
                except StopExecution:
                    stop = True
        else:
            next_state = self.take_transition_point(s, trans, "success", params)

        return (stop, next_state)

    def take_transition(self, s: AutomatonSlice, runtime_event: RuntimeEvent, trans: Transition, env: Env, begin_state: State, param_instance: SliceInstance, slices: Iterable[AutomatonSlice]) -> Tuple[bool, Optional[State]]:
        if instance_is_less_specific(param_instance, s.get_slice_instance()):
            return self.apply_transition(s, runtime_event, trans, env)

        new_s = s.fork()

        new_slice_instance = new_s.get_slice_instance()

        updated_slice_instance = tuple(
            (
                new_slice_instance[i]
                if param_instance[i] is None
                else param_instance[i]
            ) for i in range(len(self.prop.slice_bindings))
        )

        new_s.set_slice_instance(updated_slice_instance)

        if instance_compatible(updated_slice_instance, slices):
            (stop, next_state) = self.apply_transition(
                                    s,
                                    runtime_event,
                                    trans,
                                    env
                                )

            if next_state is not None: # and next_state != begin_state:
                new_s.set_current_state(next_state)
                new_s.set_env(s.get_env_copy())

            self.slices_by_instance.add(new_s)

            if next_state is not None:
                stop = self.watch_state(next_state, new_s) or stop

            next_state = new_s.get_current_state() # FIXME is this line necessary?

            if not self.quiet:
                self.updated_slices[s.get_slice_instance()] = begin_state

            if next_state.forgetSlice:
                self.definitive_states.add(new_s.get_current_state())
                self.unregister_slice(new_s)

            return (stop, None)
        else:
            new_s.remove()

        return (False, None)

    def param_subset_relevant_to_event(self, params: Env, event) -> Env:
        return {k: params[k] for k in event.get_parameter_names() if k in params}

    def apply_event_slice(self, s: AutomatonSlice, runtime_event: RuntimeEvent, program_target: RuntimeEventProgramTarget, param_instance: Tuple[Any, ...], slices: Iterable[AutomatonSlice]):
        dest_state = None

        assert s.env is not None, "s.env is None in apply_event_slice"

        env = s.get_env_copy()

        assert env is not None, "env is None in apply_event_slice"

        took_transition = False

        stop = False

        current_state = s.get_current_state()

        evt = runtime_event.formalEvent

        for trans in current_state.transitions:
            if trans.event != evt:
                # FIXME inefficient?
                continue

            took_transition = True

            (new_stop, next_state) = self.take_transition(
                s,
                runtime_event,
                trans,
                env,
                current_state,
                param_instance,
                slices
            )

            stop = new_stop or stop

            if next_state is not None:
                dest_state = next_state
                break


        if dest_state is None:
            return stop

        return self.set_current_state(s, dest_state) or stop

    def get_param_instance(self, event_params: Env) -> Tuple[Any, ...]:
        return tuple(
            (event_params[var] if var in event_params else None)
                for var in self.prop.slice_bindings
        )

    def unregister_slice(self, s: AutomatonSlice) -> None:
        self.slices_by_instance.remove(s)
        for child_s in s.get_children_recursive():
            self.unregister_slice(child_s)
        s.remove()

    def register_slice(self, s: AutomatonSlice) -> None:
        self.slices_by_instance.add(s)
        for child_s in s.get_children_recursive():
            self.slices_by_instance.add(s)

    def set_root_slice(self, s: AutomatonSlice) -> None:
        self.root_slice = s
        self.slices_by_instance = SlicesByInstance(s)

    def set_current_state(self, s: AutomatonSlice, state: State) -> bool:
        """ Called internally. Calls unsafe_set_current_state, while handling
            the SetCurrentState exception. Yep, this can loop infinitely. """

        stop = False

        try:
            stop = self.unsafe_set_current_state(s, state)
        except SetCurrentState as exc:
            return self.set_current_state(s, exc.state)

        if s.get_current_state().forgetSlice:
            # forget the slice as requested by the property for memory
            # consumption and / or correctness.

            self.definitive_states.add(s.get_current_state())
            self.unregister_slice(s)

        return stop

    def trigger_event(self, runtime_event: Optional[RuntimeEvent], program_target: RuntimeEventProgramTarget = None) -> int:
        """ Called from within a gdb breakpoint, when the event evt_name
            happens. """

        if not self.quiet:
            print(colored(SLICES_COLOR, "\nEvent:"), runtime_event)

        self.emit(events["event_applying"], (runtime_event,))

        self.scenario_triggered_by_moment[False].clear()
        self.scenario_triggered_by_moment[True].clear()

        self.accepting_states_have_been_involved_during_event = {
            False: False,
            True: False
        }

        self.non_accepting_states_have_been_involved_during_event = {
            False: False,
            True: False
        }

        self.states_involved_during_event[True].clear()
        self.states_involved_during_event[False].clear()

        stop = False

        if runtime_event:
            assert(isinstance(runtime_event.parameters, Dict))
            param_instance = self.get_param_instance(runtime_event.parameters)
            slices = self.slices_by_instance.get(param_instance)

            for s in slices:
                try:
                    new_stop = self.apply_event_slice(
                        s,
                        runtime_event,
                        program_target,
                        param_instance,
                        slices
                    )

                    stop = stop or new_stop
                except SetCurrentState as exc:
                    (new_state, stop) = (exc.state, exc.stop)
                    stop = self.set_current_state(s, new_state) or stop

            if not self.quiet:
                self.print_current_state()
        else:
            self.set_current_state(self.root_slice, self.initial_state)

        if self.scenarioEventsByStateByMoment:
            for moment in (False, True):
                if ScenarioEventType.ALL_SLICES not in self.scenarioEventsByStateByMoment[moment]:
                    continue

                sc_all_slices = self.scenarioEventsByStateByMoment[moment][ScenarioEventType.ALL_SLICES]

                if sc_all_slices:
                    for scenario_evt in sc_all_slices:
                        if scenario_evt.accepting is None and scenario_evt.state_name:
                            if self.states_by_name[scenario_evt.state_name] in self.states_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop
                        elif scenario_evt.accepting:
                            if not self.non_accepting_states_have_been_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop
                        else:
                            if not self.accepting_states_have_been_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop

        self.states_involved_during_event[False].clear()
        self.states_involved_during_event[True].clear()

        self.scenario_triggered_by_moment[False].clear()
        self.scenario_triggered_by_moment[True].clear()

        self.emit(events["event_applied"], (runtime_event,))

        if stop:
            if self.stop_is_debugger_shell:
                return DEBUGGER_SHELL
            else:
                return STOP

        if self.step_by_step:
            return DEBUGGER_SHELL

        return CONTINUE

    def emit(self, event_type: int, event_params: Tuple[Any, ...]) -> None:
        """ When something happens in the monitor like a current states change
            or a transition taking, this method is called so these events can
            be monitored.
        """

        if event_type in self.registered_events:
            for callback in self.registered_events[event_type]:
                callback(*event_params)

    def register_event(self, event_type: int, callback: Callable) -> None:
        """ Register a callback for this event type.

            See the help for MonitorInterface.register_event for more
            information.

            Notice: contrary to MonitorInterface.register_event, event_type is
            not a string but an int given by monitorcommon.event[event_name].
        """

        if event_type not in self.registered_events:
            self.registered_events[event_type] = set()

        self.registered_events[event_type].add(callback)

    def unregister_event(self, event_type: int, callback: Callable):
        """ Unregister this callback for this event type.
            See also register_event.
        """

        if event_type in self.registered_events:
            self.registered_events[event_type].discard(callback)

