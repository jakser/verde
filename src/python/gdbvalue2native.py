#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import gdb
from typing import Any, Optional, List

def gdb_value_to_native_python(gdb_value: gdb.Value, explicit_type: Optional[str] = None) -> Any:
    """ tries to convert gdb values to python values, using its native types """

    if explicit_type == "string" or explicit_type == "str":
        return gdb_value.string()
    if explicit_type == "int":
        return int(gdb_value)
    if explicit_type == "float" or explicit_type == "double":
        return float(gdb_value)

    if gdb_value is None:
        return None

    v = gdb_value
    c = gdb_value.type.strip_typedefs().code

    if   c == gdb.TYPE_CODE_PTR:
        # pointer
        try:
            return int(v)
        except:
            return v

    elif c == gdb.TYPE_CODE_ARRAY:
        # array
        try:
            v.string() # FIXME suspicious
        except:
            return v

    elif c == gdb.TYPE_CODE_STRUCT:
        # structure
        return v

    elif c == gdb.TYPE_CODE_UNION:
        # union
        return v

    elif c == gdb.TYPE_CODE_ENUM:
        # enum
        return int(v)

    elif c == gdb.TYPE_CODE_FLAGS:
        # bit flags, used for things such as status registers
        return int(v)

    elif c == gdb.TYPE_CODE_FUNC:
        # function
        return v

    elif c == gdb.TYPE_CODE_INT:
        # integer
        return int(v)

    elif c == gdb.TYPE_CODE_FLT:
        # floating point
        return float(v)

    elif c == gdb.TYPE_CODE_VOID:
        # void
        return None

    elif c == gdb.TYPE_CODE_SET:
        # Pascal set
        return set(v)

    elif c == gdb.TYPE_CODE_RANGE:
        # range, that is, an integer with bounds
        return int(v)

    elif c == gdb.TYPE_CODE_STRING:
        # string (only used for certain languages with language-defined string types; C strings are not represented this way)
        return str(v)

    #elif c == gdb.TYPE_CODE_BITSTRING
        # string of bits. It is deprecated.


    elif c == gdb.TYPE_CODE_ERROR:
        # unknown or erroneous
        return v

    elif c == gdb.TYPE_CODE_METHOD:
        # method, as found in C++ or Java
        return v

    elif c == gdb.TYPE_CODE_METHODPTR:
        # pointer-to-member-function
        return v

    elif c == gdb.TYPE_CODE_MEMBERPTR:
        # pointer-to-member
        return v

    elif c == gdb.TYPE_CODE_REF:
        # reference
        return gdb_value_to_native_python(v.referenced_value())

    elif c == gdb.TYPE_CODE_CHAR:
        # character
        return str(v)

    elif c == gdb.TYPE_CODE_BOOL:
        # boolean
        return bool(v)

    elif c == gdb.TYPE_CODE_COMPLEX:
        # complex float
        return complex(v)

    elif c == gdb.TYPE_CODE_TYPEDEF:
        # typedef to some other type
        return v

    elif c == gdb.TYPE_CODE_NAMESPACE:
        # C++ namespace
        return v

    elif c == gdb.TYPE_CODE_DECFLOAT:
        # decimal floating point
        return float(v)

    #elif c == gdb.TYPE_CODE_INTERNAL_FUNCTION
        # function internal to gdb. This is the type used to represent convenience functions.

    return v

def gdb_get_args() -> List[gdb.Value]:
    fr = gdb.newest_frame()
    b = fr.block()

    while b and b.function is None:
        b = b.superblock

    if not b:
        raise Exception("Could not find the function's block")

    res = [arg.value(fr) for arg in b if arg.is_argument]
    return res
