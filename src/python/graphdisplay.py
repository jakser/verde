#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module which displays and animates the graph of a monitor's automaton.
"""

import subprocess
import graphviz as gv
import sys
import os
import re

from typing import Any
from property import Transition, State
from monitorinterface import MonitorInterface

class GraphDisplay(object):
    def id_from_transition_point(self, trans: Transition, point: str, dest_state: State):
        return self.mi.get_transition_begin_state(trans).name + " " + trans.event.name + ":" + point + " " + dest_state.name

    def send(self, msg: str) -> None:
        try:
            self.p.stdin.write((msg + "\n").encode("utf-8"))
            self.p.stdin.flush()
        except: # if the window is closed, we receive a BrokenPipeError exception.
            pass

    def event_applied(self, runtime_event: Any) -> None:
        self.update_current_states(False);

    def update_current_states(self, clear_trans: bool = True) -> None:
        if clear_trans:
            self.send("clearTransitions()")

        current_states = [state.name for state in self.mi.get_current_states()]
        self.send("setCurrent(new Set(" + str(current_states) + "), " + ("true" if clear_trans else "false") + ")")

    def transition_taken(self, trans: Transition, point: str, state: State) -> None:
        self.send("takeTransition('" + self.id_from_transition_point(trans, point, state) + "')")

    def __init__(self, mi: MonitorInterface):
        super(self.__class__, self).__init__()
        self.mi = mi
        g = self.graph = gv.Digraph("Monitor's automaton", format='svg')

        mi.register_event("event_applied",    self.event_applied)
        mi.register_event("transition_taken", self.transition_taken)
        mi.register_event("checkpoint_restart", self.update_current_states)

        states = mi.get_states()
        self.non_accepting = []

        for state in states:
            if not state.accepting:
                self.non_accepting.append(state.name)

            g.node(state.name, id=state.name)

            for trans in state.transitions:
                for point in ("success", "failure"):
                    p = getattr(trans, point)
                    if p:
                        g.edge(
                            state.name,
                            p.state,
                            label=
                                trans.event.name + ":" +
                                point[0],
                            id=self.id_from_transition_point(trans, point, mi.monitor.states_by_name[p.state])
                        )


        self.svg = re.sub(
            r"<![^>]+>",
            "",
            re.sub(
                r"<\?[^>]+>",
                "",
                g.pipe(format="svg").decode(),
                flags=re.MULTILINE
            ),
            flags=re.MULTILINE
        )

    def start(self) -> None:
        self.p = subprocess.Popen(
            [
                "python3" if sys.version_info.major == 3 else "python",
                os.path.dirname(os.path.realpath(__file__)) + "/graphdisplayer.py"
            ],
            stdin=subprocess.PIPE
        )

        # Using sys.executable is broken in gdb.
        #(
            #sys.executable
            #if os.path.isfile(sys.executable)
            #else "python" + str(sys.version_info.major)
        #),

        self.send(str(self.mi.get_internal_id()) + "\n" + str(self.non_accepting) + "\n" + self.svg + "\nEND")
