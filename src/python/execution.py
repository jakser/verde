#!/usr/bin/env python3
# -*- coding: utf-8 -*-

CONTINUE       = 0
DEBUGGER_SHELL = 1
STOP           = 2
