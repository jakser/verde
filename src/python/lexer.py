#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A generalistic lexer to help parsing simple languages.
"""

import re
import traceback
from typing import Any, Optional, Callable, Match, Set

class ParseError(Exception):
    """ This exception should be raised whenever a parse error happens """

    def __init__(self, m: str):
        """ m is a string explaining the error """
        self.message = m
        Exception.__init__(self)

    def __str__(self):
        return repr(self.message)


class EOF(Exception):
    """ This exception should be raised whenever the end of an input is reached
        though the parser tried to read further characters """

    def __init__(self):
        Exception.__init__(self)


class Lexer:
    """ This is the main class of the module """

    def __init__(self, string: str):
        self.cur_line = 1
        self.cur_char = 1
        self.pos = 0
        self.slen = len(string)
        self.input = string

    def advance_cur_pos(self, length:int) -> None:
        """ Sets the current position to current position + length """
        for _ in range(0, length):
            if self.pos >= self.slen:
                return

            if self.input[self.pos] == '\n':
                self.cur_line += 1
                self.cur_char = 0

            self.pos += 1
            self.cur_char += 1

    def getchar(self, advance: bool = True) -> str:
        """ Returns the current character and goes forward one character """

        if self.pos >= self.slen:
            raise EOF

        cur_char = self.input[self.pos]

        if advance:
            self.advance_cur_pos(1)

        return cur_char

    def ungetchar(self) -> str:
        """ Goes back one character and returns the new current caracter"""
        self.pos -= 1

        if self.input[self.pos] == '\n':
            self.cur_line -= 1
            i = self.pos

            while i >= 0 and self.input[i] != '\n':
                self.cur_char += 1
                i += 1

        return self.input[self.pos]

    def eat_up_to_excluded(self, string: str) -> str:
        """ Finds string from the current pos, returns what is between the
            two and updates the current position (before the position of string
            in the lexer) """

        length = len(string)
        old_pos = self.pos

        while self.pos < self.slen:
            if self.input[self.pos] == string[0]:
                i = 0

                while i < length and self.input[self.pos + i] == self.input[i]:
                    i += 1

                if i == length:
                    return self.input[old_pos:self.pos]
            self.advance_cur_pos(1)

        raise self.parse_error(
            "Unexpected end of file, expecting '{}'".format(string)
        )

    def eat(self, string, error: str = "") -> None:
        """ Reads string in the lexer and updates the current position """

        length = len(string)

        while self.pos < self.slen and self.input[self.pos].strip() == '':
            if self.input[self.pos] == string[0]:
                i = 0
                while self.input[self.pos + i] == string[i]:
                    i += 1
                    if i == length:
                        self.advance_cur_pos(length)
                        return
            self.advance_cur_pos(1)

        for i in range(0, length):
            if self.pos + i >= self.slen:
                if error:
                    raise self.parse_error(error)

                raise EOF

            if self.input[self.pos + i] != string[i]:
                raise self.parse_error("Expected '" + string + "'")

        self.advance_cur_pos(length)

    def skip_white(self) -> None:
        """ Skips blank characters from the lexer """
        try:
            cur_char = self.getchar()
        except EOF:
            return

        while cur_char.strip() == '':
            try:
                cur_char = self.getchar()
            except EOF:
                return

        self.ungetchar()

    def pos_str(self) -> str:
        """ Builds the string which describes current lexer position """
        return (
            " on line " + str(self.cur_line) +
            ", character " + str(self.cur_char) + "."
        )


    def parse_error(self, reason: str) -> ParseError:
        """ creates a ParseError with the reason and the current position """
        return ParseError(reason + self.pos_str())

    def try_eat(self, string: str) -> bool:
        """ Tries to eat the string from the lexer. Returns True on success """
        try:
            self.eat(string)
            return True
        except (EOF, ParseError):
            return False

    def eat_regex(self, regex: str, skip_white: bool = True) -> Match:
        """ Tries to eat a string matching the regex from the lexer.
            Returns True on success """

        if skip_white:
            self.skip_white()

        m = re.search("^" + regex, self.input[self.pos:])

        if m is None:
            raise self.parse_error("Expected '" + regex + "'")

        self.advance_cur_pos(len(m.group(0)))

        return m

    def try_eat_regex(self, regex: str, skip_white: bool = True) -> Optional[Match]:
        try:
            m = self.eat_regex(regex, skip_white)
            return m
        except (EOF, ParseError):
            return None

    def expect_end(self) -> None:
        """ Checks whether we are at the end of input """
        if self.pos < self.slen:
            raise self.parse_error("Expected end of input")

#FIXME: types. We need to know what is the type of a trace and how to describe
# "has a write method that takes a string". Maybe File is good enough.
def out_pretty_parse_error(trace: Any, msg: str, out: Any) -> None:
    """ pretty-prints a ParseError from its traceback and its message. """

    calltrace = traceback.extract_tb(trace)
    already_printed: Set[str] = set()

    out.write(
        "\033[91mParse error:\033[0;1m " + msg + "\033[93m\n\npath: "
    )

    for call in calltrace:
        fun = call[2]

        if fun.startswith("parse_remaining"):
            fun = fun[len("parse_remaining"):]
        elif fun.startswith("parse_maybe") or fun.endswith("_content"):
            continue
        elif fun.startswith("parse"):
            fun = fun[len("parse"):]
        else:
            continue

        if fun not in already_printed:
            if already_printed:
                out.write(" > ")

            out.write(fun.replace("_", " "))
            already_printed.add(fun)

    out.write("\033[0;0m\n")

def parse_thing(lexer: Lexer, expected_msg: str, is_valid_char: Callable[[str, bool], bool]) -> str:
    thing = ""
    lexer.skip_white()
    cur_char = lexer.getchar()
    if not is_valid_char(cur_char, True):
        lexer.ungetchar()
        raise ParseError(expected_msg + lexer.pos_str())
    thing += cur_char

    try:
        cur_char = lexer.getchar()
        while is_valid_char(cur_char, False):
            thing += cur_char
            cur_char = lexer.getchar()

        lexer.ungetchar()
    except EOF:
        pass

    return thing

def parse_string_remaining(lexer: Lexer) -> str:
    identifier = ""
    while lexer.getchar(advance=False) != '"':
        c = lexer.getchar()
        if c == "\\":
            identifier += lexer.getchar()
        else:
            identifier += c
    lexer.eat('"')
    return identifier

def parse_string(lexer: Lexer) -> str:
    if lexer.try_eat('"'):
        return parse_string_remaining(lexer)

    raise ParseError("Expected a string" + lexer.pos_str())
