#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import irv_pb2 as irv
import os
import socket
import time
import struct

PROTOCOL_VERSION    = 20170613
BUS_SERVICE_VERSION = 20170613
BUS_SERVICE_NAME    = "bus"
BUS_APP_NAME        = "Verde-Python"
BUS_APP_VERSION     = "2017.06.13"

BUS_HOST = (
    os.environ["VERDE_BUS_HOST"]
        if "VERDE_BUS_HOST" in os.environ
        else "127.0.0.1"
)

BUS_PORT = (
    int(os.environ["VERDE_BUS_PORT"])
        if "VERDE_BUS_PORT" in os.environ
        else 13375
)

BUS_UNIX_SOCKET = (
    os.environ["VERDE_BUS_UNIX_SOCKET"]
        if "VERDE_BUS_UNIX_SOCKET" in os.environ
        else ("" if "VERDE_BUS_HOST" in os.environ else "/tmp/irv-bus")
)

DBG_MSGS = "VERDE_DBG_MSGS" in os.environ
TRACE_MSGS = os.environ["VERDE_TRACE_MSGS"] if "VERDE_TRACE_MSGS" in os.environ else ""
TRACE_TIME = "VERDE_TRACE_TIME" in os.environ


trace_msgs = None
if TRACE_MSGS:
    trace_msgs = open(TRACE_MSGS, "wb")
    trace_msgs.write(bytes([TRACE_TIME]))

def getMessage(sock: socket.socket, withbytes: bool = False):
    global DBG_MSGS, TRACE_TIME, trace_msgs

    try:
        msglenbytes = sock.recv(4, socket.MSG_WAITALL)
        if not msglenbytes:
            raise

        msglen = int.from_bytes(msglenbytes, byteorder='big')
        if not msglen:
            raise

        msgbytes = sock.recv(msglen, socket.MSG_WAITALL)
    except:
        return (None, None) if withbytes else None

    msg = irv.Root()
    msg.ParseFromString(msgbytes)

    if DBG_MSGS:
        print("Received: " + str(msg))

    if trace_msgs:
        if TRACE_TIME:
            trace_msgs.write(bytes(struct.pack("d", time.time())))
        trace_msgs.write(msglenbytes)
        trace_msgs.write(msgbytes)
        trace_msgs.flush()

    return (msg, msglenbytes + msgbytes) if withbytes else msg

def rootMessage(senderId: int, recipientId: int) -> irv.Root:
    msg = irv.Root()
    msg.sender = senderId
    msg.recipients.append(recipientId)
    return msg

def msgToBytes(msg: irv.Root):
    msgstr = msg.SerializeToString()
    return len(msgstr).to_bytes(4, byteorder='big') + msgstr

def send(sock: socket.socket, msg: irv.Root):
    global DBG_MSGS, trace_msgs, TRACE_TIME

    if DBG_MSGS:
        print("Sending: " + str(msg))

    mbytes = msgToBytes(msg)

    if trace_msgs:
        if TRACE_TIME:
            trace_msgs.write(bytes(struct.pack("d", time.time())))
        trace_msgs.write(mbytes)
        trace_msgs.flush()

    try:
        sock.sendall(mbytes)
    except:
        if msg.HasField("bye"):
            return

        raise

def connectToBus() -> socket.socket:
    global BUS_UNIX_SOCKET, BUS_HOST, BUS_PORT

    connected = False

    if BUS_UNIX_SOCKET:
        if DBG_MSGS:
            print("Connecting to " + BUS_UNIX_SOCKET)
        try:
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sock.connect(BUS_UNIX_SOCKET)
            if DBG_MSGS:
                print("Connected.")
            connected = True
        except FileNotFoundError:
            pass


    if not connected:
        print("Connecting to " + BUS_HOST + ":" + str(BUS_PORT))
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
        sock.connect((BUS_HOST, BUS_PORT))
        if DBG_MSGS:
            print("Connected.")


    return sock
