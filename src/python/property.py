#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module which contains the automaton model.
"""

from collections import Counter, namedtuple
from monitorcommon import events

from typing import Any, Union, NamedTuple, Optional, Tuple, List, Dict, Set, Iterable

from dataclasses import dataclass, field
from abc import ABC, abstractmethod

RuntimeEventProgramTarget = Union[int, str, None]
Env                = Dict[str, Any]
SliceInstance = Tuple[Any, ...]


#NOTE: field 'kind' is needed for event named tuples because otherwise,
#      different event type can be equal.
#      e.g. namedtuple("A", "a b")(1,2) == namedtuple("B", "a b")(1,2)
#
#      See https://bugs.python.org/issue31239

class EventParameter(NamedTuple):
    name: Optional[str]
    c_value: Union[str, int, None] = None
    pointer: bool = False
    ref: bool = False
    py_value: Optional[str] = None
    explicit_type: Optional[str] = None
    expression: Optional[str] = None
    isMacro: bool = False

    def __str__(self):
        if self.ref:
            s = "&"
        else:
            s = ""

        if self.pointer:
            s += "*"

        if self.c_value:
            s += "val " + str(self.c_value)
        elif self.py_value:
            s += 'py ' + self.py_value
        elif self.expression:
            s += 'expr ' + self.py_value

        if self.explicit_type:
            s += ":" + self.explicit_type

        return "Param " + self.name + ("<" + s + ">" if s else "")

FormalProgramTarget = Optional[Tuple[str, Optional[EventParameter]]]
EventProgramTarget = Optional[Tuple[str, Optional[EventParameter]]]

class CallEvent(NamedTuple):
    name: str
    before: bool  = True
    methodArgs: Optional[Tuple[str, ...]] = None
    parameters: Tuple[EventParameter, ...] = ()
    return_var: Optional[EventParameter] = None
    prgm_target: EventProgramTarget = None
    kind: str = "call"

class JoinEvent(NamedTuple):
    parameters: Tuple[EventParameter, ...]
    prgm_target: EventProgramTarget
    kind: str = "join"

class LeaveEvent(NamedTuple):
    parameters: Tuple[EventParameter, ...]
    prgm_target: EventProgramTarget
    kind: str = "leave"

class WatchEvent(NamedTuple):
    name: str
    access_type: int
    parameters: Tuple[EventParameter, ...] = ()
    return_var: Optional[EventParameter] = None
    prgm_target: EventProgramTarget = None
    kind: str = "watch"

class WatchEventType:
    READ   = 1
    WRITE  = 2
    ACCESS = 4

class ExceptionEvent(NamedTuple):
    name:        str
    caught:      bool
    uncaught:    bool
    parameters:  Tuple[EventParameter, ...]
    prgm_target: EventProgramTarget

    def get_parameter_names(self) -> Iterable[Optional[str]]:
        return (parameter.name for parameter in self.parameters)

Event = Union[CallEvent, WatchEvent, JoinEvent, LeaveEvent, ExceptionEvent]

class TransitionEndBlock(NamedTuple):
    inline_action          : Optional[str] = None
    state                  : Optional[str] = None
    action_name            : Optional[str] = None

class Transition(NamedTuple):
    name : Optional[str]
    event : Event
    guard : Optional[str]
    success : Optional[TransitionEndBlock]
    failure : Optional[TransitionEndBlock]

class State(NamedTuple):
    name: str

    accepting: Optional[bool]
        # True:  the state is accepting
        # False: the state is non-accepting
        # None:  this is undefined (possible for 'state *')

    action_name: Optional[str] = None
    import_transitions: Optional[str] = None
    transitions: Tuple[Transition, ...]  = ()
    initial: bool = False

    forgetSlice: bool = False
        # If True, if the state is reached, the slice will be forgotten

    def __str__(self):
        return "State <" + self.name + ">"

class Property(NamedTuple):
    name: Optional[str] = None
    initialization: Env = {}
    functions: Env = {}
    states: Tuple[State, ...] = ()
    slice_bindings: Tuple[str, ...] = ()
    default_prgm_name: Optional[str] = None
    default_class: Optional[str] = None

class RuntimeEvent:
    formalEvent : Event
    parameters : Union[List[EventParameter], Dict[str, EventParameter]]
    return_val_index : Optional[int]
    return_val_param : Optional[EventParameter]

    def __init__(self, event: Event, parameters_as_list: bool = False):
        self.formalEvent = event

        # Turning this into a ternary annoys mypy: https://github.com/python/mypy/issues/7780
        if parameters_as_list:
            self.parameters = []
        else:
            self.parameters = {}

        self.return_val_index = None
        self.return_val_param = None

    def __str__(self):
        if isinstance(self.formalEvent, CallEvent):
            return ("" if self.formalEvent.before else "after ") + self.formalEvent.name + str(self.parameters)

        return str(self.formalEvent) + ", parameters: " + str(self.parameters)

    def set_return_val_param_info(self, index: int, param: EventParameter):
        self.return_val_index = index
        self.return_val_param = param

def eventName(event: Event) -> str:
    if isinstance(event, JoinEvent):     return "#join"
    if isinstance(event, LeaveEvent):    return "#leave"

    if isinstance(event, CallEvent) or isinstance(event, WatchEvent) or isinstance(event, ExceptionEvent):
        return event.name

    raise Exception("Unhandled event type")
