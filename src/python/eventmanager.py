#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

from property import RuntimeEvent, RuntimeEventProgramTarget, Event

from abc import ABC, abstractmethod
from typing import Callable, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from monitor import Monitor

class EventManager(ABC):
    @abstractmethod
    def checkpoint(self, cid: int) -> None:
        pass

    @abstractmethod
    def restore_checkpoint(self, cid: int) -> None:
        pass

    @abstractmethod
    def drop_checkpoint(self, cid :int) -> None:
        pass

    @abstractmethod
    def watch(self, event, program_target=None) -> None:
        pass

    @abstractmethod
    def set_callback(self, callback: Callable[[RuntimeEvent, RuntimeEventProgramTarget], int]) -> None:
        pass

    @abstractmethod
    def set_monitor(self, monitor: "Monitor") -> None:
        pass

    @abstractmethod
    def unwatch(self, event: Event, program_target: RuntimeEventProgramTarget = None) -> None:
        pass

    @abstractmethod
    def suspend(self) -> None:
        pass

    @abstractmethod
    def resume(self) -> None:
        pass
