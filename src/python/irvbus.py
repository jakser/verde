#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + "/target")

import irv_pb2 as irv
import errno
import socket
import signal
import select
import irvstuff
from collections import namedtuple
import time
from typing import Dict, Tuple, NamedTuple, Any, Optional, Callable
from importlib.abc import Loader

start_time=time.time()

serversock: Optional[socket.socket] = None
unixserversock: Optional[socket.socket]  = None
busscript_trigger: Optional[Callable[..., Any]] = None

def exit(*args):
    global serversock, unixserversock

    bye = irvstuff.rootMessage(0, 0)
    bye.bye.SetInParent()

    for (cid, sock) in dict(clientSocketById).items():
        bye.recipients[0] = cid
        irvstuff.send(sock, bye)
        unregisterClient(cid, sock)

    if serversock:
        serversock.close()

    if unixserversock:
        unixserversock.close()
        os.unlink(irvstuff.BUS_UNIX_SOCKET)

    if "VERDE_BUS_PRINT_TIME" in os.environ:
        print("Verde-Bus total time:", time.time() - start_time, "sec")
    sys.exit()

signal.signal(signal.SIGINT, exit)
signal.signal(signal.SIGTERM, exit)

def byeFrom(clientId, sock):
    if not QUIET:
        print("Client #" + str(clientId) + " is leaving.")

    # https://stackoverflow.com/questions/29643295/how-to-set-a-protobuf-field-which-is-an-empty-message-in-python
    bye = irvstuff.rootMessage(0, clientId)
    bye.bye.SetInParent()

    if sock:
        irvstuff.send(sock, bye)
    else:
        sock = clientSocketById[clientId]

    unregisterClient(clientId, sock)

    bye.sender = clientId
    for (cid, sck) in clientSocketById.items():
        bye.recipients[0] = cid
        irvstuff.send(sck, bye)

    if busscript_trigger:
        assert(hasattr(busscript, "trigger"))
        busscript_trigger(businterface, "bye", clientId)


def unregisterClient(clientId, sock):
    sock.close()
    sockets.remove(sock)
    del helloMessageByClientId[clientId]
    del clientSocketById[clientId]
    del clientIdBySocket[sock]

def fatalClientError(sck, clientId, info, sender=None):
    if not sender:
        sender = clientId

    reply = irvstuff.rootMessage(0, sender)
    reply.error.info = info
    irvstuff.send(sck, reply)
    byeFrom(clientId, sck)

    print("[error] " + info + " for client " + str(clientId))


nextClientId = 1
clientSocketById: Dict[int, socket.socket] = {}
clientIdBySocket: Dict[socket.socket, int] = {}
helloMessageByClientId: Dict[int, bytes] = {}

busHello = irvstuff.rootMessage(0, 0)
busHello.hello.protocolVersion = irvstuff.PROTOCOL_VERSION
busHello.hello.serviceVersion  = irvstuff.BUS_SERVICE_VERSION
busHello.hello.serviceName     = irvstuff.BUS_SERVICE_NAME
busHello.hello.appName         = irvstuff.BUS_APP_NAME
busHello.hello.appVersion      = irvstuff.BUS_APP_VERSION
busHello.hello.nbMonitors      = -1

def setNbMonitors(n):
    global busHello
    busHello.hello.nbMonitors = n

QUIET = False

args = iter(sys.argv[1:])
while True:
    try:
        arg = next(args)
    except StopIteration:
        break

    if arg == "--quiet":
        QUIET = True

    elif arg == "--script":
        import importlib.util

        if "VERDE_TRACE_MSGS" in os.environ:
            del os.environ["VERDE_TRACE_MSGS"]

        spec = importlib.util.spec_from_file_location("busscript", next(args))

        if not spec:
            print("[error] Failed to load the bus script.")
            sys.exit()

        busscript = importlib.util.module_from_spec(spec)

        if not isinstance(spec.loader, Loader):
            print("[error] Failed to load the bus script.")
            sys.exit()

        spec.loader.exec_module(busscript)

        businterface = type("", (), {
            "shutdown": exit,
            "setNbMonitors": lambda s,n: setNbMonitors(n)
        })()

        busscript_trigger = getattr(busscript, "trigger")

        if not callable(busscript_trigger):
            print("[error] The bus script should define a trigger function.")
            sys.exit()

        busscript_trigger(businterface, "init", args)

        break

sockets = []

if irvstuff.BUS_HOST:
    serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)

    while True:
        try:
            serversock.bind((irvstuff.BUS_HOST, irvstuff.BUS_PORT))
            break
        except socket.error as exc:
            if exc.errno == errno.EADDRINUSE and "VERDE_BUS_AUTO_PORT" in os.environ:
                irvstuff.BUS_PORT += 1
            else:
                raise

    serversock.listen(10)
    sockets.append(serversock)

    if not QUIET:
        print("Listening to " + irvstuff.BUS_HOST + ":" + str(irvstuff.BUS_PORT) + "...")


if irvstuff.BUS_UNIX_SOCKET:
    #try:
        #os.unlink(irvstuff.BUS_UNIX_SOCKET)
    #except OSError:
        #if os.path.exists(irvstuff.BUS_UNIX_SOCKET):
            #raise
    if "VERDE_BUS_AUTO_UNIX_SOCKET" in os.environ:
        base_unix_socket = irvstuff.BUS_UNIX_SOCKET + "-"
        i = 1
        while os.path.exists(irvstuff.BUS_UNIX_SOCKET):
            irvstuff.BUS_UNIX_SOCKET = base_unix_socket + str(i)
            i += 1

    unixserversock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    unixserversock.bind(irvstuff.BUS_UNIX_SOCKET)
    unixserversock.listen(10)
    sockets.append(unixserversock)

    if not QUIET:
        print("Listening to " + irvstuff.BUS_UNIX_SOCKET + "...")


class BusConnection(NamedTuple):
    tcp: Tuple[str, int] # host, port
    unix: str

if busscript_trigger:
    busscript_trigger(
        businterface,
        "listening",
        BusConnection(
            tcp=(irvstuff.BUS_HOST, irvstuff.BUS_PORT),
            unix=irvstuff.BUS_UNIX_SOCKET
        )
    )

while True:
    (ready_socks, _, _) = select.select(sockets, [], [])
    for sck in ready_socks:
        if sck == serversock or sck == unixserversock:
            clientsock, _ = sck.accept()
            sockets.append(clientsock)
            if not QUIET:
                print("Incoming client...")
        else:
            (msg, msgbytes) = irvstuff.getMessage(sck, withbytes=True)
            if msg == None:
                try:
                    byeFrom(clientIdBySocket[sck], None)
                    break
                except KeyError:
                    sockets.remove(sck)
                    break # The client probably didn't say hello, so just let it leave

            try:
                clientId = clientIdBySocket[sck]
            except KeyError:
                if msg.HasField("hello"):
                    clientId = nextClientId
                    nextClientId += 1
                    clientIdBySocket[sck] = clientId
                    clientSocketById[clientId] = sck

                    if not QUIET:
                        print(
                            "Hello from client #" + str(clientId) +
                            " (" + msg.hello.serviceName + ")"
                        )

                    busHello.recipients[0] = clientId
                    irvstuff.send(sck, busHello)

                    msg.sender = clientId
                    msgstr = msg.SerializeToString()
                    msgbytes = irvstuff.msgToBytes(msg)

                    for (cid, hello) in helloMessageByClientId.items():
                        clientSocketById[cid].sendall(msgbytes)
                        sck.sendall(hello)

                    helloMessageByClientId[clientId] = msgbytes

                    if busscript_trigger:
                        busscript_trigger(businterface, "hello", msg)
                else:
                    reply = irvstuff.rootMessage(0, 0)
                    reply.error.info = "You did not say hello"
                    irvstuff.send(sck, reply)
                    sockets.remove(sck)
                    sck.close()
                break

            if msg.sender == clientId:
                for recipient in msg.recipients:
                    if recipient == 0:
                        if msg.HasField("hello"):
                            fatalClientError(sck, clientId, "Unexpected hello")

                            reply = irvstuff.rootMessage(0, clientId)
                            reply.error.info = "Unexpected hello"
                            irvstuff.send(sck, reply)
                            unregisterClient(clientId, sck)
                            print("[error] Unexpected hello from client #" + str(clientId))

                        elif msg.HasField("bye"):
                            byeFrom(clientId, sck)

                        else:
                            fatalClientError(sck, clientId, "I did not understand your message. Bye!")
                    else:
                        try:
                            clientSocketById[recipient].sendall(msgbytes)
                        except KeyError:
                            fatalClientError(sck, clientId, "Recipient " + str(recipient) + " is unknown or left.")
                        except:
                            byeFrom(recipient, sock=None)

            else:
                fatalClientError(sck, clientId, "Bad sender ID.", sender=msg.sender)
exit()
