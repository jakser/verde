#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

from typing import Optional, List, Callable, Tuple, Any

from property import CallEvent, WatchEvent, RuntimeEvent, Event, ExceptionEvent, JoinEvent, LeaveEvent, WatchEventType, RuntimeEventProgramTarget, State
from eventmanager import EventManager
import irv_pb2 as irv

from irvbusclient import IrvBusClient

import property_parser

import signal
import sys
import os
import traceback

DBG_MSGS = "VERDE_DBG_MSGS" in os.environ

def irvValueToPythonValue(v):
    if v.HasField("vDouble"):
        return v.vDouble
    if v.HasField("vFloat"):
        return v.vFloat
    if v.HasField("vSignedInt8"):
        return v.vSignedInt8
    if v.HasField("vUnsignedInt8"):
        return v.vUnsignedInt8
    if v.HasField("vSignedInt4"):
        return v.vSignedInt4
    if v.HasField("vUnsignedInt4"):
        return v.vUnsignedInt4
    if v.HasField("vString"):
        return v.vString
    if v.HasField("vBytes"):
        return v.vBytes
    if v.HasField("vBool"):
        return v.vBool
    if v.HasField("vError"):
        return "Error (" + str(v.vError.errId) + "):" + v.vError.info
    if v.HasField("values"):
        return map(irvValueToPythonValue, v.values)

    return None

@EventManager.register
class IRVEventManager(IrvBusClient):
    def getEcids(self, program_target: RuntimeEventProgramTarget) -> List[int]:
        if program_target is None:
            return self.programNameByEcid.keys()
        if isinstance(program_target, str):
            try:
                return self.ecSenderIdsByProgramName[program_target]
            except KeyError:
                return None

        if program_target in self.programNameByEcid:
            # program_target should be an ecid
            return [program_target]

        return None

    def __init__(self):
        super().__init__()

        self.pendingReset = None
        self.pendingResume = None
        self.pendingSuspend = None
        self.resumeCheckpoint = None
        self.scnSenderId = 0
        self.defaultEcid = 0
        self.ecSenderIdsByProgramName = {}
        self.programNameByEcid = {}

        self.pendingOksForRequestByEcid = {}
        self.pendingOksForReleaseByEcid = {}
        self.pendingVerdicts = []

        self.reqIdByevtRequestWithTarget  = {}
        self.evtRequestWithTargetByReqId  = {}
        self.monitorCheckpoints  = {}

        self.joinEventsByTarget  = {}
        self.leaveEventsByTarget = {}

        self.current_request_id = 1

        self.checkpoints = {}

        signal.signal(signal.SIGINT, self.exit)

    def exit(self, *args) -> None:
        self.bye()
        sys.exit()

    def set_callback(self, callback: Callable[[RuntimeEvent, RuntimeEventProgramTarget], Any]) -> None:
        self.callback = callback

    def set_monitor(self, monitor: "Monitor") -> None:
        self.connectToBus("monitor", monitor.prop.name)
        self.monitor = monitor

    # we can start the execution of a new program as soon as the monitor had a
    # chance to request events.
    # Execution should be driven by the scenario.
    # The scenario should react to verdicts.
    # The initial state used to be reached when instrumentation for the only
    # execution controller was ready.
    # The verdict does not contain the information about which execution controller
    # was instrumented or what execution controller is ready to be restored.
    # Monitor should change state when an execution controller is ready to be resumed?
    # The verdict most certainly should contain the information about "EC should be resumed".
    # The scenario should resume the execution for an EC when every monitors replied ok to resume as
    # a verdict.
    # Ok, monitors should listen to join events and they have the opportunity to
    # respond ok to resume.
    # Should we trust monitors to react to any join event?
    # Probably yes for now but this is fragile.
    # But do we have the choice?
    # All right, requirement: every monitor send a verdict on ec join
    # Verdicts received after an ec join is necessarily a reaction to an ec join.
    #   -> wrong. Their can be a join + an event in another ec at the same time...
    #   -> Verdict should contain data on what triggered it.
    # Is it the role of the scenario to wait for all monitors?
    # Is it the role of a new component?
    #   + : Scenario can remain generic
    #   - : more complexity

    def executionControllerJoined(self, sender: int, prgm_name: str) -> None:
        try:
            self.ecSenderIdsByProgramName[prgm_name].add(sender)
        except KeyError:
            self.ecSenderIdsByProgramName[prgm_name] = set([sender])

        self.programNameByEcid[sender] = prgm_name

        # FIXME: do not hardcode "defaultEcid is the first ec to come.
        # e.g. if a property applies to one program name, this should be the
        # one which is used. Maybe even ignore other programs.
        if not self.defaultEcid:
            self.defaultEcid = sender

        evts = []

        if prgm_name in self.joinEventsByTarget:
            evts.extend(self.joinEventsByTarget[prgm_name])

        if None in self.joinEventsByTarget:
            evts.extend(self.joinEventsByTarget[None])

        for evt in evts:
            self.handle_event(
                None,
                ecid=sender,
                formalEvent=evt,
                produceVerdict=False
            )

            # FIXME is it correct to have several handle_event calls and then
            # only one verdict? Probably yes, if these events are from the same
            # execution controller. Though there should be only one event
            # in this loop in a correct property.

        # new execution controllers will receive requests for the current states
        # that concern all programs with the same name.

        self.sendExistingRequestsToNewEC(prgm_name, sender)

        self.produceVerdict(ecid=sender)

    def sendExistingRequestsToNewEC(self, prgm_name: str, sender: int) -> None:
        for (requestId, (event, program_target)) in self.evtRequestWithTargetByReqId.items():
            if program_target is None or program_target == prgm_name or program_target == sender:
                self.buildAndSendEventRequest(
                    ecSenderIds=[sender],
                    requestId=requestId,
                    event=event
                )

    def executionControllerLeft(self, sender: int) -> None:
        prgm_name = self.programNameByEcid[sender]
        del self.programNameByEcid[sender]

        try:
            self.ecSenderIdsByProgramName[prgm_name].remove(sender)
        except:
            pass

        if sender == self.defaultEcid:
            self.defaultEcid = 0 # FIXME should we pick another EC here?

        try:
            del self.pendingOksForRequestByEcid[sender]
        except KeyError:
            pass

        evts = []

        if sender in self.leaveEventsByTarget:
            evts.extend(self.leaveEventsByTarget[sender])

        if prgm_name in self.leaveEventsByTarget:
            evts.extend(self.leaveEventsByTarget[prgm_name])
            #del self.leaveEventsByTarget[prgm_name] ?? FIXME

        if None in self.leaveEventsByTarget:
            evts.extend(self.leaveEventsByTarget[None])

        if self.defaultEcid == sender:
            self.defaultEcid = 0

        for evt in evts:
            self.handle_event(
                None,
                ecid=sender,
                formalEvent=evt
            )

    def sendReplyToSuspendResetResumeRequest(self, req: Tuple[int, int]) -> None:
        reply = self.rootMessage(req[0])
        reply.requestOk.requestId = req[1]
        self.send(reply)

    def run(self) -> None:
        global DBG_MSGS

        self.monitor.monitor_id = self.senderId

        self.monitor.run()
        while True:
            msg = self.getMessage()

            if not msg:
                return

            if msg.HasField("hello"):
                if msg.hello.serviceName == "scenario":
                    if DBG_MSGS:
                        print("Hello from the scenario.")
                    self.scnSenderId = msg.sender
                    self.maybeSendVerdicts()

                elif msg.hello.serviceName == "executionController":
                    if DBG_MSGS:
                        print("Hello from an execution controller.")
                    self.executionControllerJoined(msg.sender, msg.hello.programName)

            elif msg.HasField("bye"):
                if msg.sender == 0:
                    if DBG_MSGS:
                        print("Bye from the bus.")
                    return
                elif msg.sender == self.scnSenderId:
                    if DBG_MSGS:
                        print("Bye from the scenario.")
                    self.scnSenderId = 0
                elif msg.sender in self.programNameByEcid:
                    if DBG_MSGS:
                        print("Bye from an execution controller.")
                    self.executionControllerLeft(msg.sender)

            elif msg.HasField("event"):
                self.handle_event(msg.event, ecid=msg.sender)

            elif msg.HasField("checkpointRequest"):
                cid = (msg.sender, msg.checkpointRequest.cid)
                if cid in self.monitorCheckpoints:
                    reply = self.rootMessage(msg.sender)
                    reply.error.info = "This checkpoint id is already taken."
                    self.send(reply)
                else:
                    self.monitorCheckpoints[cid] = self.monitor.checkpoint()
                    reply = self.rootMessage(msg.sender)
                    reply.checkpoint.requestId = msg.checkpointRequest.requestId
                    reply.checkpoint.cid = msg.checkpointRequest.cid
                    self.send(reply)

            elif msg.HasField("checkpointRestoreRequest"):
                cid = (msg.sender, msg.checkpointRestoreRequest.cid)

                try:
                    self.monitor.restore_checkpoint(self.monitorCheckpoints[cid], True)

                    reply = self.rootMessage(msg.sender)
                    reply.checkpointRestored.requestId = msg.checkpointRestoreRequest.requestId
                    reply.checkpointRestored.cid = msg.checkpointRestoreRequest.cid
                    self.send(reply)

                except KeyError:
                        reply = self.rootMessage(msg.sender)
                        reply.error.info = "This checkpoint was not found."
                        self.send(reply)
                        traceback.print_exc()
                        print("self.monitorCheckpoints:" + str(self.monitorCheckpoints))

                except:
                        reply = self.rootMessage(msg.sender)
                        reply.error.info = "Restoring checkpoint failed."
                        self.send(reply)
                        traceback.print_exc()

            elif msg.HasField("resetRequest"):
                self.pendingReset = (msg.sender, msg.resetRequest.requestId)
                self.monitor.reset()

            elif msg.HasField("suspendRequest"):
                self.pendingSuspend = (msg.sender, msg.suspendRequest.requestId)
                self.monitor.suspend()

            elif msg.HasField("resumeRequest"):
                self.pendingResume = (msg.sender, msg.resumeRequest.requestId)
                self.monitor.resume()

            elif msg.HasField("requestOk"):
                reqId = msg.requestOk.requestId

                try:
                    self.pendingOksForRequestByEcid[msg.sender].remove(reqId);
                except KeyError:
                    try:
                        self.pendingOksForReleaseByEcid[msg.sender].remove(reqId);
                    except KeyError:
                        print("Unexpected requestOk from " + str(msg.sender) + ", requestId=" + str(reqId))
                        return

                if self.pendingResume:
                    if not self.hasPendingRequests():
                        self.sendReplyToSuspendResetResumeRequest(self.pendingResume)
                        self.pendingResume = None
                elif self.pendingReset:
                    if not self.hasPendingRequests():
                        self.sendReplyToSuspendResetResumeRequest(self.pendingReset)
                        self.pendingReset = None

                elif self.pendingSuspend:
                    if not self.hasPendingRequests():
                        self.sendReplyToSuspendResetResumeRequest(self.pendingSuspend)
                        self.pendingSuspend = None

                elif self.ecReadyForVerdict(msg.sender):
                    self.maybeSendVerdicts()
            else:
                print("Unexpected message kind " + str(msg))

    def hasPendingRequests(self) -> bool:
        for requests in self.pendingOksForRequestByEcid.values():
            if requests:
                return True

        for requests in self.pendingOksForReleaseByEcid.values():
            if requests:
                return True

        return False

    def ecReadyForVerdict(self, ecid: int) -> bool:
        return ecid in self.programNameByEcid or not (
            self.pendingOksForRequestByEcid.get(ecid, None) and
            not self.pendingOksForReleaseByEcid.get(ecid, None)
        )

    def watch(self, event: Event, program_target: RuntimeEventProgramTarget = None) -> None:
        if isinstance(event, JoinEvent):
            if program_target not in self.joinEventsByTarget:
                self.joinEventsByTarget[program_target] = set()
            self.joinEventsByTarget[program_target].add(event)
            return

        if isinstance(event, LeaveEvent):
            if program_target not in self.leaveEventsByTarget:
                self.leaveEventsByTarget[program_target] = set()
            self.leaveEventsByTarget[program_target].add(event)
            return

        if (event, program_target) in self.reqIdByevtRequestWithTarget:
            return

        self.buildAndSendEventRequest(
            ecSenderIds=self.getEcids(program_target),
            requestId=self.current_request_id,
            event=event
        )

        evtRequestWithTarget = (event, program_target)
        self.evtRequestWithTargetByReqId[self.current_request_id] = evtRequestWithTarget
        self.reqIdByevtRequestWithTarget[evtRequestWithTarget] = self.current_request_id

        self.current_request_id += 1

    def buildAndSendEventRequest(self, ecSenderIds: List[int], requestId: int, event: Event) -> None:
        if not ecSenderIds:
            return

        msg = self.rootMessage(ecSenderIds)
        msg.eventRequest.requestId = requestId

        if isinstance(event, ExceptionEvent):
            msg.eventRequest.className = event.name
            msg.eventRequest.includeCaughtExceptions = event.caught
            msg.eventRequest.includeUncaughtExceptions = event.uncaught
        else:
            name = event.name.rpartition(':')
            if name[0] == "":
                name = event.name.rpartition('.')
                if name[0] == "":
                    if self.monitor.prop.default_class:
                        msg.eventRequest.className  = self.monitor.prop.default_class

                    if isinstance(event, CallEvent):
                        msg.eventRequest.methodName = event.name
                    elif isinstance(event, WatchEvent):
                        msg.eventRequest.fieldName = event.name
                else:
                    msg.eventRequest.className = name[0]
                    if isinstance(event, CallEvent):
                        msg.eventRequest.methodName = name[2]
                    elif isinstance(event, WatchEvent):
                        msg.eventRequest.fieldName = name[2]
                    else:
                        raise Exception("This kind of event is not supported.")

            else:
                msg.eventRequest.sourceName = name[0]
                msg.eventRequest.lineNumber = int(name[2])
                msg.eventRequest.kind = irv.EventRequest.BREAKPOINT

            if not msg.eventRequest.className:
                msg.eventRequest.className = name[0]

        if isinstance(event, CallEvent):
            if event.methodArgs is not None:
                msg.eventRequest.methodArgs.arg.extend(event.methodArgs)
                msg.eventRequest.methodArgs.specified = True

        if not msg.eventRequest.kind:
            if isinstance(event, CallEvent):
                if event.before:
                    msg.eventRequest.kind = irv.EventRequest.METHOD_ENTRY
                else:
                    msg.eventRequest.kind = irv.EventRequest.METHOD_EXIT
            elif isinstance(event, WatchEvent):
                if event.access_type == WatchEventType.READ:
                    msg.eventRequest.kind = irv.EventRequest.FIELD_ACCESS
                elif event.access_type == WatchEventType.WRITE:
                    msg.eventRequest.kind = irv.EventRequest.FIELD_MODIFICATION
                else:
                    raise Exception("read AND write watchpoints are not supported for now.")
                    # FIXME setting two requests would be sufficient
            elif isinstance(event, ExceptionEvent):
                msg.eventRequest.kind = irv.EventRequest.EXCEPTION_CATCH
            else:
                raise Exception("This kind of event is not supported.")

        if hasattr(event, "return_var") and event.return_var:
            paramIrv = msg.eventRequest.parameters.add()
            paramIrv.argNumber = 0;
            msg.eventRequest.kind = irv.EventRequest.METHOD_EXIT_WITH_RETURN_VALUE

        for p in event.parameters:
            if p.py_value is None:
                paramIrv = None
                if p.expression is None:
                    param = p.name if p.c_value is None else p.c_value
                    if isinstance(param, int):
                        paramIrv = msg.eventRequest.parameters.add()
                        paramIrv.argNumber = param
                    elif param == "this":
                        paramIrv = msg.eventRequest.parameters.add()
                        paramIrv.thisObject = True
                    elif param == "#ecid":
                        pass
                    else:
                        paramIrv = msg.eventRequest.parameters.add()
                        paramIrv.name = param
                        if p.isMacro:
                            paramIrv.scope = irv.ValueRequest.MACRO
                else:
                    paramIrv = msg.eventRequest.parameters.add()
                    paramIrv.expression = p.expression

                if p.pointer:
                    paramIrv.dereference = 1
                elif p.ref:
                    paramIrv.dereference = -1

        self.send(msg)
        self.setPendingRequestOk(ecSenderIds, requestId)


    def setPendingRequestOk(self, ecSenderIds: List[int], reqId: int) -> None:
        for ecid in ecSenderIds:
            try:
                self.pendingOksForRequestByEcid[ecid].add(reqId)
            except KeyError:
                self.pendingOksForRequestByEcid[ecid] = set([reqId])

    def setPendingOkForRelease(self, ecSenderIds: int, requestId: int) -> None:
        for ecid in ecSenderIds:
            try:
                self.pendingOksForReleaseByEcid[ecid].add(requestId)
            except KeyError:
                self.pendingOksForReleaseByEcid[ecid] = set([requestId])

    def unwatchAllExceptJoinAndLeave(self) -> None:
        ecSenderIds = self.programNameByEcid.keys()
        msg = self.rootMessage(ecSenderIds)
        msg.eventRelease.requestId = 0 # Clear all events
        self.send(msg)
        self.setPendingOkForRelease(ecSenderIds, 0) # FIXME 0 will do but is not ideal

        #self.joinEventsByTarget  = {}
        #self.leaveEventsByTarget = {}

        self.reqIdByevtRequestWithTarget = {}
        self.evtRequestWithTargetByReqId = {}

    def unwatch(self, event: Event, program_target: RuntimeEventProgramTarget) -> None:
        #print("unwatching " + str(event))

        try:
            if isinstance(event, JoinEvent):
                self.joinEventsByTarget[program_target].remove(event)
                if not self.joinEventsByTarget[program_target]:
                    del self.joinEventsByTarget[program_target]
                return

            if isinstance(event, LeaveEvent):
                self.leaveEventsByTarget[program_target].remove(event)
                if not self.leaveEventsByTarget[program_target]:
                    del self.leaveEventsByTarget[program_target]
                return
        except KeyError:
            return

        idEvent = (event, program_target)
        try:
            reqId = self.reqIdByevtRequestWithTarget[idEvent]
        except KeyError:
            return

        del self.reqIdByevtRequestWithTarget[idEvent]
        del self.evtRequestWithTargetByReqId[reqId]

        ecSenderIds = self.getEcids(program_target)

        if ecSenderIds:
            # Execution controller(s) could have left, so sending messages to
            # them is pointless and is a violation of the protocol that gets
            # us kicked out.

            msg = self.rootMessage(ecSenderIds)
            msg.eventRelease.requestId = reqId
            self.send(msg)

            self.setPendingOkForRelease(ecSenderIds, reqId)

    def checkpoint(self, cid: int) -> None:
        self.checkpoints[cid] = set(self.evtRequestWithTargetByReqId.values())

    def restore_checkpoint(self, cid: int) -> None:
        evtRequestWithTargets = self.checkpoints[cid]

        self.unwatchAllExceptJoinAndLeave()

        for (evt, target) in evtRequestWithTargets:
            self.watch(evt, target)

    def suspend(self) -> None:
        self.resumeCheckpoint = set(self.evtRequestWithTargetByReqId.values())
        self.unwatchAllExceptJoinAndLeave()

    def resume(self) -> None:
        if self.resumeCheckpoint:
            for (evt, target) in self.resumeCheckpoint:
                self.watch(evt, target)

            self.resumeCheckpoint = None

    def drop_checkpoint(self, cid: int) -> None:
        del self.checkpoints[cid]

    def handle_event(self, irvEvent: irv.Event, ecid: int, formalEvent: Optional[Event] = None, produceVerdict=True) -> None:
        #print("Handling event: " + str(irvEvent) + " " + str(formalEvent))
        program_target = ecid

        if irvEvent:
            if formalEvent is None:
                try:
                    (formalEvent, program_target) = self.evtRequestWithTargetByReqId[irvEvent.requestId]
                except KeyError as exc:
                    if irvEvent.requestId in self.pendingOksForReleaseByEcid.get(ecid, {}):
                        # This execution controller has been asked to release
                        # an event and we haven't received a reply for this
                        # request.
                        # Let's ignore this event by sending a Void
                        # verdict to the scenario.

                        reply = self.rootMessage(self.scnSenderId)
                        reply.verdict.accepting = irv.Verdict.VOID
                        reply.verdict.fromEC    = ecid
                        self.send(reply)
                    else:
                        print("WARNING: Received an unrequested event from ecid " + str(ecid) + ", requestId=" + str(irvEvent.requestId))
                    return
            runtimeparameters = iter(irvEvent.parameters)
            #print("Parameters: ", formalEvent.parameters,  irvEvent.parameters)

        else:
            # For Join and Leave event. There can't be runtime parameters
            # FIXME error handling
            runtimeparameters = None

        re = RuntimeEvent(formalEvent)
        re.parameters = {}

        if hasattr(formalEvent, "return_var") and formalEvent.return_var:
            re.parameters[formalEvent.return_var.name] = irvValueToPythonValue(next(runtimeparameters))

        formalparameters  = iter(formalEvent.parameters)

        while True:
            try:
                fp = next(formalparameters)
            except StopIteration:
                break

            param = fp.name if fp.c_value is None else fp.c_value

            if param == "#ecid":
                re.parameters[fp.name] = ecid
            elif fp.py_value:
                re.parameters[fp.name] = self.monitor.eval(fp.py_value.replace("#ecid", str(ecid)))
                # FIXME:
                # This does not reflect the value in the specific slice in the monitor.
                # However, this is a circular problem: assignment to a slice may depend
                # on this very value. So we seem out of luck here.
            else:
                try:
                    re.parameters[fp.name] = irvValueToPythonValue(next(runtimeparameters))
                except TypeError as exc:
                    if runtimeparameters is None:
                        print(
                            "ERROR: We did not expect having to retrieve a "
                            "parameter from the program for a join "
                            "or leave event.\n"
                            "Parameter: " + str(fp)
                        )
                    else:
                        print(
                            "ERROR: We are having difficulties retrieving value"
                            "from the program.\n"
                            "Parameter: " + str(fp)
                        )

                    raise exc

        # FIXME handle return_var

        stop = self.callback(re, program_target)

        if produceVerdict:
            self.produceVerdict(ecid=ecid)

    def produceVerdict(self, ecid: int) -> None:
        states = self.monitor.active_states()

        accepting = True
        initial = False
        for state in states:
            if state.initial:
                initial = True

            if not state.accepting:
                accepting = False
                break

        msg = self.rootMessage()

        msg.verdict.accepting  = irv.Verdict.YES if accepting else irv.Verdict.NO
        msg.verdict.initial    = initial
        msg.verdict.fromEC     = ecid

        for state in states:
            msg.verdict.state.extend([state.name])

        self.maybeSendVerdict(msg, ecid)

    def maybeSendVerdict(self, msg: irv.Root, ecid: int):
        self.pendingVerdicts.append(msg)

        if self.ecReadyForVerdict(ecid):
            self.maybeSendVerdicts()

    def maybeSendVerdicts(self) -> None:
        if self.scnSenderId:
            newPendingVerdicts = []
            for msg in self.pendingVerdicts:
                if self.pendingOksForRequestByEcid.get(msg.verdict.fromEC, None):
                    newPendingVerdicts.append(msg)
                else:
                    msg.recipients.append(self.scnSenderId)
                    self.send(msg)

            self.pendingVerdicts = newPendingVerdicts
