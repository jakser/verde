#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import gdbbreakpoint as breakpoint
from property import Event, RuntimeEvent, CallEvent, eventName
from eventmanager import EventManager
from typing import Dict, List

from functools import partial
import execution
import gdb

def is_before(event: Event) -> bool:
    return (not isinstance(event, CallEvent)) or event.before


@EventManager.register
class GDBEventManager:
    bp_by_before_and_evt_name: Dict[bool, Dict[str, breakpoint.PendingBreakpoint]]
    evt_by_before_and_evt_name: Dict[bool, Dict[str, Event]]
    checkpoints: Dict[int, List[Event]]

    def __init__(self):
        self.bp_by_before_and_evt_name  = {True:{}, False:{}}
        self.evt_by_before_and_evt_name = {True:{}, False:{}}
        self.checkpoints = {}
        self.monitor = None

    def set_callback(self, callback):
        self.callback = callback

    def suspend(self) -> None:
        raise Exception("Not implemented")

    def resume(self) -> None:
        raise Exception("Not implemented")

    def set_monitor(self, monitor):
        self.monitor = monitor

    def enable_breakpoint(self, event):
        beforeentry = self.bp_by_before_and_evt_name[is_before(event)]

        if eventName(event) in beforeentry:
            breakpoint.enable(beforeentry[eventName(event)])
        else:
            beforeentry[eventName(event)] = breakpoint.on(event, self.callback)

    def watch(self, event, program_target=None):
        self.enable_breakpoint(event)

        beforeentry = self.evt_by_before_and_evt_name[is_before(event)]
        try:
            b = beforeentry[eventName(event)]
        except KeyError:
            b = set()
            beforeentry[eventName(event)] = b

        b.add(event)

    def unwatch(self, event, program_target=None):
        l = self.evt_by_before_and_evt_name[is_before(event)][eventName(event)]
        l.remove(event)

        if not l:
            breakpoint.disable(self.bp_by_before_and_evt_name[is_before(event)][eventName(event)])

    def get_event_list(self) -> List[Event]:
        l = []

        for b in (True, False):
            bentry = self.evt_by_before_and_evt_name[b]

            for eventList in bentry.values():
                l += eventList

        return l

    def checkpoint(self, cid: int) -> None:
        self.checkpoints[cid] = self.get_event_list()

    def disable_bps(self) -> None:
        for b in (True, False):
            bentry = self.bp_by_before_and_evt_name[b]

            for (name, bp) in bentry.items():
                try:
                    breakpoint.disable(bp)
                except:
                    pass

        #self.bp_by_before_and_evt_name = {True:{}, False:{}}

    def restore_checkpoint(self, cid: int) -> None:
        self.disable_bps()
        self.evt_by_before_and_evt_name = {True:{}, False:{}}

        checkpoint = self.checkpoints[cid]

        for event in checkpoint:
            self.watch(event)

    def drop_checkpoint(self, cid: int) -> None:
        del self.checkpoints[cid]
