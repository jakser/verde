#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + "/target")

import socket
import irv_pb2 as irv
from irvstuff import *
from typing import Optional, Union, List

class IrvBusClient:
    def __init__(self):
        self.senderId = 0
        self.connected = False

    def rootMessage(self, recipientIds : Optional[Union[List[int], int]] = None) -> irv.Root:
        msg = irv.Root()
        msg.sender = self.senderId

        if recipientIds is not None:
            try:
                msg.recipients.extend(recipientIds)
            except TypeError:
                msg.recipients.append(recipientIds)

            if not msg.recipients:
                raise Exception("Creating a message without recipients")

        return msg

    def send(self, msg: irv.Root) -> None:
        global DBG_MSGS
        if DBG_MSGS:
            print("Sending: " + str(msg))

        msgstr = msg.SerializeToString()
        try:
            self.sock.sendall((len(msgstr)).to_bytes(4, byteorder='big') + msgstr)
        except BrokenPipeError:
            print("Disconnected from the bus.")
            return

    def getMessage(self) -> Optional[irv.Root]:
        global DBG_MSGS

        b = self.sock.recv(4, socket.MSG_WAITALL)

        if not b:
            return None

        msglen = int.from_bytes(b, byteorder='big')

        if not msglen:
            return None

        msg = irv.Root()
        msg.ParseFromString(self.sock.recv(msglen, socket.MSG_WAITALL))

        if DBG_MSGS:
            print("Received: " + str(msg))

        return msg

    def bye(self) -> None:
        bye = rootMessage(self.senderId, 0)
        bye.bye.SetInParent()

        try:
            self.send(bye)
            self.sock.close()
        except:
            pass


    def handleHello(self, serviceName: str, programName: str  = ""):
        global BUS_SERVICE_VERSION
        global BUS_APP_NAME
        global BUS_APP_VERSION
        global PROTOCOL_VERSION
        global DBG_MSGS

        msg = irv.Root()
        msg.recipients.append(0)
        msg.hello.serviceName     = serviceName
        msg.hello.serviceVersion  = BUS_SERVICE_VERSION
        msg.hello.appName         = BUS_APP_NAME
        msg.hello.appVersion      = BUS_APP_VERSION
        msg.hello.protocolVersion = PROTOCOL_VERSION

        msg.hello.programName     = programName

        self.send(msg)

        expectedHello = self.getMessage();

        if not expectedHello:
            raise Exception("Unable to get a message")

        if not expectedHello.HasField("hello"):
            raise Exception("Unexpected packet, expected Hello")

        self.senderId = expectedHello.recipients[0];

        if "VERDE_PRINT_ECID" in os.environ:
            print("Sender ID: " + str(self.senderId))

        if DBG_MSGS:
            print("Hello from the bus:" + str(expectedHello.hello));

        self.connected = True

    def connectToBus(self, serviceName: str, programName: str = "") -> None:
        if self.connected:
            return

        if DBG_MSGS:
            print("Connecting to the bus…")

        self.sock = connectToBus()
        self.handleHello(serviceName, programName);

    def unexpectedMessage(self, irvMessage: irv.Root, expected: str) -> None:
        print("Unexpected message, expected " + expected + ". Message: " + str(irvMessage))
