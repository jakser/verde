#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2018-2019 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from busclient import BusClient

from collections import Counter

class Scenario(VerdeBusClient):
    def __init__(self):
        self.expectedVerdicts = {}
        self.receivedVerdicts = Counter()
        self.knownMonitors = set()
        self.connectToBus("scenario")

    def run(self) -> None:
        while True:
            msg = self.getMessage()

            if msg.HasField("bye"):
                try:
                    self.knownMonitors.remove(msg.sender)
                except KeyError:
                    pass

            elif msg.HasField("hello"):
                serviceName = msg.hello.serviceName
                if serviceName == "executionController":
                    self.expectedVerdicts[(msg.sender, 0)] = len(self.knownMonitors)
                elif serviceName == "monitor":
                    self.knownMonitors.add(msg.sender)

                    for ecid in self.expectedVerdicts:
                        self.expectedVerdicts[ecid] += 1

            elif msg.HasField("verdict"):
                ec = msg.verdict.fromEC
                self.receivedVerdicts[ec] += 1
                self.maybeReact(ec)

            elif msg.HasField("expectedVerdicts"):
                self.expectedVerdicts[msg.sender] = msg.expectedVerdicts.number
                self.maybeReact(msg.sender)

    def maybeReact(self, ecid: int) -> None:
        if ecid in self.expectedVerdicts and self.receivedVerdicts[ecid] == self.expectedVerdicts[ecid]:
            del self.expectedVerdicts[ecid]
            del self.receivedVerdicts[ecid]

            reply = self.rootMessage(ecid)
            reply.resumeExec.suspend = False
            self.send(reply)

scn = Scenario()
scn.run()
