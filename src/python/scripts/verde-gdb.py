#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import functools
import gdb

if __name__ == "__main__":
    import signal
    import sys

    def signal_handler(signal: int, frame: any):
        gdb.execute("quit")
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)

    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))

import gdbbreakpoint as breakpoint
from gdbcontroller import GDBDebuggerController
from property import RuntimeEvent, EventParameter, Event
from gdbvalue2native import gdb_get_args

def val_to_expected_val(p: EventParameter, val: gdb.Value):
    #FIXME copy paste
    if p.pointer:
        val = val.dereference()
    elif p.ref:
        val = val.address

    return val


class GdbAdapter:
    def __init__(self):
        self.eventsCallbacksToBreakpoints = dict();
        self.checkpoints = {}
        controller = GDBDebuggerController(self)

    # This method adds necessary instrumentation for the given event, and
    # maps this event to the given callback.
    def instrument(self, event: Event, f):
        b = breakpoint.on(event, f, parameters_as_list=True)
        key = (event, f)
        self.eventsCallbacksToBreakpoints[key] = b
        return key

    def checkpoint(self, cid: int):
        self.checkpoints[cid] = set(self.eventsCallbacksToBreakpoints.keys())

    def restore_checkpoint(self, cid: int):
        for b in self.eventsCallbacksToBreakpoints.values():
            b.disable()

        self.eventsCallbacksToBreakpoints = {}

        for (event, f) in self.checkpoints[cid]:
            self.instrument(event, f)

    def release(self, key: any):
        try:
            b = self.eventsCallbacksToBreakpoints[key]
        except:
            print("WARNING: release: did not find specified PointRequest.");
            return;

        b.disable()
        del self.eventsCallbacksToBreakpoints[key]

if __name__ == "__main__":
    adapter = GdbAdapter()
