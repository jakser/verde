#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module which makes a bridge between the new modular verde-scala and the monitor of verde
"""

from __future__ import print_function

import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))) + "/target")

from irveventmanager import IRVEventManager
from lexer import ParseError, out_pretty_parse_error
import property_parser
import monitor

if len(sys.argv) < 2:
    sys.exit(1)

f = open(sys.argv[1], "r", encoding='utf-8')
try:
    prop = property_parser.parse(
        f.read(),
        default_prgm_name=(
            os.environ["VERDE_DEFAULT_PRGM_NAME"]
            if "VERDE_DEFAULT_PRGM_NAME" in os.environ
            else None
        )
    )


    f.close()

    evMgr = IRVEventManager()
    m = monitor.Monitor(evMgr)
    prop = prop._replace(name = sys.argv[1])
    if len(sys.argv) == 3:
        m.set_quiet(bool(sys.argv[2]))

    m.set_property(prop)
    evMgr.set_monitor(m)
    evMgr.run()

except ParseError as exc:
    _, _, trace = sys.exc_info()
    f.close()
    out_pretty_parse_error(trace, exc.message, sys.stderr)
    print("The property could not be parsed")
