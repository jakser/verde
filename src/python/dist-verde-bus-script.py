#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import os
import sys
import subprocess
import re
from typing import Any, List, Set, Optional, Iterable
from dataclasses import dataclass
import irv_pb2 as irv

bin_folder = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + "/bin"

@dataclass
class PropertyParameter:
    file: str
    verbose: Optional[bool]

@dataclass
class ProgramParameter:
    name: str
    dbg: str
    argv: List[str]
    wd: str
    dbgopts: List[str]

seenmonitors: int = 0
seenprograms: int = 0
properties: List[PropertyParameter] = []
scn: str = bin_folder + "/../scenarios/stopexec.js"
programs: List[ProgramParameter] = []
executionControllers: Set[int] = set()
scnRunning: bool = False

verboseProperties: int = 0
noneVerboseProperties: int = 0

def trigger(bus, event:str, p: Any = None):
    global bin_folder, seenmonitors, seenprograms, properties, scn, programs, executionControllers, scnRunning
    global verboseProperties, noneVerboseProperties
    autoVerbose = None

    if event == "init":
        assert(isinstance(p, Iterable))
        args = iter(p)

        try:
            while True:
                arg = next(args)

                if arg == "--verbose-properties":
                    autoVerbose = True

                elif arg == "--prop":
                    lastproperty = PropertyParameter(next(args), autoVerbose)
                    properties.append(lastproperty)
                    if autoVerbose == None:
                        noneVerboseProperties += 1
                    elif autoVerbose:
                        verboseProperties += 1

                elif arg == "--scn":
                    scn = next(args)

                elif arg == "--prgm":
                    name = next(args)

                    lastprgm = ProgramParameter(
                        name=name,
                        dbg="",
                        argv=[],
                        wd=".",
                        dbgopts=[]
                    )

                    programs.append(lastprgm)

                elif arg == "-ex":
                    lastprgm.dbgopts.extend(["-ex", next(args)])

                elif arg == "-dbg":
                    lastprgm.dbg = next(args)

                elif arg == "-arg":
                    lastprgm.argv.append(next(args))

                elif arg == "-wd":
                    lastprgm.wd = next(args)

                elif arg == "-verbose":
                    v = lastproperty.verbose

                    if v == None:
                        noneVerboseProperties -= 1

                    if not v:
                        verboseProperties += 1

                    lastproperty.verbose = True

                elif arg == "-quiet":
                    v = lastproperty.verbose

                    if v == None:
                        noneVerboseProperties -= 1

                    if v:
                        verboseProperties -= 1

                    lastproperty.verbose = False

                else:
                    print("Unrecognized argument " + arg)
                    bus.shutdown()

        except StopIteration:
            pass

        bus.setNbMonitors(len(properties))

    elif event == "listening":
        assert(p is not None and hasattr(p, "tcp") and hasattr(p, "unix"))
        (_, port) = p.tcp
        os.environ["VERDE_BUS_PORT"] = str(port)
        os.environ["VERDE_BUS_UNIX_SOCKET"] = p.unix

        for program in programs:
            print("Program:", program)

            dbg = program.dbg

            if not dbg:
                print("The debugger must be given for this program")
                bus.shutdown()

            if dbg == "jdb" or dbg == "gdb" or dbg == "checkpointed-jdb":
                dbg = bin_folder + "/verde-" + dbg

            cmd = [dbg] + program.dbgopts + [program.name] + program.argv

            if len(programs) > 1:
                subprocess.Popen([bin_folder + "/verde-terminal"] + cmd, cwd=program.wd)
            else:
                subprocess.Popen(cmd, cwd=program.wd)

        mon = bin_folder + "/verde-monitor"

        for prop in properties:
            print("Property:", prop.file)
            v = prop.verbose

            if verboseProperties > 1 and v:
                os.spawnlp(os.P_NOWAIT, bin_folder + "/verde-terminal", "verde-terminal", mon, prop.file)
            else:
                if v == True or (v == None and (noneVerboseProperties + verboseProperties == 1)):
                    subprocess.Popen([mon, prop.file])
                else:
                    subprocess.Popen([mon, prop.file, "quiet"])

        print("Scenario:", scn)
        verdescn = bin_folder + "/verde-scenario"
        os.spawnlp(os.P_NOWAIT, verdescn, verdescn, scn)
        scnRunning = True

    elif event == "hello":
        assert(isinstance(p, irv.Root))
        if p.hello.serviceName == "executionController":
            executionControllers.add(p.sender)

    elif event == "bye":
        try:
            assert(isinstance(p, int))
            executionControllers.remove(p)

            if not executionControllers and len(programs) > 0:
                bus.shutdown()
        except KeyError:
            pass
