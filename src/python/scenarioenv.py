#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import context
from context import verde_ctx
import gdb
import breakpoint as bpmodule
from functools import partial
from monitorcommon import StopExecution

from typing import Callable, Optional, Any

WATCHPOINT_READ   = gdb.WP_READ
WATCHPOINT_WRITE  = gdb.WP_WRITE
WATCHPOINT_ACCESS = gdb.WP_ACCESS

class Breakpoint(bpmodule.BaseBreakpoint):
        def __init__ (self, spec: str, is_watchpoint: bool, callback: Callable[[], Any], wp_class: Optional[int] = None):
            bptype = gdb.BP_WATCHPOINT if is_watchpoint else gdb.BP_BREAKPOINT
            self.original_spec = spec
            gdb.Breakpoint.__init__(self, spec, bptype, wp_class, internal=True)
            self.silent = True
            self.callback = callback

        def trigger(self) -> bool:
            self.callback()
            return False

class PendingBreakpoint:
    def __init__(self, spec: str, is_watchpoint: bool, callback: Callable[[], None], wp_class: Optional[int] = None):
        self.bp = None
        bpmodule.after_breakpoint(partial(self.__create, spec, is_watchpoint, callback, wp_class))

    def __create(self, spec: str, is_watchpoint: bool, callback: Callable[[], None], wp_class: Optional[int] = None) -> None:
        self.bp = Breakpoint(spec, is_watchpoint, callback, wp_class)

    def delete(self) -> None:
        bpmodule.after_breakpoint(self.__delete)

    def __delete(self) -> None:
        if self.bp is not None:
            self.bp.delete()
            self.bp = None


class PendingCheckpoint:
    def __init__(self):
        self.cid = -1
        self.deleted = False
        bpmodule.after_breakpoint(self.__create)

    def __create(self) -> None:
        if not self.deleted:
            self.cid = context.checkpoint()

    def drop(self) -> None:
        self.deleted = True
        if self.cid != -1:
            context.drop_checkpoint(self.cid)

    def restore(self, stop: bool = False) -> Callable[[], None]:
        if self.cid == -1:
            raise Exception("Cannot restore this checkpoint: not ready!")

        p = partial(context.restore_checkpoint, self.cid, stop=stop)
        bpmodule.after_breakpoint(p)
        return p


def watchpoint(spec: str, access_type: Optional[int], callback: Callable[[], None]) -> PendingBreakpoint:
    return PendingBreakpoint(
        spec=spec,
        is_watchpoint=True,
        wp_class=access_type,
        callback=callback
    )


def breakpoint(spec: str, callback: Callable[[], None]) -> PendingBreakpoint:
    return PendingBreakpoint(
        spec=spec,
        is_watchpoint=False,
        callback=callback
    )


def remove_breakpoint(breakpoint: PendingBreakpoint) -> None:
    breakpoint.delete()


def remove_watchpoint(watchpoint: PendingBreakpoint) -> None:
    watchpoint.delete()

def checkpoint_get_id(c: PendingCheckpoint) -> int:
    return c.cid

def checkpoint() -> PendingCheckpoint:
    return PendingCheckpoint()


def restore_checkpoint(checkpoint, stop=False) -> Callable[[], None]:
    return checkpoint.restore(stop=stop)


def drop_checkpoint(checkpoint: PendingCheckpoint) -> None:
    checkpoint.drop()

def after_breakpoint(f: Callable[[], Any]) -> None:
    bpmodule.after_breakpoint(f)

def suspend_execution(msg: str = "The scenario suspended the execution") -> None:
    raise StopExecution(msg)
