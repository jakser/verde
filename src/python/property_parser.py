#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A parser for the Extended Automaton-Based Description Language.

    The main function of this module is parse(string). It Returns an AST of the
    property.
"""

import re

from lexer import Lexer, ParseError, EOF, parse_thing, parse_string, parse_string_remaining

from typing import Union, Callable, Tuple, Optional, List

from property import (
    FormalProgramTarget,
    TransitionEndBlock,
    EventParameter,
    Event,
    CallEvent,
    WatchEvent,
    ExceptionEvent,
    JoinEvent,
    LeaveEvent,
    WatchEventType,
    Transition,
    State,
    Env,
    Property
)


def is_valid_numeric_char(char: str, first_char: bool) -> bool:
    return char.isnumeric()

def is_valid_identifier_char(char:str , first_char: bool) -> bool:
    """ Checks if the character is a valid identifier character.

        char       -- the character to check
        first_char -- whether if char is the first character of an identifier
    """

    return (
        char == "_" or (
            (char.isalpha() or char == "<") if first_char else (char.isalnum() or char == ":" or char == "." or char == ">")
            # We should allow <init> for Java constructors
        )
    )

def is_valid_function_name(char: str, first_char: bool) -> bool:
    return char == ":" or char == "<" or char == "$" or char == ">" or is_valid_identifier_char(char, first_char)

def parse_comments(lexer: Lexer) -> None:
    lexer.skip_white()
    if lexer.try_eat("#"):
        try:
            while lexer.getchar() != '\n':
                pass
            parse_comments(lexer)
        except EOF:
            pass

def parse_identifier(lexer: Lexer) -> str:
    """ Parses an identifier from the lexer """
    return parse_thing(lexer, "Expected a valid identifier", is_valid_identifier_char)

def parse_function_name(lexer: Lexer) -> str:
    """ Parses an identifier from the lexer """

    if lexer.try_eat('"'):
        return parse_string_remaining(lexer)

    return parse_thing(lexer, "Expected a valid function name", is_valid_function_name)

# Removes extra indentation from a block of code as much as possible
def optimize_indentation(code: str) -> str:
    code = "\n" + code
    beginings = re.finditer(r"^[\s]+", code)

    min_begining = None

    for begining in beginings:
        begining_line = begining.group(0)
        if not min_begining or len(begining_line) < len(min_begining):
            min_begining = begining_line

    if min_begining:
        return code.replace(min_begining, "\n").strip()

    return code.strip()

def parse_python_code(lexer: Lexer) -> str:
    """ Returns a python code from the lexer """


    res = ""

    # Looks like an infinite loop, but it's not: an exception will be raised if
    # the end of file is reached before the end of the block of code
    while True:
        cur_char = lexer.getchar()
        while cur_char != "\n":
            res += cur_char
            cur_char = lexer.getchar()

        while cur_char.strip() == "":
            res += cur_char
            cur_char = lexer.getchar()

        if cur_char == "}":
            while lexer.ungetchar() != "\n":
                pass

            return optimize_indentation(res.rstrip()) + "\n"
        else:
            res += cur_char


def parse_braces_python_code(lexer: Lexer, brace_already_eaten: bool = False) -> str:
    """ Parses a python code surrounded by braces from the lexer """

    if not brace_already_eaten:
        lexer.eat("{")

    cur_char = lexer.getchar()

    while cur_char != "\n" and cur_char.strip() == "":
        cur_char = lexer.getchar()

    if cur_char == "}":
        python_code = ""
    else:
        lexer.ungetchar()

        lexer.eat("\n")

        python_code = parse_python_code(lexer)

        lexer.eat("\n")
        lexer.eat("}")

    return python_code


def parse_maybe_braces_python_code(lexer: Lexer) -> str:
    """ Tries to Parses a python code surrounded by braces from the lexer """

    if lexer.try_eat("{"):
        return parse_braces_python_code(lexer, True)

    return ""


def maybe_parse_identifier(lexer: Lexer) -> Optional[str]:
    try:
        return parse_identifier(lexer)
    except:
        return None

def parse_maybe_success(lexer: Lexer) -> Optional[TransitionEndBlock]:
    """ Parses a success block from the lexer """

    if lexer.try_eat("success"):
        inline_action = parse_maybe_braces_python_code(lexer)
        state = maybe_parse_identifier(lexer)
        action_name = None

        if lexer.try_eat("()"):
            action_name = state
            state = maybe_parse_identifier(lexer)

        return TransitionEndBlock(
            inline_action=inline_action,
            state=state,
            action_name=action_name
        )

    return None

def parse_success(lexer: Lexer) -> TransitionEndBlock:
    success = parse_maybe_success(lexer)

    if success is None:
        raise lexer.parse_error("Expected a success block")

    return success

def parse_remaining_failure(lexer: Lexer) -> TransitionEndBlock:
    """ Parses a failure block from the lexer
        (without the keyword "failure") """

    inline_action = parse_maybe_braces_python_code(lexer)
    state         = maybe_parse_identifier(lexer)

    if lexer.try_eat("()"):
        action_name = state
        state = None
    else:
        action_name = None

    return TransitionEndBlock(
        inline_action=inline_action,
        action_name=action_name,
        state=state
    )


def parse_failure(lexer: Lexer) -> TransitionEndBlock:
    """ Parses a failure block from the lexer """

    lexer.eat("failure")
    return parse_remaining_failure(lexer)


def parse_maybe_failure(lexer: Lexer) -> Optional[TransitionEndBlock]:
    """ Tries to parse a failure block from the lexer """

    if lexer.try_eat("failure"):
        return parse_remaining_failure(lexer)
    return None

def parse_maybe_guard(lexer: Lexer) -> Optional[str]:
    """ Parses a guard block from the lexer """

    if lexer.try_eat("{"):
        return parse_braces_python_code(lexer, True)

    return None

def parse_maybe_int(lexer: Lexer) -> Optional[int]:
    try:
        return int(parse_thing(lexer, "Expected a valid integer", is_valid_numeric_char))
    except:
        return None

def parse_parameter(lexer: Lexer, allow_arg_number: bool, allow_nameless: bool = False) -> EventParameter:
    pointer    : bool                      = False
    ref        : bool                      = False
    pyeval     : bool                      = False
    py_value   : Optional[str]             = None
    c_value    : Optional[Union[str, int]] = None
    expression : Optional[str]             = None
    identifier : Optional[str]             = None
    isMacro    : bool                      = False

    if lexer.try_eat("*"):
        pointer = True
    elif lexer.try_eat("&"):
        ref = True
    elif lexer.try_eat("!"):
        py_value = parse_string(lexer)
        pyeval = True

    if not pyeval:
        if lexer.try_eat("#"):
            isMacro = lexer.try_eat("macro")
            if isMacro:
                identifier = parse_identifier(lexer)
            else:
                lexer.eat("ecid")
                identifier = "#ecid"
        elif lexer.try_eat('expr("'):
            expression = parse_string_remaining(lexer)
            lexer.eat(")")
        else:
            identifier = parse_identifier(lexer)

        if allow_arg_number and identifier == "arg":
            c_value = parse_maybe_int(lexer)
            if c_value is not None:
                lexer.eat("as")
                identifier = parse_identifier(lexer)

        if c_value is None:
            c_value = identifier

    if lexer.try_eat("as"):
        identifier = parse_identifier(lexer)
    elif not identifier and not allow_nameless:
        raise lexer.parse_error("Expected parameter name introduced with 'as'")

    explicit_type = None
    if lexer.try_eat(":"):
        explicit_type = parse_identifier(lexer)

    return EventParameter(
        name=identifier,
        c_value=c_value,
        py_value=py_value,
        pointer=pointer,
        ref=ref,
        explicit_type=explicit_type,
        expression=expression,
        isMacro=isMacro
    )


def parse_parameter_list_content(lexer: Lexer) -> List[EventParameter]:
    """ Parses a parameter list (without the parentheses) from the lexer """

    params: List[EventParameter] = []
    comma_expected: bool = False

    while True:
        if lexer.try_eat(")"):
            lexer.ungetchar()
            return params

        if comma_expected:
            lexer.eat(",")

        params.append(parse_parameter(lexer, allow_arg_number=True))
        comma_expected = True

def parse_method_arg(lexer: Lexer, end: str = "],") -> str:
    nc = lexer.getchar()
    if nc in end:
        lexer.ungetchar()
        return ""

    if nc == "[":
        res = "[" + parse_method_arg(lexer, "]")
        return res + lexer.getchar() + parse_method_arg(lexer)

    return nc + parse_method_arg(lexer)

def parse_method_arg_list_content(lexer: Lexer) -> List[str]:
    """ Parses a parameter list (without the parentheses) from the lexer """

    params: List[str] = []
    comma_expected: bool = False
    while True:
        if lexer.try_eat("]"):
            lexer.ungetchar()
            return params

        if comma_expected:
            lexer.eat(",")

        params += [parse_method_arg(lexer)]

        comma_expected = True

def parse_parameter_list(lexer:Lexer) -> List[EventParameter]:
    """ Parses a parameter list from the lexer """

    if lexer.try_eat("("):
        parameter_list = parse_parameter_list_content(lexer)
        lexer.eat(")")
        return parameter_list

    return []

def parse_method_arg_list(lexer: Lexer) -> Optional[List[str]]:
    """ Parses a method argument list from the lexer """

    if lexer.try_eat("["):
        method_arg_list = parse_method_arg_list_content(lexer)
        lexer.eat("]")
        return method_arg_list

    return None

def parse_maybe_before_after(lexer:Lexer) -> bool:
    if lexer.try_eat("after"):
        return False

    lexer.try_eat("before")
    return True

def parse_return(lexer: Lexer) -> Optional[EventParameter]:
    """ parses the name of the variable which will contain the return value of
        the function """

    lexer.skip_white()
    if lexer.try_eat("->") or lexer.try_eat(":"):
        lexer.skip_white()
        return parse_parameter(lexer, allow_arg_number=False)

    return None


def parse_call_event(lexer: Lexer, name: str, before: bool, prgm_target: FormalProgramTarget) -> CallEvent:
    methodArgs = parse_method_arg_list(lexer)
    params = parse_parameter_list(lexer)
    ret = parse_return(lexer)

    return CallEvent(
        name=name,
        methodArgs=None if methodArgs is None else tuple(methodArgs),
        parameters=tuple(params),
        return_var=ret,
        before=before and (ret is None),
        prgm_target=prgm_target
    )

def parse_join_leave_event(lexer: Lexer, name: str, prgm_target: FormalProgramTarget) -> Union[JoinEvent, LeaveEvent]:
    params = parse_parameter_list(lexer)

    if name == "join":
        return JoinEvent(
            parameters=tuple(params),
            prgm_target=prgm_target
        )

    return LeaveEvent(
        parameters=tuple(params),
        prgm_target=prgm_target
    )

def watchpoint_address_char(c: str, first: bool = False) -> bool:
    return not c.isspace() and c != "("

def parse_watchpoint_address(lexer: Lexer) -> str:
    return parse_thing(lexer, "", watchpoint_address_char)

def to_watch_event_type(access_type: str) -> int:
    if access_type == "read":
        return WatchEventType.READ

    if access_type == "write":
        return WatchEventType.WRITE

    return WatchEventType.ACCESS

def parse_watch_event(lexer: Lexer, access_type: str, prgm_target: FormalProgramTarget) -> WatchEvent:
    lexer.skip_white()
    name = parse_watchpoint_address(lexer)
    parameters = parse_parameter_list(lexer)
    ret = parse_return(lexer)

    return WatchEvent(
        name=name,
        parameters=tuple(parameters),
        access_type=to_watch_event_type(access_type),
        prgm_target=prgm_target,
        return_var=ret
    )

def parse_exception_event(lexer: Lexer, prgm_target: FormalProgramTarget) -> ExceptionEvent:
    caught   = lexer.try_eat("caught")
    uncaught = lexer.try_eat("uncaught")

    caught   = caught   or lexer.try_eat("caught")
    uncaught = uncaught or lexer.try_eat("uncaught")

    if not caught and not uncaught:
        uncaught = True

    return ExceptionEvent(
        name=parse_identifier(lexer),
        parameters=tuple(parse_parameter_list(lexer)),
        caught=caught,
        uncaught=uncaught,
        prgm_target=prgm_target
    )

def parse_event(lexer: Lexer) -> Event:
    """ Parses an event block from the lexer """

    prgm_param_id: Optional[EventParameter]

    before = parse_maybe_before_after(lexer)
    lexer.eat("event")

    prgm_target = None
    if lexer.try_eat("from"):
        prgm_name = parse_identifier(lexer)
        if lexer.try_eat("("):
            prgm_param_id = parse_parameter(lexer, allow_arg_number=False, allow_nameless=True)
            lexer.eat(")")
        else:
            prgm_param_id = None

        prgm_target = (prgm_name, prgm_param_id)

    if lexer.try_eat("#"):
        name = parse_function_name(lexer)
        if name == "join" or name == "leave":
            return parse_join_leave_event(lexer, name, prgm_target)
        raise ParseError("Expected join or leave " + lexer.pos_str())
    else:
        name = parse_function_name(lexer)

    if name == "read" or name == "write" or name == "access" or name == "exception":
        if lexer.try_eat("("):
            lexer.ungetchar()
            return parse_call_event(lexer, name, before, prgm_target)

        if name == "exception":
            return parse_exception_event(lexer, prgm_target)

        return parse_watch_event(lexer, name, prgm_target)

    return parse_call_event(lexer, name, before, prgm_target)

def parse_maybe_accepting(lexer: Lexer, undefinedIsAccepting: bool = True) -> Optional[bool]:
    """ Parses the state's "(non-)accepting" keyword from the lexer """

    if lexer.try_eat("non-accepting"):
        return False

    if lexer.try_eat("accepting"):
        return True

    return True if undefinedIsAccepting else None

def parse_action(lexer: Lexer) -> Optional[str]:
    """ Parses the state's "(non-)accepting" keyword from the lexer """

    m = lexer.try_eat_regex(r"([a-zA-Z0-9_]+)\(\)")

    if m:
        return m.group(1)
    return None

def parse_slice_inside(lexer: Lexer) -> Tuple[str, ...]:
    identifier_unit_tuple = (parse_identifier(lexer),)

    if lexer.try_eat(","):
        return identifier_unit_tuple + parse_slice_inside(lexer)

    return identifier_unit_tuple

def parse_slice(lexer: Lexer) -> Tuple[str, ...]:
    paren = lexer.try_eat("(")

    slice_unit = parse_slice_inside(lexer)

    if paren:
        lexer.eat(")")

    return slice_unit

def parse_remaining_transition(lexer: Lexer) -> Transition:
    """ Parses a transition from the lexer
    (without the keyword "transition") """

    name = maybe_parse_identifier(lexer)

    lexer.eat("{")

    parse_comments(lexer)

    event   = parse_event(lexer)
    parse_comments(lexer)
    guard   = parse_maybe_guard(lexer)
    parse_comments(lexer)
    success = parse_maybe_success(lexer)
    parse_comments(lexer)
    failure = parse_maybe_failure(lexer)
    parse_comments(lexer)

    if not success:
        success = parse_success(lexer)

    parse_comments(lexer)

    lexer.eat("}")

    return Transition(
        name=name,
        event=event,
        guard=guard,
        success=success,
        failure=failure
    )

def parse_transition_list(lexer: Lexer, slice_bindings: Tuple[str, ...]) -> Tuple[Transition, ...]:
    """ Parses a transition list from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("transition"):
        transition = parse_remaining_transition(lexer)
        return (transition, ) + parse_transition_list(lexer, slice_bindings)

    return ()

def parse_maybe_import_transitions(lexer: Lexer) -> Optional[str]:
    parse_comments(lexer)

    if lexer.try_eat("import transitions from state"):
        return parse_identifier(lexer)
    return None

def parse_remaining_state(lexer: Lexer, slice_bindings: Tuple[str, ...]) -> State:
    """ Parses a state from the lexer (without the keyword "state") """

    name       = "*" if lexer.try_eat("*") else parse_identifier(lexer)
    initial    = lexer.try_eat("initial") or name == "init"
    accepting  = parse_maybe_accepting(lexer, undefinedIsAccepting=(name != "*"))
    forget     = lexer.try_eat("forget-slice")
    action_name = parse_action(lexer)
    import_transitions = parse_maybe_import_transitions(lexer)

    if lexer.try_eat("{"):
        transitions = parse_transition_list(lexer, slice_bindings)
        lexer.eat("}")
    else:
        transitions = ()

    return State(
        name=name,
        accepting=accepting,
        action_name=action_name,
        transitions=transitions,
        import_transitions=import_transitions,
        forgetSlice=forget,
        initial=bool(initial)
    )

def parse_state_list(lexer: Lexer, slice_bindings: Tuple[str, ...]) -> Tuple[State, ...]:
    """ Parses a state list from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("state"):
        state = parse_remaining_state(lexer, slice_bindings)
        return (state,) + parse_state_list(lexer, slice_bindings)

    return ()

def parse_maybe_initialization(lexer: Lexer) -> Optional[str]:
    """ Parses the initialization block from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("initialization"):
        return parse_braces_python_code(lexer)

    return None

def parse_maybe_function(lexer: Lexer) -> Optional[str]:
    """ Parses the functions block from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("functions"):
        return parse_braces_python_code(lexer)
    else:
        return None

def parse_slice_bindings(lexer: Lexer) -> Tuple[str, ...]:
    parse_comments(lexer)

    if lexer.try_eat("slice"):
        lexer.eat("on")

        return parse_slice(lexer)
    return ()

def parse_maybe_default_class(lexer: Lexer) -> Optional[str]:
    parse_comments(lexer)

    if lexer.try_eat("default class"):
        return parse_identifier(lexer)

    return None

def parse_body(lexer: Lexer, default_prgm_name: Optional[str]) -> Property:
    """ Parses the entire property automaton from the lexer """

    parse_comments(lexer)

    initialization: Env = {}
    functions: Env = {}

    slice_bindings = parse_slice_bindings(lexer)
    default_class  = parse_maybe_default_class(lexer)
    init = parse_maybe_initialization(lexer)
    functs = parse_maybe_function(lexer)

    # these make this parser unsafe
    if init:   exec(init, initialization)
    if functs: exec(functs, functions)

    ret = Property(
        slice_bindings=slice_bindings,
        initialization=initialization,
        functions=functions,
        states=parse_state_list(lexer, slice_bindings),
        default_prgm_name=default_prgm_name,
        default_class=default_class
    )

    lexer.expect_end()
    return ret


def parse(string: str, default_prgm_name: Optional[str] = None) -> Property:
    """ Parses the entire property automaton from a string """
    return parse_body(Lexer(string), default_prgm_name)
