# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

# Usage: CONTAINER_NAME=irvdebugging python checkpointer.py checkpoint_modcontainer

import shutil
import os
import signal
import socket

# to debug, replace:
from subprocess import call
#with:
#import subprocess

#def which(program):
    #import os
    #def is_exe(fpath):
        #return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    #fpath, fname = os.path.split(program)
    #if fpath:
        #if is_exe(program):
            #return program
    #else:
        #for path in os.environ["PATH"].split(os.pathsep):
            #exe_file = os.path.join(path, program)
            #if is_exe(exe_file):
                #return exe_file

    #return None

#def call(args):
    #print(os.environ["PATH"])
    #subprocess.call(["sh", "-x"] + [which("container")] + args[1:])

pid = int(os.environ["CHECKPOINTEE_PID"]) if "CHECKPOINTEE_PID" in os.environ else 0

checkpointee_run_command = None

def set_parameters(argv: List[str]) -> None:
    global checkpointee_run_command
    checkpointee_run_command = argv

def cpFolderName(cid: int) -> str:
    return "/tmp/irv-checkpoints/" + cid

def create(cid: int, allow_replace: bool = True) -> bool:
    global pid

    if allow_replace:
        # We delete the existing checkpoint, if exists
        discard(cid)

    cpFolder = cpFolderName(cid)
    os.makedirs(cpFolder)

    return call([
        "criu", "dump",
        "-t" , str(pid),
        "--shell-job",
        "--leave-running",
        "--tcp-established",
        "--ext-unix-sk",
        "--images-dir", cpFolder
    ]) == 0

def prepareRestore(cid: int, sock: any):
    global pid

    if pid:
        os.kill(pid, signal.SIGKILL)
        os.waitpid(0, 0)

    return

def restore(cid: int) -> bool:
    print("Restoring using CRIU...")

    cpFolder = cpFolderName(cid)
    newpid = os.fork()
    if newpid == 0:
        os.execvp(
            "criu", (
                "criu", "restore",
                "--tcp-established",
                "--ext-unix-sk",
                "--shell-job",
                "--images-dir", cpFolder
            )
        )

    return True

def run(server_host: str, server_port: int) -> None:
    global pid, checkpointee_run_command

    if checkpointee_run_command:
        pid = os.fork()
        if pid == 0:
            cmdwdir = os.environ["CHECKPOINTEE_RUN_COMMAND_WORKING_DIR"] if "CHECKPOINTEE_RUN_COMMAND_WORKING_DIR" in os.environ else None
            newenv = os.environ.copy()
            newenv["VERDE_BUS_HOST"] = server_host
            newenv["VERDE_BUS_PORT"] = str(server_port)
            newenv["VERDE_AUTO_RECONNECT"] = "1"
            newenv["JAVA_TOOL_OPTIONS"] = ((newenv["JAVA_TOOL_OPTIONS"] + " ") if "JAVA_TOOL_OPTIONS" in newenv else "") + "-XX:-UsePerfData"

            if cmdwdir:
                os.chdir(cmdwdir)

            os.execvpe(checkpointee_run_command[0], checkpointee_run_command, newenv)

    print("PID of the checkpointee: " + str(pid))

def discard(cid: int) -> None:
    cpFolder = cpFolderName(cid)
    shutil.rmtree(cpFolder, ignore_errors=True)
