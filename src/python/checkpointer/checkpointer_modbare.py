# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

# Usage: CONTAINER_NAME=irvdebugging python checkpointer.py checkpoint_modcontainer

import shutil
import os
import signal
import socket

# to debug, replace:
from subprocess import call

pid = int(os.environ["CHECKPOINTEE_PID"]) if "CHECKPOINTEE_PID" in os.environ else 0
newenv = None


def set_parameters(*argv):
    pass

def cpFolderName(cid: int) -> str:
    return "/tmp/irv-checkpoints/" + cid

def create(cid: int, allow_replace: bool = True) -> int:
    global pid
    global newenv

    if allow_replace:
        # We delete the existing checkpoint, if exists
        discard(cid)

    newenv["VERDE_CHECKPOINT_FOLDER"] = cpFolderName(cid)
    createcmd = os.environ["CHECKPOINTEE_CHECKPOINT_COMMAND"]

    newpid = os.fork()
    if newpid == 0:
        if cmdwdir:
            os.chdir(cmdwdir)

        os.execvpe("sh", ("sh", "-x", "-c", "exec " + restorecmd), newenv)

    return 0

def prepareRestore(cid: int, sock: any) -> None:
    global pid

    newenv["VERDE_CHECKPOINT_FOLDER"] = cpFolderName(cid)
    restorecmd = os.environ["CHECKPOINTEE_RESTORE_COMMAND"]

    newpid = os.fork()
    if newpid == 0:
        if cmdwdir:
            os.chdir(cmdwdir)

        os.execvpe("sh", ("sh", "-x", "-c", "exec " + restorecmd), newenv)

    return

def restore(cid: int) -> int:
    global newenv

    print("Restoring...")
    newenv["VERDE_CHECKPOINT_FOLDER"] = cpFolderName(cid)
    restorecmd = os.environ["CHECKPOINTEE_RESTORE_COMMAND"]

    newpid = os.fork()
    if newpid == 0:
        if cmdwdir:
            os.chdir(cmdwdir)

        os.execvpe("sh", ("sh", "-x", "-c", "exec " + restorecmd), newenv)

    return 0

def run(server_host: str, server_port: int) -> None:
    global pid
    global newenv

    if "CHECKPOINTEE_RUN_COMMAND" in os.environ:
        pid = os.fork()
        if pid == 0:
            runcmd = os.environ["CHECKPOINTEE_RUN_COMMAND"]
            cmdwdir = os.environ["CHECKPOINTEE_RUN_COMMAND_WORKING_DIR"] if "CHECKPOINTEE_RUN_COMMAND_WORKING_DIR" in os.environ else None
            newenv = os.environ.copy()
            newenv["VERDE_BUS_HOST"] = server_host
            newenv["VERDE_BUS_PORT"] = str(server_port)
            newenv["VERDE_AUTO_RECONNECT"] = "1"
            newenv["JAVA_TOOL_OPTIONS"] = ((newenv["JAVA_TOOL_OPTIONS"] + " ") if "JAVA_TOOL_OPTIONS" in newenv else "") + "-XX:-UsePerfData"

            if cmdwdir:
                os.chdir(cmdwdir)

            os.execvpe("sh", ("sh", "-x", "-c", "exec " + runcmd), newenv)

    print("PID of the checkpointee: " + str(pid))

def discard(cid: int) -> None:
    cpFolder = cpFolderName(cid)
    shutil.rmtree(cpFolder, ignore_errors=True)
