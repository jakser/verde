#!/usr/bin/env sh

# Usage:
#  ./docker-checkpoint.sh create  [cid] [pid]
#  ./docker-checkpoint.sh restore [cid] [pid]

# Beware, used in a regular expression as is.
CHECKPOINT_PREFIX="verde-"


if [ -z ${VERDE_DOCKER_CONTAINER_NAME+x} ]; then
    VERDE_DOCKER_CONTAINER_NAME=mycontainer
fi


# $lastCheckpointID = "$(docker checkpoint ls mycontainer | tail -n +2 | sort | tail -n 1 | sed "s/^$PREFIX//")"
#
# checkpointName="${CHECKPOINT_PREFIX}$((lastCheckpointID+1))"

checkpointName="${CHECKPOINT_PREFIX}$2"

if [ "$1" = "create" ]; then

#    if [ ! -z ${VERDE_XPRA_SESSION+x} ]; then
#        eval "xpra detach ${VERDE_XPRA_SESSION_PARAMETERS} ${VERDE_XPRA_SESSION}"
#    fi

    docker checkpoint rm "${VERDE_DOCKER_CONTAINER_NAME}" "${checkpointName}" 2>&1 >/dev/null

    if ! docker checkpoint create --leave-running "${VERDE_DOCKER_CONTAINER_NAME}" "${checkpointName}" 1>/dev/null; then
        echo "docker checkpoint create failed." 1>&2
        exit 1
    fi
else
    docker stop "${VERDE_DOCKER_CONTAINER_NAME}"
    if ! docker start --checkpoint "${checkpointName}" "${VERDE_DOCKER_CONTAINER_NAME}"; then
        >&2 echo "Unable to restore the checkpoint."
        exit 1
    fi
fi


#if [ -z ${VERDE_XPRA_SESSION+x} ]; then
#    eval "nohup xpra attach ${VERDE_XPRA_SESSION_PARAMETERS}" "${VERDE_XPRA_SESSION}"
#fi
