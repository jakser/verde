#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

#FIXME: make it python2 compatible (int.from_bytes does not exist here, see https://stackoverflow.com/questions/30402743/python-2-7-equivalent-of-built-in-method-int-from-bytes)

import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))) + "/target")

import errno
import socket
import select
import irv_pb2 as irv
import importlib
from irvstuff import *


CHECKPOINTEE_SERVER_HOST = (
    os.environ["VERDE_CHECKPOINTEE_SERVER_HOST"]
        if "VERDE_CHECKPOINTEE_SERVER_HOST" in os.environ
        else "0.0.0.0"
)

CHECKPOINTEE_SERVER_PORT = (
    int(os.environ["VERDE_CHECKPOINTEE_SERVER_PORT"])
        if "VERDE_CHECKPOINTEE_SERVER_PORT" in os.environ
        else 13374
)

if len(sys.argv) < 2:
    print(
        "The checkpointer module must be given in argument.\n"
        "Moreover, these environment variable can be defined (current values are shown):\n"
        "    VERDE_CHECKPOINTEE_SERVER_HOST = '" + CHECKPOINTEE_SERVER_HOST + "'\n"
        "        The host used by the checkpointer to listen\n"
        "\n"
        "    VERDE_CHECKPOINTEE_SERVER_PORT = " + str(CHECKPOINTEE_SERVER_PORT) + "\n"
        "        The port used by the checkpointee to connect to the checkpointer\n"
        "\n"
        "    VERDE_BUS_HOST = '" + BUS_HOST + "'\n"
        "        The host of the bus\n"
        "\n"
        "    VERDE_BUS_PORT = " + str(BUS_PORT) + "\n"
        "        The port of the bus"
        "\n"
        "    VERDE_CHECKPOINTER_AUTO_PORT = " + str(VERDE_CHECKPOINTER_AUTO_PORT) + "\n"
        "        Set this variable if the checkpointer should try to use another"
        "        port if the chosen one is already in use"
    )
    sys.exit(1)

checkpoint = importlib.import_module(sys.argv[1])

# FIXME support clients leaving / new clients joining the bus.


def connect():
    global CHECKPOINTEE_SERVER_PORT

    checkpointeeserversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    checkpointeeserversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    checkpointeeserversock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)

    while True:
        try:
            checkpointeeserversock.bind((CHECKPOINTEE_SERVER_HOST, CHECKPOINTEE_SERVER_PORT))
            break
        except socket.error as exc:
            if exc.errno == errno.EADDRINUSE and "VERDE_CHECKPOINTER_AUTO_PORT" in os.environ:
                CHECKPOINTEE_SERVER_PORT += 1
            else:
                raise

    if hasattr(checkpoint, "run"):
        print("Running the checkpointee.")
        checkpoint.set_parameters(sys.argv[2:])
        checkpoint.run(CHECKPOINTEE_SERVER_HOST, CHECKPOINTEE_SERVER_PORT)

    print("Waiting for the checkpointee on " + str(CHECKPOINTEE_SERVER_HOST) + ":" + str(CHECKPOINTEE_SERVER_PORT) + "…")
    checkpointeeserversock.listen(10)
    checkpointeesock, _ = checkpointeeserversock.accept()

    print("Waiting for hello from the checkpointee.")

    (msg, msgbytes) = getMessage(checkpointeesock, withbytes=True)

    if not msg.HasField("hello"):
        print("The first message of the checkpointee is not hello. This is rude. Leaving.")
        sys.exit(1)

    print("Received hello from the checkpointee. Forwarding.")

    bussock.sendall(msgbytes)

    print("Waiting hello from the bus.")

    (msg, msgbytes) = getMessage(bussock, withbytes=True)

    if not msg.HasField("hello"):
        print("The first message of the bus is not hello. This is rude. Leaving.")
        sys.exit(1)

    senderId = msg.recipients[0]

    print("Ok. Checkpointee's sender id is " + str(senderId) + ". Forwarding hello to the checkpointee.")

    checkpointeesock.sendall(msgbytes)

    print("Ok. All set.")

    socks = [checkpointeesock, bussock]

    while True:
        (ready_socks, _, _) = select.select(socks, [], [])
        for sck in ready_socks:
            try:
                msglenbytes = sck.recv(4, socket.MSG_WAITALL)
                if not msglenbytes:
                    print("The checkpointee left")
                    return

            except ConnectionResetError as exc:
                if sck == checkpointeesock:
                    print("The checkpointee left")
                    return
                else:
                    raise exc

            msglen = int.from_bytes(msglenbytes, byteorder='big')
            msgbytes = sck.recv(msglen, socket.MSG_WAITALL)

            if sck is bussock:
                msg = irv.Root()
                msg.ParseFromString(msgbytes)

                if msg.HasField("checkpointRequest"):
                    cid = (str(msg.sender) + "." + str(msg.checkpointRequest.cid))

                    reply = rootMessage(senderId, msg.sender)

                    print("Request for checkpointing, cid=" + cid)

                    if checkpoint.create(cid):
                        reply.checkpoint.requestId = msg.checkpointRequest.requestId
                        reply.checkpoint.cid = msg.checkpointRequest.cid
                    else:
                        reply.error.info = "Checkpointing failed."

                    print("Checkpointing done")

                    send(bussock, reply)

                elif msg.HasField("checkpointRestoreRequest"):
                    cid = (str(msg.sender) + "." + str(msg.checkpointRestoreRequest.cid))

                    reply = rootMessage(senderId, msg.sender)

                    print("Request for restoring a checkpoint, cid=" + cid)

                    if hasattr(checkpoint, "prepareRestore"):
                        checkpoint.prepareRestore(cid, sock=checkpointeesock)

                    checkpointeesock.close()

                    if checkpoint.restore(cid):
                        reply.checkpointRestored.requestId = msg.checkpointRestoreRequest.requestId
                        reply.checkpointRestored.cid = msg.checkpointRestoreRequest.cid
                    else:
                        reply.error.info = "Restore failed."
                        return

                    socks.remove(checkpointeesock)

                    checkpointeesock, _ = checkpointeeserversock.accept()

                    socks.append(checkpointeesock)

                    send(bussock, reply)

                    print("Restore complete.")

                elif msg.HasField("checkpointDiscard"):
                    cid = (str(msg.sender) + "." + str(msg.checkpointRestoreRequest.cid))
                    checkpoint.discard(cid)

                else:
                    checkpointeesock.sendall(msglenbytes + msgbytes)
            else:
                bussock.sendall(msglenbytes + msgbytes)

print("Connecting to the bus…")
bussock = connectToBus()

try:
    connect()
except ValueError:
    bussock.close()
