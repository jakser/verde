# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

# Usage: CONTAINER_NAME=irvdebugging python checkpointer.py checkpoint_modcontainer

import subprocess
import os

#def which(program):
    #import os
    #def is_exe(fpath):
        #return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    #fpath, fname = os.path.split(program)
    #if fpath:
        #if is_exe(program):
            #return program
    #else:
        #for path in os.environ["PATH"].split(os.pathsep):
            #exe_file = os.path.join(path, program)
            #if is_exe(exe_file):
                #return exe_file

    #return None

#def call(args):
    #print(os.environ["PATH"])
    #subprocess.call(["sh", "-x"] + [which("container")] + args[1:])
import getpass

def set_parameters(*argv):
    pass

def run(server_host: str, server_port: int) -> None:
    runcmd=os.environ["CHECKPOINTEE_RUN_COMMAND"]
    if os.fork() == 0:
        newenv = os.environ.copy()
        newenv["VERDE_BUS_HOST"] = server_host
        newenv["VERDE_BUS_PORT"] = str(server_port)
        newenv["VERDE_AUTO_RECONNECT"] = "1"

        os.execvpe("sh", ("sh", "-x", "-c", "exec " + runcmd), newenv)

def create(cid: int, allow_replace: bool = True) -> bool:
    return subprocess.call(["container", "checkpoint", "replace" if allow_replace else "create", cid]) == 0

def prepareRestore(cid: int, sock: any) -> bool:
    # Let's break the socket connection.
    # This is fucking dirty.
    subprocess.call(["container", "delete", cid])
    return subprocess.call(["container", "create", cid]) == 0

def restore(cid: int) -> bool:
    return subprocess.call(["container", "checkpoint", "restore", cid]) == 0

def discard(cid: int) -> bool:
    return subprocess.call(["container", "checkpoint", "delete", cid]) == 0
