# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

# Usage: CONTAINER_NAME=irvdebugging python checkpointer.py checkpoint_modcontainer

import shutil
import os

# to debug, replace:
from subprocess import call
#with:
#import subprocess

#def which(program):
    #import os
    #def is_exe(fpath):
        #return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    #fpath, fname = os.path.split(program)
    #if fpath:
        #if is_exe(program):
            #return program
    #else:
        #for path in os.environ["PATH"].split(os.pathsep):
            #exe_file = os.path.join(path, program)
            #if is_exe(exe_file):
                #return exe_file

    #return None

#def call(args):
    #print(os.environ["PATH"])
    #subprocess.call(["sh", "-x"] + [which("container")] + args[1:])

docker_container = os.environ["CHECKPOINTED_DOCKER_CONTAINER"]

def set_parameters(*argv):
    pass

def cpFolderName(cid: int) -> str:
    return "/tmp/irv-checkpoints/" + cid

def create(cid: int, allow_replace: bool = True) -> bool:
    if allow_replace:
        # We delete the existing checkpoint, if exists
        discard(cid)

    cpFolder = cpFolderName(cid)
    os.makedirs(cpFolder)

    return call(["docker", "checkpoint", "create", "--tcp-established", "--leave-running", docker_container, cid]) == 0

def restore(cid: int) -> bool:
    return (
        (call(["docker", "stop", docker_container]) == 0)
            and
        (call(["docker", "start", docker_container, "--checkpoint", cid, "--tcp-established"]) == 0)
    )

def discard(cid: int) -> None:
    cpFolder = cpFolderName(cid)
    shutil.rmtree(cpFolder, ignore_errors=True)
