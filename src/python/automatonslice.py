from typing import Any, Optional, List, Dict, Counter, Set, Generator, Tuple
from property import Event, State, Env, SliceInstance, State, RuntimeEventProgramTarget

EventWithTarget = Tuple[Event, RuntimeEventProgramTarget]

dummyState = State("dummy", None)

class AutomatonSlice:
    current_state      : State = dummyState
    children           : List["AutomatonSlice"]
    parent             : Optional["AutomatonSlice"]
    root_slice         : "AutomatonSlice"
    checkpointsByCid   : Dict[int, "AutomatonSlice"]
    watched_events     : Set[EventWithTarget]
    tree_active_states : Optional[Counter[State]]
    env                : Optional[Env]

    def __init__(self, slice_length : int , old: "AutomatonSlice" = None, parent: "AutomatonSlice" = None):
        self.children   = []
        self.parent     = parent
        self.root_slice = parent.root_slice if parent else self
        self.checkpointsByCid = {}
        self.watched_events = set()
        self.tree_active_states = Counter() if parent is None else None

        if old:
            self.set_slice_instance(old.get_slice_instance())
            self.set_current_state(old.get_current_state())
            self.set_env(old.get_env_copy())
        else:
            self.set_slice_instance((None,) * slice_length)
            # WARNING: set it when instanciating
            self.set_env(None)

    def is_state_active(self, state: State) -> bool:
        assert(self.root_slice.tree_active_states is not None)
        return self.root_slice.tree_active_states[state] != 0

    def get_children_recursive(self) -> Generator["AutomatonSlice", None, None]:
        for s in self.children:
            yield s
            for c in s.get_children_recursive():
                yield c

    def has_children(self) -> bool:
        return len(self.children) != 0

    def fork(self) -> "AutomatonSlice":
        new_s = AutomatonSlice(old=self, slice_length=-1, parent=self)
        self.add_child(new_s)
        return new_s

    def add_child(self, new_s) -> None:
        self.children.append(new_s)

    def remove(self) -> None:
        if self.parent:
            self.parent.children.remove(self)

    def set_current_state(self, state: State) -> None:
        if self.current_state is not None:
            self.child_remove_current_state(self.current_state)

        self.current_state = state

        if state is not None:
            self.child_add_current_state(state)

    def child_add_current_state(self, state: State) -> None:
        if self.is_root():
            assert(self.tree_active_states is not None)
            self.tree_active_states[state] += 1
        else:
            self.root_slice.child_add_current_state(state)

    def child_remove_current_state(self, state: State) -> None:
        if self.is_root():
            assert(self.tree_active_states is not None)
            self.tree_active_states[state] -= 1
        else:
            self.root_slice.child_remove_current_state(state)

    def is_root(self) -> bool:
        return self.root_slice is self

    def get_current_state(self) -> State:
        return self.current_state

    def set_env_key(self, key: str, val: Any):
        if self.env is None:
            self.env = {}
        self.env[key] = val

    def get_env_copy(self) -> Optional[Env]:
        # WARNING:
        # this is a shallow copy. If dictionaries or other complex objects are
        # in there, they will be shared accross slices.
        # This is intentional.
        env = self.get_env()

        if env is None:
            return None

        return env.copy()

    def get_env(self) -> Optional[Env]:
        return self.env

    def set_env(self, env: Optional[Env]) -> None:
        self.env = env

    def get_slice_instance(self) -> SliceInstance:
        return self.slice_instance

    def get_parameter_value(self, p: str, param_names: List[str]) -> Any:
        for (param_instance, param_name) in zip(self.get_slice_instance(), param_names):
            if param_name == p:
                return param_instance
        raise KeyError("Unknown parameter name.")

    def set_slice_instance(self, slice_instance: SliceInstance) -> None:
        self.slice_instance = tuple(slice_instance)

    def active_states(self) -> List[State]:
        assert(self.tree_active_states is not None)
        return [key for (key, val) in self.tree_active_states.items() if val != 0 and key != dummyState]

    def deepcopy(self, parent: Optional["AutomatonSlice"], root_slice: Optional["AutomatonSlice"]) -> "AutomatonSlice":
        new_s = AutomatonSlice(old=self, slice_length = -1, parent=parent)

        if not root_slice:
            root_slice = new_s
            new_s.root_slice = root_slice

        new_s.children = list(map(lambda child: child.deepcopy(parent=new_s, root_slice=root_slice), self.children))
        return new_s

    def checkpoint(self, cid: int) -> None:
        assert(self.tree_active_states is not None)

        cp = self.deepcopy(None, None)
        cp.tree_active_states = self.tree_active_states.copy()

        self.checkpointsByCid[cid] = cp

    def restore_checkpoint(self, cid: int) -> None:
        cp = self.checkpointsByCid[cid].deepcopy(None, None)
        self.current_state = cp.current_state
        self.children = cp.children
        self.parent = cp.parent
        self.env = cp.env
        self.set_slice_instance(cp.get_slice_instance())
        self.root_slice = self

        assert(cp.tree_active_states is not None)
        self.tree_active_states = cp.tree_active_states.copy()

    def drop_checkpoint(self, cid: int) -> None:
        del self.checkpointsByCid[cid]
