#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    A module providing an interface which allows to manipulate the monitor.
"""

from typing import Any, Dict, Tuple, Optional, Callable, Union, Set, Generator, Iterable, TYPE_CHECKING

from property import State, Env, Transition

if TYPE_CHECKING:
    from monitor import Monitor

from automatonslice import AutomatonSlice

from monitorcommon import (
    StopExecution,
    SetCurrentState,
    events
)

class MonitorInterface:
    """ An instance of the MonitorActionInterface class is created by instance
        of the Monitor class. It is passed to user defined action functions to
        let them control and consult the monitor state.

        WARNING: This class exposes the monitor object and while the interface
        of this class is expected to be more or less backward / forward
        compatible, the monitor is expected to evolve and direct access to it
        might cause backward/forward compatibility issues. Please use methods of
        MonitorActionInterface whenever possible. """

    monitor: "Monitor"
    globals: Env

    def __init__(self, monitor: "Monitor"):
        """ This is the constructor of the class. A monitor must be given. """
        self.monitor = monitor
        self.globals = dict()

    def set_globals(self, g: Dict[str, Any]) -> None:
        """ Sets the dictionary in which functions will be found, if needed,
            when e.g. calling (un)register_event. """
        self.globals = g

    def debugger_shell(self) -> None:
        """ Raises an exception making the monitor drop to the debugger's
            shell. """
        self.monitor.stop_is_debugger_shell = True
        raise StopExecution(
            "user's function asked to drop to the debugger's shell"
        )

    def stop_execution(self) -> None:
        """ Raises an exception making the execution of the monitor stop the
            program's and the monitor's execution. """
        self.monitor.stop_is_debugger_shell = False
        raise StopExecution(
            "user's function asked to stop the execution of the program"
        )

    def get_slices(self) -> Generator[AutomatonSlice, None, None]:
        return self.monitor.get_slices()

    def get_env(self, s: Optional[AutomatonSlice] = None) -> Optional[Dict[str, Any]]:
        """ Returns the environment dictionary of the property.

            WARNING: It is unspecified whether a modification to this dictionary
            will be reflected in the environment, and whether a modification to
            the environment will be reflected in the dictionary.
            Call get_env() each time you need to get the most up-to-date
            environment.

            If needed, use copy() to be sure to keep a static version of the
            environment correponding to the moment when this method is called,
            and the set_env_dict() and set_env_value() methods to modify the
            environment. """

        if s is None:
            for s in self.get_slices():
                return s.get_env()

        return s.env

    def set_env_dict(self, s: AutomatonSlice, new_env: Dict[str, Any]) -> None:
        """ Sets the environment of the property.

        WARNING: Please don't modify the given dictionary after passing it to
        this method. This causes undefined behavior. Use copy() if needed when
        passing the dictionary to set_env_dict(). new_env is not guaranteed to
        be a reference to the actual new dictionary """

        if s.env is None:
            s.env = {}
        else:
            s.env.clear()

        for p in new_env:
            s.env[p] = new_env[p]


    def get_env_value(self, s: AutomatonSlice, key: str) -> Any:
        """ Returns the value of the given variable in the property's
        environment. Raises if the key is not present in the environment. """

        return None if s.env is None else s.env[key]

    def set_env_value(self, s: AutomatonSlice, key: str, value: Any) -> None:
        """ Sets the value of the given variable in the property's
            environment. """

        if s.env is None:
            s.env = {}

        s.env[key] = value

    # FIXME Any
    def get_env_keys(self, s: AutomatonSlice) -> Any:
        """ Returns the keys of the property's environment """

        if s.env is None:
            return []
        return s.env.keys()

    def print_monitor_state(self) -> None:
        """ Prints the monitor's current state. """

        self.monitor.print_state()

    def get_current_states(self) -> Set[State]:
        """ Returns the set of the current states of the
            property.

            WARNING: Follow the same precautions as for
            the get_env() method. """

        res: Set[State] = set()

        res.update(self.monitor.active_states())
        res.update(self.monitor.definitive_states)

        return res

    def set_current_state(self, state: State) -> None:
        """ Sets the current state of (the root slice of) the property."""

        raise SetCurrentState(state, False)

    def get_states(self) -> Iterable[State]:
        """ Returns the list of the states of the property.

            WARNING: Follow the same precautions as for
            the get_env() method. """

        return self.monitor.states

    def get_slice_bindings(self) -> Tuple[str, ...]:
        return self.monitor.prop.slice_bindings

    def set_quiet(self, b: Union[bool, str] = "True") -> None:
        """ Sets the monitor quiet or not."""

        if b is True or b == "True" or b == "true" or b == "1":
            b = True
        else:
            b = False

        self.monitor.set_quiet(b)

    def step_by_step(self, b: Union[bool, str] = "True") -> None:
        """ Set step by step monitor """

        if b is True or b == "True" or b == "true" or b == "1":
            self.monitor.step_by_step = True
        else:
            self.monitor.step_by_step = False

    def register_event(self, event_type: Union[int, str], callback: Union[str, Callable[..., Any]]) -> None:
        """ Register a callback for this event type.

            Possible events:
             - state_changed(new_states)
                new_states is the set of the new current states
             - transition_taken(transition, point)
                transition is the object representing the transition
                point is either "success" or "failure"
             - event_applied

            The event type is given by its name and the parameters passed to the
            callback is what is given in parenthesis.
        """

        if isinstance(callback, str):
            callback = self.globals[callback]

        if isinstance(event_type, str):
            event_type = events[event_type]

        assert(callable(callback))
        self.monitor.register_event(event_type, callback)

    def get_internal_id(self) -> int:
        return self.monitor.monitor_id

    def get_transition_begin_state(self, trans: Transition) -> State:
        return self.monitor.begin_state_by_trans[trans]

    def unregister_event(self, event_type: Union[int, str], callback: Union[str, Callable[..., Any]]) -> None:
        """ Unregister a callback for this event type.
            See also register_event.
        """

        if isinstance(callback, str):
            callback = self.globals[callback]

        if isinstance(event_type, str):
            event_type = events[event_type]

        assert(callable(callback))
        self.monitor.unregister_event(event_type, callback)
