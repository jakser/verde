#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

from typing import Any, Dict, List
import gdbbreakpoint as breakpoint
import criucheckpoint
import gdb

def criu_checkpoint(block_workaround: bool = False) -> int:
    #if not is_running():
        #raise Exception("Cannot checkpoint: the process is not running.")

    #print("BLOCK_WORKAROUND {}".format(block_workaround))
    if True:
        breakpoint.workaround_avoid_second_bp_hit_after_reattach_at(gdb.newest_frame().pc())
    cid = criucheckpoint.criu_checkpoint(block_workaround)

    for name, m in verde_ctx.monitors.items():
        m.checkpoint(cid)

    return cid

def criu_restore_checkpoint(checkpoint_id: int, stop: bool = False) -> None:
    (pc, block_workaround) = criucheckpoint.criu_restore(checkpoint_id, stop=stop)

    if True:
        breakpoint.workaround_avoid_second_bp_hit_after_reattach_at(pc)

    for name, m in verde_ctx.monitors.items():
        m.restore_checkpoint(checkpoint_id)

def criu_drop_checkpoint(checkpoint_id: int) -> None:
    #criucheckpoint.criu_delete(checkpoint_id)

    for name, m in verde_ctx.monitors.items():
        m.remove_checkpoint(checkpoint_id)

def is_running() -> bool:
    return gdb.selected_inferior().is_valid()

checkpoint = criu_checkpoint
restore_checkpoint = criu_restore_checkpoint
drop_checkpoint = criu_drop_checkpoint

class verde_ctx:
    monitors:        Dict[str, "Monitor"] = {}
    current_monitor: str                = ""
    last_failed:     bool               = False
    inside_group:    bool               = False
    globals:         Any                = globals()
    checkpoints:     List[int]          = []
    timecmds:        bool               = False
    timeid:          int                = 1
