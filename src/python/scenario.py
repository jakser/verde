#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

"""
    Scenario-related classes
"""

from typing import Tuple, Optional, NamedTuple
from property import Env

class ScenarioEventType:
    # BEWARE: these three variables have to be numbers and not strings
    # see calls to scenario_bind_evt_to_state in monitor.py
    # strings would potentially conflict with state names

    ANY_SLICE  = 1
    ONE_SLICE  = 2
    ALL_SLICES = 3

class ScenarioEvent(NamedTuple):
    state_name: Optional[str]
    regex: bool
    entering: bool
    accepting: Optional[bool]
    code: str
    slice_policy: int

    def __str__(self):
        return "<" + ("entering" if self.entering else "leaving" ) + " " + (
            ("state " + self.state_name) if self.state_name else (
                self.regex if self.regex else (
                    (
                        "accepting" if self.accepting else "non-accepting"
                    ) + " state"
                )
            )
        ) + " for " + (
            "all slices"
                if self.slice_policy == ScenarioEvent.ALL_SLICES
                else (
                    "one slice"
                        if self.slice_policy == ScenarioEvent.ONE_SLICE
                        else "any slice"
                )
        ) + ">"

class Scenario(NamedTuple):
    initialization: Env
    events: Tuple[ScenarioEvent, ...]
