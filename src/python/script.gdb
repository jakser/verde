python import sys, os
python sys.path.append(".")
python import gdbbreakpoint as verdebpmodule
define hook-run
    pi verdebpmodule.interactive = False
end
define hook-step
    pi verdebpmodule.interactive = True
end
define hook-start
    pi verdebpmodule.interactive = True
end
define hook-continue
    pi verdebpmodule.interactive = False
end
set can-use-hw-watchpoints 0

set python print-stack full
