#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + "/target")

import functools
import traceback

from irvbusclient import IrvBusClient
import irv_pb2 as irv
import gdb
from property import EventParameter, CallEvent, WatchEvent, WatchEventType
from context import *

class GDBDebuggerController(IrvBusClient):
    def __init__(self, dbgAdapter):
        super().__init__()
        self.dbgAdapter = dbgAdapter

        self.requestByEventId = dict()
        self.prgmCheckpoints = dict()
        self.scnSenderId = 0
        self.knownMonitors = set()

        self.connectToBus("executionController")
        if self.waitResume():
            gdb.execute("run")

    def paramFromIrv(self, irvValueRequest):
        if irvValueRequest.HasField("name"):
            return EventParameter(irvValueRequest.name,
                pointer = irvValueRequest.dereference == 1,
                ref     = irvValueRequest.dereference == -1,
                isMacro = irvValueRequest.scope == irv.ValueRequest.MACRO
            )

        if irvValueRequest.HasField("argNumber"):
            return EventParameter(str(irvValueRequest.argNumber),
                c_value    = irvValueRequest.argNumber,
                pointer    = irvValueRequest.dereference == 1,
                ref        = irvValueRequest.dereference == -1
            )

        if irvValueRequest.HasField("thisObject"):
            return EventParameter("this",
                pointer    = irvValueRequest.dereference == 1,
                ref        = irvValueRequest.dereference == -1
            )

        if irvValueRequest.HasField("expression"):
            return EventParameter("expression",
                expression = irvValueRequest.expression,
                pointer    = irvValueRequest.dereference == 1,
                ref        = irvValueRequest.dereference == -1
            )

        print("Unsupported Parameter:", irvValueRequest)
        raise Exception("Unsupported parameter type")

    def irvValueFromAdapterParameterValue(self, v):
        irvVal = irv.Value()

        t = v.type.strip_typedefs().code

        if t == gdb.TYPE_CODE_PTR:
            # The type is a pointer.
            irvVal.vSignedInt8 = int(v)
            # Lets treat pointers as long for now.

        elif t == gdb.TYPE_CODE_ARRAY:
            # The type is an array.
            raise Exception("Unsupported type array")

        elif t == gdb.TYPE_CODE_STRUCT:
            # The type is a structure.
            raise Exception("Unsupported type struct")

        elif t == gdb.TYPE_CODE_UNION:
            # The type is a union.
            raise Exception("Unsupported type union")

        elif t == gdb.TYPE_CODE_ENUM:
            # The type is an enum. Handle like a int.
            irvVal.vSignedInt8 = int(v)

        elif t == gdb.TYPE_CODE_FLAGS:
            #A bit flags type, used for things such as status registers.
            raise Exception("Unsupported type bit flags")

        elif t == gdb.TYPE_CODE_FUNC:
            # The type is a function.
            raise Exception("Unsupported type function")

        elif t == gdb.TYPE_CODE_INT:
            # The type is an integer type.
            irvVal.vSignedInt8 = int(v)

        elif t == gdb.TYPE_CODE_FLT:
            #A floating point type.
            irvVal.vFloat = float(v)

        elif t == gdb.TYPE_CODE_VOID:
            # The special type void.
            pass # irvVal uninitialized is void

        elif t == gdb.TYPE_CODE_SET:
            #A Pascal set type.
            raise Exception("Unsupported type pascal set")

        elif t == gdb.TYPE_CODE_RANGE:
            #A range type, that is, an integer type with bounds.
            irvVal.vSignedInt8 = int(v)

        elif t == gdb.TYPE_CODE_STRING:
            #A string type. Note that this is only used for certain languages with language-defined string types; C strings are not represented this way.
            irvVal.vString = str(v)

        #elif t == gdb.TYPE_CODE_BITSTRING
            ##A string of bits. It is deprecated.

        elif t == gdb.TYPE_CODE_ERROR:
            #An unknown or erroneous type.
            irvVal.vError.info = str(v)

        elif t == gdb.TYPE_CODE_METHOD:
            #A method type, as found in C++.
            raise Exception("Unsupported type method")

        elif t == gdb.TYPE_CODE_METHODPTR:
            #A pointer-to-member-function.
            raise Exception("Unsupported type pointer-to-function")

        elif t == gdb.TYPE_CODE_MEMBERPTR:
            #A pointer-to-member.
            raise Exception("Unsupported type pointer-to-member")

        elif t == gdb.TYPE_CODE_REF:
            #A reference type.
            raise Exception("Unsupported type reference")

        elif t == gdb.TYPE_CODE_RVALUE_REF:
            #A C++11 rvalue reference type.
            raise Exception("Unsupported type C++11 rvalue")

        elif t == gdb.TYPE_CODE_CHAR:
            #A character type.
            irvVal.vString = str(v)

        elif t == gdb.TYPE_CODE_BOOL:
            #A boolean type.
            irvVal.vBool = bool(v)

        elif t == gdb.TYPE_CODE_COMPLEX:
            #A complex float type.
            raise Exception("Unsupported type complex")

        elif t == gdb.TYPE_CODE_TYPEDEF:
            #A typedef to some other type.
            raise Exception("Unsupported type typedef")

        elif t == gdb.TYPE_CODE_NAMESPACE:
            #A C++ namespace.
            raise Exception("Unsupported type namespace")

        elif t == gdb.TYPE_CODE_DECFLOAT:
            #A decimal floating point type.
            raise Exception("Unsupported type decimal float")

        elif t == gdb.TYPE_CODE_INTERNAL_FUNCTION:
            #A function internal to GDB. This is the type used to represent convenience functions.
            raise Exception("Unsupported type internal function")

        else:
            error = irv.Error()
            irvVal.vError.info = "Unknown Value type" + str(v.type)
            print("Unknown Value type" + str(v.type))

        return irvVal

    def produceEvent(self, eventId, sender, runtimeEvent):
        try:
            isMonitor = sender in self.knownMonitors
            if isMonitor:
                msg = self.rootMessage(self.scnSenderId)
                msg.expectedVerdicts.number = 1
                # FIXME: The scenario will only ever receive one verdict
                # at a time. Less that ideal but okay for many situations.
                # The program will stop at the same place for several
                # breakpoints set at the same location.

                self.send(msg)

            msg = self.rootMessage(sender)

            msg.event.requestId = eventId

            msg.event.parameters.extend([
                self.irvValueFromAdapterParameterValue(p)
                    for p in runtimeEvent.parameters
            ])

            self.send(msg)

        except:
            traceback.print_exc()

        return self.waitResume()

    def waitResume(self):
        try:
            while True:
                msg = self.getMessage()

                if msg.HasField("bye"):
                    #FIXME: Let us ignore this for now
                    pass

                elif msg.HasField("hello"):
                    if msg.hello.serviceName == "scenario":
                        self.scnSenderId = msg.sender

                    elif msg.hello.serviceName == "monitor":
                        self.knownMonitors.add(msg.sender);

                elif msg.HasField("bye"):
                    if msg.sender == self.scnSenderId:
                        self.scnSenderId = 0
                    elif msg.sender in self.knownMonitors:
                        self.knownMonitors.remove(msg.sender);

                elif msg.HasField("eventRequest"):
                    eventId = msg.eventRequest.requestId

                    params = tuple(self.paramFromIrv(p) for p in msg.eventRequest.parameters)

                    if msg.eventRequest.kind in [irv.EventRequest.METHOD_ENTRY, irv.EventRequest.METHOD_EXIT, irv.EventRequest.METHOD_EXIT_WITH_RETURN_VALUE]:
                        eventName = (
                            (msg.eventRequest.className + "::" + msg.eventRequest.methodName)
                                if msg.eventRequest.className
                                else msg.eventRequest.methodName
                        )

                        p = CallEvent(
                            eventName,
                            before = (msg.eventRequest.kind == irv.EventRequest.METHOD_ENTRY),
                            parameters = params
                        )

                    elif msg.eventRequest.kind == irv.EventRequest.BREAKPOINT:
                        p = CallEvent(
                            msg.eventRequest.className + ":" + msg.eventRequest.lineNumber,
                            parameters = params
                        )

                    elif msg.eventRequest.kind == irv.EventRequest.FIELD_ACCESS:
                        eventName = (
                            (msg.eventRequest.className + "." + msg.eventRequest.fieldName)
                                if msg.eventRequest.className
                                else msg.eventRequest.fieldName
                        )

                        p = WatchEvent(
                            name=eventName,
                            access_type=WatchEventType.READ,
                            parameters=tuple(self.paramFromIrv(p) for p in eventRequest.parameters)
                        )

                    elif msg.eventRequest.kind == irv.EventRequest.FIELD_MODIFICATION:
                        eventName = (
                            (msg.eventRequest.className + "." + msg.eventRequest.fieldName)
                                if msg.eventRequest.className
                                else msg.eventRequest.fieldName
                        )

                        p = WatchEvent(
                            name=eventName,
                            access_type=WatchEventType.WRITE,
                            parameters=tuple(self.paramFromIrv(p) for p in msg.eventRequest.parameters)
                        )

                    else:
                        # METHOD_EXIT, METHOD_EXIT_WITH_RETURN_VALUE, other:
                        raise Exception("Unsupported Event Kind " + str(msg.eventRequest.kind))

                    request = self.dbgAdapter.instrument(
                        p,
                        functools.partial(self.produceEvent, eventId, msg.sender)
                    )

                    self.requestByEventId[eventId] = request

                    reply = self.rootMessage(msg.sender)
                    reply.requestOk.requestId = msg.eventRequest.requestId
                    self.send(reply)

                elif msg.HasField("eventRelease"):
                    eventId = msg.eventRelease.requestId

                    request = self.requestByEventId.get(eventId)

                    if request:
                            self.dbgAdapter.release(request)
                    else:
                        print("WARNING: We've been asked to release an event that is not instrumented.")

                    reply = self.rootMessage(msg.sender)
                    reply.requestOk.requestId = msg.eventRelease.requestId
                    self.send(reply)

                elif msg.HasField("resumeExec"):
                    return msg.resumeExec.suspend

                elif msg.HasField("checkpointRequest"):
                    cid = (msg.sender, msg.checkpointRequest.requestId)
                    if cid in self.prgmCheckpoints:
                        reply = self.rootMessage(msg.sender)
                        reply.error.info = "This checkpoint id is already taken."
                        self.send(reply)
                    else:
                        try:
                            self.prgmCheckpoints[cid] = criu_checkpoint()
                            self.dbgAdapter.checkpoint(cid)

                            reply = self.rootMessage(msg.sender)
                            reply.checkpoint.requestId = msg.checkpointRequest.requestId
                            self.send(reply)
                        except:
                            reply = self.rootMessage(msg.sender)
                            reply.error.info = "Checkpointing failed."
                            print("Checkpointing failed:")
                            traceback.print_exc()
                            self.send(reply)
                            # Fixme include checkpoint id in the error.

                elif msg.HasField("checkpointRestoreRequest"):
                    cid = (msg.sender, msg.checkpointRestoreRequest.requestId)

                    try:
                        criu_restore_checkpoint(self.prgmCheckpoints[cid])
                        self.dbgAdapter.restore_checkpoint(cid)

                    except KeyError:
                            reply = self.rootMessage(msg.sender)
                            reply.error.info = "This checkpoint was not found."
                            self.send(reply)

                    except:
                            reply = self.rootMessage(msg.sender)
                            reply.error.info = "Restoring checkpoint failed."
                            self.send(reply)

                else:
                    self.unexpectedMessage(msg, "EventRequest or ResumeExec")
        except:
            traceback.print_exc()

        return True
