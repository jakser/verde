package verde;

public interface ScriptInterpreterDict {
    public void set(String key, Object value);
    public Object get(String key);
}
