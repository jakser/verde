// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.util.Collection;

import org.mozilla.javascript.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JSInterpreter extends ScriptInterpreter {
    Scriptable scope;
    Context cx;

    JSInterpreter() {
        cx = Context.enter();
        scope = cx.initStandardObjects();
    }

    private class Dict extends ScriptableObject implements ScriptInterpreterDict {
        public void set(String key, Object value) {
            defineProperty(key, value, ScriptableObject.CONST);
        }

        public Object get(String key) {
            return get(key, this);
        }

        public String getClassName() {
            return "Dict";
        }
    }

    public void exec() throws IOException {
        cx.evaluateString(scope, new String(Files.readAllBytes(Paths.get(filename))), filename, 1, null);
    }

    public Object call(Object f) throws Exception {
        if (!(f instanceof Function)) {
            throw new Exception("This is not a function.");
        }

        return ((Function)f).call(cx, scope, scope, new Object[] {});
    }

    public Dict newDict() {
        return new Dict();
    }

    public Object array(Collection c) {
        return new NativeArray(c.toArray());
    }

    public boolean isFunction(Object f) {
        return (f instanceof BaseFunction);
    }

    public Object call(Object f, Object arg1) throws Exception {
        if (!(f instanceof Function)) {
            throw new Exception("This is not a function.");
        }

        return ((Function)f).call(cx, scope, scope, new Object[] {arg1});
    }

    public Object call(Object f, Object[] args) throws Exception {
        if (!(f instanceof Function)) {
            throw new Exception("This is not a function.");
        }

        return ((Function)f).call(cx, scope, scope, args);
    }

    public int getArity(Object f) throws Exception {
        if (!(f instanceof BaseFunction)) {
            throw new Exception("This is not a function.");
        }

        return ((BaseFunction) f).getLength();
        // getLength is used instead of getArity because of wrong results when
        // arrow functions are used (https://github.com/mozilla/rhino/pull/478)
    }

    public Object objectFromString(String s) {
        return s;
    }

    public Object toObject(Object o) {
        return o;
    }

    public Object objectFromInteger(int i) {
        return i;
    }

    public Object get(String key) {
        return scope.get(key, scope);
    }

    public void set(String key, Object value) {
        scope.put(key, scope, value);
    }
}
