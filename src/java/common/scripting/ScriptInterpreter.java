// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.io.IOException;
import java.util.Collection;

public abstract class ScriptInterpreter {
    protected String filename;

    public static ScriptInterpreter fromFile(String fn) throws Exception {
        ScriptInterpreter si = null;
        if (fn.endsWith(".js")) {
            si = new JSInterpreter();
        } else if (fn.endsWith(".py")) {
            si = new PythonInterpreter();
        } else {
            throw new Exception("Unrecognized filename extension");
        }

        si.setFileName(fn);
        return si;
    }

    public void setFileName(String fn) {
        filename = fn;
    }

    public abstract void exec() throws IOException;

    public abstract Object objectFromString(String s);
    public abstract Object objectFromInteger(int i);
    public abstract Object toObject(Object o);

    public abstract int getArity(Object f) throws Exception;
    public abstract boolean isFunction(Object f);

    public abstract Object array(Collection c);

    public abstract Object call(Object f) throws Exception;
    public abstract Object call(Object f, Object arg1) throws Exception;
    public abstract Object call(Object f, Object[] args) throws Exception;
    public abstract Object get(String key);
    public abstract void set(String key, Object o);

    public abstract ScriptInterpreterDict newDict();
}
