// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.util.Collection;

import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyInteger;
import org.python.core.PyLong;
import org.python.core.PyString;
import org.python.core.PyObject;
import org.python.core.PyFunction;
import org.python.core.PyList;
import org.python.core.PyDictionary;
import org.python.core.PyBaseCode;

public class PythonInterpreter extends ScriptInterpreter {
    org.python.util.PythonInterpreter interp;

    private class Dict extends PyDictionary implements ScriptInterpreterDict {
        public void set(String key, Object value) {
            put(new PyString(key), value instanceof PyObject ? value : Py.java2py(value));
        }

        public Object get(String key) {
            return get(new PyString(key));
        }
    }

    PythonInterpreter() {
        interp = new org.python.util.PythonInterpreter();
    }

    public void exec() {
        interp.execfile(filename);
    }

    public Object objectFromString(String s) {
        return new PyString(s);
    }

    public Object objectFromInteger(int i) {
        return new PyInteger(i);
    }

    public Object toObject(Object o) {
        return Py.java2py(o);
    }

    public ScriptInterpreterDict newDict() {
        return new Dict();
    }

    public int getArity(Object f) throws Exception {
        if (!(f instanceof PyFunction)) {
            throw new Exception("This is not a function.");
        }

        //FIXME: Check that casting to PyBaseCode this works
        return ((PyBaseCode) ((PyFunction) f).__code__).co_argcount;
    }

    public Object call(Object f) throws Exception {
        if (!(f instanceof PyFunction)) {
            throw new Exception("This is not a function.");
        }

        return ((PyFunction) f).__call__();
    }

    public Object call(Object f, Object arg1) throws Exception {
        if (!(f instanceof PyFunction)) {
            throw new Exception("This is not a function.");
        }

        //FIXME type safety of arg1

        return ((PyFunction) f).__call__(new PyObject[] {(PyObject) arg1});
    }

    public Object call(Object f, Object[] args) throws Exception {
        if (!(f instanceof PyFunction)) {
            throw new Exception("This is not a function.");
        }

        PyObject[] pyArgs = new PyObject[args.length];

        int i = 0;
        for (Object o : args) {
            pyArgs[i++] = (PyObject) o;
        }

        return ((PyFunction) f).__call__(pyArgs);
    }

    public boolean isFunction(Object f) {
        return f instanceof PyFunction;
    }

    public Object array(Collection c) {
        PyList a = new PyList();

        for (Object o: c) {
            a.append(Py.java2py(o));
        }

        return a;
    }

    public Object get(String key) {
        return interp.get(key);
    }

    public void set(String key, Object o) {
        interp.set(key, o);
    }

}
