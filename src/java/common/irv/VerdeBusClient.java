// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.net.Socket;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class VerdeBusClient {
    private int senderId = 0;

    private Socket          busSocket;
    private OutputStream    out;
    private DataInputStream in;
    private final String serviceName;

    static private final boolean DBG_MSGS = (System.getenv("VERDE_DBG_MSGS") != null);
    static private final String TRACE_MSGS = System.getenv("VERDE_TRACE_MSGS");
    static private final boolean TRACE_TIME = (System.getenv("VERDE_TRACE_TIME") != null);
    static private final String PRGM_NAME = System.getenv("VERDE_PRGM_NAME");

    private FileOutputStream msgTrace = null;

    VerdeBusClient(String serviceName) {
        this.serviceName = serviceName;
        if (TRACE_MSGS != null && !TRACE_MSGS.isEmpty()) {
            try {
                msgTrace = new FileOutputStream(TRACE_MSGS); //FIXME not closed
                msgTrace.write(TRACE_TIME ? 1 : 0);
            } catch (IOException exc) {
                System.err.println("Could not open trace file for writing (" + TRACE_MSGS + ")");
            }
        }
    }

    protected Irv.Root.Builder rootMessage(int recipientId) {
        return Irv.Root.newBuilder()
            .addRecipients(recipientId)
            .setSender(senderId);
    }

    protected Irv.Root.Builder rootMessage(Iterable<Integer> recipientIds) {
        return Irv.Root.newBuilder()
            .addAllRecipients(recipientIds)
            .setSender(senderId);
    }

    // https://javadeveloperzone.com/java-basic/java-convert-double-to-byte-array/
    private static byte[] doubletoBytes(double dblValue) {
        long data = Double.doubleToRawLongBits(dblValue);
        return new byte[]{
            (byte) ((data >> 56) & 0xff),
            (byte) ((data >> 48) & 0xff),
            (byte) ((data >> 40) & 0xff),
            (byte) ((data >> 32) & 0xff),
            (byte) ((data >> 24) & 0xff),
            (byte) ((data >> 16) & 0xff),
            (byte) ((data >> 8) & 0xff),
            (byte) ((data >> 0) & 0xff),
        };
    }

    private static byte[] intToByteArray(int value) {
        return new byte[] {
            (byte)(value >>> 24),
            (byte)(value >>> 16),
            (byte)(value >>> 8),
            (byte)value};
    }

    protected void sendMessage(Irv.Root msg) throws IOException {
        if (DBG_MSGS) {
            System.out.println("Sending: " + msg);
        }

        byte[] irvMessageByteArray = msg.toByteArray();
        byte[] msgbytes = new byte[irvMessageByteArray.length + 4];
        System.arraycopy(intToByteArray(irvMessageByteArray.length), 0, msgbytes, 0, 4);
        System.arraycopy(irvMessageByteArray, 0, msgbytes, 4, irvMessageByteArray.length);
        out.write(msgbytes);

        if (msgTrace != null) {
            if (TRACE_TIME) {
                msgTrace.write(doubletoBytes(((double) System.nanoTime()) / 1000));
            }

            msgTrace.write(msgbytes);
            msgTrace.flush();
        }
    }

    protected Irv.Root getMessage() throws IOException {
        int msgSize;

        try {
            msgSize = in.readInt();
        } catch (IOException e) {
            if (System.getenv("VERDE_AUTO_RECONNECT") != null) {
                System.out.println("Connection lost, trying to reconnect.");
                // try to reconnect?
                establishConnectionToBus();
                return getMessage();
            } else {
                System.out.println("Connection lost. Set VERDE_AUTO_RECONNECT if you want be to automatically reconnect.");
                throw e;
            }
        }

        byte[] msgByteArray = new byte[msgSize];

        in.readFully(msgByteArray);

        Irv.Root msg = Irv.Root.parseFrom(msgByteArray);

        if (DBG_MSGS) {
            System.out.println("Received: " + msg);
        }

        if (msgTrace != null) {
            if (TRACE_TIME) {
                msgTrace.write(doubletoBytes(((double) System.nanoTime()) / 1000));
            }

            msgTrace.write(intToByteArray(msgSize));
            msgTrace.write(msgByteArray);
            msgTrace.flush();
        }

        return msg;
    }

    Irv.Hello handleHello(String programName, String programmingLanguage) throws Exception {
        if (programName         == null) programName         = "";
        if (programmingLanguage == null) programmingLanguage = "";

        sendMessage(
            rootMessage(Conf.BUS_SENDER_ID)
            .setHello(
                Irv.Hello.newBuilder()
                    .setProtocolVersion(Conf.PROTOCOL_VERSION)
                    .setServiceName(serviceName)
                    .setServiceVersion(Conf.SERVICE_VERSION)
                    .setAppName(Conf.APP_NAME)
                    .setAppVersion(Conf.APP_VERSION)
                    .setProgramName(programName)
                    .setProgrammingLanguage(programmingLanguage)
                    .build()
            ).build()
        );

        Irv.Root expectedHello = getMessage();

        if (expectedHello.getContentCase() != Irv.Root.ContentCase.HELLO) {
            throw new Exception("Unexpected packet, expected Hello");
        }

        Irv.Hello helloFromBus = expectedHello.getHello();
        senderId = expectedHello.getRecipients(0);

        if (System.getenv("VERDE_PRINT_ECID") != null) {
            System.out.println("Sender ID:" + senderId);
        }

        if (DBG_MSGS) {
            System.out.println("Hello from the bus:" + helloFromBus);
        }

        return helloFromBus;
    }

    void establishConnectionToBus() throws IOException {
        String busHost = System.getenv("VERDE_BUS_HOST");
        if (busHost == null) {
            busHost = Conf.BUS_HOST;
        }

        String busPortString = System.getenv("VERDE_BUS_PORT");
        int busPort = (
            (busPortString == null)
                ? Conf.BUS_PORT
                : Integer.parseInt(busPortString)
        );

        if (DBG_MSGS) {
            System.out.println("Connecting to bus " + busHost + ":" + busPort + " ...");
        }

        busSocket = new Socket(busHost, busPort);
        busSocket.setTcpNoDelay(true);

        out = busSocket.getOutputStream();
        in  = new DataInputStream(busSocket.getInputStream());
    }

    Irv.Hello connectToBus(String programName, String programmingLanguage) throws Exception {
        establishConnectionToBus();
        return handleHello(programName == null ? PRGM_NAME : programName, programmingLanguage);
    }

    Irv.Hello connectToBus(String programName) throws Exception {
        return connectToBus(programName, "");
    }

    Irv.Hello connectToBus() throws Exception {
        return connectToBus(PRGM_NAME, "");
    }

    void unexpectedMessage(Irv.Root irvMessage, String expected) {
        System.out.println("Unexpected message, expected " + expected + ". Message: " + irvMessage);
    }
}
