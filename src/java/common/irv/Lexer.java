// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

public class Lexer {
    int curLine = 1;
    int curCol  = 1;
    int curChar = 0;
    public final String s;

    public Lexer(String s) {
        this.s = s.replace("\r\n","\n").replace("\r", "\n");
        curLine = 1;
        curCol  = 1;
    }

    public void skipWhite() {
        while (!end() && java.lang.Character.isWhitespace(s.charAt(curChar))) {
            nextChar();
        }
    }

    public String next() {
        skipWhite();
        int firstChar = curChar;
        while (!end() && !java.lang.Character.isWhitespace(s.charAt(curChar))) {
            nextChar();
        }

        return s.substring(firstChar, curChar);
    }

    public String next(char end) {
        skipWhite();
        int firstChar = curChar;
        while (!end() && s.charAt(curChar) != end && !java.lang.Character.isWhitespace(s.charAt(curChar))) {
            nextChar();
        }

        return s.substring(firstChar, curChar);
    }

    public String next(String end) {
        skipWhite();
        int firstChar = curChar;
        while (!end() && end.indexOf(s.charAt(curChar)) == -1 && !java.lang.Character.isWhitespace(s.charAt(curChar))) {
            nextChar();
        }

        return s.substring(firstChar, curChar);
    }

    public char nextChar() {
        if (curChar >= s.length()) {
            return '\0';
        }

        char c = s.charAt(curChar);
        if (c == '\n') {
            curLine++;
            curCol = 0;
        }

        curChar++;
        curCol++;

        return c;
    }

    public char getChar() {
        if (end()) {
            return '\0';
        }

        return s.charAt(curChar);
    }

    public double nextNumber() {
        skipWhite();
        boolean dotEncountered = false;
        int begin = curChar;

        if (!end() && getChar() == '0') {
            nextChar();
            if (!end() && (java.lang.Character.toLowerCase(getChar()) == 'x')) {
                do {
                    nextChar();
                } while ("0123456789ABCDEF_".indexOf(java.lang.Character.toUpperCase(getChar())) != -1);
                return Double.parseDouble(s.substring(begin, curChar).replace("_", ""));
            }
        }

        while (
            !end()
            && (
                "0123456789_".indexOf(getChar()) != -1
                || (!dotEncountered && getChar() == '.')
            )) {
            if (!dotEncountered) {
                dotEncountered = getChar() == '.';
            }
            nextChar();
        }

        // TODO: checking if the number is correct (e.g: doesn't end with a dot)
        if (!end() && (getChar() == 'e' || getChar() == 'E')) {
            nextChar();
            if (!end() && (getChar() == '+' || getChar() == '-')) {
                nextChar();
            }
        }

        while (!end() && "0123456789_".indexOf(getChar()) != -1) {
            nextChar();
        }

        return Double.parseDouble(s.substring(begin, curChar));
    }

    public void eat(String str) throws Exception {
        if (!this.tryEat(str)) {
            parseError("Unexpected '" + next() + "', expecting '" + str + "'" + ". String: " + s);
        }
    }

    public void parseError(String str) throws Exception {
        throw new Exception("Parse error on line " + curLine + ", column " + curCol + " : " + str + ". String: " + s);
    }

    public boolean tryEat(String str) {
        skipWhite();

        if (curChar + str.length() > s.length()) {
            return false;
        }

        for (int i = 0; i < str.length(); i++) {
            if (s.charAt(curChar + i) != str.charAt(i)) {
                return false;
            }
        }

        for (int i = 0; i < str.length(); i++) {
            nextChar();
        }

        return true;
    }


    public boolean end() {
        return curChar >= s.length();
    }
}
