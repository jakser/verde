// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

public class Conf {
    public final static String BUS_HOST                             = "127.0.0.1";
    public final static int    BUS_PORT                             = 13375;
    public final static int    BUS_SENDER_ID                        = 0;
    public final static int    PROTOCOL_VERSION                     = 20170613;
    public final static int    SERVICE_VERSION                      = 20170613;
    public final static String APP_NAME                             = "Verde";
    public final static String EXECUTION_CONTROLLER_SERVICE_NAME    = "executionController";
    public final static String MONITOR_SERVICE_NAME                 = "monitor";
    public final static String SCENARIO_SERVICE_NAME                = "scenario";
    public final static String APP_VERSION                          = "2017.06.13";
}
