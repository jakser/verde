// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

import verde.point.parameter.*;

import verde.Lexer;

public abstract class Point {
    List<ParameterSpec> params;

    public Point(List<ParameterSpec> params) {
        this.params = params;
    }

    public Point(ParameterSpec[] params) {
        this.params = Arrays.asList(params);
    }

    public abstract boolean equals(Point p);

    private static ParameterSpec parseParameter(Lexer l) throws Exception {
        return Parameter.parse(l, ",)");
    }

    private static List<ParameterSpec> parseParameters(Lexer l) throws Exception {
        l.skipWhite();

        if (l.getChar() != '(') {
            return java.util.Collections.emptyList();
        }

        l.nextChar();

        List<ParameterSpec> parameters = new LinkedList<>();

        l.skipWhite();

        while (l.getChar() != ')') {
            ParameterSpec p = parseParameter(l);

            parameters.add(p);

            if (l.getChar() != ')') {
                l.eat(",");
                l.skipWhite();
                if (l.getChar() == ')') {
                    l.parseError("Unexpected ')'");
                }
            }
        }

        l.eat(")");

        return parameters;
    }

    private static MethodBreakpoint parseMethod(Lexer l, boolean entry) throws Exception {
        String className = "";
        String methodName = l.next('(');
        int classMethodSep = methodName.lastIndexOf(".");
        if (classMethodSep != -1) {
            className = methodName.substring(0, classMethodSep);
            methodName = methodName.substring(classMethodSep + 1);
        }

        return new MethodBreakpoint(className, methodName, entry, null, parseParameters(l));
    }

    private static LineBreakpoint parseLine(Lexer l) throws Exception {
        String fileName = l.next(':');
        l.eat(":");
        int line = (int) l.nextNumber();

        return new LineBreakpoint(fileName, line, parseParameters(l));
    }

    public static Point fromSpec(String spec) throws Exception {
        Lexer l = new Lexer(spec);

        if (l.tryEat("MethodEntry")) {
            return parseMethod(l, true);
        }

        if (l.tryEat("MethodExit")) {
            return parseMethod(l, false);
        }

        if (l.tryEat("Method") || l.tryEat("method")) {
            return parseMethod(l, true);
        }

        if (l.tryEat("Line") || l.tryEat("line")) {
            return parseLine(l);
        }

        l.parseError("Expected Method or Line");
        return null;
    }

    public abstract verde.Irv.EventRequest.Builder toIrv();
}
