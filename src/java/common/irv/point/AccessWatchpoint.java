// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

import java.util.List;
import verde.point.parameter.ParameterSpec;
import verde.point.parameter.Parameter;

public class AccessWatchpoint extends Point {
    public String className;
    public String fieldName;

    public AccessWatchpoint(String className, String methodName, List<ParameterSpec> params) {
        super(params);
        this.className  = className;
        this.fieldName  = fieldName;
    }

    public AccessWatchpoint(String className, String methodName, ParameterSpec[] params) {
        super(params);
        this.className  = className;
        this.fieldName  = fieldName;
    }

    public boolean equals(Point p) {
        if (p instanceof AccessWatchpoint) {
            return (
                this.className.equals(((AccessWatchpoint) p).className) &&
                this.fieldName.equals(((AccessWatchpoint) p).fieldName)
            );
        }

        return false;
    }

    public verde.Irv.EventRequest.Builder toIrv() {
        return (
            verde.Irv.EventRequest.newBuilder()
                .setKind(verde.Irv.EventRequest.EventKind.FIELD_ACCESS)
                .setClassName(className)
                .setFieldName(fieldName)
                .addAllParameters(Parameter.listToIrv(params))
        );
    }

    public String toString() {
        return "AccessWatchpoint<" + className + "." + fieldName + ">";
    }
}
