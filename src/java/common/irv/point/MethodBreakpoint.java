// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

import java.util.List;
import java.util.Arrays;
import verde.point.parameter.ParameterSpec;
import verde.point.parameter.Parameter;

public class MethodBreakpoint extends Point {
    public String className;
    public String methodName;
    public List<String> methodArgs;
    public boolean isEntry;

    public MethodBreakpoint(String className, String methodName, boolean isEntry, List<String> methodArgs, List<ParameterSpec> params) {
        super(params);
        this.className  = className;
        this.methodName = methodName;
        this.methodArgs = methodArgs;
        this.isEntry    = isEntry;
    }

    public MethodBreakpoint(String className, String methodName, boolean isEntry, String[] methodArgs, ParameterSpec[] params) {
        super(params);
        this.className  = className;
        this.methodName = methodName;
        this.methodArgs = Arrays.asList(methodArgs);
        this.isEntry    = isEntry;
    }

    public boolean equals(Point p) {
        if (p instanceof MethodBreakpoint) {
            return (
                this.className.equals(((MethodBreakpoint) p).className) &&
                this.methodName.equals(((MethodBreakpoint) p).methodName) &&
                this.methodArgs.equals(((MethodBreakpoint) p).methodArgs)
            );
        }

        return false;
    }

    public verde.Irv.EventRequest.Builder toIrv() {
        return (
            verde.Irv.EventRequest.newBuilder()
                .setKind(isEntry ? verde.Irv.EventRequest.EventKind.METHOD_ENTRY : verde.Irv.EventRequest.EventKind.METHOD_EXIT)
                .setClassName(className)
                .setMethodName(methodName)
                .addAllParameters(Parameter.listToIrv(params))
        );
    }

    public String toString() {
        return "MethodBreakpoint<" + className + "." + methodName + ">";
    }
}
