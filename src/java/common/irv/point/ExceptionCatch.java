// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

import java.util.List;
import verde.point.parameter.ParameterSpec;
import verde.point.parameter.Parameter;

public class ExceptionCatch extends Point {
    public String className;
    public boolean includeCaughtExceptions;
    public boolean includeUncaughtExceptions;

    public ExceptionCatch(String className, boolean includeCaughtExceptions, boolean includeUncaughtExceptions, List<ParameterSpec> params) {
        super(params);
        this.className                 = className;
        this.includeCaughtExceptions   = includeCaughtExceptions;
        this.includeUncaughtExceptions = includeUncaughtExceptions;
    }

    public ExceptionCatch(String className, String methodName, ParameterSpec[] params) {
        super(params);
        this.className                 = className;
        this.includeCaughtExceptions   = includeCaughtExceptions;
        this.includeUncaughtExceptions = includeUncaughtExceptions;
    }

    public boolean equals(Point p) {
        if (p instanceof ExceptionCatch) {
            ExceptionCatch ec = (ExceptionCatch) p;
            return (
                this.className.equals(ec.className) &&
                this.includeCaughtExceptions   == ec.includeCaughtExceptions   &&
                this.includeUncaughtExceptions == ec.includeUncaughtExceptions
            );
        }

        return false;
    }

    public verde.Irv.EventRequest.Builder toIrv() {
        return (
            verde.Irv.EventRequest.newBuilder()
                .setKind(verde.Irv.EventRequest.EventKind.EXCEPTION_CATCH)
                .setClassName(className)
                .setIncludeCaughtExceptions(includeCaughtExceptions)
                .setIncludeUncaughtExceptions(includeUncaughtExceptions)
                .addAllParameters(Parameter.listToIrv(params))
        );
    }

    public String toString() {
        return "ExceptionCatch <" + className + ", caught=" + includeCaughtExceptions + ", uncaught=" + includeUncaughtExceptions + ">";
    }
}
