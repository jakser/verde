// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

public class ExpressionParameter implements ParameterSpec {
    public String expression;
    public ExpressionParameter(String expression) {
        this.expression = expression;
    }

    public String toString() {
        return "Expression parameter " + expression;
    }

    public verde.Irv.ValueRequest.Builder toIrv() {
        return verde.Irv.ValueRequest.newBuilder().setExpression(expression);
    }
}
