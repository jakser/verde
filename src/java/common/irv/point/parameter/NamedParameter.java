// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

public class NamedParameter implements ParameterSpec {
    public final String name;

    public NamedParameter(String name) {
        this.name = name;
    }

    public String toString() {
        return "Named parameter " + this.name;
    }

    public verde.Irv.ValueRequest.Builder toIrv() {
        return verde.Irv.ValueRequest.newBuilder().setName(name);
    }
}
