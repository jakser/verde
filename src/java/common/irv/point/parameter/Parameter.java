// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import verde.Lexer;

// FIXME merge with ParameterSpec?

public class Parameter {

    //FIXME: using an HashMap for that is probably not ideal
    private static HashMap<Integer, MethodParameterByIndex> paramsByIndex = new HashMap<>();

    private static HashMap<String, NamedParameter> paramsByName = new HashMap<>();
    private static HashMap<String, ExpressionParameter> paramsByExpression = new HashMap<>();

    public static NamedParameter byName(String name) {
        NamedParameter p = paramsByName.get(name);

        if (p == null) {
            p = new NamedParameter(name);
            paramsByName.put(name, p);
        }

        return p;
    }

    public static ExpressionParameter byExpression(String expr) {
        ExpressionParameter p = paramsByExpression.get(expr);

        if (p == null) {
            p = new ExpressionParameter(expr);
            paramsByExpression.put(expr, p);
        }

        return p;
    }

    public static MethodParameterByIndex byIndex(int index) {
        MethodParameterByIndex p = paramsByIndex.get(index);

        if (p == null) {
            p = new MethodParameterByIndex(index);
            paramsByIndex.put(index, p);
        }

        return p;
    }

    public static ThisParameter THIS = new ThisParameter();

    // WARNING: don't use these facilities if you use localName.

    public static ParameterSpec parse(Lexer l, String end) throws Exception {
        l.skipWhite();

        if (l.tryEat("#")) {
            return new MethodParameterByIndex((int) l.nextNumber());
        }

        if (l.tryEat("this")) {
            return new ThisParameter();
        }

        String n = l.next(end);

        if (n == null || "".equals(n)) {
            l.parseError("Expected a parameter");
        }

        return new NamedParameter(n); //FIXME check correctness of the identifier here
    }

    public static ParameterSpec parse(String s) throws Exception {
        return parse(new Lexer(s), "");
    }

    public static List<verde.Irv.ValueRequest> listToIrv(List<ParameterSpec> params) {
        ArrayList<verde.Irv.ValueRequest> res = new ArrayList<>(params.size());

        for (ParameterSpec p : params) {
            res.add(p.toIrv().build());
        }

        return res;
    }
}
