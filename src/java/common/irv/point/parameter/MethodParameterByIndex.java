// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

public class MethodParameterByIndex implements ParameterSpec {
    public final int index;

    public MethodParameterByIndex(int index) {
        this.index = index;
    }

    public String toString() {
        return "Parameter number " + this.index;
    }

    public verde.Irv.ValueRequest.Builder toIrv() {
        return verde.Irv.ValueRequest.newBuilder().setArgNumber(index);
    }
}
