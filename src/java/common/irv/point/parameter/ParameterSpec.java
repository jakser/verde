// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

public interface ParameterSpec {
    verde.Irv.ValueRequest.Builder toIrv();
}
