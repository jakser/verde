// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point.parameter;

public class ThisParameter implements ParameterSpec {
    public ThisParameter() { }

    public String toString() {
        return "Parameter this";
    }

    public verde.Irv.ValueRequest.Builder toIrv() {
        return verde.Irv.ValueRequest.newBuilder().setThisObject(true);
    }
}
