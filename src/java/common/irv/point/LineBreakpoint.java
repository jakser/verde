// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

import java.util.List;
import verde.point.parameter.ParameterSpec;
import verde.point.parameter.Parameter;

public class LineBreakpoint extends Point {
    public String fileName;
    public int lineNumber;

    public LineBreakpoint(String fileName, int lineNumber, List<ParameterSpec> params) {
        super(params);
        this.fileName = fileName;
        this.lineNumber = lineNumber;
    }

    public LineBreakpoint(String className, int lineNumber, ParameterSpec[] params) {
        super(params);
        this.fileName = className;
        this.lineNumber = lineNumber;
    }

    public boolean equals(Point p) {
        if (p instanceof LineBreakpoint) {
            return (
                this.lineNumber == ((LineBreakpoint) p).lineNumber &&
                this.fileName.equals(((LineBreakpoint) p).fileName)
            );
        }

        return false;
    }

    public verde.Irv.EventRequest.Builder toIrv() {
        return (
            verde.Irv.EventRequest.newBuilder()
                .setKind(verde.Irv.EventRequest.EventKind.BREAKPOINT)
                .setSourceName(fileName)
                .setClassName(fileName)
                .setLineNumber(lineNumber)
                .addAllParameters(Parameter.listToIrv(params))
        );
    }

    public String toString() {
        return "LineBreakpoint <" + fileName + ":" + lineNumber + ">";
    }
}
