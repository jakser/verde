// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

public class PointNotFoundException extends Exception {
    public Exception originalException = null;
    public PointNotFoundException(Exception originalException) {
        this.originalException = originalException;
    }
}
