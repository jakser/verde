// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde.point;

public class PointNotSupportedException extends Exception {
    String reason;
    public PointNotSupportedException(String reason) {
        this.reason = reason;
    }
}
