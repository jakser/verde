from __future__ import print_function


def nonAccepting():
    scenario.suspendExecution(True)
    scenario.debuggerPrompt(
        "Hello! the program is wrong. Please input 'ok'\n",
        (lambda a: (print("User answered: " + a)))
    )

scenario.addReaction(
    "any non-accepting verdict",
    nonAccepting
)
