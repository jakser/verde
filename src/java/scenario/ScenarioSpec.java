// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.util.Set;
import java.util.Collection;
import java.util.HashSet;
import java.util.Collections;

public class ScenarioSpec {
    public enum Type {
        IS_ACCEPTING,
        IS_NON_ACCEPTING,
        IS_STATE,
        IS_INITIAL_VERDICT,
        IS_NON_INITIAL_VERDICT,
        CONTAINS_VALUE,
        IS_PROPERTY,
        AND,
        OR,
        NOT,
        TRUE
    }

    static private class Quantifier {
        public enum Type {
            ANY,
            ALL,
            MORE_THAN_NUMBER,
            LESS_THAN_NUMBER,
            MORE_THAN_RATE,
            LESS_THAN_RATE
        }

        Type type;
        double number;

        Quantifier(Type t) {
            type = t;
        }

        public static final Quantifier ANY = new Quantifier(Quantifier.Type.ANY);
        public static final Quantifier ALL = new Quantifier(Quantifier.Type.ALL);

        public static Quantifier parse(Lexer sc) throws Exception {
            if (sc.tryEat("any")) {
                return Quantifier.ANY;
            }

            if (sc.tryEat("all")) {
                return Quantifier.ALL;
            }

            String comparative;
            if (sc.tryEat("more")) {
                comparative = "more";
            } else if (sc.tryEat("less")) {
                comparative = "less";
            } else {
                return null;
            }

            sc.eat("than");

            double d;

            try {
                d = sc.nextNumber();
            } catch (Exception e) {
                sc.parseError("Specification: " + sc.s + "\n" + "Parse error: expected a number after " + comparative + " than");
                return null;
            }

            sc.skipWhite();

            if (sc.getChar() == '%') {
                sc.nextChar();

                Quantifier q = new Quantifier(
                    "more".equals(comparative)
                        ? Quantifier.Type.MORE_THAN_RATE
                        : Quantifier.Type.LESS_THAN_RATE
                );
                q.number = d / 100;

                return q;
            }

            Quantifier q = new Quantifier(
                "more".equals(comparative)
                    ? Quantifier.Type.MORE_THAN_NUMBER
                    : Quantifier.Type.LESS_THAN_NUMBER
            );
            q.number = d;

            return q;
        }
    }

    private ScenarioSpec(Type t) {
        type = t;
    }


    Quantifier q = Quantifier.ANY;

    ScenarioSpec s1 = null;
    ScenarioSpec s2 = null;

    Type type = null;
    Object value;
    String state;
    String property;

    public static ScenarioSpec parse(String spec) throws Exception {
        ScenarioSpec s = parse(new Lexer(spec));

        if (s == null) {
            throw new Exception("Could not parse the specification.");
        }

        return s;
    }

    public static ScenarioSpec parse(Lexer sc) throws Exception {
        if (sc.tryEat("(")) {
            ScenarioSpec spec = parse(sc);
            sc.eat(")");

            ScenarioSpec specRoot;

            if (sc.tryEat("and")) {
                specRoot = new ScenarioSpec(Type.AND);
            } else if (sc.tryEat("or")) {
                specRoot = new ScenarioSpec(Type.OR);
            } else {
                return spec;
            }

            specRoot.s1 = spec;
            specRoot.s2 = parse(sc);

            if (specRoot.s2 == null) {
                sc.parseError("Expected a Scenario spec after 'and' or 'or'");
            }

            return specRoot;
        }

        ScenarioSpec spec = null;

        Quantifier q = Quantifier.parse(sc);

        boolean verdictKeyword = false;

        if (sc.tryEat("accepting")) {
            spec = new ScenarioSpec(Type.IS_ACCEPTING);
        } else if (sc.tryEat("non-accepting")) {
            spec = new ScenarioSpec(Type.IS_NON_ACCEPTING);
        } else if (sc.tryEat("state")) {
            spec = new ScenarioSpec(Type.IS_STATE);
            spec.state = sc.next();
        } else if (sc.tryEat("initial")) {
            spec = new ScenarioSpec(Type.IS_INITIAL_VERDICT);
            if (sc.tryEat("verdict")) {
                verdictKeyword = true;
            } else {
                sc.eat("state");
            }
        }

        if (verdictKeyword || sc.tryEat("verdicts") || sc.tryEat("verdict")) {
            if (sc.tryEat("with")) {
                ScenarioSpec after = parseAfter(sc);

                if (spec == null) {
                    return after;
                }

                ScenarioSpec specRoot = new ScenarioSpec(Type.AND);
                specRoot.s1 = spec;
                specRoot.s2 = after;

                spec = specRoot;
            } else {
                if (spec == null) {
                    return new ScenarioSpec(Type.TRUE);
                }

                return spec;
            }
        }

        if (spec == null) {
            return null;
        }

        if (q == null) {
            if (sc.tryEat("for")) {
                q = Quantifier.parse(sc);
                if (q == null) {
                    sc.parseError("Could not parse scenario specification");
                }
            } else {
                q = Quantifier.ANY;
            }
        }

        spec.q = q;

        return spec;
    }

    public static ScenarioSpec parseAfter(Lexer sc) throws Exception {
        ScenarioSpec spec = null;

        if (sc.tryEat("not")) {
            spec = new ScenarioSpec(Type.NOT);
            spec.s1 = parseAfter(sc);
            if (spec.s1 == null) {
                sc.parseError("Expected condition after 'not'");
            }
            return spec;
        } else if (sc.tryEat("non-accepting state")) {
            spec = new ScenarioSpec(Type.IS_NON_ACCEPTING);
        } else if (sc.tryEat("accepting state")) {
            spec = new ScenarioSpec(Type.IS_ACCEPTING);
//         } else if (sc.tryEat("value")) {
//             spec = new ScenarioSpec(Type.CONTAINS_VALUE);
//             spec.value = parseValue();
        } else if (sc.tryEat("state")) {
            spec = new ScenarioSpec(Type.IS_STATE);
            spec.state = sc.next();
        } else if (sc.tryEat("initial")) {
            spec = new ScenarioSpec(Type.IS_INITIAL_VERDICT);
            if (!sc.tryEat("verdict")) {
                sc.eat("state");
            }
        } else if (sc.tryEat("non-initial")) {
            spec = new ScenarioSpec(Type.IS_NON_INITIAL_VERDICT);
            if (!sc.tryEat("verdict")) {
                sc.eat("state");
            }
        } else if (sc.tryEat("for property")) {
            spec = new ScenarioSpec(Type.IS_PROPERTY);
            spec.property = sc.next();
            return spec;
        }

        boolean and = sc.tryEat("and");
        boolean or  = !and && sc.tryEat("or");

        if (and || or) {
            boolean withParentheses = false;

            withParentheses = sc.tryEat("(");

            ScenarioSpec s2 = parseAfter(sc);

            if (withParentheses) {
                sc.eat(")");
            }

            if (s2 == null) {
                sc.parseError("Expected a condition after '" + (and ? "and" : "or") + "'");
            }

            ScenarioSpec specRoot = new ScenarioSpec(and ? Type.AND : Type.OR);
            specRoot.s1 = spec;
            specRoot.s2 = s2;
            return specRoot;
        }

        return spec;
    }

    public boolean verdictMatches(Verdict verdict) {
        switch (type) {
            case IS_ACCEPTING:
                return verdict.getAccepting().equals(Irv.Verdict.Accepting.YES);

            case IS_NON_ACCEPTING:
                return verdict.getAccepting().equals(Irv.Verdict.Accepting.NO);

            case IS_STATE:
                return verdict.getStates().contains(state);

            case IS_INITIAL_VERDICT:
                return verdict.getInitial();

            case IS_NON_INITIAL_VERDICT:
                return !verdict.getInitial();

            case CONTAINS_VALUE:
                return verdict.getValues().contains(value);

            case IS_PROPERTY:
                return property.equals(verdict.getPropertyName()); // FIXME

            default:
                throw new RuntimeException("Unexpected scenario specification type");
        }
    }

    public boolean matches(Collection<Verdict> verdicts) {
        switch (type) {
            case TRUE:
                return true;

            case AND:
                return s1.matches(verdicts) && s2.matches(verdicts);

            case OR:
                return s1.matches(verdicts) || s2.matches(verdicts);

            case NOT:
                return !s1.matches(verdicts);

            default: {
                int matchingVerdictNumber = 0;

                for (Verdict v : verdicts) {
                    if (verdictMatches(v)) {
                        if (q.type == Quantifier.Type.ANY) {
                            return true;
                        }

                        if (q.type != Quantifier.Type.ALL) {
                            matchingVerdictNumber++;
                        }
                    } else if (q.type == Quantifier.Type.ALL) {
                        return false;
                    }
                }

                switch (q.type) {
                    case ANY:
                    case ALL:
                        return false;

                    case MORE_THAN_NUMBER:
                        return matchingVerdictNumber >= q.number;

                    case LESS_THAN_NUMBER:
                        return matchingVerdictNumber <= q.number;

                    case MORE_THAN_RATE:
                        return matchingVerdictNumber / verdicts.size() >= q.number;

                    case LESS_THAN_RATE:
                        return matchingVerdictNumber / verdicts.size() <= q.number;

                    default:
                        throw new RuntimeException("Wrong quantifier type " + q.type);
                }
            }
        }
    }

    public Collection<Verdict> matchingVerdicts(Collection<Verdict> verdicts) {
        switch (type) {
            case TRUE: {
                return verdicts;
            }

            case AND: {
                Collection<Verdict> v1 = s1.matchingVerdicts(verdicts);
                if (v1.isEmpty()) {
                    return v1;
                }

                Collection<Verdict> v2 = s2.matchingVerdicts(verdicts);
                if (v2.isEmpty()) {
                    return v2;
                }

                v1.addAll(v2);
                return v1;
            }

            case OR: {
                Collection<Verdict> v = s1.matchingVerdicts(verdicts);
                v.addAll(s2.matchingVerdicts(verdicts));
                return v;
            }

            case NOT: {
                Collection<Verdict> res = new HashSet<>(verdicts);
                res.removeAll(
                    s1.matchingVerdicts(verdicts)
                );

                return res;
            }

            default: {
                Collection<Verdict> res = (
                    q.type == Quantifier.Type.ALL
                        ? verdicts
                        : new HashSet<Verdict>()
                );

                for (Verdict v : verdicts) {
                    if (verdictMatches(v)) {
                        if (q.type != Quantifier.Type.ALL) {
                            res.add(v);
                        }
                    } else if (q.type == Quantifier.Type.ALL) {
                        return Collections.emptySet();
                    }
                }

                switch (q.type) {
                    case ANY:
                    case ALL:
                        return res;

                    case MORE_THAN_NUMBER:
                        return (
                            res.size() >= q.number
                                ? res
                                : Collections.emptySet()
                        );

                    case LESS_THAN_NUMBER:
                        return (
                            res.size() <= q.number
                                ? res
                                : Collections.emptySet()
                        );

                    case MORE_THAN_RATE:
                        return (
                            res.size() / verdicts.size() >= q.number
                                ? res
                                : Collections.emptySet()
                        );

                    case LESS_THAN_RATE:
                        return (
                            res.size() / verdicts.size() <= q.number
                                ? res
                                : Collections.emptySet()
                        );

                    default:
                        throw new RuntimeException("Wrong quantifier type " + q.type);
                }
            }
        }
    }
}
