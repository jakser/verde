scenario.addReaction(
    "any non-accepting verdict",
    lambda: scenario.suspendExecution(True)
)

scenario.warnOnMissingReaction(False)
