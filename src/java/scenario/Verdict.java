package verde;

import java.util.List;

public class Verdict {
    private Irv.Verdict verdict;
    private int from;

    Verdict(Irv.Verdict verdict, int from) {
        this.from = from;
        this.verdict = verdict;
    }

    public Irv.Verdict.Accepting getAccepting() {
        return verdict.getAccepting();
    }

    public boolean getInitial() {
        return verdict.getInitial();
    }

    public int getPropertyID() {
        return verdict.getPropertyID();
    }

    public String getPropertyName() {
        return verdict.getPropertyName();
    }

    public int getFromEC() {
        return verdict.getFromEC();
    }

    public List<String> getStates() {
        return verdict.getStateList();
    }

    public List<Irv.Value> getValues() {
        return verdict.getValueList();
    }

    public List<String> getOldStates() {
        return verdict.getOldStateList();
    }

    public List<String> getNewStates() {
        return verdict.getNewStateList();
    }

    public int getMonitor() {
        return this.from;
    }
}
