// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.io.File;
import java.io.FileReader;

import java.util.Set;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;

import verde.point.*;
import verde.point.parameter.Parameter;

public class Scenario extends VerdeBusClient {
    private class EventListener {
        public Object callback;
        public boolean once;

        public EventListener(Object callback, boolean once) {
            this.callback = callback;
            this.once = once;
        }
    }

    private class ScenarioReaction extends EventListener {
        public ScenarioSpec spec;

        public ScenarioReaction(ScenarioSpec spec, Object callback, boolean once) {
            super(callback, once);
            this.spec = spec;
        }
    }

    Set<EventListener> joinListeners = new HashSet<>();
    Set<EventListener> leaveListeners = new HashSet<>();

    private class RequestCallback {
        Point point;
        Object callback;
        boolean once;

        RequestCallback(Point point, Object callback, boolean once) {
            this.point = point;
            this.callback = callback;
            this.once = once;
        }
    }

    private boolean warnOnMissingReaction = true;

    private LinkedList<ScenarioReaction> reactions = new LinkedList<>();

    static private Scenario INSTANCE;

    private Set<Integer> knownMonitors = new HashSet<>();
    private Map<Integer, String> names = new HashMap<>();

    private Map<Integer, Integer> expectedVerdictNumbers = new HashMap<>();
    private Map<Integer, Boolean> receivedExpectedVerdictNumber = new HashMap<>();

    // This is a list and not a set because the same verdict can be received
    // from different monitors
    private Map<Integer, List<Verdict>> receivedVerdictsById = new HashMap<>();

    // Suspend execution next time we react to a set of verdicts
    private boolean suspendExecution = false;

    private Map<Long, Object> debuggerCmdCallbacks = new HashMap<>();

    private boolean blockResume = false;

    // FIXME: Maybe store this value in ScenarioReaction to avoid a "global" variable?
    private int currentEcid = 0;

    private ScriptInterpreter interp;
    long currentRequestId = 1;

    private long requestNumber = 1;
    Map<Long, RequestCallback> points = new HashMap<>();


    private int nbMonitors = -1;

    private class CallbackWithCounter {
        public Object callback;
        public int c;
        public int ecid;

        CallbackWithCounter(Object callback, int c, int ecid) {
            this.callback = callback;
            this.c = c;
            this.ecid = ecid;
        }
    }

    void handleJoinLeaveListeners(Set<EventListener> listenerSet, Object arg) throws Exception {
        Iterator<EventListener> it = listenerSet.iterator();
        while (it.hasNext()) {
            EventListener el = it.next();
            if (interp.getArity(el.callback) > 0) {
                interp.call(el.callback, arg);
            } else {
                interp.call(el.callback);
            }
        }
    }

    private void handleDbgCallback(int ecid, long id, Object reply) throws Exception {
        handleDbgCallbackCall(ecid, id, debuggerCmdCallbacks.get(id), reply);
    }

    private void handleDbgCallbackCall(int ecid, long id, Object callback, Object reply) throws Exception {
        currentEcid = ecid;

        blockResume = false;
        if (callback != null) {
            if (reply == null || interp.getArity(callback) == 0) {
                interp.call(callback);
            } else {
                interp.call(callback, interp.toObject(reply));
            }
        }

        currentEcid = 0;

        debuggerCmdCallbacks.remove(id);

        resume(ecid);
    }

    private void handleDbgCallback(int ecid, long id) throws Exception {
        handleDbgCallback(ecid, id, id);
    }

    private void handleDbgCallbackWithCounter(long id) throws Exception {
        CallbackWithCounter callbackWithCounter = (CallbackWithCounter) debuggerCmdCallbacks.get(id);

        if (callbackWithCounter != null && --callbackWithCounter.c == 0) {
            handleDbgCallbackCall(callbackWithCounter.ecid, id, callbackWithCounter.callback, id);
        }
    }

    public Scenario(String scenarioFile) throws Exception {
        super(Conf.SCENARIO_SERVICE_NAME);

        interp = ScriptInterpreter.fromFile(scenarioFile);
        interp.set("scenario", this);
        interp.exec();

        nbMonitors = connectToBus().getNbMonitors();

        boolean cont = true;

        do {
            Irv.Root msg = getMessage();

            if (msg == null) {
                return;
            }

            switch (msg.getContentCase()) {
                case BYE: {
                    int senderId = msg.getSender();

                    if (senderId == 0) {
//                         System.err.println("The bus left");
                        return;
                    }

                    handleJoinLeaveListeners(leaveListeners, senderId);

                    names.remove(senderId);

                    knownMonitors.remove(senderId);
                    // Maybe this sender is not a monitor but this is ok, this
                    // will just fail silently.
                    break;
                }


                case HELLO: {
                    Irv.Hello hello = msg.getHello();
                    int senderId = msg.getSender();
                    if (hello.getServiceName().equals(Conf.EXECUTION_CONTROLLER_SERVICE_NAME)) {
//                         System.out.println("An execution controller joined.");

                        expectedVerdictNumbers.put(
                            senderId,
                            Math.max(nbMonitors, knownMonitors.size())
                        );
                        receivedExpectedVerdictNumber.put(senderId, true);

                        if (joinListeners.size() > 0) {
                            ScriptInterpreterDict dict = interp.newDict();
                            dict.set("id", msg.getSender());
                            dict.set("programName", hello.getProgramName());
                            dict.set("programmingLanguage", hello.getProgrammingLanguage());

                            currentEcid = msg.getSender();
                            handleJoinLeaveListeners(joinListeners, dict);
                            currentEcid = 0;
                        }
                    } else if (hello.getServiceName().equals(Conf.MONITOR_SERVICE_NAME)) {
                        knownMonitors.add(senderId);

                        if (nbMonitors < knownMonitors.size()) {
                            for (Map.Entry<Integer, Integer> evn : expectedVerdictNumbers.entrySet()) {
                                // We now expect one more verdict for execution controllers
                                // because a monitor joined.
                                evn.setValue(evn.getValue() + 1);
                            }
                        }
                    }

                    names.put(senderId, hello.getProgramName());
                    break;
                }

                case EXPECTEDVERDICTS: {
                    int ecid = msg.getSender();

                    Irv.ExpectedVerdicts expectedVerdicts = msg.getExpectedVerdicts();

                    expectedVerdictNumbers.put(
                        ecid,
                        expectedVerdictNumbers.get(ecid) +
                        expectedVerdicts.getNumber()
                    );

                    receivedExpectedVerdictNumber.put(ecid, true);

                    maybeReact(ecid);
                    break;
                }

                case VERDICT: {
                    Irv.Verdict verdict = msg.getVerdict();
                    Integer ecid = verdict.getFromEC();
                    List<Verdict> verdicts = receivedVerdictsById.get(ecid);
                    if (verdicts == null) {
                        verdicts = new LinkedList<>();
                        receivedVerdictsById.put(ecid, verdicts);
                    }

                    verdicts.add(new Verdict(verdict, msg.getSender()));

                    if (receivedExpectedVerdictNumber.get(ecid)) {
                        maybeReact(ecid);
                    }
                    break;
                }

                case DEBUGGERCMDREPLY: {
                    blockResume = false;
                    Irv.DebuggerCmdReply reply = msg.getDebuggerCmdReply();
                    handleDbgCallback(msg.getSender(), reply.getRequestId(), reply.getReply());
                    break;
                }

                case CHECKPOINT: {
                    handleDbgCallbackWithCounter(msg.getCheckpoint().getRequestId());
                    break;
                }

                case CHECKPOINTRESTORED: {
                    handleDbgCallbackWithCounter(msg.getCheckpointRestored().getRequestId());
                    break;
                }

                case ERROR: {
                    System.err.println("We received an error: " + msg.getError().getInfo());
                    debuggerPrintln("SCENARIO: We received an error: " + msg.getError().getInfo());
                    break;
                }

                case REQUESTOK: {
                    handleDbgCallbackWithCounter(msg.getRequestOk().getRequestId());
                    break;
                }

                case EVENT: {

                    Irv.Event event = msg.getEvent();

                    long requestId = event.getRequestId();

                    RequestCallback rc = points.get(requestId);

                    if (rc == null) {
                        System.err.println("Received an event that was not asked.");
                        break;
                    }

                    currentEcid = msg.getSender();

                    interp.call(
                        rc.callback,
                        parametersToValues(event.getParametersList())
                    );

                    if (rc.once) {
                        // FIXME: use the count EventRequest field.
                        removePoint(requestId);
                    }

                    resume(msg.getSender());

                    currentEcid = 0;

                    break;
                }

                case VALUELIST: {
                    blockResume = false;

                    Irv.ValueList valueList = msg.getValueList();
                    long id = valueList.getRequestId();
                    Object callback = debuggerCmdCallbacks.get(id);

                    if (callback == null) {
                        System.err.println("Received a value list that was not asked.");
                        break;
                    }

                    debuggerCmdCallbacks.remove(id);

                    interp.call(
                        callback,
                        parametersToValues(valueList.getValueList())
                    );

                    break;
                }

                default: {
                    unexpectedMessage(msg, "Verdict");
                }
            }
        } while (cont);
    }

    Object parameterIrvToValue(Irv.Value v) {
        switch (v.getVCase()) {
            case VDOUBLE:
                return v.getVDouble();

            case VFLOAT:
                return v.getVFloat();

            case VSIGNEDINT8:
                return v.getVSignedInt8();

            case VUNSIGNEDINT8:
                return v.getVUnsignedInt8();

            case VSIGNEDINT4:
                return v.getVSignedInt4();

            case VUNSIGNEDINT4:
                return v.getVUnsignedInt4();

            case VSTRING:
                return v.getVString();

            case VBYTES:
                return v.getVBytes();

            case VBOOL:
                return v.getVBool();

            case VERROR:
                return new Exception(v.getVError().getErrId() + ": " + v.getVError().getInfo());

            case VALUES:
                List<Irv.Value> values = v.getValues().getValueList();
                ArrayList<Object> res = new ArrayList<>(values.size());
                for (Irv.Value val : values) {
                    res.add(parameterIrvToValue(val));
                }
                return res;

            default:
                throw new RuntimeException("Bad IRV value type");
        }
    }

    Object[] parametersToValues(List<Irv.Value> values) {
        Object[] res = new Object[values.size()];

        int i = 0;

        for (Irv.Value value : values) {
            res[i] = parameterIrvToValue(value);
            i++;
        }

        return res;
    }

    private void resume(int ecid) throws Exception {
        if (!blockResume) {
            sendMessage(
                rootMessage(ecid)
                .setResumeExec(
                    Irv.ResumeExec.newBuilder()
                    .setSuspend(suspendExecution)
                    .build()
                ).build()
            );
        }
    }

    private void maybeReact(int ecid) throws Exception {
        // We only allow scenarios to react and control the execution controller
        // that corresponds to this verdict id.


        List<Verdict> verdicts = receivedVerdictsById.get(ecid);
        if (verdicts == null) {
            verdicts = new LinkedList<>();
            receivedVerdictsById.put(ecid, verdicts);
        }

        int expectedVerdictNumber = expectedVerdictNumbers.get(ecid);

        if (verdicts.size() != expectedVerdictNumber) {
            return;
        }

        suspendExecution = false;
        blockResume = false;

        // All reactions will be executed, in order.

        boolean atLeastOneMatchingReaction = false;

        Iterator<ScenarioReaction> it = reactions.descendingIterator();

        currentEcid = ecid;

        while (it.hasNext()) {
            ScenarioReaction reaction = it.next();

            if (interp.getArity(reaction.callback) > 0) {
                Collection<Verdict> matchingVerdicts = reaction.spec.matchingVerdicts(verdicts);
                if (matchingVerdicts.size() > 0) {
                    interp.call(reaction.callback, interp.array(matchingVerdicts));
                    atLeastOneMatchingReaction = true;

                    if (reaction.once) {
                        it.remove();
                    }
                }
            } else {
                if (reaction.spec.matches(verdicts)) {
                    interp.call(reaction.callback);
                    atLeastOneMatchingReaction = true;

                    if (reaction.once) {
                        it.remove();
                    }
                }
            }
        }

        currentEcid = 0;

        if (!atLeastOneMatchingReaction && warnOnMissingReaction) {
            System.err.println("WARNING: No reactions were found for these verdicts: " + verdicts);
        }

/*
        for (Irv.Verdict  : receivedVerdictsById) {
            suspend |= verdict.getAccepting() == Irv.Verdict.Accepting.NO;
        }*/

        resume(ecid);

        receivedVerdictsById.remove(ecid);
        expectedVerdictNumbers.put(ecid, 0);
        receivedExpectedVerdictNumber.put(ecid, false);
    }

    // This implements a minimal scenario that suspends the execution on a non accepting state.

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: Scenario <scriptfile>");
            return;
        }

        INSTANCE = new Scenario(args[0]);
    }

    public EventListener addReaction(String spec, Object callback, boolean once) throws Exception {
        if (!interp.isFunction(callback)) {
            throw new Exception("This is not a function");
        }

        if ("program join".equals(spec)) {
            EventListener el = new EventListener(callback, once);
            joinListeners.add(el);
            return el;
        } else if ("program leave".equals(spec)) {
            EventListener el = new EventListener(callback, once);
            leaveListeners.add(el);
            return el;
        } else {
            ScenarioSpec s = ScenarioSpec.parse(spec);
            ScenarioReaction r = new ScenarioReaction(s, callback, once);
            reactions.addFirst(r); // First added reaction has priority.
            return r;
        }
    }

    public EventListener on(String spec, Object callback) throws Exception {
        return addReaction(spec, callback, false);
    }

    public EventListener addReaction(String spec, Object callback) throws Exception {
        return addReaction(spec, callback, false);
    }

    public EventListener once(String spec, Object callback) throws Exception {
        return addReaction(spec, callback, true);
    }

    public void release(EventListener el) {
        if (el instanceof ScenarioReaction) {
            reactions.remove((ScenarioReaction) el);
        }

        joinListeners.remove(el);
        leaveListeners.remove(el);
    }

    public void suspendExecution(boolean suspend) {
        suspendExecution = suspend;
    }

    public void suspendExecution() {
        suspendExecution = true;
    }

    public void debuggerCmd(String cmd, Object callback) throws Exception {
        blockResume = true;

        debuggerCmdCallbacks.put(currentRequestId, callback);

        sendMessage(
            rootMessage(currentEcid)
            .setDebuggerCmd(
                Irv.DebuggerCmd.newBuilder()
                .setCmd(cmd)
                .setRequestId(currentRequestId)
                .build()
            ).build()
        );

        currentRequestId++;
    }

    public void debuggerCmd(String cmd) throws Exception {
        debuggerCmd(cmd, null);
    }

    public void debuggerPrompt(String prompt, Object callback) throws Exception {
        blockResume = true;

        debuggerCmdCallbacks.put(currentRequestId, callback);
        sendMessage(
            rootMessage(currentEcid)
            .setDebuggerCmd(
                Irv.DebuggerCmd.newBuilder()
                .setPrompt(prompt)
                .setRequestId(currentRequestId)
                .build()
            ).build()
        );

        currentRequestId++;
    }

    public void debuggerPrint(String str) throws Exception {
        sendMessage(
            rootMessage(currentEcid)
            .setDebuggerCmd(
                Irv.DebuggerCmd.newBuilder()
                .setPrint(str)
                .setRequestId(currentRequestId++)
                .build()
            ).build()
        );
    }

    public void debuggerPrintln(String str) throws Exception {
        debuggerPrint(str + "\n");
    }

    public long addPoint(String pointSpec, Object callback, boolean once) throws Exception {
        Point request = Point.fromSpec(pointSpec);

        sendMessage(
            rootMessage(currentEcid)
            .setEventRequest(request.toIrv().setRequestId(requestNumber))
            .build()
        );

        points.put(requestNumber, new RequestCallback(request, callback, once));

        return requestNumber++;
    }

    public long addPoint(String pointSpec, Object callback) throws Exception {
        return addPoint(pointSpec, callback, false);
    }

    public long addPointOnce(String pointSpec, Object callback) throws Exception {
        return addPoint(pointSpec, callback, true);
    }

    public void removePoint(long id) throws IOException {
        // FIXME: we should wait for the corresponding requestOk
        // reply.
        // At this point, we only risk showing an error message

        RequestCallback rc = points.get(id);

        if (rc == null) {
            System.err.println("The scenario tried to remove a non-existant point.");
            return;
        }

        points.remove(id);

        sendMessage(
            rootMessage(currentEcid)
            .setEventRelease(Irv.EventRelease.newBuilder().setRequestId(id))
            .build()
        );
    }

    public void getValues(String[] parameters, Object callback) throws Exception {
        blockResume = true;
        List<Irv.ValueRequest> params = new ArrayList<>(parameters.length);

        for (String p : parameters) {
            System.out.println("Parameter spec: " + p);
            params.add(Parameter.parse(p).toIrv().build());
        }

        sendMessage(
            rootMessage(currentEcid)
            .setGetValues(
                Irv.GetValues.newBuilder()
                    .setRequestId(currentRequestId)
                    .addAllParameters(params)
            )
            .build()
        );

        debuggerCmdCallbacks.put(currentRequestId, callback);

        currentRequestId++;
    }

    public void createCheckpoint(Object callback) throws Exception {
        blockResume = true;

        Irv.CheckpointRequest.Builder checkpointRequest = (
            Irv.CheckpointRequest.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(currentRequestId) // This could be improved
        );

        sendMessage(
            rootMessage(currentEcid)
            .setCheckpointRequest(checkpointRequest).build()
        );

        int expectedCheckpoints = 1;
        // we react once response for each request has been received

        for (int monitorSenderId: knownMonitors) {
            expectedCheckpoints++;
            sendMessage(
                rootMessage(monitorSenderId)
                .setCheckpointRequest(checkpointRequest).build()
            );
        }

        // FIXME wait for monitor checkpoint answers.

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, expectedCheckpoints, currentEcid)
        );

        currentRequestId++;
    }

    public void createCheckpoint(int senderId, Object callback) throws Exception {
        blockResume = true;

        Irv.CheckpointRequest.Builder checkpointRequest = (
            Irv.CheckpointRequest.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(currentRequestId) // This could be improved
        );

        sendMessage(
            rootMessage(senderId)
            .setCheckpointRequest(checkpointRequest).build()
        );

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, 1, currentEcid)
        );
        currentRequestId++;
    }

    public void restoreCheckpoint(int senderId, int id, Object callback) throws Exception {
        blockResume = true;

        Irv.CheckpointRestoreRequest.Builder restoreRequest = (
            Irv.CheckpointRestoreRequest.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(id)
        );

        sendMessage(
            rootMessage(senderId)
            .setCheckpointRestoreRequest(restoreRequest).build()
        );

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, 1, currentEcid)
        );
        currentRequestId++;
    }

    public void suspendMonitor(int senderId, Object callback) throws Exception {
        blockResume = true;

        Irv.SuspendRequest.Builder suspendRequest = (
            Irv.SuspendRequest.newBuilder().setRequestId(currentRequestId)
        );

        sendMessage(
            rootMessage(senderId).setSuspendRequest(suspendRequest).build()
        );

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, 1, currentEcid)
        );
        currentRequestId++;
    }

    public void restoreCheckpoint(int id, Object callback) throws Exception {
        blockResume = true;
        Irv.CheckpointRestoreRequest.Builder restoreRequest = (
            Irv.CheckpointRestoreRequest.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(id)
        );

        sendMessage(
            rootMessage(currentEcid)
            .setCheckpointRestoreRequest(restoreRequest).build()
        );

        int checkpointRestoredMessages = 1;
        // we react once response for each request has been received

        for (int monitorSenderId: knownMonitors) {
            checkpointRestoredMessages++;
            sendMessage(
                rootMessage(monitorSenderId)
                .setCheckpointRestoreRequest(restoreRequest).build()
            );
        }

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, checkpointRestoredMessages, currentEcid)
        );

        currentRequestId++;
    }

    public void discardCheckpoint(int id) throws Exception {
        Irv.CheckpointDiscard.Builder checkpointDiscard = (
            Irv.CheckpointDiscard.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(id)
        );

        sendMessage(
            rootMessage(currentEcid)
            .setCheckpointDiscard(checkpointDiscard).build()
        );

        for (int monitorSenderId: knownMonitors) {
            sendMessage(
                rootMessage(monitorSenderId)
                .setCheckpointDiscard(checkpointDiscard).build()
            );
        }

//         points.put(currentRequestId, new RequestCallback(null, callback, false));

        currentRequestId++;
    }

    public void discardCheckpoint(int senderId, int id) throws Exception {
        Irv.CheckpointDiscard.Builder checkpointDiscard = (
            Irv.CheckpointDiscard.newBuilder()
                .setRequestId(currentRequestId)
                .setCid(id)
        );

        sendMessage(
            rootMessage(senderId)
            .setCheckpointDiscard(checkpointDiscard).build()
        );

//         points.put(currentRequestId, new RequestCallback(null, callback, false));

        currentRequestId++;
    }

    public void printf(String fmt, Object... args) {
        System.out.printf(fmt, args);
    }

    public void print(Object... p) {
        for (Object o : p) {
            System.out.print(o);
        }
        System.out.println();
    }

    public void resetMonitor(int senderId, Object callback) throws Exception {
        blockResume = true;
        sendMessage(
            rootMessage(senderId)
            .setResetRequest(
                Irv.ResetRequest.newBuilder().setRequestId(currentRequestId)
            ).build()
        );

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, 1, currentEcid)
        );

        currentRequestId++;
    }

    public void resetMonitors(Collection<Integer> senderIds, Object callback) throws Exception {
        blockResume = true;
        sendMessage(
            rootMessage(senderIds)
            .setResetRequest(
                Irv.ResetRequest.newBuilder().setRequestId(currentRequestId)
            ).build()
        );

        debuggerCmdCallbacks.put(
            currentRequestId,
            new CallbackWithCounter(callback, senderIds.size(), currentEcid)
        );

        currentRequestId++;
    }

    public void resetAllMonitors(Object callback) throws Exception {
        resetMonitors(knownMonitors, callback);
    }

    public Object getMonitors() {
        return interp.array(knownMonitors);
    }

    public String getName(int senderId) {
        return new File(getFullName(senderId)).getName();
    }

    public String getFullName(int senderId) {
        String n = names.get(senderId);
        if (n == null) { return ""; }
        return n;
    }

    public void warnOnMissingReaction(boolean warn) {
        warnOnMissingReaction = warn;
    }
}
