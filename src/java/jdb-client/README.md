## Usage

```
make compile
make run
```

Needs AJC installed (provided as [aspectj.tgz](aspectj.tgz) extract and add bin to your $PATH

## Useful stuff

* [TTY documentation and methods](http://www.docjar.com/docs/api/com/sun/tools/example/debug/tty/TTY.html)
* Check Makefile
* I added a small hook that exits when you call `help`
