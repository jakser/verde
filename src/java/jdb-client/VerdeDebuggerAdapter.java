// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import java.util.function.Function;
import java.util.List;
import verde.point.Point;
import verde.point.PointNotFoundException;
import verde.point.PointNotSupportedException;
import verde.point.parameter.ParameterSpec;
import com.sun.jdi.Value;

public interface VerdeDebuggerAdapter {
    // Callback function f returns true to stop the execution, false to continue;
    // See around JDB's EventHandler.java:76
    // Note: this only works if the Java application is stopped (e.g. by an
    // event with suspend policy, which should always be the case)

    VerdePointRequest instrument(Point p, Function<VerdeEventData, Void> f) throws PointNotFoundException, PointNotSupportedException;
    void release(VerdePointRequest r);

    void getValues(List<ParameterSpec> params, Function<List<Value>, Void> callback) throws Exception;

    // run a debugger command. This is asynchronous because some commands are
    // indeed asynchronous in JDB
    void run(String cmd, Function<String, Void> callback);

    void prompt(String cmd, Function<String, Void> callback);
    void print(String cmd);
}
