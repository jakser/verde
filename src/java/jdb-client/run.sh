#!/usr/bin/env sh

if [ ! ${JAVA_HOME+x} ]; then
        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
fi

if [ ! -d "${JAVA_HOME}" ]; then
        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-arm64
fi

if [ ! ${VERDE_DIR+x} ]; then
    VERDE_DIR="$(realpath "$(dirname "$0"})/../../..")"
fi

if [ ! ${TARGET+x} ]; then
    TARGET="${VERDE_DIR}/target"
fi

if [ ! ${ASPECTJ_HOME+x} ]; then
    ASPECTJ_HOME="${TARGET}/dependencies/aspectj"
fi

if [ ! ${JDB_TOOLS_JAR+x} ]; then
    JDB_TOOLS_JAR="${JAVA_HOME}/lib/tools.jar"
fi

if [ ! ${RVCP+x} ]; then
    RVCP="${ASPECTJ_HOME}/lib/aspectjrt.jar:${TARGET}/dependencies/protobuf-java.jar:${JDB_TOOLS_JAR}:${TARGET}"
fi

if [ ! ${RUNCP+x} ]; then
    RUNCP="${RVCP}:${TARGET}"
fi

if [ ! ${AGENT+x} ]; then
    AGENT="${ASPECTJ_HOME}/lib/aspectjweaver.jar"
fi

if [ ${WD+x} ]; then
	cd ${WD}
fi

exec "${JAVA_HOME}/bin/java" -cp "${RUNCP}" -javaagent:"${AGENT}" com.sun.tools.example.debug.tty.TTY "$@"
