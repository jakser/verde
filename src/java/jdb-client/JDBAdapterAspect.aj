package com.sun.tools.example.debug.tty;

import com.sun.jdi.request.EventRequest;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.WatchpointRequest;
import com.sun.jdi.request.AccessWatchpointRequest;
import com.sun.jdi.request.EventRequestManager;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventIterator;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.EventQueue;
import com.sun.jdi.event.LocatableEvent;
import com.sun.jdi.event.MethodExitEvent;
import com.sun.jdi.event.VMStartEvent;
import com.sun.jdi.event.ThreadStartEvent;
import com.sun.jdi.event.ThreadDeathEvent;
import com.sun.jdi.StackFrame;
import java.util.Deque;
import java.util.ArrayDeque;
// import com.sun.jdi.ObjectReference;
// import com.sun.jdi.Value;
// import com.sun.jdi.LocalVariable;
// import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.*;
import com.sun.tools.example.debug.tty.TTY;
import java.util.StringTokenizer;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;
import verde.point.*;
import verde.point.parameter.*;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.sun.tools.example.debug.tty.*;

import verde.*;

public privileged aspect JDBAdapterAspect implements VerdeDebuggerAdapter {
    private class PointAndReaction implements VerdePointRequest {
        public Point p;
        public Function<VerdeEventData, Void> f;

        PointAndReaction(Point p, Function<VerdeEventData, Void> f) {
            this.p = p;
            this.f = f;
        }

        public String toString() {
            return "<" + p + ", " + f + ">";
        }
    }

    private class AfterPointAndReaction extends PointAndReaction {
        public Function<VerdeEventData, Void> finishCallback;
        public JDBAdapterAspect adapter;
        public VerdePointRequest finishVPR;
        public Deque<VerdeEventData> entryDataStack = new ArrayDeque<>();

        AfterPointAndReaction(JDBAdapterAspect adapter, MethodBreakpoint p, Function<VerdeEventData, Void> f, StackFrame frame) throws PointNotFoundException, IncompatibleThreadStateException {
            super (p, null);

            this.adapter = adapter;
            this.finishCallback = f;

            this.f = (entryData) -> {
                this.entryDataStack.push(entryData);
                if (this.finishVPR == null) {
                    try {
                        this.finishVPR = adapter.createAndRegisterFinishBreakpoint(this);
                    } catch (Exception e) {
                        System.err.println("creating method exit request failed");
                    }
                }
                return null;
            };

            // Let's check whether the current method is the method of the breakpoint.

            ThreadAndFrame fom = getFrameOfMethod(p.className, p.methodName);

            if (fom == null) {
                return;
            }

            // FIXME: if we are in the middle of the method, the values could be wrong (e.g. parameters modified in the function).
            // Life sucks, right?
            try {
                getValues(fom.thread, fom.frame, null, p.params, (arguments) -> {
                    this.f.apply(new VerdeEventData(null, arguments));
                    return null;
                });
            } catch (Exception e) {
                throw new PointNotFoundException(e);
            }
        }
    }

    // Maps specifications to corresponding Verde Points and callbacks
    private Map<EventRequestSpec, Set<PointAndReaction>>  evtReqSpecsToPRs = new HashMap<>();

    // Maps Verde points to specifications. This is used to remove
    // instrumentation for points that are to be released.
    private Map<Point, EventRequestSpec>              requestSpecsByPoints = new HashMap<>();

    // Stores all specs that have not been resolved to a request yet
    private Set<EventRequestSpec>                                 ourSpecs = new HashSet<>();

    // Stores all specs that do not belong to us
    private Set<EventRequestSpec>                            debuggerSpecs = new HashSet<>();

    // For each thread, the thread id is associated to false whenever the end of
    // an EventIterator is reached
    private Map<Long, Boolean>             notReachedEndOfIteratorByThread = new HashMap<>();

    // The set of PointAndReaction to trigger after iterating over a set o events
    private Set<PointAndReaction>               pointAndReactionsToTrigger = new HashSet<>();

    // This variable is true when the adapter is adding instrumentation.
    // This is useful to know whether a request is set for us or for the user.
    private boolean instrumenting  = false;

    // This variable stores what we should return at the last handleEvent call
    // for the EventSet being iterated, if at least one event belonging to the
    // debugger has been processed, provided we replaced all the return values
    // of handleEvent to true.
    // See jdb's EventHandler.handleEvent.
    private boolean retHandleEvent = true;

    // True if a debugger event has been proceeded in the current EventSet.
    private boolean hasProceededADebuggerEvent = false;


    ByteArrayOutputStream baOutStream = new ByteArrayOutputStream();

    private PrintStream printStream = new PrintStream(baOutStream);


    private Function<String, Void> promptCallback = null;
    private boolean rightAfterCallback = false;

    private TTY tty;
    private EventSet currentEventSet = null;
    private EventHandler eventHandler = null;


    private class FinishBreakpointSpec extends EventRequestSpec {
        FinishBreakpointSpec(String className) throws ClassNotFoundException {
            super(new PatternReferenceTypeSpec(className));
        }

        @Override
        EventRequest resolveEventRequest(ReferenceType refType)
                            throws AmbiguousMethodException,
                                    AbsentInformationException,
                                    InvalidTypeException,
                                    NoSuchMethodException,
                                    LineNotFoundException {
            EventRequestManager em = refType.virtualMachine().eventRequestManager();
            EventRequest bp = em.createMethodExitRequest();
            bp.setSuspendPolicy(suspendPolicy);
            bp.enable();
            return bp;
        }
    }

    private VerdeEventData mergeEventDataObjects(AfterPointAndReaction apar, VerdeEventData entryData, VerdeEventData exitData) {
        VerdeEventData merged = new VerdeEventData(apar, new LinkedList<Value>());

        Iterator<Value> iEntry = entryData.args.iterator();
        Iterator<Value> iExit  = exitData.args.iterator();

        while (iEntry.hasNext() && iExit.hasNext()) {
            Value nextEntryValue = iEntry.next();
            Value nextExitValue  = iExit.next();

            merged.args.add(
                (nextExitValue == null)
                    ? nextEntryValue
                    : nextExitValue
            );
        }

        return merged;

    }

    class FinishPoint extends Point {
        public MethodBreakpoint methodBreakpoint;
        public FinishPoint(MethodBreakpoint p) {
            super(p.params);
            methodBreakpoint = p;
        }

        public boolean equals(Point p) {
            return this == p;
        }

        public verde.Irv.EventRequest.Builder toIrv() {
            return null;
        }

        public String toString() {
            return "FinishPoint";
        }
    }

    VerdePointRequest createAndRegisterFinishBreakpoint(AfterPointAndReaction apar) throws ClassNotFoundException, PointNotFoundException {
        final MethodBreakpoint methodBreakpoint = (MethodBreakpoint) (apar.p);

        EventRequestSpec evReqSpec = createFinishBreakpoint(methodBreakpoint.className);

        Function<VerdeEventData, Void> callbackAndCleanup = (exitData) -> {
            apar.finishCallback.apply(mergeEventDataObjects(apar, apar.entryDataStack.pop(), exitData));
            if (apar.entryDataStack.size() == 0) {
                release(apar.finishVPR);
                apar.finishVPR = null;
            }
            return null;
        };

        return registerPoint(new FinishPoint((MethodBreakpoint) apar.p), evReqSpec, callbackAndCleanup, false);
    }

    private EventRequestSpec createFinishBreakpoint(String className) throws ClassNotFoundException {
        return new FinishBreakpointSpec(className);
    }

    private VerdePointRequest registerPoint(Point p, EventRequestSpec evReqSpec, Function<VerdeEventData, Void> f, boolean isAfterBp) throws ClassNotFoundException, PointNotFoundException {
        // We ask JDB to handle the request.
        int indexOfRequestSpec;
        synchronized(Env.specList.eventRequestSpecs) {
            indexOfRequestSpec = Env.specList.eventRequestSpecs.indexOf(evReqSpec);
        }

        if (indexOfRequestSpec == -1) {
            Env.specList.addEagerlyResolve(evReqSpec);
        }

        // We map the point to the reaction
        Set<PointAndReaction> PRs = evtReqSpecsToPRs.get(evReqSpec);

        if (PRs == null) {
            PRs = new HashSet<>();
            evtReqSpecsToPRs.put(evReqSpec, PRs);
        }

        PointAndReaction par;
        try {
             par = (
                isAfterBp
                    ? new AfterPointAndReaction(
                        this,
                        (MethodBreakpoint) p,
                        f,
                        getDefaultThreadAndFrame().frame
                      )
                    : new PointAndReaction(p, f)
            );
        } catch (Exception e) {
            throw new PointNotFoundException(e);
        }

        PRs.add(par);

        // We map the point to JDB event request specification.
        requestSpecsByPoints.put(p, evReqSpec);

        // We keep track of our JDB event request specifications.
        ourSpecs.add(evReqSpec);

        return par;
    }

    // This method adds necessary instrumentation for the given Point, and
    // maps this Point to the given callback.
    public VerdePointRequest instrument(Point p, Function<VerdeEventData, Void> f) throws PointNotFoundException, PointNotSupportedException {
        instrumenting = true;

        try {
            // A JDB event request specification is created.
            EventRequestSpec evReqSpec = null;

            boolean isAfterBp = false;

            if (p instanceof LineBreakpoint) {
                LineBreakpoint bpLine = (LineBreakpoint) p;
                evReqSpec = Env.specList.createBreakpoint(
                    bpLine.fileName,
                    bpLine.lineNumber
                );
            } else if (p instanceof MethodBreakpoint) {
                MethodBreakpoint bpME = (MethodBreakpoint) p;
                isAfterBp = !bpME.isEntry;
                evReqSpec = Env.specList.createBreakpoint(
                    bpME.className,
                    bpME.methodName,
                    bpME.methodArgs
                );
            } else if (p instanceof AccessWatchpoint) {
                AccessWatchpoint w = (AccessWatchpoint) p;
                evReqSpec = Env.specList.createAccessWatchpoint(
                    w.className,
                    w.fieldName
                );
            } else if (p instanceof ModificationWatchpoint) {
                ModificationWatchpoint w = (ModificationWatchpoint) p;
                evReqSpec = Env.specList.createModificationWatchpoint(
                    w.className,
                    w.fieldName
                );
            } else if (p instanceof ExceptionCatch) {
                ExceptionCatch ec = (ExceptionCatch) p;
                evReqSpec = Env.specList.createExceptionCatch(
                    ec.className,
                    ec.includeCaughtExceptions,
                    ec.includeUncaughtExceptions
                );
            } else {
                throw new PointNotSupportedException("This kind of point is not supported");
            }

            return registerPoint(p, evReqSpec, f, isAfterBp);
        } catch (ClassNotFoundException exc) {
            throw new PointNotFoundException(exc);
        } catch (MalformedMemberNameException exc) {
            throw new PointNotFoundException(exc);
        } finally {
            instrumenting = false;
        }
    }

    public void release(VerdePointRequest r) {
        if (r instanceof AfterPointAndReaction) {
            if (((AfterPointAndReaction) r).finishVPR != null) {
                release(((AfterPointAndReaction) r).finishVPR);
            }
        } else if (!(r instanceof PointAndReaction)) {
            System.err.println("release: bad argument");
            return;
        }

        PointAndReaction pf = (PointAndReaction) r;

        EventRequestSpec ers = requestSpecsByPoints.get(pf.p);

        if (ers == null) {
            MessageOutput.printDirectln("WARNING: release: did not find specified PointRequest.");
            (new Exception()).printStackTrace();
            return;
        }

        Set<PointAndReaction> PRs = evtReqSpecsToPRs.get(ers);
        if (PRs != null) {
            PRs.remove(pf);
            if (PRs.isEmpty()) {
                evtReqSpecsToPRs.remove(ers);
                requestSpecsByPoints.remove(pf.p);

                if (!debuggerSpecs.contains(ers)) {
                    boolean ret = false;
//                     do {
                        ret = Env.specList.delete(ers);
                        //FIXME: Is this loop still useful? Probably slows
                        //       things down a bit.
                        //       We should not be adding event request specs
                        //       twice in the specList anymore anyway.
//                     } while (ret);
                }

                ourSpecs.remove(ers);
            }
        }
    }

    before(EventRequestSpec spec): args(spec) && call(boolean EventRequestSpecList.addEagerlyResolve(EventRequestSpec)) {
        if (!instrumenting) {
            debuggerSpecs.add(spec);
        }
    }

    before(EventRequestSpec spec): args(spec) && call(boolean EventRequestSpecList.delete(EventRequestSpec)) {
        if (!instrumenting) {
            debuggerSpecs.remove(spec);
        }
    }

    Event around(EventIterator it): target(it) && (
        execution(Event EventIterator.nextEvent()) ||
        execution(Event EventIterator.next())
    ) {
        Event e = proceed(it);
        notReachedEndOfIteratorByThread.put(Thread.currentThread().getId(), it.hasNext());
        return e;
    }

    // Let's remove undesirable output
    boolean around(): execution(boolean TTY.methodExitEvent(MethodExitEvent)) {
        Thread.yield();
        return false;
    }

    // compares two JDI event requests
    boolean eventRequestsAreEqual(EventRequest r1, EventRequest r2) {
        // Caveat: filters of request are ignored for breakpoints and watchpoints.

        if (r1 == null) {
            return r2 == null;
        }

        // Comparing two BreakpointRequest do not work.
        // Comparing locations of this requests seems to work.
        if (r1 instanceof BreakpointRequest) {
            if (r2 instanceof BreakpointRequest) {
                return ((BreakpointRequest) r1).location().equals(((BreakpointRequest) r2).location());
            }

            return false;
        }

        if (r1 instanceof AccessWatchpointRequest) {
            if (r2 instanceof AccessWatchpointRequest) {
                return ((WatchpointRequest) r1).field().equals(((WatchpointRequest) r2).field());
            }

            return false;
        }

        if (r1 instanceof WatchpointRequest) {
            // AccessWatchpointRequest is a subtype of WatchpointRequest
            // so this covers AccessWatchpointRequest too.

            if (r2 instanceof WatchpointRequest) {
                return ((WatchpointRequest) r1).field().equals(((WatchpointRequest) r2).field());
            }

            return false;
        }

        // For Request types like MethodEntryBreakpoint, MethodExitBreakpoint,
        // There is nothing to compare, so let's hope that the following works.
        return r1.equals(r2);
    }

    // return : should proceed?
    private boolean handleHandleEvent(Event e) {
        if (e instanceof VMStartEvent) {
            // When the Virtual Machine sends the start event, we initialize the
            // Verde controller.
            // Requesting anything to the Virtual Machine before this event is
            // a bad idea, according to the JDWP specs.

            try {
                VerdeDebuggerController.init(this);
            } catch (Exception exc) {
                exc.printStackTrace();
            }

            return true;
        }

        if (e instanceof ThreadStartEvent || e instanceof ThreadDeathEvent) {
            return true;
        }

        // Let us get the request that created this event.
        // Requests seem unique, so if the user set a breakpoint on HelloWorld:25
        // and the Verde controller also set a breakpoint on HelloWorld:25,
        // there is only one request in the debugger structures.

        EventRequest evtReq = e.request();

        // If no request match this event, let the debugger do its normal work.
        if (evtReq == null) {
            return true;
        }

        Set<EventRequestSpec> evtReqSpecs = new HashSet<>();

        for (EventRequestSpec evtReqSpecCandidate : ourSpecs) {
            // We try to make the debugger resolve each specification to a request.

            EventRequest evtReqCandidate = evtReqSpecCandidate.resolved();

            if (evtReqCandidate != null) {
                // And if a specification maps to the request of the event
                // that is happening, we add it to the set of specifications
                // to handle.
                if (eventRequestsAreEqual(evtReq, evtReqCandidate)) {
//                     MessageOutput.printDirectln("    ok");
                    evtReqSpecs.add(evtReqSpecCandidate);
                }
            }
        }

        // In case we found the corresponding request (that is, we asked for this event)
        if (!evtReqSpecs.isEmpty()) {
            // does the debugger have a request that do not belong to us and matches this event?
            boolean debuggerHasSpec = false;
            for (EventRequestSpec evtReqSpec : evtReqSpecs) {
                if (!debuggerHasSpec) {
                    for (EventRequestSpec spec : debuggerSpecs) {
                        EventRequest resolvedSpec = spec.resolved();
                        if (resolvedSpec.isEnabled() && evtReqSpec.equals(spec)) {
                            debuggerHasSpec = true;
                            break;
                        }
                    }
                }

                pointAndReactionsToTrigger.addAll(evtReqSpecsToPRs.get(evtReqSpec));
            }

            if (!debuggerHasSpec) {
                return false;
            }
        }

        return true;
    }

    private Value invokeMethod(ThreadReference thread, ReferenceType t, ObjectReference v, String methodName)
            throws InvalidTypeException, ClassNotLoadedException, IncompatibleThreadStateException, InvocationException {
        List<Method> methods = t.methodsByName(methodName);
        Method method = methods.get(0);

        //FIXME handle null

        return v.invokeMethod(thread, method, java.util.Collections.emptyList(), ObjectReference.INVOKE_SINGLE_THREADED);
    }

    private Value resolveValue(ThreadReference thread, Value v)
            throws InvalidTypeException, ClassNotLoadedException, IncompatibleThreadStateException, InvocationException {

        if (v instanceof ObjectReference) {
            // This is an object
            ReferenceType t = ((ObjectReference) v).referenceType();
            if ("java.lang.Integer".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "intValue");
            }

            if ("java.lang.Long".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "longValue");
            }

            if ("java.lang.Float".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "floatValue");
            }

            if ("java.lang.Double".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "doubleValue");
            }

            if ("java.lang.Byte".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "byteValue");
            }

            if ("java.lang.Short".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "shortValue");
            }

            if ("java.lang.String".equals(t.name())) {
                return invokeMethod(thread, t, (ObjectReference) v, "toString");
            }
        }

        // FIXME handle arrays
        return v;
    }

    public void getValues(List<ParameterSpec> params, Function<List<Value>, Void> callback) throws Exception {
        getValues(null, params.iterator(), new LinkedList<>(), callback);
    }

    public void getValues(ThreadReference thread, StackFrame frame, Event e, List<ParameterSpec> params, Function<List<Value>, Void> callback) throws Exception {
        getValues(thread, frame, e, params.iterator(), new LinkedList<>(), callback);
    }

    public void getValues(Event e, List<ParameterSpec> params, Function<List<Value>, Void> callback) throws Exception {
        getValues(e, params.iterator(), new LinkedList<>(), callback);
    }

    private class ThreadAndFrame {
        public ThreadReference thread;
        public StackFrame frame;

        public ThreadAndFrame() {}

        public ThreadAndFrame(ThreadReference t, StackFrame f) {
            thread = t;
            frame = f;
        }
    }

    private boolean methodMatches(StackFrame frame, FinishPoint fp) {
        return methodMatches(frame, fp.methodBreakpoint.className, fp.methodBreakpoint.methodName);
    }

    private boolean methodMatches(StackFrame frame, String className, String methodName) {
        Location location = frame.location();
        Method currentMethod = location.method();
        if (methodName.equals(currentMethod.name())) {
            String declaringType = location.declaringType().name();
            if (declaringType.equals(className)) {
                return true;
            }
        }
        return false;
    }

    private ThreadAndFrame getFrameOfMethod(String className, String methodName) throws IncompatibleThreadStateException {
        ThreadReference thread = getDefaultThreadAndFrame().thread;

        if (thread == null) {
            return null;
        }

        int n = thread.frameCount();
        for (int i = 0; i < n; i++) {
            if (methodMatches(thread.frame(i), className, methodName)) {
                return new ThreadAndFrame(thread, thread.frame(i));
            }
        }

        return null;
    }

    private ThreadAndFrame getDefaultThreadAndFrame() throws IncompatibleThreadStateException {
        ThreadAndFrame tf = new ThreadAndFrame();

        ThreadInfo threadInfo = ThreadInfo.getCurrentThreadInfo();

        if (threadInfo == null) {
            for (ThreadInfo ti : ThreadInfo.threads) {
                tf.frame = ti.getCurrentFrame();
                tf.thread = ti.thread;

                if (tf.frame != null) {
                    try {
                        tf.frame.visibleVariables();
                    } catch (Exception exc) {
                        tf.frame = null;
                        continue;
                    }

                    break;
                }
            }
        } else {
            tf.frame = threadInfo.getCurrentFrame();
            tf.thread = threadInfo.thread;
        }

        return tf;
    }

    public void getValues(Event e, Iterator<ParameterSpec> params, List<Value> arguments, Function<List<Value>, Void> callback) throws Exception {
        StackFrame frame = null;
        ThreadReference thread = null;

        if (e instanceof LocatableEvent) {
            thread = ((LocatableEvent) e).thread();
            frame = thread.frames(0, 1).get(0);
        } else {
            ThreadAndFrame threadAndFrame = getDefaultThreadAndFrame();
            thread = threadAndFrame.thread;
            frame  = threadAndFrame.frame;
        }

        if (frame == null) {
            throw new AbsentInformationException();
        }

        getValues(thread, frame, e, params, arguments, callback);
    }

    public void getValues(ThreadReference thread, StackFrame frame, Event e, Iterator<ParameterSpec> params, List<Value> arguments, Function<List<Value>, Void> callback) throws Exception {
        List<LocalVariable> vars = frame.visibleVariables();
        Map<LocalVariable, Value> values = frame.getValues(vars);

        //FIXME should we use StackFrame.getArgumentValue()?
        while (params.hasNext()) {
            ParameterSpec p = params.next();
            boolean found = false;

            if (e instanceof MethodExitEvent) {
                if (p instanceof MethodParameterByIndex && ((MethodParameterByIndex) p).index == 0) {
                    Value val = ((MethodExitEvent) e).returnValue();
                    arguments.add(resolveValue(thread, val));
                    found = true;
                }
            }

            if (!found) if (p instanceof MethodParameterByIndex) {
                if (((MethodParameterByIndex) p).index == 0) {
                    arguments.add(null);
                    found = true;
                } else {
                    int curVariableArgNumber = 0;
                    for (LocalVariable var : vars) {
                        if (var.isArgument()) {
                            curVariableArgNumber++;

                            if (curVariableArgNumber == ((MethodParameterByIndex) p).index) {
                                Value val = values.get(var);
                                arguments.add(resolveValue(thread, val));
                                found = true;
                                break;
                            }
                        }
                    }
                }
            } else if (p instanceof NamedParameter) {
                String name = ((NamedParameter) p).name;
                for (LocalVariable var : vars) {
                    if (var.name().equals(name)) {
                        Value val = values.get(var);
                        arguments.add(resolveValue(thread, val));
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    ObjectReference thisObject = frame.thisObject();
                    if (thisObject != null) {
                        ReferenceType thisRefType = thisObject.referenceType();
                        Field field = thisRefType.fieldByName(name);
                        if (field != null) {
                            Value val = thisObject.getValue(field);

                            if (val != null) {
                                arguments.add(resolveValue(thread, val));
                                found = true;
                            }
                        }
                    }
                }
            } else if (p instanceof ThisParameter) {
                ObjectReference thisObject = frame.thisObject();

                Value val = Env.vm().mirrorOf(
                    (thisObject == null)
                        ? 0
                        : thisObject.uniqueID()
                );

                arguments.add(val);

                found = true;
            } else if (p instanceof ExpressionParameter) {
                ThreadReference t = thread;
                eval(
                    ((ExpressionParameter) p).expression,
                    (res) -> {
                        Value val = res;
                        try {
                            arguments.add(resolveValue(t, val));
                            getValues(t, t.frame(0), e, params, arguments, callback);
                        } catch (Exception exc) {
                            MessageOutput.printDirectln("Something wrong happened when retrieving arguments or executing point and reaction: " + exc);
                            exc.printStackTrace();
                        }
                        return null;
                    }
                );
                return;
            }

            if (!found) {
                MessageOutput.printDirectln("Could not find parameter " + p);
                throw new Exception("Could not find parameter " + p);
            }
        }

        callback.apply(arguments);
    }

    EventSet around(): execution(EventSet EventQueue.remove()) {
        currentEventSet = proceed();
        return currentEventSet;
    }

    boolean handleReactions(Event e, Iterator<PointAndReaction> iter) {
        if (!iter.hasNext()) {
            pointAndReactionsToTrigger.clear();

            try {
                boolean stopAfterProcessingEvents = VerdeDebuggerController.INSTANCE.endEventGroup();
                boolean ret = (
                    stopAfterProcessingEvents
                        ? true
                        : (hasProceededADebuggerEvent ? retHandleEvent : false)
                );

                retHandleEvent = true;
                hasProceededADebuggerEvent = false;
                return ret;
            } catch (Exception exc) {
                exc.printStackTrace();
            }

            return true;
        }

        PointAndReaction par = iter.next();
        // We handle reactions to events that have been generated.
        try {
            // WORKAROUND: MethodExitEvent seems to be triggered for any method exit…
            if (!(e instanceof MethodExitEvent) || methodMatches(((LocatableEvent) e).thread().frames(0, 1).get(0), (FinishPoint) par.p)) {
                getValues(e, par.p.params, (arguments) -> {
                    par.f.apply(new VerdeEventData(par, arguments));
                    return null;
                });
            }
        } catch (Exception exc) {
            MessageOutput.printDirectln("Something wrong happened when retrieving arguments or executing point and reaction " + par + ": " + exc);
            exc.printStackTrace();
        }

        return handleReactions(e, iter);
    }

    boolean around(Event e, EventHandler eventHandler): target(eventHandler) && args(e) && execution(boolean EventHandler.handleEvent(Event)) {
        this.eventHandler = eventHandler;
        boolean shouldProceed = handleHandleEvent(e);

        if (shouldProceed) {
            hasProceededADebuggerEvent = true;
            retHandleEvent &= proceed(e, eventHandler);
        }

        long tid = Thread.currentThread().getId();
        if (notReachedEndOfIteratorByThread.get(tid)) { // if is event is not last
            return true;
        }

        notReachedEndOfIteratorByThread.remove(tid);

        VerdeDebuggerController.INSTANCE.beginEventGroup();

        return handleReactions(e, pointAndReactionsToTrigger.iterator());
    }

    before(TTY o) : (initialization(TTY.new()) || initialization(TTY.new(..))) && target(o) {
        tty = o;
    }

    PrintStream commandOut = null;
    PrintStream commandErr = null;
    Function<String, Void> commandCallback = null;

    void around(): call(void MessageOutput.printPrompt()) {
        // Get the result of a command, if commandOut ≠ null. See run().

        if (commandOut == null) {
            // commandOut is tested here instead of the more logical
            // commandCallback because this one can be null
            proceed();
            return;
        }

        printStream.flush();

        String res = baOutStream.toString();
        baOutStream.reset();

        System.setOut(commandOut);
        System.setErr(commandErr);

        if (commandCallback != null) {
            commandCallback.apply(res);
        }

        commandOut = null;
        commandErr = null;
        commandCallback = null;
    }

    public void eval(String expression,  Function<Value, Void> callback) {
        eventHandler.setCurrentThread(currentEventSet);
        Thread.yield();

        (new Commands()).new AsyncExecution() {
            @Override
            void action() {
                callback.apply(new Commands().evaluate(expression));
            }
        };
    }

    public void run(String cmd, Function<String, Void> callback) {
        // FIXME add in the protocol a way to distinguish err and out.
//         System.out.println("running command");
        commandOut = System.out;
        commandErr = System.err;

        System.setOut(printStream);
        System.setErr(printStream);

         // allows executing debugger commands from the scenario
        eventHandler.setCurrentThread(currentEventSet);

        // FIXME: This is unsafe: everything will be fucked up if two commands
        // are run in a row
        commandCallback = callback;

        // Let JDB execute the command its way, as seen in TTY.vmInterrupted(),
        // called in handleEvent.

        Thread.yield();
        tty.executeCommand(new StringTokenizer(cmd));

        // Result of the command is taken when printPrompt is called.
//         commandOut.println("ran command");
    }

    after() : (call(void MessageOutput.printPrompt())) {
        rightAfterCallback = true;
    }

    String around(BufferedReader in): target(in) && call(String BufferedReader.readLine() throws IOException) {
        String line = proceed(in);

        if (promptCallback == null || !rightAfterCallback) {
            return line;
        }

        rightAfterCallback = false;
        promptCallback.apply(line);
        return "";
    }

    public void prompt(String p, Function<String, Void> callback) {
        MessageOutput.printDirect(p);
        promptCallback = callback;
    }

    public void print(String p) {
        MessageOutput.printDirect(p);
    }
}
