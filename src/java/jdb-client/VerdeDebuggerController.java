// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;

import verde.Conf;
import verde.point.*;
import verde.point.parameter.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import java.net.Socket;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import com.sun.jdi.Value;
import com.sun.jdi.BooleanValue;
import com.sun.jdi.ByteValue;
import com.sun.jdi.CharValue;
import com.sun.jdi.FloatValue;
import com.sun.jdi.ShortValue;
import com.sun.jdi.IntegerValue;
import com.sun.jdi.LongValue;
import com.sun.jdi.DoubleValue;
import com.sun.jdi.StringReference;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.VoidValue;

public class VerdeDebuggerController extends VerdeBusClient {
    private final VerdeDebuggerAdapter dbgAdapter;

    private static VerdeDebuggerController INSTANCE;

    private static Map<Integer, Map<Long, VerdePointRequest>> verdeRequestByEventIdBySender = new HashMap<>();

    private int nbEvents = 0;
    private int otherEvents = 0;
    private int scnSenderId = 0;

    private Set<Integer> knownMonitors = new HashSet<>();

    public static void init(VerdeDebuggerAdapter dbgAdapter) throws Exception {
        INSTANCE = new VerdeDebuggerController(dbgAdapter);
    }

    public void beginEventGroup() {
        nbEvents = 0;
        otherEvents = 0;
    }

    public boolean endEventGroup() throws Exception {
        if (nbEvents > 0) {
            if (scnSenderId != 0) {
                sendMessage(
                    rootMessage(scnSenderId)
                    .setExpectedVerdicts(
                        Irv.ExpectedVerdicts.newBuilder().setNumber(nbEvents)
                    ).build()
                );
            }
        }

        if (nbEvents + otherEvents > 0) {
            return waitResume();
        }

        return false;
    }

    private VerdeDebuggerController(VerdeDebuggerAdapter dbgAdapter) throws Exception {
        super(Conf.EXECUTION_CONTROLLER_SERVICE_NAME);
        this.dbgAdapter = dbgAdapter;
        connectToBus();
        waitResume();
    }

    ParameterSpec paramFromIrv(Irv.ValueRequest r) throws Exception {
        Irv.ValueRequest.SpecCase specCase = r.getSpecCase();

        switch (specCase) {
            case NAME:
                return Parameter.byName(r.getName());

            case ARGNUMBER:
                return Parameter.byIndex(r.getArgNumber());

            case EXPRESSION:
                return Parameter.byExpression(r.getExpression());

            case THISOBJECT:
                return Parameter.THIS;

            default: throw new Exception("Unsupported parameter type " + r.getSpecCase());
        }
    }

    List<ParameterSpec> paramListFromIrv(List<Irv.ValueRequest> l) throws Exception {
        List<ParameterSpec> rl = new LinkedList<>();

        for (Irv.ValueRequest r : l) {
            rl.add(paramFromIrv(r));
        }

        return rl;
    }

    Irv.Value.Builder irvValueFromAdapterParameterValue(Value v) {
        if (v instanceof BooleanValue) {
            return Irv.Value.newBuilder().setVBool(((BooleanValue) v).value());
        }

        if (v instanceof ByteValue) {
            return Irv.Value.newBuilder().setVBytes(
                com.google.protobuf.ByteString.copyFrom(
                    new byte[] {((ByteValue) v).value()}
                )
            );
        }

        if (v instanceof CharValue) {
            return Irv.Value.newBuilder().setVString(((CharValue) v).value() + "");
        }

        if (v instanceof FloatValue) {
            return Irv.Value.newBuilder().setVFloat(((FloatValue) v).value());
        }

        if (v instanceof ShortValue) {
            return Irv.Value.newBuilder().setVSignedInt4(((ShortValue) v).value());
        }

        if (v instanceof IntegerValue) {
            return Irv.Value.newBuilder().setVSignedInt4(((IntegerValue) v).value());
        }

        if (v instanceof LongValue) {
            return Irv.Value.newBuilder().setVSignedInt8(((LongValue) v).value());
        }

        if (v instanceof DoubleValue) {
            return Irv.Value.newBuilder().setVDouble(((DoubleValue) v).value());
        }

        if (v instanceof VoidValue) {
            return Irv.Value.newBuilder();
        }

        if (v instanceof StringReference) {
            return Irv.Value.newBuilder().setVString(((StringReference) v).value());
        }

        return Irv.Value.newBuilder().setVError(
            Irv.Error.newBuilder()
            .setInfo("Unknown value " + v.toString() + " (class " + v.getClass().getCanonicalName() + ")")
        );
    }

    Irv.Event.Builder setEventParamsIrvValuesFromAdapterParameters(Irv.Event.Builder e, List<Value> values) {
        for (Value v: values) {
            e.addParameters(irvValueFromAdapterParameterValue(v));
        }

        return e;
    }

    private void release(Map<Long, VerdePointRequest> verdeRequestByEventId, long eventId) {
        VerdePointRequest verdeRequest = (
            verdeRequestByEventId == null
                ? null
                : verdeRequestByEventId.get(eventId)
        );

        if (verdeRequest == null) {
            System.out.println("WARNING: We've been asked to release an event that is not instrumented.");
        } else {
            dbgAdapter.release(verdeRequest);
            verdeRequestByEventId.remove(eventId);
        }
    }

    Irv.ValueList.Builder setValueListIrvValuesFromAdapterParameters(Irv.ValueList.Builder vl, List<Value> values) {
        for (Value v: values) {
            vl.addValue(irvValueFromAdapterParameterValue(v));
            // FIXME inconsistency values parameters
        }

        return vl;
    }

    boolean waitResume() {
//         System.out.println("Waiting resume...");
        try {
            while (true) {
                Irv.Root msg = getMessage();
                int msgSender = msg.getSender();

                switch (msg.getContentCase()) {
                    case BYE: {
                        if (msg.getSender() == 0) {
//                             System.err.println("The bus left");
                            return true;
                        }

                        Map<Long, VerdePointRequest> verdeRequestByEventId = verdeRequestByEventIdBySender.get(msgSender);
                        if (verdeRequestByEventId != null) {
                            for (VerdePointRequest verdeRequest : verdeRequestByEventId.values()) {
                                dbgAdapter.release(verdeRequest);
                            }

                            verdeRequestByEventIdBySender.remove(msgSender);
                        }

                        if (msgSender == scnSenderId) {
                            System.out.println("The scenario left.");
                            scnSenderId = 0;
                        }

                        knownMonitors.remove(msgSender);
                        break;
                    }

                    case HELLO: {
                        Irv.Hello hello = msg.getHello();
                        if (hello.getServiceName().equals(Conf.SCENARIO_SERVICE_NAME)) {
                            scnSenderId = msg.getSender();

                        } else if (hello.getServiceName().equals(Conf.MONITOR_SERVICE_NAME)) {
                            knownMonitors.add(msgSender);
                        }

                        break;
                    }

                    case EVENTREQUEST: {
                        Irv.EventRequest eventRequest = msg.getEventRequest();
                        long eventId = eventRequest.getRequestId();

                        final Point p;

                        List<String> methodArgs = null;
                        if (eventRequest.getMethodArgs().getSpecified()) {
                            methodArgs = eventRequest.getMethodArgs().getArgList();
                        }

                        switch (eventRequest.getKind()) {
                            case METHOD_ENTRY:
                                p = new MethodBreakpoint(
                                    eventRequest.getClassName(),
                                    eventRequest.getMethodName(),
                                    true,
                                    methodArgs,
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            case BREAKPOINT:
                                p = new LineBreakpoint(
                                    eventRequest.getClassName(),
                                    eventRequest.getLineNumber(),
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            case FIELD_ACCESS:
                                p = new AccessWatchpoint(
                                    eventRequest.getClassName(),
                                    eventRequest.getFieldName(),
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            case EXCEPTION_CATCH:
                                p = new ExceptionCatch(
                                    eventRequest.getClassName(),
                                    eventRequest.getIncludeCaughtExceptions(),
                                    eventRequest.getIncludeUncaughtExceptions(),
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            case FIELD_MODIFICATION:
                                p = new ModificationWatchpoint(
                                    eventRequest.getClassName(),
                                    eventRequest.getFieldName(),
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            case METHOD_EXIT:
                            case METHOD_EXIT_WITH_RETURN_VALUE:
                                p = new MethodBreakpoint(
                                    eventRequest.getClassName(),
                                    eventRequest.getMethodName(),
                                    false,
                                    methodArgs,
                                    paramListFromIrv(eventRequest.getParametersList())
                                );
                                break;

                            default:
                                throw new Exception("Unsupported Event Kind " + eventRequest.getKind());
                        }

                        final boolean isMonitor = knownMonitors.contains(msgSender);

//                         System.out.println("Instrumenting, point=" + p);

                        VerdePointRequest verdeRequest = dbgAdapter.instrument(
                            p,
                            eventData -> {
                                try {
                                    sendMessage(
                                        rootMessage(msgSender)
                                        .setEvent(
                                            setEventParamsIrvValuesFromAdapterParameters(
                                                Irv.Event.newBuilder()
                                                .setRequestId(eventId),
                                                eventData.args
                                            )
                                        ).build()
                                    );

                                    if (isMonitor) {
                                        INSTANCE.nbEvents++;
                                    } else {
                                        INSTANCE.otherEvents++;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                return null;
                            }
                        );

                        Map<Long, VerdePointRequest> verdeRequestByEventId = verdeRequestByEventIdBySender.get(msgSender);
                        if (verdeRequestByEventId == null) {
                            verdeRequestByEventId = new HashMap<>();
                            verdeRequestByEventIdBySender.put(msgSender, verdeRequestByEventId);
                        }

                        verdeRequestByEventId.put(eventId, verdeRequest);

                        sendMessage(
                            rootMessage(msgSender)
                            .setRequestOk(
                                Irv.RequestOk.newBuilder()
                                .setRequestId(eventId)
                            ).build()
                        );

                        break;
                    }

                    case EVENTRELEASE: {
                        Irv.EventRelease eventRelease = msg.getEventRelease();
                        long eventId = eventRelease.getRequestId();

                        Map<Long, VerdePointRequest> verdeRequestByEventId = verdeRequestByEventIdBySender.get(msgSender);

                        if (eventId > 0) {
                            release(verdeRequestByEventId, eventId);
                        } else {
                            for (VerdePointRequest verdeRequest : verdeRequestByEventId.values()) {
                                dbgAdapter.release(verdeRequest);
                            }

                            verdeRequestByEventId.clear();
                        }

                        sendMessage(
                            rootMessage(msgSender)
                            .setRequestOk(
                                Irv.RequestOk.newBuilder()
                                .setRequestId(eventId)
                            ).build()
                        );

                        break;
                    }

                    case GETVALUES: {
                        Irv.GetValues getValues = msg.getGetValues();
                        dbgAdapter.getValues(
                            paramListFromIrv(getValues.getParametersList()),
                            arguments -> {
                                try {
                                    sendMessage(
                                        rootMessage(msgSender)
                                        .setValueList(
                                            setValueListIrvValuesFromAdapterParameters(
                                                Irv.ValueList.newBuilder().setRequestId(getValues.getRequestId()),
                                                arguments
                                            )
                                        ).build()
                                    );
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        );

                        break;
                    }

                    case RESUMEEXEC: {
//                         System.out.println("Resuming, suspend=" + msg.getResumeExec().getSuspend());
                        return msg.getResumeExec().getSuspend();
                    }


                    case DEBUGGERCMD: {
                        Irv.DebuggerCmd cmd = msg.getDebuggerCmd();

                        switch (cmd.getCCase()) {
                            case CMD:
                                dbgAdapter.run(
                                    cmd.getCmd(),
                                    answer -> {
                                        try {
                                            sendMessage(
                                                rootMessage(msg.getSender())
                                                .setDebuggerCmdReply(
                                                    Irv.DebuggerCmdReply.newBuilder()
                                                        .setReply(answer)
                                                        .setRequestId(cmd.getRequestId())
                                                ).build()
                                            );
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                );

                                break;

                            case PRINT:
                                dbgAdapter.print(cmd.getPrint());
                                break;

                            case PROMPT:
                                dbgAdapter.prompt(
                                    cmd.getPrompt(), result -> {
                                        try {
                                            sendMessage(
                                                rootMessage(msg.getSender())
                                                .setDebuggerCmdReply(
                                                    Irv.DebuggerCmdReply.newBuilder()
                                                        .setReply(result)
                                                        .setRequestId(cmd.getRequestId())
                                                ).build()
                                            );
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                );

                                break;

                            default:
                                unexpectedMessage(msg, "a debugger command");
                        }
                        break;
                    }

                    case CHECKPOINT:
                        System.out.println("Received a checkpoint request. This is not supported in verde-jdb directly. Please use verde-checkpointed-jdb.");
                        break;

                    default: unexpectedMessage(msg, "EventRequest or ResumeExec");
                }
            }
        } catch (PointNotFoundException e) {
            System.out.println("Breakpoint not found!. Original exception:");
            e.originalException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
