// This work is licensed under the GNU GPLv2 or later.
// See the COPYING file in the top-level directory.

package verde;
import java.util.List;
import com.sun.jdi.Value;

public class VerdeEventData {
    public final VerdePointRequest request;
    public final List<Value> args;

    VerdeEventData(VerdePointRequest request, List<Value> args) {
        this.request = request;
        this.args = args;
    }
}
