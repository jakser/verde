package verde;

import java.net.Socket;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class VerdeBusClient {
    private int senderId = 0;

    private Socket          busSocket;
    private OutputStream    out;
    private DataInputStream in;
    private final String serviceName;

    VerdeBusClient(String serviceName) {
        this.serviceName = serviceName;
    }

    protected Irv.Root.Builder rootMessage(int recipientId) {
        return Irv.Root.newBuilder()
            .addRecipients(recipientId)
            .setSender(senderId);
    }

    protected void sendMessage(Irv.Root msg) throws IOException {
        byte[] irvMessageByteArray = msg.toByteArray();
        out.write(ByteBuffer.allocate(4).putInt(irvMessageByteArray.length + 4).array());
        out.write(irvMessageByteArray);
    }

    protected Irv.Root getMessage() throws IOException {
        byte[] msgByteArray = new byte[in.readInt() - 4];
        in.readFully(msgByteArray);
        return Irv.Root.parseFrom(msgByteArray);
    }

    void handleHello() throws Exception {
        sendMessage(
            rootMessage(Conf.VERDE_BUS_SENDER_ID)
            .setHello(
                Irv.Hello.newBuilder()
                    .setProtocolVersion(Conf.VERDE_PROTOCOL_VERSION)
                    .setServiceName(serviceName)
                    .setServiceVersion(Conf.VERDE_SERVICE_VERSION)
                    .setAppName(Conf.VERDE_APP_NAME)
                    .setAppVersion(Conf.VERDE_APP_VERSION)
                    .build()
            ).build()
        );

        Irv.Root expectedHello = getMessage();

        if (expectedHello.getContentCase() != Irv.Root.ContentCase.HELLO) {
            throw new Exception("Unexpected packet, expected Hello");
        }

        Irv.Hello helloFromBus = expectedHello.getHello();
        senderId = expectedHello.getRecipients(0);
        System.out.println("Hello from the bus:" + helloFromBus);
    }

    void connectToBus() throws Exception {
        busSocket = new Socket(Conf.VERDE_BUS_HOST, Conf.VERDE_BUS_PORT);

        out = busSocket.getOutputStream();
        in  = new DataInputStream(busSocket.getInputStream());

        handleHello();
    }

    void unexpectedMessage(Irv.Root irvMessage, String expected) {
        System.out.println("Unexpected message, expected " + expected + ". Message: " + irvMessage);
    }
}
