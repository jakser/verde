package verde;

public class Conf {
    public final static String VERDE_BUS_HOST                             = "127.0.0.1";
    public final static int    VERDE_BUS_PORT                             = 13375;
    public final static int    VERDE_BUS_SENDER_ID                        = 0;
    public final static int    VERDE_PROTOCOL_VERSION                     = 20170613;
    public final static int    VERDE_SERVICE_VERSION                      = 20170613;
    public final static String VERDE_APP_NAME                             = "Verde";
    public final static String VERDE_EXECUTION_CONTROLLER_SERVICE_NAME    = "executionController";
    public final static String VERDE_MONITOR_SERVICE_NAME                 = "monitor";
    public final static String VERDE_SCENARIO_SERVICE_NAME                = "scenarioEngine";
    public final static String VERDE_APP_VERSION                          = "2017.06.13";
}
