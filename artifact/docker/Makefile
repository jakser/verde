ROOT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
INAME=dist-verde-artifact
FLAGS=--force-rm=true
DISPLAY_CONTAINER_PORT?=3
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
XDG_RUNTIME_DIR=/tmp/runtime-root

.PHONY: build run save

build: dist-verde.tar.gz
	docker build ${FLAGS} -t "${INAME}" .

dist-verde.tar.gz:
	f="$$(mktemp)"; tar -czf "$$f" ../../; mv "$$f" dist-verde.tar.gz

save: ${INAME}

${INAME}:
	docker image save "${INAME}" > "${INAME}.tar"

run-mac:
	@if [ "$$(which socat)" = "" ]; then echo "*****\nsocat is not installed. You will see errors about command socat no being found. This is harmless. However, you will not be able to see the graphical view of the properties in the experiments. To use graphical features, please install XQuartz and socat.\n*****\n"; fi
	socat TCP-LISTEN:600${DISPLAY_CONTAINER_PORT},reuseaddr,fork UNIX-CLIENT:\"$$DISPLAY\" & \
	SOCAT_PID=$$! ; \
	ADDR="$$MACHINE_IP" ; \
	if [ -z "$$ADDR" ]; then \
		while read line; do \
			if [ -n "$$ADDR" ]; then \
				break ; \
			fi; \
			if [ -n "$$line" ]; then \
				ADDR="$$line"; \
			fi ; \
		done <<< "$$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | awk '{print $$2}')"; \
	fi; \
	echo "IP address of the computer is: '$$ADDR'. To override, set the MACHINE_IP environment variable." ; \
	docker run \
	--volume=${XSOCK}:${XSOCK}:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="VERDE_DISABLE_CRIU_TRACK_MEM=1" \
        --env="XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR}" \
        --env="DISPLAY=$$ADDR:${DISPLAY_CONTAINER_PORT}" \
	-v "${HOME}/.Xauthority:${XAUTH}" \
	--net host --rm -P --privileged -i --tty   \
	-v "${WD}:/root/:rw" -t "${INAME}"         \
	boot ; \
	kill $$SOCAT_PID ; \
	true

run:
	touch ${XAUTH}
	xauth nlist ${DISPLAY} | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -
	docker run \
	--volume=${XSOCK}:${XSOCK}:rw \
        --volume=${XAUTH}:${XAUTH}:rw \
        --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR}" \
        --env="DISPLAY" \
	-v "${HOME}/.Xauthority:/root/.Xauthority" \
	--net=host --rm -P --privileged -ti   \
	-v "${WD}:/root/:rw" -t "${INAME}"         \
	boot

make run-su:
	su -c "cp /home/$$USER/.Xauthority ~;  xhost +local:;make run"

clean:
	rm -rf dist-verde.tar.gz files

#--security-opt seccomp:unconfined --cap-add=SYS_PTRACE --security-opt=apparmor:unconfined --privileged

# "cd /home/user/eval/ && ./exp-apr16.sh && bash"

