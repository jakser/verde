#!/usr/bin/env python3

import os
import sys

try:
    from PyQt5.QtWebKitWidgets import *
    from PyQt5.QtWebKit import *
    from PyQt5.QtWidgets import *
    from PyQt5.QtGui import *
    from PyQt5.QtCore import *
except ImportError:
    try:
        from PyQt4.QtWebKit import *
        from PyQt4.QtGui import *
        from PyQt4.QtCore import *
    except ImportError:
        from PySide.QtWebKit import *
        from PySide.QtGui import *
        from PySide.QtCore import *
        from PySide.QtCore import Signal as pyqtSignal
        #from PySide.QtCore import Slot as pyqtSlot


class QW(QMainWindow):
    def __init__(self, parent=None):
        super(QMainWindow, self).__init__(parent)

    def event(self, ev):
        isHandled = QMainWindow.event(self, ev);
        if isHandled:
            if ev.type() == QEvent.WindowActivate:
                isWindowLoaded = True
                sys.exit()

        return isHandled

app = QApplication(sys.argv)
window = QW()
window.show()
app.exec_()
