#!/usr/bin/env sh

if [ "$1" = "" ]; then
    exec cat <<EOF
Usage: $0 destination

    Prepare an artifact of verde in the destination folder.
    if DISABLE_ANONYMITY is empty, don't anonymize.

EOF
fi

dest="$1/dist-verde"
mkdir -p "$1"

if [ "$DISABLE_ANONYMITY" = "" ]; then
    "$(dirname "$0")/anonymize.sh" "$dest/"
else
    rsync -azp --delete "$(dirname "$0")/../" "$dest/"
fi

cd "$dest"
make clean
