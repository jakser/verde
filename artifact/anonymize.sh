#!/usr/bin/env sh

if [ "$1" = "" ]; then
    exec cat <<EOF
Usage: $0 destination

    Makes an anonymized version of verde in the destination folder.

EOF
fi

startsWith() {
    case "$1" in "$2"*)
        return 0
    esac
    return 1
}

endsWith() {
    case "$1" in *"$2")
        return 0
    esac
    return 1
}

contains() {
    case "$1" in *"$2"*)
        return 0
    esac
    return 1
}

sanitizeFileName() {
    printf "%s" "$1" | sed -e 's/-rigel/-laptop/g' -e 's/-idrouille/-bigmachine/g'
}

cd "$(dirname "$0")/.."

echo "Cleaning target: $1"
rm -i -rf "$1/"

echo "Copying to target, stripping names."
find | while read file; do
    if [ "$file" != "./artifact/anonymize.sh" ] && [ "$file" != "./README.md" ] && ! startsWith "$file" "./experiments/raft/logcabin" && ! startsWith "$file" "./experiments/raft/storage"  && ! startsWith "$file" "./target/" && ! contains "$file" "/.git/" && ! contains "$file" "__pycache__" && ! endsWith "$file" ".class"  && ! startsWith "$file" "./artifact/docker/dist-verde.tar.gz"; then
        sanitized=$(sanitizeFileName $file)
        if [ -d "$file" ]; then
            if ! endsWith "$file" "/.git"; then
                mkdir -p "$1/$sanitized"
            fi
        else
            bbe \
                -e "s|/home/corse/jakse|xxxxxxxxxxxxxxxxx|g" \
                -e "s|/home/raph|xxxxxxxxxx|g" \
                -e "s|raph@rigel|xxxx@host|g" \
                -e "s|Raphaël Jakse, Jean-François Méhaut and Yliès Falcone|XXX|g" \
                -e "s|Raphaël Jakse|XXX|g" \
                -e "s|Université Grenoble Alpes (France)|XXX|g" \
                -e "s|Université Grenoble Alpes|XXX|g" \
                "$file" > "$1/$sanitized"
        fi
    fi
done

cd "$1"

echo "Testing for potential breach of anonymity."
offendingStrings="Grenoble\|Jakse\|Mehaut\|Méhaut\|Yliès\|Ylies\|Falcone\|UGA\|raph\|France\|corse|idrouille"
grep -i "$offendingStrings" . -R | grep -F -v -i graph | grep -F -v 'tr.ADMIN_createUser'
find | grep -i "$offendingStrings" | grep -F -v -i graph
