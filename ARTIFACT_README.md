This package is composed of:

- Simple-Verde: the original implementation of monolithic interactive runtime
  verification, suitable for debugging single C/C++ programs with GDB using one
  property. Simple-Verde is a Python plugin.
- Dist-Verde: a distributed implementation allowing interactive runtime
  verification of multiple process programs; using several properties.
  It also has a more powerful implementation of scenarios.
  Currently, it supports:
   - Java 8 programs through a AspectJ-based extension of JDB
   - C/C++ programs through a Python GDB extension.

For a more torough presentation of Dist-Verde, see [doc/ORGANIZATION.md](doc/ORGANIZATION.md)

# Play with the model or run the experiments

We suggest you to build our Docker image and run it from a GNU/Linux machine.
However, you can also setup and run Dist-Verde on your system. See Install Dist-Verde.

## Docker
WARNING: We could not check whether this docker image correctly runs on macOS or
Windows. We only tested it on GNU/Linux.
However, we do provide instructions for macOS, should you want to try to run
the artifact on macOS anyway.

To use dist-verde in a Docker container, first build the Docker image:

    cd artifact/docker
    sudo make

Then, run it:

1) On GNU/Linux:

    make run-su

    # If it does not work, try:
    sudo make run

2) On macOS:

    make run-mac

When the container is run, you are logged as a user with sudo privileges, so
you can install any extra program you need.

Dist-verde is installed at /home/user/dist-verde and dist-verde commands, located
at /home/user/dist-verde/bin, are in the PATH.

## Play with the Model

WARNING: we advise you to keep a close eye to the memory usage on your system
when running the model, and stop it the memory usage comes close to 100%.

The docker image includes a recent version of Spin.
In the docker container, our results can be reproduced by running:

    cd dist-verde/extra/irv-spin/
    ./run
    ./format-results.sh RESULTS_FOLDER

WARNING: be aware that by default, this requires a powerful machine with 64G of
RAM. Try commenting out these lines in `./run` on a less powerful machine.

    exp 1 2 0 0
    exp 2 1 0 0
    exp 1 2 1 1
    exp 2 1 1 1

Command `exp $NBMONITORS $NBECS $NBMONREQS $NBACTIONS` runs the model checking
with $NBMONITORS monitors doing at most $NBMONREQS requests to the execution
controllers per event, $NBECS execution controllers and a scenario requesting at
most $NBACTIONS to the execution controllers before resuming them.

Results are output to folder `results/`.

To run Spin manually:
The model can be configured by editing file `constants.m4` and compiled with `make`.

To check the model againts the invariants:

    ./pan -m100000 -f -a -b -N prop_invariants

To check the model againts the liveness properties:

    ./pan -m100000 -f -a -b -N prop_all

Meaning of the parameters:

- -m100000: The model checker should stop if an execution with more that 100000 states is found.
- -b: consider it an error if this limit is reached
- -f: enable weak fairness
- -N prop_name: the property to check. See the end of irv.pml.m4 for a list of properties.
# Install Dist-Verde

## Run the Experiments

### Banking Transaction System

To reproduce our results:

    cd dist-verde/experiments/FinancialTransactions
    make
    ./run
    ./format-results.sh OUTPUT_FOLDER_GIVEN_IN_THE_LAST_COMMAND

To play with Dist-Verde on this experiments:

1) Stop on the first detected error. Suggestion: type `where` when this happens.

    ./run try

Please note that once an error is detected, the execution will stop on each
monitored function call, since the system is in a non-accepting state.

2) Detect all the errors and print the corresponding stack traces. It will take
around 30 seconds to 1 min 30 seconds depending on your setup.

    ./run output

Properties are stored in files *.prop in the folder of the experiment. You
can take a look if you are interrested.

### MPI Deadlock

This experiment runs 8 MPI tasks simulating an incorrect solution to the dining
philosophers problem. For this experiment to work, you need to be able to open
X11 windows.
This should work out of the box with from the docker container with the given
instructions. Unfortunately, we could not be sure this will work with every
configuration.

To run the experiment:

    cd dist-verde/experiments/mpi-deadlock
    make
    ./run

WARNING: this will open 8 windows corresponding to the 8 tasks. We may suggest
you to set up your window manager such that these windows are minimized by
default, should you run the experiment multiple times.

There are two possible outcomes:

- there is no deadlock. Then, type CTRL+C and rerun the experiment
- there is a deadlock. You sould notice the monitor transitioning to the state
  deadlock. Nodes are either suspended or deadlocked. You may interrupt their
  execution by typing CTRL+C in the debugger. Regular GDB commands like `where`
  will work.

# LogCabin

To reproduce this experiment with 3 nodes 2 times:

    export LD_LIBRARY_PATH=$HOME/.local/lib
    make
    LOGCABIN_NB_NODES=3 NB_SERIES=2 ./run
    ./format-results.sh OUTPUT_FOLDER

Results are stored in folder `results`.

# Install Dist-Verde on your System

## Notice on Supported Systems

Dist-Verde is only supported on GNU/Linux and tested on the latest and up to date
Ubuntu LTS and Debian testing. As such, it is advised to use a Virtual Machine
or a Debian / Ubuntu chroot on other systems.

That said, few things are specific to a particular system and Verde will
certainly work fine on most GNU/Linux distributions provided the right
dependencies are there, should work on BSD, might work on macOS.
In particular, Dist-Verde is known to run correctly on Fedora and openSUSE.

On Windows, your best bet is probably WSL, though Cygwin might work too.
You might need to disable UNIX sockets in `src/python/irvstuff.py`.
Newer versions of Windows support UNIX sockets but this might not be
available in Windows build of Python 3 just yet.

Checkpointing is based on CRIU and is therefore only available on Linux.

Patches are welcome for unsupported systems.

# Install system dependencies

## Install everything:

This commands install everything needed to run both Simple-Verde and Dist-Verde.
It does not include dependencies for the graph displaying feature of Simple-Verde.
See later to install these dependencies. On Debian and Ubuntu:

    sudo apt update
    sudo apt install --no-install-recommends git make openjdk-8-jdk protobuf-compiler wget python3 gdb libc6-dev bc libbsd-dev python3-protobuf

Ubuntu users: Please note that `openjdk-8-jdk` is in `universe` on Ubuntu 18.04.
Debian users: openjdk-8-jdk disappeared from Debian Buster. You might want to install the one from Debian Stretch.
This channel must be enabled in `/etc/apt/sources.list`.

For convenience (but needs double check), these commands should install what is needed in Fedora:

    dnf install java-1.8.0-openjdk-devel protobuf-devel wget python3 gdb glibc-devel python3-protobuf python3-pexpect
    dnf group install "C Development Tools and Libraries"

Explanations of the dependencies:

- `make`: run make, to prepare, install and run Dist-Verde
- `openjdk-8-jdk`: compile and run the scenario, the jdb client and java 8
   program being interactive runtime verified.
- `protobuf-compiler`: Protocol Buffers are used for communication between the
   different Dist-Verde components
- `wget` is used to download dependencies when installing Dist-Verde.
- `python3` is used to run the bus.
- `gdb` is used to interatively verify C/C++ programs
- `libc6-dbg` is used to debug programs using the libc (C/C++ programs).
- `python3-protobuf` is used in the bus and the gdb Dist-Verde extension for
   communications.
- `python3-pexpect` is used in verde-jdb to start the prorgam execution
- automatically by sending `cont` to the standard input of jdb.

NOTE: if you whish to be able to use features related to checkpointing, please
also install CRIU (Checkpoint / Restore In Userspace).

## To use Dist-Verde with Java Programs Only:

    sudo apt install --no-install-recommends openjdk-8-jdk protobuf-compiler wget python3 python3-protobuf python3-pexpect

## To use Simple-Verde Only:

    sudo apt install gdb libc6-dbg wget

Simple-Verde provides a graphical view of properties.
To be able to show graph of properties in Simple-Verde:

    sudo apt install --no-install-recommends graphviz python3-pip python3-pyqt5 python3-pyqt5.qtwebkit
    sudo pip3 install graphviz

# Prepare Dist-Verde

Please change the current directory to the verde folder and then run make and
read given instructions carefully.
First runs will invite you to set `JAVA_HOME`, install missing dependencies and
source Verde's environment set up script.

    cd verde
    make

WARNING: Verde will download about 54,6 M of dependencies on first make.
If you want to save some megabytes by removing support for Python scripting
in scenarios, execute the following line before running `make`:

    make strip-jython

Jython, which is about 36 M, will not be downloaded.

# Try Dist-Verde

## On a C example:

    cd $VERDE_DIR/experiments/sort
    gcc -c -fopenmp qsort.c -o qsort

    dist-verde --prop $VERDE_DIR/experiments/sort/qsort-c.prop --prgm $VERDE_DIR/experiments/sort/qsort -dbg gdb -arg 100

## On a Java Example:

    source dist-verde/verde.source.sh

    cd $VERDE_DIR/experiments/sort

    javac -g QSort.java

    dist-verde --prop $VERDE_DIR/experiments/sort/qsort-java.prop --prgm QSort -dbg jdb -arg 100

When main[1] is shown, issue `cont`.
The default scenario, `stopexec`, which suspends the execution when a
non-accepting state is reached, is used.
A scenario can be explicitely specified using the `--scn` switch (e.g. `--scn $VERDE_DIR/scenarios/stopexec.js`)

Usage of Dist-Verde is described in [doc/USING_DIST_VERDE.md](doc/USING_DIST_VERDE.md).

# Try Simple-Verde

For Simple-Verde, sourcing `verde.source.sh` is not actually needed, but still
useful to define VERDE_DIR.

    source dist-verde/verde.source.sh

    simple-verde $VERDE_DIR/experiments/sort/qsort-c.prop $VERDE_DIR/experiments/sort/qsort 100

Notice: sourcing `verde.source.sh` requires installing dependencies for Dist-Verde.
If you do not want that, just declare `$VERDE_DIR` like that:

    export VERDE_DIR="$(realpath dist-verde/)"

Usage Simple-Verde is described in [doc/USING_SIMPLE_VERDE.md](doc/USING_SIMPLE_VERDE.md).
